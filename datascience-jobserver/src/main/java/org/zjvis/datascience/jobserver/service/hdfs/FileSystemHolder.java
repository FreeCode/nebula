package org.zjvis.datascience.jobserver.service.hdfs;

import java.io.IOException;
import javax.annotation.PostConstruct;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.HdfsConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @description HDFS文件系统初始化
 * @date 2021-12-28
 */
@Component
public class FileSystemHolder {

    @Value("${yarn.home}")
    private String yarnHome;

    private FileSystem fs = null;

    @PostConstruct
    public void init() throws IOException {
        Configuration conf = new HdfsConfiguration();
        conf.addResource(new Path(yarnHome + "/hdfs-site.xml"));
        conf.addResource(new Path(yarnHome + "/core-site.xml"));
        fs = FileSystem.get(conf);
    }

    public FileSystem getFileSystem() {
        return fs;
    }
}

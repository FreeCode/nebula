package org.zjvis.datascience.jobserver.service.yarn;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @description Spark启动设置类
 * @date 2021-12-28
 */
@Component
@Data
public class SparkLauncherConfig {

    @Value("${spark.submit.sparkHome}")
    private String sparkHome;

    @Value("${spark.submit.master}")
    private String master;

    @Value("${spark.submit.appResource}")
    private String appResource;

    @Value("${spark.submit.mainClass}")
    private String mainClass;

    @Value("${spark.submit.deployMode}")
    private String deployMode;

    @Value("${spark.submit.pyFiles}")
    private String pyFiles;

    @Value("${spark.submit.scriptFolder}")
    private String scriptFolder;

    @Value("${spark.driver.memory}")
    private String driverMem;

    @Value("${spark.executor.memory}")
    private String executorMemory;

    @Value("${spark.executor.cores}")
    private String executorCores;

    @Value("${yarn.home}")
    private String yarnHome;

    @Value("${spark.submit.aiworkspyMain}")
    private String aiworkspyMain;

    @Value("${spark.submit.aiworkspyFiles}")
    private String aiworkspyFiles;
}

package org.zjvis.datascience.jobserver.service.yarn;

import javax.annotation.PostConstruct;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.yarn.client.api.YarnClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @description Yarn集群内服务调度初始化
 * @date 2021-12-28
 */
@Component
public class YarnClientHolder {

    @Value("${yarn.resourcemanager.address}")
    private String yarnWebAddress;

    private YarnClient yarnClient;

    @PostConstruct
    public void init() {
        // 初始化 yarn的配置
        Configuration cf = new Configuration();
        cf.set("yarn.resourcemanager.address", yarnWebAddress);
        // 创建yarn的客户端
        YarnClient yarnClient = YarnClient.createYarnClient();
        // 初始化yarn的客户端
        yarnClient.init(cf);
        // yarn客户端启动
        yarnClient.start();
        this.yarnClient = yarnClient;
    }

    public YarnClient getYarnClient() {
        return yarnClient;
    }
}

#!/bin/bash

APPLICATION_YAML_PATH=${APPLICATION_YAML_PATH:-/app/application.yaml}
if [ -f "$APPLICATION_YAML_PATH" ]; then
    exec java $JAVA_OPS -jar /app/datascience-web.jar --spring.config.location=file:${APPLICATION_YAML_PATH}
else
  echo "file $APPLICATION_YAML_PATH not exists"
  exit -1
fi

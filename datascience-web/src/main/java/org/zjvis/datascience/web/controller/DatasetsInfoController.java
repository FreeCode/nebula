package org.zjvis.datascience.web.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.zjvis.datascience.common.dto.datasetsinfo.DatasetsInfoDTO;
import org.zjvis.datascience.common.dto.user.UserDTO;
import org.zjvis.datascience.common.model.ApiResult;
import org.zjvis.datascience.common.model.ApiResultCode;
import org.zjvis.datascience.common.util.DozerUtil;
import org.zjvis.datascience.common.util.JwtUtil;
import org.zjvis.datascience.common.vo.datasetsinfo.SelectDataInfoVO;
import org.zjvis.datascience.service.DatasetsInfoService;

import javax.validation.Valid;
import java.util.List;

/**
 * @description 元数据信息接口 Controller
 * @date 2021-05-12
 */
@Api(tags = "元数据信息")
@RestController
@RequestMapping("/datasetsinfo")
public class DatasetsInfoController {

    @Autowired
    private DatasetsInfoService datasetsInfoService;


    @PostMapping("/selectBy")
    @ApiOperation(value = "按条件查询当前用户所有的元数据信息")
    // 根据条件筛选，可选择的条件有数据表名、数据类型、存储数据源
    public ApiResult<List<DatasetsInfoDTO>> selectBy(@RequestBody SelectDataInfoVO selectDataInfoVO) {
        UserDTO user = JwtUtil.getCurrentUserDTO();
        DatasetsInfoDTO dataInfo = DozerUtil.mapper(selectDataInfoVO, DatasetsInfoDTO.class);
        List<DatasetsInfoDTO> res = datasetsInfoService.selectBy(user.getId(), dataInfo);
        if (res.size()==0) {
            return ApiResult.valueOf(ApiResultCode.DATASET_INFO_NULL, null, "符合条件的数据集信息不存在");
        }
        return ApiResult.valueOf(ApiResultCode.SUCCESS, DozerUtil.mapper(res, DatasetsInfoDTO.class));
    }

}

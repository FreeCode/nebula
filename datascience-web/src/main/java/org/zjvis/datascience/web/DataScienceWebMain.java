package org.zjvis.datascience.web;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @description 系统入口类
 * @date 2021-11-01
 */
@EnableAsync
@EnableTransactionManagement
@EnableScheduling
@SpringBootApplication
@MapperScan("org.zjvis.datascience.service.mapper")
@ComponentScan("org.zjvis.datascience.*")
public class DataScienceWebMain {

    @Value("${app.port}")
    private int httpPort;

    @Value("${server.port}")
    private int httpsPort;

    public static void main(String[] args) {
        SpringApplication.run(DataScienceWebMain.class, args);
    }
}

package org.zjvis.datascience.web.controller;

import cn.weiguangfu.swagger2.plus.annotation.ApiGroup;
import cn.weiguangfu.swagger2.plus.annotation.ApiPlus;
import cn.weiguangfu.swagger2.plus.enums.ApiExecutionEnum;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.zjvis.datascience.common.annotation.ProjectRoleAuth;
import org.zjvis.datascience.common.dto.RoleDTO;
import org.zjvis.datascience.common.enums.ProjectRoleAuthEnum;
import org.zjvis.datascience.common.model.ApiResult;
import org.zjvis.datascience.common.util.DozerUtil;
import org.zjvis.datascience.common.vo.project.BatchCreateUserProjectVO;
import org.zjvis.datascience.common.vo.project.UserProjectVO;
import org.zjvis.datascience.service.UserProjectService;

/**
 * @description 项目角色管理接口 Controller
 * @date 2021-12-27
 */
@ApiPlus(value = true)
@RestController
@RequestMapping("/projects/")
@Api(tags = "projectRole", description = "项目角色管理接口")
@Validated
public class ProjectRoleController {

    @Autowired
    private UserProjectService userProjectService;

    @PostMapping(value = "/role/list")
    public ApiResult<List<RoleDTO>> listRole() {
        return ApiResult.valueOf(userProjectService.listRole());
    }

    @ApiGroup(groups = UserProjectVO.ProjectId.class, requestExecution = ApiExecutionEnum.INCLUDE)
    @PostMapping(value = "/role/queryByProjectId")
    public ApiResult<List<UserProjectVO>> query(@ProjectRoleAuth(role = ProjectRoleAuthEnum.VISITOR)
    @Validated(value = UserProjectVO.ProjectId.class)
    @RequestBody UserProjectVO vo) {
        return ApiResult
            .valueOf(DozerUtil.mapper(userProjectService.query(vo.getProjectId()), UserProjectVO.class));
    }

    @ApiGroup(groups = UserProjectVO.Delete.class, requestExecution = ApiExecutionEnum.INCLUDE)
    @PostMapping(value = "/role/deleteById")
    public ApiResult<Boolean> deleteById(@Validated(value = UserProjectVO.Delete.class)
    @ProjectRoleAuth(role = ProjectRoleAuthEnum.ADMIN) @RequestBody UserProjectVO vo) {
        return ApiResult.valueOf(userProjectService.delete(vo.getId()) > 0);
    }

    @PostMapping(value = "/role/add")
    @ApiOperation(value = "添加用户角色")
    public ApiResult<Boolean> add(@Valid @ProjectRoleAuth(role = ProjectRoleAuthEnum.ADMIN, checkLock = true)
    @RequestBody BatchCreateUserProjectVO vo) {
        userProjectService.create(vo);
        return ApiResult.valueOf(true);
    }

    @ApiGroup(groups = UserProjectVO.Update.class, requestExecution = ApiExecutionEnum.INCLUDE)
    @ApiOperation(value = "修改用户角色", notes = "只更改权限，用户和项目不允许修改")
    @PostMapping(value = "/role/update")
    public ApiResult<Boolean> update(@Validated(value = UserProjectVO.Update.class)
    @ProjectRoleAuth(role = ProjectRoleAuthEnum.ADMIN, checkLock = true) @RequestBody UserProjectVO vo) {
        userProjectService.update(vo);
        return ApiResult.valueOf(true);
    }

    @ApiGroup(groups = UserProjectVO.ProjectId.class, requestExecution = ApiExecutionEnum.INCLUDE)
    @ApiOperation(value = "获取当前用户在指定项目中的角色信息")
    @PostMapping(value = "/role/getUserRole")
    public ApiResult<Object> getUserRole(@Validated(value = UserProjectVO.ProjectId.class)
    @RequestBody UserProjectVO vo) {
        Object obj = userProjectService.getUserRole(vo.getProjectId());
        return ApiResult.valueOf(obj);
    }

    @ApiGroup(groups = UserProjectVO.Check.class, requestExecution = ApiExecutionEnum.INCLUDE)
    @PostMapping(value = "/role/userDatasetUsedCheck")
    public ApiResult<JSONObject> userDatasetUsedCheck(@Validated(value = UserProjectVO.Check.class)
    @ProjectRoleAuth(role = ProjectRoleAuthEnum.ADMIN) @RequestBody UserProjectVO vo) {
        return ApiResult.valueOf(userProjectService.userDatasetUsedCheck(vo));
    }
}

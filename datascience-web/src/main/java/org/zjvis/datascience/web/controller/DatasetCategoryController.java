package org.zjvis.datascience.web.controller;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.zjvis.datascience.common.annotation.CategoryAuth;
import org.zjvis.datascience.common.annotation.ProjectAuth;
import org.zjvis.datascience.common.dto.DatasetCategoryDTO;
import org.zjvis.datascience.common.dto.user.UserDTO;
import org.zjvis.datascience.common.enums.ProjectAuthEnum;
import org.zjvis.datascience.common.model.ApiResult;
import org.zjvis.datascience.common.model.ApiResultCode;
import org.zjvis.datascience.common.util.DozerUtil;
import org.zjvis.datascience.common.util.JwtUtil;
import org.zjvis.datascience.common.vo.category.CreateDatasetCategoryVO;
import org.zjvis.datascience.common.vo.category.DatasetAndCategoryForProjectVO;
import org.zjvis.datascience.common.vo.category.DatasetAndCategoryVO;
import org.zjvis.datascience.common.vo.category.DeleteDatasetCategoryVO;
import org.zjvis.datascience.common.vo.category.SearchDatasetAndCategoryVO;
import org.zjvis.datascience.common.vo.category.UpdateDatasetCategoryVO;
import org.zjvis.datascience.common.vo.project.ProjectIdVO;
import org.zjvis.datascience.service.DatasetCategoryService;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @description 分类管理接口 Controller
 * @date 2021-12-23
 */
@Api(tags = "分类管理")
@RestController
@RequestMapping("/category")
public class DatasetCategoryController {

  @Autowired
  DatasetCategoryService datasetCategoryService;

  @PostMapping("/queryCurrentUserDatasetCategory")
  @ApiOperation(value = "获取当前用户分类目录及数据集")
  public ApiResult<List<DatasetAndCategoryVO>> queryCurrentUserDatasetCategory() {
    UserDTO user = JwtUtil.getCurrentUserDTO();

    List<DatasetAndCategoryVO> res = datasetCategoryService
        .queryDatasetAndCategoryByUserId(user.getId());
    return ApiResult.valueOf(DozerUtil.mapper(res, DatasetAndCategoryVO.class));
  }

  @PostMapping("/queryCurrentUserCategory")
  @ApiOperation(value = "获取当前用户的分类目录")
  public ApiResult<List<DatasetCategoryDTO>> queryCurrentUserCategory() {
    UserDTO user = JwtUtil.getCurrentUserDTO();

    List<DatasetCategoryDTO> res = datasetCategoryService.queryByUserId(user.getId());
    return ApiResult.valueOf(DozerUtil.mapper(res, DatasetCategoryDTO.class));
  }

  @PostMapping("/queryDatasetCategoryByProjectId")
  @ApiOperation(value = "根据项目id获取当前用户分类目录及数据集及其状态")
  public ApiResult<List<DatasetAndCategoryForProjectVO>> queryDatasetCategoryByProjectId(
      @ProjectAuth(auth = ProjectAuthEnum.READ) @Valid @RequestBody ProjectIdVO pv) {
    List<DatasetAndCategoryForProjectVO> res = datasetCategoryService
        .queryDatasetCategoryByProjectId(pv.getProjectId());
    return ApiResult.valueOf(DozerUtil.mapper(res, DatasetAndCategoryForProjectVO.class));
  }

  @PostMapping("/searchInProject")
  @ApiOperation(value = "项目中检索数据集")
  public ApiResult<List<DatasetAndCategoryForProjectVO>> searchInProject(
      @ProjectAuth(auth = ProjectAuthEnum.READ) @Valid @RequestBody SearchDatasetAndCategoryVO vo) {
    List<DatasetAndCategoryForProjectVO> res = datasetCategoryService
        .searchInProject(vo.getProjectId(), vo.getContent());
    return ApiResult.valueOf(DozerUtil.mapper(res, DatasetAndCategoryForProjectVO.class));
  }

  @PostMapping(value = "/save")
  @ResponseBody
  @ApiOperation(value = "创建数据集分类目录")
  public ApiResult<Long> save(@Valid @RequestBody CreateDatasetCategoryVO dcVO) {
    UserDTO user = JwtUtil.getCurrentUserDTO();

    DatasetCategoryDTO dcDTO = DozerUtil.mapper(dcVO, DatasetCategoryDTO.class);
    dcDTO.setUserId(user.getId());
    dcDTO.setGmtCreator(user.getId());
    dcDTO.setGmtModifier(user.getId());
    return ApiResult.valueOf(datasetCategoryService.save(dcDTO));
  }

  @PostMapping(value = "/update")
  @ResponseBody
  @ApiOperation(value = "修改数据集分类目录")
  public ApiResult<Boolean> update(@CategoryAuth() @RequestBody UpdateDatasetCategoryVO dcVO) {
    UserDTO user = JwtUtil.getCurrentUserDTO();
    DatasetCategoryDTO dcDTO = DozerUtil.mapper(dcVO, DatasetCategoryDTO.class);
    dcDTO.setUserId(user.getId());
    dcDTO.setGmtCreator(user.getId());
    dcDTO.setGmtModifier(user.getId());
    return ApiResult.valueOf(datasetCategoryService.update(dcDTO));
  }

  @PostMapping(value = "/delete")
  @ResponseBody
  @ApiOperation(value = "删除数据集分类目录")
  public ApiResult<Boolean> delete(@CategoryAuth() @RequestBody DeleteDatasetCategoryVO dcVO)
      throws Exception {
    //物理删除，只有当前用户为userId才能删除
    UserDTO user = JwtUtil.getCurrentUserDTO();
    DatasetCategoryDTO dcDTO = DozerUtil.mapper(dcVO, DatasetCategoryDTO.class);
    dcDTO.setUserId(user.getId());
    JSONObject res = datasetCategoryService.delete(dcDTO);
    if(res.getInteger("error")==1){
      return ApiResult.valueOf(ApiResultCode.SYS_ERROR, Boolean.FALSE, "该分类下数据集："+res.getString("datasetNames")+"已被加载到项目中，请先从项目中移出再删除!");
    } else {
      return ApiResult.valueOf(res.getBoolean("result"));
    }
  }
}

package org.zjvis.datascience.web.swagger;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;
import org.zjvis.datascience.common.model.ApiResultCode;
import com.google.common.collect.Lists;
import cn.weiguangfu.swagger2.plus.annotation.EnableSwagger2Plus;
import io.swagger.annotations.Api;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * @description Swagger 设置类
 * @date 2021-11-30
 */
@Configuration
@EnableSwagger2Plus
public class Swagger2Config {

    @Value("${jwt.header}")
    private String tokenHeader;

    @Value("${jwt.token-start-with}")
    private String tokenStartWith;

    List<ResponseMessage> resultSetApi = Lists.newArrayList();

    @PostConstruct
    public void init() {
        for (ApiResultCode apiResultCode : ApiResultCode.values()) {
            resultSetApi.add(new ResponseMessageBuilder().code(apiResultCode.getCode()).message(apiResultCode.getMessage()).build());
        }
    }

    @Bean
    public Docket api() {
        List<Parameter> pars = new ArrayList<>(1);
        pars.add(new ParameterBuilder().name(tokenHeader).description("用户登录接口返回的token")
                .modelRef(new ModelRef("string"))
                .parameterType("header")
                .defaultValue(tokenStartWith + " ")
                .required(true)
                .build());

        return new Docket(DocumentationType.SWAGGER_2)
                //修正Byte转string的Bug
                .directModelSubstitute(Byte.class, Integer.class)
                .select()
//                .apis(RequestHandlerSelectors.any())  //显示所有类
                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))  //只显示添加@Api注解的类
//                .apis(RequestHandlerSelectors.basePackage("org.zjvis.datascience.web.controller")) // 指定包位置
                .build()
                .globalResponseMessage(RequestMethod.GET, resultSetApi)
                .globalResponseMessage(RequestMethod.POST, resultSetApi)
                .globalOperationParameters(pars)
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("开放接口API")
                .description("HTTP对外开放接口")
                .version("1.0.0")
                .termsOfServiceUrl("http://127.0.0.1:8080/swagger-ui.html")
                .license("LICENSE")
                .licenseUrl("http://127.0.0.1:8080/swagger-ui.html")
                .build();
    }


}

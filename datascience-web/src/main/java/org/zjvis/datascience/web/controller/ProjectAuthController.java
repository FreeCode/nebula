package org.zjvis.datascience.web.controller;

import cn.weiguangfu.swagger2.plus.annotation.ApiGroup;
import cn.weiguangfu.swagger2.plus.annotation.ApiPlus;
import cn.weiguangfu.swagger2.plus.enums.ApiExecutionEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.zjvis.datascience.common.annotation.ProjectAuth;
import org.zjvis.datascience.common.enums.ProjectAuthEnum;
import org.zjvis.datascience.common.model.ApiResult;
import org.zjvis.datascience.common.util.DozerUtil;
import org.zjvis.datascience.common.vo.project.BatchCreateUserProjectVO;
import org.zjvis.datascience.common.vo.project.UserProjectVO;
import org.zjvis.datascience.service.UserProjectService;

/**
 * @description 项目权限管理接口 Controller
 * @date 2021-09-07
 */
@ApiPlus(value = true)
@RestController
@RequestMapping("/projects/")
@Api(tags = "projectAuth", description = "项目权限管理接口")
@Validated
public class ProjectAuthController {

    private final static Logger logger = LoggerFactory.getLogger(ProjectAuthController.class);

    @Autowired
    private UserProjectService userProjectService;

    @ApiGroup(groups = UserProjectVO.ProjectId.class, requestExecution = ApiExecutionEnum.INCLUDE)
    @PostMapping(value = "/auth/queryByProjectId")
    public ApiResult<List<UserProjectVO>> query(@ProjectAuth(auth = ProjectAuthEnum.ADMIN)
    @Validated(value = UserProjectVO.ProjectId.class)
    @RequestBody UserProjectVO vo) {
        return ApiResult.valueOf(
            DozerUtil.mapper(userProjectService.query(vo.getProjectId()), UserProjectVO.class));
    }

    @ApiGroup(groups = UserProjectVO.Id.class, requestExecution = ApiExecutionEnum.INCLUDE)
    @PostMapping(value = "/auth/deleteById")
    public ApiResult<Boolean> deleteById(
        @Validated(value = UserProjectVO.Id.class) @RequestBody UserProjectVO vo) {
        return ApiResult.valueOf(userProjectService.delete(vo.getId()) > 0);
    }

    @PostMapping(value = "/auth/create")
    @ApiOperation(value = "添加权限")
    public ApiResult<Boolean> create(@Valid @RequestBody
    @ProjectAuth(auth = ProjectAuthEnum.ADMIN) BatchCreateUserProjectVO vo) {
        userProjectService.create(vo);
        return ApiResult.valueOf(true);
    }

    @ApiGroup(groups = UserProjectVO.Update.class, requestExecution = ApiExecutionEnum.INCLUDE)
    @ApiOperation(value = "修改权限", notes = "只更改权限，用户和项目不允许修改")
    @PostMapping(value = "/auth/update")
    public ApiResult<Boolean> update(
        @Validated(value = UserProjectVO.Update.class) @RequestBody UserProjectVO vo) {
        userProjectService.update(vo);
        return ApiResult.valueOf(true);
    }

}

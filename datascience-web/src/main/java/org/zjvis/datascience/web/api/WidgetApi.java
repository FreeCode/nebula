package org.zjvis.datascience.web.api;

import cn.weiguangfu.swagger2.plus.annotation.ApiPlus;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.zjvis.datascience.common.annotation.ProjectAuth;
import org.zjvis.datascience.common.model.ApiResult;
import org.zjvis.datascience.common.model.ApiResultCode;
import org.zjvis.datascience.common.util.JwtUtil;
import org.zjvis.datascience.common.widget.dto.WidgetDTO;
import org.zjvis.datascience.common.widget.vo.WidgetVO;
import org.zjvis.datascience.service.WidgetFavouriteService;
import org.zjvis.datascience.service.WidgetService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @description 可视化插件接口 Controller
 * @date 2021-09-23
 */
@ApiPlus(value = true)
@RequestMapping("/widgetApi")
@RestController
@Api(tags = "widgetApi", description = "可视化插件接口")
@Validated
public class WidgetApi {

    @Autowired
    private WidgetService widgetService;

    @Autowired
    private WidgetFavouriteService widgetFavouriteService;

    @PostMapping(value = "/save")
    @ResponseBody
    @ApiOperation(value = "新增可视化", notes = "新增可视化")
    public ApiResult<Long> save(HttpServletRequest request, @RequestBody @ProjectAuth WidgetVO vo) {

        Long userId = JwtUtil.getCurrentUserId();
        if (userId == null || vo.getData() == null || vo.getData().isEmpty()) {
            return ApiResult.valueOf(ApiResultCode.PARAM_ERROR);
        }

        if (!"dataset".equals(vo.getType()) && !"task".equals(vo.getType())
                && !"task_copy".equals(vo.getType()) && !"recommend".equals(vo.getType())) {
            return ApiResult.valueOf(ApiResultCode.PARAM_ERROR);
        }

        WidgetDTO widget = vo.toWidget();
        widget.setGmtCreator(userId);
        widget.setGmtModifier(userId);

        Long id = widgetService.save(widget);

        return ApiResult.valueOf(id);
    }

    @PostMapping(value = "/deleteById")
    @Transactional
    @ResponseBody
    @ApiOperation(value = "删除可视化", notes = "根据id删除单个可视化")
    public ApiResult<Void> deleteById(HttpServletRequest request, @RequestBody JSONObject params) {
        Long widgetId = params.getLong("widgetId");
        widgetFavouriteService.deleteWidget(widgetId);
        widgetService.delete(widgetId);
        return ApiResult.valueOf(ApiResultCode.SUCCESS);
    }

    @PostMapping(value = "/queryByTypeAndUser")
    @Transactional
    @ResponseBody
    @ApiOperation(value = "根据类型及用户查询可视化", notes = "根据类型及用户查询可视化")
    public ApiResult<List<WidgetDTO>> queryByTypeAndUser(HttpServletRequest request, @RequestBody JSONObject params) {
        String type = params.getString("type");
        Long userId = params.getLong("userId");
        List<WidgetDTO> widgets = widgetService.queryByTypeAndUser(type, userId);
        return ApiResult.valueOf(widgets);
    }

}

package org.zjvis.datascience.web.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.zjvis.datascience.common.annotation.DatasetAuth;
import org.zjvis.datascience.common.dto.DatasetActionDTO;
import org.zjvis.datascience.common.dto.DatasetActionUnreadDTO;
import org.zjvis.datascience.common.dto.user.UserDTO;
import org.zjvis.datascience.common.model.ApiResult;
import org.zjvis.datascience.common.model.ApiResultCode;
import org.zjvis.datascience.common.util.DozerUtil;
import org.zjvis.datascience.common.util.JwtUtil;
import org.zjvis.datascience.common.vo.dataset.DatasetActionVO;
import org.zjvis.datascience.common.vo.dataset.DatasetIdVO;
import org.zjvis.datascience.service.DatasetActionService;

import javax.validation.Valid;
import java.util.List;

/**
 * @description 数据表操作信息接口 Controller
 * @date 2021-05-12
 */
@Api(tags = "数据表操作信息")
@RestController
@RequestMapping("/datasetaction")
public class DatasetActionController {

    @Autowired
    private DatasetActionService datasetActionService;

    @PostMapping("/selectAllAction")
    @ApiOperation(value = "查询所有数据表的所有动作")
    public ApiResult<List<DatasetActionDTO>> selectAllAction() {
        UserDTO user = JwtUtil.getCurrentUserDTO();
        List<DatasetActionDTO> res = datasetActionService.selectAllAction(user.getId());
        if (res.size()==0) {
            return ApiResult.valueOf(ApiResultCode.DATASET_ACT_NULL, null, "不存在数据增删操作");
        }
        return ApiResult.valueOf(DozerUtil.mapper(res, DatasetActionDTO.class));
    }

    @PostMapping("/selectActionByDtaId")
    @ApiOperation(value = "查询该用户特定数据表的所有动作")
    public ApiResult<List<DatasetActionDTO>> selectActionByDtaId(
            @DatasetAuth()
            @Validated(value = DatasetIdVO.Id.class) @RequestBody DatasetActionVO datasetActionVO) {
        UserDTO user = JwtUtil.getCurrentUserDTO();
        List<DatasetActionDTO> res = datasetActionService.selectActionByDtaId(user.getId(), datasetActionVO.getId(), datasetActionVO.getDaydiff());
        if (res.size()==0) {
            return ApiResult.valueOf(ApiResultCode.DATASET_ACT_NULL, null, "不存在数据增删操作");
        }
        return ApiResult.valueOf(DozerUtil.mapper(res, DatasetActionDTO.class));
    }

    @PostMapping("/selectUnread")
    @ApiOperation(value = "查看该用户的数据表的未读信息")
    public ApiResult<List<DatasetActionUnreadDTO>> selectUnread() {
        UserDTO user = JwtUtil.getCurrentUserDTO();
        List<DatasetActionUnreadDTO> res = datasetActionService.selectUnread(user.getId());
        if (res.size()==0) {
            return ApiResult.valueOf(ApiResultCode.DATASET_ACT_UNREAD_NULL, null, "不存在未读信息");
        }
        return ApiResult.valueOf(DozerUtil.mapper(res, DatasetActionUnreadDTO.class));
    }

}

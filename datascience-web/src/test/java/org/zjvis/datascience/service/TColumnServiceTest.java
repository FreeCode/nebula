package org.zjvis.datascience.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Range;
import java.sql.Types;
import java.util.List;
import java.util.Map;
import org.apache.shiro.util.CollectionUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.zjvis.datascience.common.dto.TaskDTO;
import org.zjvis.datascience.common.model.Table;
import org.zjvis.datascience.common.model.stat.ColumnAction;
import org.zjvis.datascience.common.model.stat.ColumnConstant;
import org.zjvis.datascience.common.model.stat.ColumnStat;
import org.zjvis.datascience.common.sql.DataCleanSqlHelper;
import org.zjvis.datascience.common.util.ToolUtil;
import org.zjvis.datascience.common.vo.TaskInstanceVO;
import org.zjvis.datascience.common.vo.column.ColumnQueryVO;
import org.zjvis.datascience.service.dataprovider.GPDataProvider;
import org.zjvis.datascience.service.mapper.TaskMapper;

public class TColumnServiceTest extends BaseTest {

    @Autowired
    private TColumnService tColumnService;

    @Autowired
    private TaskMapper taskMapper;

    @Autowired
    private GPDataProvider gpDataProvider;

    private static String TABLE = "dataset.sales_fact_sample_flat";

    private static String ORDERS_TABLE = "orders_emea_1603242244347";

    public static void main(String[] args) {
        Integer i = 1;
        System.out.println(i.compareTo(2));
        System.out.println("North Holland".compareTo("N. Rhine Westphalia"));
//    System.out.println(Range.closedOpen("North Holland", "N. Rhine Westphalia"));
    }

//    @Test
//    public void testInitSql() {
//        String sourceTable = "orders_emea_1603242244347";//填入来源表
//        sourceTable = ToolUtil.alignTableName(sourceTable, 0l);
//        Table table = new Table(1L, sourceTable);
//        Map<String, Integer> columnTypes = gpDataProvider.getColumnTypesOriginal(table);
//        //填入清洗配置
//        String data = "{\"action\":\"PROCESS_UPPERCASE\",\"table\":\"orders_emea_1603242244347\",\"col\":\"客户名称\",\"description\":\"所有值已更改为大写\",\"id\":3000}";
//        System.out
//            .println(DataCleanSqlHelper.initSql(columnTypes, JSONObject.parseObject(data), 3750l));
//    }
//
//    @Test
//    public void testAddActionFilter() {
//        TaskInstanceVO vo = new TaskInstanceVO();
//        vo.setTaskId(3750l);
//        vo.setTaskName("筛选器");
//        vo.setUserId(110l);
//        vo.setProjectId(241l);
//        JSONObject data = new JSONObject();
//        data.put("table", ORDERS_TABLE);
//        data.put("action", "FILTER");
//        JSONArray filter = new JSONArray();
//        JSONArray filterChild = new JSONArray();
//        JSONObject columnFilter = new JSONObject();
//        columnFilter.put("col", "行_id");
//        columnFilter.put("filterType", "<=");
//        JSONArray valueArray = new JSONArray();
//        valueArray.add("1000");
//        columnFilter.put("values", valueArray);
//        filterChild.add(columnFilter);
//        filter.add(filterChild);
//        data.put("filter", filter);
//        vo.setData(data);
//        System.out.println(JSONObject.toJSONString(vo));
//        TaskInstanceVO taskInstanceVO = tColumnService.addAction(vo, true);
//        System.out.println(JSONObject.toJSONString(taskInstanceVO));
////    tColumnService.deleteActions(241l, vo.getTaskId(), taskInstanceVO.getId());
//    }
//
//    @Test
//    public void testActionCRUD() {
//        String split = "{\"data\":{\"action\":\"SPLIT\",\"table\":\"orders_emea_1603242244347\",\"col\":\"订单_id\",\"separator\":\"-\",\"splitType\":3,\"splitNum\":2},\"projectId\":241,\"taskId\":3750,\"taskName\":\"清洗\"}";
//        System.out.println("==========字段拆分==========");
//        TaskInstanceVO vo = JSONObject.parseObject(split, TaskInstanceVO.class);
//        vo.setUserId(110l);
//        TaskInstanceVO lastSplitVO = tColumnService.addAction(vo, true);
//        System.out.println(JSONObject.toJSONString(lastSplitVO));
//        System.out.println("==========全量查询历史记录==========");
//        List<ColumnAction> query1 = tColumnService.queryAction(vo.getTaskId());
//        System.out.println(JSONObject.toJSONString(query1));
//        System.out.println("清洗节点表名-----" + getTable(vo.getTaskId()));
//        System.out.println("==============修改============");
//        JSONObject updateData = JSONObject.parseObject(
//            "{\"action\":\"PROCESS_UPPERCASE\",\"table\":\"orders_emea_1603242244347\",\"col\":\"客户名称\",\"description\":\"所有值已更改为大写\"}");
//        updateData.put("id", lastSplitVO.getId());
//        lastSplitVO.setData(updateData);
//        TaskInstanceVO updateVo = tColumnService.updateAction(lastSplitVO);
//        System.out.println(JSONObject.toJSONString(updateVo));
//        System.out.println("清洗节点表名-----" + getTable(vo.getTaskId()));
//        System.out.println("===========全部删除===========");
////    tColumnService.deleteActions(241l, vo.getTaskId(), updateVo.getId());
//        List<ColumnAction> query2 = tColumnService.queryAction(vo.getTaskId());
//        System.out.println(JSONObject.toJSONString(query2));
//        System.out.println("清洗节点表名-----" + getTable(vo.getTaskId()));
//    }
//
//    private String getTable(Long taskId) {
//        TaskDTO taskDTO = taskMapper.queryById(taskId);
//        JSONObject taskDataJson = JSONObject.parseObject(taskDTO.getDataJson());
//        JSONArray output = taskDataJson.getJSONArray("output");
//        JSONObject outputJson = output.getJSONObject(0);
//        return outputJson.getString("tableName");
//    }
//
//    @Test
//    public void testQueryStat_IntegerAndLong_abstract() {
//        ColumnQueryVO vo = new ColumnQueryVO();
//        vo.setTable(TABLE);
//        vo.setName("total_children");
//        vo.setMode(ColumnConstant.MODE_ABSTRACT);
//        vo.setType(Types.INTEGER);
//        ColumnStat stat = tColumnService.queryStat(vo);
//        System.out.println(JSONObject.toJSONString(stat));
//    }
//
//    @Test
//    public void testQueryStat_IntegerAndLong_detail_sort1() {
//        ColumnQueryVO vo = new ColumnQueryVO();
//        vo.setTable(TABLE);
//        vo.setName("total_children");
//        vo.setMode(ColumnConstant.MODE_DETAIL);
//        vo.setType(Types.INTEGER);
//        vo.setSortType(1);
//        vo.setSortVal("ASC");
//        ColumnStat stat = tColumnService.queryStat(vo);
//        System.out.println(JSONObject.toJSONString(stat));
//    }
//
//    @Test
//    public void testQueryStat_IntegerAndLong_detail_sort2() {
//        ColumnQueryVO vo = new ColumnQueryVO();
//        vo.setTable(TABLE);
//        vo.setName("total_children");
//        vo.setMode(ColumnConstant.MODE_DETAIL);
//        vo.setType(Types.INTEGER);
//        vo.setSortType(2);
//        vo.setSortVal("DESC");
//        ColumnStat stat = tColumnService.queryStat(vo);
//        System.out.println(JSONObject.toJSONString(stat));
//    }
//
//    @Test
//    public void testQueryStat_FloatAndDouble_abstract() {
//        ColumnQueryVO vo = new ColumnQueryVO();
//        vo.setTable(TABLE);
//        vo.setName("store_sales");
//        vo.setMode(ColumnConstant.MODE_ABSTRACT);
//        vo.setType(Types.DOUBLE);
//        ColumnStat stat = tColumnService.queryStat(vo);
//        System.out.println(JSONObject.toJSONString(stat));
//    }
//
//    @Test
//    public void testQueryStat_FloatAndDouble_detail_sort_page() {
//        ColumnQueryVO vo = new ColumnQueryVO();
//        vo.setTable(TABLE);
//        vo.setName("store_sales");
//        vo.setMode(ColumnConstant.MODE_DETAIL);
//        vo.setType(Types.DOUBLE);
//        vo.setSortType(2);
//        vo.setSortVal("DESC");
//        vo.setCurPage(5);
//        vo.setPageSize(ColumnConstant.STAT_DETAIL_PAGESIZE);
//        ColumnStat stat = tColumnService.queryStat(vo);
//        System.out.println(JSONObject.toJSONString(stat));
//    }
//
//    @Test
//    public void testQueryStat_Date_abstract() {
//        ColumnQueryVO vo = new ColumnQueryVO();
//        vo.setTable(TABLE);
//        vo.setName("the_date");
//        vo.setMode(ColumnConstant.MODE_ABSTRACT);
//        vo.setType(Types.DATE);
//        ColumnStat stat = tColumnService.queryStat(vo);
//        System.out.println(JSONObject.toJSONString(stat));
//    }
//
//    @Test
//    public void testQueryStat_Date_detail_sort_page() {
//        ColumnQueryVO vo = new ColumnQueryVO();
//        vo.setTable(TABLE);
//        vo.setName("the_date");
//        vo.setMode(ColumnConstant.MODE_DETAIL);
//        vo.setType(Types.DATE);
//        vo.setSortType(2);
//        vo.setSortVal("DESC");
//        vo.setCurPage(1);
//        vo.setPageSize(ColumnConstant.STAT_DETAIL_PAGESIZE);
//        ColumnStat stat = tColumnService.queryStat(vo);
//        System.out.println(JSONObject.toJSONString(stat));
//    }
//
//    @Test
//    public void testQueryStat_String_detail() {
//        ColumnQueryVO vo = new ColumnQueryVO();
//        vo.setTable(TABLE);
//        vo.setName("sales_district");
//        vo.setMode(ColumnConstant.MODE_DETAIL);
//        vo.setType(Types.VARCHAR);
//        vo.setSortType(1);
//        vo.setSortVal("ASC");
//        vo.setCurPage(1);
//        vo.setPageSize(ColumnConstant.STAT_DETAIL_PAGESIZE);
//        ColumnStat stat = tColumnService.queryStat(vo);
//        System.out.println(JSONObject.toJSONString(stat));
//    }
//
//    @Test
//    public void testQueryStat_String_detail_filter() {
//        ColumnQueryVO vo = new ColumnQueryVO();
//        vo.setTable(TABLE);
//        vo.setName("sales_district");
//        vo.setMode(ColumnConstant.MODE_DETAIL);
//        vo.setType(Types.VARCHAR);
//        vo.setSortType(1);
//        vo.setSortVal("ASC");
//        vo.setCurPage(1);
//        vo.setPageSize(ColumnConstant.STAT_DETAIL_PAGESIZE);
//
//        JSONArray filters = new JSONArray();
//        JSONArray subArray = new JSONArray();
//        JSONObject filter = new JSONObject();
//        filter.put("col", "month_of_year");
//        filter.put("filterType", "=");
//        filter.put("values", new JSONArray(CollectionUtils.asList("12")));
//        subArray.add(filter);
//        filters.add(subArray);
//        vo.setFilter(filters);
//        ColumnStat stat = tColumnService.queryStat(vo);
//        System.out.println(JSONObject.toJSONString(stat));
//    }
//
//    @Test
//    public void testQueryDetail() {
//        ColumnQueryVO vo = new ColumnQueryVO();
//        vo.setTable(TABLE);
//        vo.setName("sales_district");
//        vo.setCurPage(1);
//        vo.setPageSize(2);
//        JSONArray filters = new JSONArray();
//        JSONArray subArray = new JSONArray();
//        JSONObject filter = new JSONObject();
//        filter.put("col", "sales_district");
//        filter.put("filterType", "in");
//        filter.put("values",
//            new JSONArray(CollectionUtils.asList("Acapulco", "Bellingham", "Bremerton")));
//        subArray.add(filter);
//        filters.add(subArray);
//        vo.setFilter(filters);
//
//        System.out.println(JSONObject.toJSONString(vo));
//        JSONObject jsonArrayPage = tColumnService.queryDetail(vo);
//        System.out.println(JSONObject.toJSONString(jsonArrayPage));
//    }

}

package org.zjvis.datascience.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.zjvis.datascience.common.dto.TaskDTO;
import org.zjvis.datascience.common.dto.TaskInstanceDTO;

import java.util.List;

public class TaskInstanceServiceTest extends BaseTest {
    @Autowired
    TaskInstanceService taskInstanceService;

    @Test
    public void testCreate() throws IllegalAccessException, ClassNotFoundException, InstantiationException {
        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(54L);
        taskDTO.setProjectId(2L);
        taskDTO.setPipelineId(5l);
        taskDTO.setParentId("53");
        taskDTO.setUserId(2L);
        taskDTO.setType(1);
        // String data = "{\"source_table\":\"source_table\", \"out_table\": \"out_table\", \"feature_cols\":\"a1,a2\", \"min_samples\": 8, \"eps\": 0.5, \"metric\": \"euclidean\", \"id_col\": \"id\", \"algType\": 1}";
        String data = "{\"subTypeName\":\"聚合\",\"id_col\":\"name\", \"min_samples\":3,\"out_table\":\"out_table_test\",\"eps\":20,\"algType\":1,\"metric\":\"euclidean\",\"feature_cols\":[\"a1\", \"a2\",\"a3\"],\"subType\":1,\"source_table\":\"wyz_test\",\"outputCols\":[\"id\",\"label\"],\"setParams\":[{\"default\":0.5,\"name\":\"eps\",\"type\":\"number\",\"value\":0.5,\"english_name\":\"eps\"},{\"default\":5,\"name\":\"min_samples\",\"type\":\"number\",\"value\":5,\"english_name\":\"min_samples\"},{\"default\":\"euclidean\",\"name\":\"metric\",\"type\":\"select\",\"value\":\"euclidean\",\"items\":[\"euclidean\",\"manhattan\",\"chebyshev\"],\"english_name\":\"metric\"},{\"default\":\"\",\"name\":\"source_table\",\"type\":\"string\",\"value\":\"\",\"english_name\":\"source_table\"},{\"default\":\"\",\"name\":\"out_table\",\"type\":\"string\",\"value\":\"\",\"english_name\":\"out_table\"},{\"default\":[],\"name\":\"feature_cols\",\"type\":\"checkbox\",\"value\":[],\"english_name\":\"feature_cols\"}],\"algName\":\"DBSCAN\",\"parentCols\":[]}";
        taskDTO.setDataJson(data);
        Long sessionId = 123456L;
        TaskInstanceDTO taskInstanceDTO = new TaskInstanceDTO(taskDTO, sessionId);
        taskInstanceService.save(taskInstanceDTO);
        System.out.println(taskInstanceDTO.getId());
    }

    @Test
    public void testUpdate() {
        TaskInstanceDTO taskInstanceDTO = new TaskInstanceDTO();
        taskInstanceDTO.setId(5L);
        // taskInstanceDTO.setStatus("failed");
        taskInstanceDTO.setStatus("running");
        Long id = taskInstanceService.update(taskInstanceDTO);
        System.out.println(id);
    }

    @Test
    public void testExecTaskInstance() {
        TaskInstanceDTO taskInstanceDTO = taskInstanceService.queryById(7l);
        // String result = taskInstanceService.execTaskInstance(taskInstanceDTO);
        // System.out.println(result);
    }

    @Test
    public void testQuerySpecTaskInstance() {
        Long taskId = 602L;
        Long sessionId = 64L;
        TaskInstanceDTO taskInstanceDTO = taskInstanceService.queryBySpecTaskInstance(sessionId, taskId);
        System.out.println(taskInstanceDTO.getDataJson());
    }

    @Test
    public void testQueryTableName() {
        List<String> tableNames = taskInstanceService.queryByTableName(783L);
        System.out.println("1");
    }
}

package org.zjvis.datascience.service;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.zjvis.datascience.common.dto.PipelineInstanceDTO;

import java.util.List;

public class PipelineInstanceServiceTest extends BaseTest{
    @Autowired
    PipelineInstanceService pipelineInstanceService;

    @Test
    public void testSave() {
        PipelineInstanceDTO pipelineInstanceDTO = new PipelineInstanceDTO();
        pipelineInstanceDTO.setDuringTime(300L);
        pipelineInstanceDTO.setProjectId(1l);
        pipelineInstanceDTO.setPipelineId(2l);
        pipelineInstanceDTO.setUserId(3l);
        pipelineInstanceService.save(pipelineInstanceDTO);
    }

    @Test
    public void testQueryById() {
        PipelineInstanceDTO pipelineInstanceDTO = pipelineInstanceService.queryById(1L);
        Assert.assertTrue(pipelineInstanceDTO.getUserId().equals(3L));
    }

    @Test
    public void testQueryByPipelineId() {
        List<PipelineInstanceDTO> lists = pipelineInstanceService.queryByPipelineId(2L);
        Assert.assertTrue(lists.size() == 2);
    }

    @Test
    public void testUpdate() {
        PipelineInstanceDTO pipelineInstanceDTO = new PipelineInstanceDTO();
        pipelineInstanceDTO.setUserId(53L);
        pipelineInstanceDTO.setId(12l);
        pipelineInstanceDTO.setStatus("SUCCESS");
        pipelineInstanceDTO.setDuringTime(10000L);
        pipelineInstanceService.update(pipelineInstanceDTO);
    }

    @Test
    public void testGetLatestPipelineInstance() {
        PipelineInstanceDTO dto = pipelineInstanceService.getLatestInstance(103L);
        System.out.println(dto.getId());
    }
}

package org.zjvis.datascience.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.zjvis.datascience.common.dto.user.UserDTO;

public class UserServiceTest extends BaseTest {

    @Autowired
    private UserService userService;

    @Test
    public void test() {
        userService.delete(495L);
    }

    @Test
    public void testUpdateFunction()  {
        UserDTO dto = userService.queryById(322L);
        dto.setName(dto.getName() + "1");
        userService.update(dto);
    }
}

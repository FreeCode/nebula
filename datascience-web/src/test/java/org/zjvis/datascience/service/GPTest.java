package org.zjvis.datascience.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.zjvis.datascience.common.model.Table;
import org.zjvis.datascience.service.dataprovider.GPDataProvider;

public class GPTest extends BaseTest {

  @Autowired
  private GPDataProvider gpDataProvider;

  @Test
  public void testGetColumnType() {
    Table t = new Table(1L, "dataset.sales_fact_sample_flat");
    Map<String, Integer> ret = gpDataProvider.getColumnTypes(t);
    System.out.println(ret);
  }
  
}

package org.zjvis.datascience.service;

import java.util.Date;
import java.util.List;
import org.junit.Test;
import org.zjvis.datascience.common.util.TimeUtil;
import org.zjvis.datascience.common.model.stat.Range;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;

public class TimeUtilTest {
  private static final int SEGMENT_NUM = 14;
  @Test
  public void TestDivisionDate(){
    System.out.println("1分钟间隔==============================");
    Date left = DateUtil.parse("2020-10-22 00:00:00", DatePattern.NORM_DATETIME_PATTERN);
    Date right = DateUtil.parse("2020-10-22 00:14:00", DatePattern.NORM_DATETIME_PATTERN);
    List<Range> dates = TimeUtil.divisionDate(left, right, SEGMENT_NUM);
    System.out.println(dates.size());
    System.out.println(dates);

    System.out.println("5分钟间隔==============================");
    left = DateUtil.parse("2020-10-22 00:00:00", DatePattern.NORM_DATETIME_PATTERN);
    right = DateUtil.parse("2020-10-22 00:55:00", DatePattern.NORM_DATETIME_PATTERN);
    dates = TimeUtil.divisionDate(left, right, SEGMENT_NUM);
    System.out.println(dates.size());
    System.out.println(dates);

    System.out.println("1小时间隔==============================");
    left = DateUtil.parse("2020-10-22 00:00:00", DatePattern.NORM_DATETIME_PATTERN);
    right = DateUtil.parse("2020-10-22 08:47:00", DatePattern.NORM_DATETIME_PATTERN);
    dates = TimeUtil.divisionDate(left, right, SEGMENT_NUM);
    System.out.println(dates.size());
    System.out.println(dates);

    System.out.println("1天间隔==============================");
    left = DateUtil.parse("2020-10-22 00:00:00", DatePattern.NORM_DATETIME_PATTERN);
    right = DateUtil.parse("2020-10-31 08:47:00", DatePattern.NORM_DATETIME_PATTERN);
    dates = TimeUtil.divisionDate(left, right, SEGMENT_NUM);
    System.out.println(dates.size());
    System.out.println(dates);

    System.out.println("1周间隔==============================");
    left = DateUtil.parse("2020-10-22 00:00:00", DatePattern.NORM_DATETIME_PATTERN);
    right = DateUtil.parse("2021-01-31 08:47:00", DatePattern.NORM_DATETIME_PATTERN);
    dates = TimeUtil.divisionDate(left, right, SEGMENT_NUM);
    System.out.println(dates.size());
    System.out.println(dates);

    System.out.println("1月间隔==============================");
    left = DateUtil.parse("2020-10-22 00:00:00", DatePattern.NORM_DATETIME_PATTERN);
    right = DateUtil.parse("2022-01-31 08:47:00", DatePattern.NORM_DATETIME_PATTERN);
    dates = TimeUtil.divisionDate(left, right, SEGMENT_NUM);
    System.out.println(dates.size());
    System.out.println(dates);

    System.out.println("1年间隔==============================");
    left = DateUtil.parse("2020-10-22 00:00:00", DatePattern.NORM_DATETIME_PATTERN);
    right = DateUtil.parse("2040-10-01 08:47:00", DatePattern.NORM_DATETIME_PATTERN);
    dates = TimeUtil.divisionDate(left, right, SEGMENT_NUM);
    System.out.println(dates.size());
    System.out.println(dates);

    System.out.println("10年间隔==============================");
    left = DateUtil.parse("2020-10-22 00:00:00", DatePattern.NORM_DATETIME_PATTERN);
    right = DateUtil.parse("2050-010-31 08:47:00", DatePattern.NORM_DATETIME_PATTERN);
    dates = TimeUtil.divisionDate(left, right, SEGMENT_NUM);
    System.out.println(dates.size());
    System.out.println(dates);

    System.out.println("时间跨度几千年==============================");
    left = DateUtil.parse("1703-10-22 00:00:00", DatePattern.NORM_DATETIME_PATTERN);
    right = DateUtil.parse("4999-010-31 08:47:00", DatePattern.NORM_DATETIME_PATTERN);
    dates = TimeUtil.divisionDate(left, right, SEGMENT_NUM);
    System.out.println(dates.size());
    System.out.println(dates);
  }
}

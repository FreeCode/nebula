package org.zjvis.datascience.service;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.zjvis.datascience.common.dto.graph.GraphFilterDTO;
import org.zjvis.datascience.common.dto.graph.GraphFilterPipelineDTO;
import org.zjvis.datascience.common.model.ApiResult;
import org.zjvis.datascience.common.util.JwtUtil;
import org.zjvis.datascience.common.vo.graph.GraphFilterVO;
import org.zjvis.datascience.service.graph.GraphActionService;
import org.zjvis.datascience.service.graph.GraphAnalysisService;
import org.zjvis.datascience.service.graph.GraphFilterPipelineService;
import org.zjvis.datascience.service.graph.GraphFilterService;
import org.zjvis.datascience.web.controller.GraphAnalysisController;

import java.io.*;
import java.util.List;


public class GraphServiceTest extends BaseTest {
//    @Autowired
//    private GraphService graphService;
//
//    @Autowired
//    private GPDataProvider gpDataProvider;
//
//    @Autowired
//    private TaskInstanceService taskInstanceService;
//
//    @Autowired
//    private TaskService taskService;
    @Autowired
    private GraphAnalysisController graphAnalysisController;

    @Autowired
    private GraphAnalysisService graphAnalysisService;

    @Autowired
    private GraphFilterService graphFilterService;

    @Autowired
    private GraphFilterPipelineService graphFilterPipelineService;

    @Test
    public void test1() {
//        graphActionService.undo(447L);
        GraphFilterDTO dto = new GraphFilterDTO();
        dto.setName("filter");
        dto.setProjectId(446L);
        dto.setGraphId(474L);
        dto.setGraphFilterPipelineId(1L);
        dto.setType(1);
        dto.setSubType(1);
        dto.setParentId(1L);
        dto.setUserId(JwtUtil.getCurrentUserId());
        dto.setDataJson("{}");
        graphFilterService.save(dto);
    }


    @Test
    public void test2() {
        GraphFilterDTO dto = graphFilterService.queryById(1L);
        dto.setName("update");
        graphFilterService.update(dto);
    }

    @Test
    public void test3() {
        List<GraphFilterDTO> dtos = graphFilterService.queryByParentId(2L);
        List<GraphFilterDTO> dtos2 = graphFilterService.queryByGraphFilterPipelineId(1L);
        graphFilterService.deleteById(dtos.get(0).getId());
    }

    @Test
    public void test4() {
        GraphFilterPipelineDTO dto = new GraphFilterPipelineDTO();
        dto.setName("filterpipeline");
        dto.setProjectId(446L);
        dto.setGraphId(474L);
        dto.setUserId(JwtUtil.getCurrentUserId());
        dto.setDataJson("{}");
        Long id = graphFilterPipelineService.save(dto);
    }

    @Test
    public void test5() {
        GraphFilterPipelineDTO dto = graphFilterPipelineService.queryById(1L);
        dto.setName("update");
        graphFilterPipelineService.update(dto);
    }

    @Test
    public void test6() {
        List<GraphFilterPipelineDTO> dtos = graphFilterPipelineService.queryByGraphId(474L);
        graphFilterPipelineService.deleteById(dtos.get(0).getId());
    }

    @Test
    public void test7() {
        GraphFilterVO vo = new GraphFilterVO();
        vo.setProjectId(446L);
        vo.setGraphId(474L);
        vo.setType(0);
        vo.setSubType(0);
        vo.setGraphFilterPipelineId(1L);
//        JSONObject obj = new JSONObject();
//        vo.setData(obj);

    }

    public JSONObject read(String path) throws IOException {
        BufferedReader reader =  new BufferedReader(new InputStreamReader(new FileInputStream("data/n67e184.json")));
        StringBuilder sb = new StringBuilder();
        while (reader.ready()) {
            sb.append(reader.readLine().trim());
        }
        JSONObject obj = JSONArray.parseObject(sb.toString());
        return obj;
    }

}

package org.zjvis.datascience.service;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.SerializationUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.zjvis.datascience.common.dto.ActionDTO;
import org.zjvis.datascience.common.dto.TaskDTO;
import org.zjvis.datascience.common.enums.ActionEnum;

import java.util.ArrayList;
import java.util.List;

public class ActionServiceTest extends BaseTest {

    @Autowired
    ActionService actionService;

    @Autowired
    TaskService taskService;

    @Test
    public void testAddActionForTask() {
        TaskDTO dto = taskService.queryById(7667L);
        List<TaskDTO> array = new ArrayList<>();
        array.add(dto);
        actionService.addActionForTask(ActionEnum.ADD, array, null);
    }

    @Test
    public void testInitStackForProjectId() {
        actionService.initStackForProjectId(267L);
    }

    @Test
    public void testUndo() {
        actionService.undo(267L);
    }

    @Test
    public void testUndoAndRedoForUpdate() {
        TaskDTO task =  taskService.queryById(7824L);

        TaskDTO task2 = (TaskDTO) SerializationUtils.clone(task);
        task2.setName("testMy2");
        List<TaskDTO> ctx1 = new ArrayList<>();
        ctx1.add(task);
        List<TaskDTO> ctx2 = new ArrayList<>();
        ctx2.add(task2);
        taskService.update(task2);
        actionService.addActionForTask(ActionEnum.UPDATE, ctx1, ctx2);
        actionService.undo(267L);
        actionService.redo(267L);

    }

    @Test
    public void testUndoAndRedoForDelete() {
        TaskDTO task1 = taskService.queryById(7899L);
        TaskDTO task2 = taskService.queryById(7900L);
        List<TaskDTO> array = new ArrayList<>();
        array.add(task1);
        array.add(task2);
        taskService.delete(7899L);
        taskService.delete(7900L);
        //删除
        actionService.addActionForTask(ActionEnum.DELETE, array, null);

        actionService.undo(267L);

        actionService.redo(267L);

    }

}

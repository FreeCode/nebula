package org.zjvis.datascience.service;

import com.alibaba.fastjson.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.stylesheets.LinkStyle;
import org.zjvis.datascience.common.dto.RecommendDTO;
import org.zjvis.datascience.common.vo.DataPreviewVO;
import org.zjvis.datascience.common.vo.RecommendVO;

import java.util.List;

public class RecommendServiceTest extends BaseTest {

    @Autowired
    RecommendService recommendService;

    @Test
    public void testViewData() {
        RecommendVO vo = new RecommendVO();
        vo.setTableNames("pipeline.solid_rec_1599727594");
        List<DataPreviewVO> dataPreviewVOS = recommendService.viewData(vo);
        System.out.println(1);
    }

    @Test
    public void testQueryTableNameById() {
        List<String> tables = recommendService.queryTableNameById(1L);
        Assert.assertTrue(tables.size() == 1);
    }
}

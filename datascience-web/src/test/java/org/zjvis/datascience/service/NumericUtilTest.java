package org.zjvis.datascience.service;

import org.zjvis.datascience.common.model.stat.Range;
import java.math.BigDecimal;
import java.util.List;
import org.junit.Test;
import org.zjvis.datascience.common.util.NumericUtil;

public class NumericUtilTest {
  private static final int SEGMENT_NUM = 14;
  @Test
  public void testDivisionInteger() {
    List<Range> ranges = NumericUtil.divisionInteger(0, 5, SEGMENT_NUM);
    System.out.println(ranges.size());
    System.out.println(ranges);

    ranges = NumericUtil.divisionInteger(-501, 500, SEGMENT_NUM);
    System.out.println(ranges.size());
    System.out.println(ranges);

    ranges = NumericUtil.divisionInteger(-12345678, 87654321, SEGMENT_NUM);
    System.out.println(ranges.size());
    System.out.println(ranges);
  }

  @Test
  public void testDivisionLong() {
    List<Range> ranges = NumericUtil.divisionLong(-5l, 5l, SEGMENT_NUM);
    System.out.println(ranges.size());
    System.out.println(ranges);

    ranges = NumericUtil.divisionLong(-501l, 500l, SEGMENT_NUM);
    System.out.println(ranges.size());
    System.out.println(ranges);

    ranges = NumericUtil.divisionLong(-12345678l, 87654321l, SEGMENT_NUM);
    System.out.println(ranges.size());
    System.out.println(ranges);

    ranges = NumericUtil.divisionLong(-12345678l, 987654321987654321l, SEGMENT_NUM);
    System.out.println(ranges.size());
    System.out.println(ranges);
  }

  @Test
  public void testDivisionDouble() {
    List<Range> ranges = NumericUtil.divisionDouble(0.0, 1.0, SEGMENT_NUM);
    System.out.println(ranges.size());
    System.out.println(ranges);

    ranges = NumericUtil.divisionDouble(-3595.45, 4567.89, SEGMENT_NUM);
    System.out.println(ranges.size());
    System.out.println(ranges);

    ranges = NumericUtil.divisionDouble(0.0000, 0.10001, SEGMENT_NUM);
    System.out.println(ranges.size());
    System.out.println(ranges);
  }

  @Test
  public void testDivisionBigDecimal() {
    List<Range> ranges = NumericUtil
        .divisionBigDecimal(new BigDecimal("-10000.67"), new BigDecimal("1500.67"), SEGMENT_NUM);
    System.out.println(ranges.size());
    System.out.println(ranges);

    ranges = NumericUtil
        .divisionBigDecimal(new BigDecimal("0.7"), new BigDecimal("2.22"), SEGMENT_NUM);
    System.out.println(ranges.size());
    System.out.println(ranges);

    ranges = NumericUtil
        .divisionBigDecimal(new BigDecimal("1800.0001"), new BigDecimal("1800.0010"), SEGMENT_NUM);
    System.out.println(ranges.size());
    System.out.println(ranges);

    ranges = NumericUtil.divisionBigDecimal(new BigDecimal("12.21"), new BigDecimal("1912.5"), SEGMENT_NUM);
    System.out.println(ranges.size());
    System.out.println(ranges);
  }
}

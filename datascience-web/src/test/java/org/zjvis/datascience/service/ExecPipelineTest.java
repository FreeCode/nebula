package org.zjvis.datascience.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.zjvis.datascience.common.dto.TaskDTO;
import org.zjvis.datascience.common.dto.TaskInstanceDTO;

public class ExecPipelineTest extends BaseTest {
    @Autowired
    private TaskInstanceService taskInstanceService;

    @Autowired
    private TaskService taskService;

    private void addDataNode() {
        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setUserId(53L);
        taskDTO.setType(3);
    }

    @Test
    public void testEtlFilterSql() {
       TaskDTO taskDTO =  taskService.queryById(257L);
       // TaskInstanceDTO taskInstanceDTO = new TaskInstanceDTO(taskDTO, "12345");
        TaskInstanceDTO taskInstanceDTO = new TaskInstanceDTO();
        String sql = taskService.initSql(taskDTO, 1L);
        System.out.println(sql);
    }

    @Test
    public void testDBscan() {
        TaskDTO taskDTO =  taskService.queryById(258L);
        String sql = taskService.initSql(taskDTO, 1L);
        System.out.println(sql);
    }

    public void testPipelineBuilder() {

    }

    @Test
    public void testEtlJoin() {
        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setType(2);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("algType", 2);
        jsonObject.put("mod", 1);
        jsonObject.put("left", "name");
        jsonObject.put("right", "name");
        jsonObject.put("operator", "=");
        JSONArray input = new JSONArray();
        JSONObject item1 = new JSONObject();
        JSONObject item2 = new JSONObject();
        item1.put("tableName", "table1");
        item1.put("tableCols", new JSONArray());
        item2.put("tableName", "table2");
        item2.put("tableCols", new JSONArray());
        input.add(item1);
        input.add(item2);
        jsonObject.put("input", input);
        JSONArray output = new JSONArray();
        JSONObject item3 = new JSONObject();
        item3.put("tableName", "output");
        item3.put("tableCols", new JSONArray());
        output.add(item3);
        jsonObject.put("output", output);
        taskDTO.setDataJson(jsonObject.toJSONString());

        String sql = taskService.initSql(taskDTO, 2L);
        System.out.println(sql);
    }

    @Test
    public void testSql() {
        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setType(2);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("algType", 6);
        jsonObject.put("sql", "select * from abc where id = 3");
        JSONArray input = new JSONArray();
        JSONObject item1 = new JSONObject();
        JSONObject item2 = new JSONObject();
        item1.put("tableName", "table1");
        item1.put("tableCols", new JSONArray());
        item2.put("tableName", "table2");
        item2.put("tableCols", new JSONArray());
        input.add(item1);
        input.add(item2);
        jsonObject.put("input", input);
        JSONArray output = new JSONArray();
        JSONObject item3 = new JSONObject();
        item3.put("tableName", "output");
        item3.put("tableCols", new JSONArray());
        output.add(item3);
        jsonObject.put("output", output);
        taskDTO.setDataJson(jsonObject.toJSONString());

        String sql = taskService.initSql(taskDTO, 2L);
        System.out.println(sql);
    }

    @Test
    public void testUnion() {
        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setType(2);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("algType", 4);
        jsonObject.put("mod", 1);
        JSONArray left = new JSONArray();
        left.add("a1");
        left.add("a2");
        JSONArray right = new JSONArray();
        right.add("a1");
        right.add("a2");
        jsonObject.put("left", left);
        jsonObject.put("right", right);
        jsonObject.put("operator", "=");
        JSONArray input = new JSONArray();
        JSONObject item1 = new JSONObject();
        JSONObject item2 = new JSONObject();
        item1.put("tableName", "table1");
        item1.put("tableCols", new JSONArray());
        item2.put("tableName", "table2");
        item2.put("tableCols", new JSONArray());
        input.add(item1);
        input.add(item2);
        jsonObject.put("input", input);
        JSONArray output = new JSONArray();
        JSONObject item3 = new JSONObject();
        item3.put("tableName", "output");
        item3.put("tableCols", new JSONArray());
        output.add(item3);
        jsonObject.put("output", output);
        taskDTO.setDataJson(jsonObject.toJSONString());

        String sql = taskService.initSql(taskDTO, 2L);
        System.out.println(sql);
    }

    @Test
    public void testSample() {
        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setType(2);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("algType", 3);
//        jsonObject.put("mod", 1);
        jsonObject.put("mod", 2);
        jsonObject.put("ratio", 40);
        jsonObject.put("count", 30);
        JSONArray input = new JSONArray();
        JSONObject item1 = new JSONObject();
        JSONObject item2 = new JSONObject();
        item1.put("tableName", "table1");
        item1.put("tableCols", new JSONArray());
        item2.put("tableName", "table2");
        item2.put("tableCols", new JSONArray());
        input.add(item1);
        input.add(item2);
        jsonObject.put("input", input);
        JSONArray output = new JSONArray();
        JSONObject item3 = new JSONObject();
        item3.put("tableName", "output");
        item3.put("tableCols", new JSONArray());
        output.add(item3);
        jsonObject.put("output", output);
        taskDTO.setDataJson(jsonObject.toJSONString());

        String sql = taskService.initSql(taskDTO, 2L);
        System.out.println(sql);
    }
}

import { KeyValueStore } from '@/components/vueGridLayout/interfate/grid-Item-data-inteface'
import { ChartDetailsEnum } from '@/config/contant'

interface DataField {
  id: string
  type: 'quantitative' | 'categorical' // TODO: ordinal, datetime
}

interface RecommendParameters {
  transformSetting?: {
    transformMethod: string
    transformConfig: {
      [any: string]: any
    }
  }
  chartSetting: {
    chartType: ChartDetailsEnum
    chartConfig: {
      groups: DataField[]
      keys: DataField[]
      values: DataField[],
      [key: string]: any
    }
  }
}

/**
 * 获取kmeans推荐的参数
 * @param fields 用于可视化推荐的字段
 */
function KMEANSRecommendParameters(allFields: DataField[]): RecommendParameters[] {
  const labelFieldId = 'cluster_id'
  const quantitativeFields = allFields.filter(field => field.id !== labelFieldId && field.type === 'quantitative')
  if (quantitativeFields.length === 2) {  // 二维数据 + cluster_id 画散点图
    return [{
      chartSetting: {
        chartType: ChartDetailsEnum.scatterplot,
        chartConfig: {
          groups: [],
          keys: [quantitativeFields[0]],
          values: [quantitativeFields[1]],
          colorField: labelFieldId, // 用cluster_id区分着色
        },
      }
    }]
  }
  return []
  // 否则画平行坐标，平行坐标先禁用掉
  // return [{
  //   chartSetting: {
  //     chartType: ChartDetailsEnum.parallelCoordinates,
  //     chartConfig: {
  //       groups: [],
  //       keys: allFields.filter(f => f.type === 'categorical').slice(0, 1),
  //       values: allFields,
  //       colorField: labelFieldId
  //     },
  //   },
  // }]
}

/**
 * pca降维可视化推荐的参数
 * @param allFields 用于可视化推荐的字段
 */
function PCADENSERecommendParameters(allFields: DataField[]): RecommendParameters[] {
  if (allFields.length === 2) { // 二维画散点图
    return [{
      chartSetting: {
        chartType: ChartDetailsEnum.scatterplot,
        chartConfig: {
          groups: [],
          keys: [allFields[0]],
          values: [allFields[1]]
        },
      }
    }]
  }
  return []
  // 否则画平行坐标，平行坐标先禁用掉
  // return [{
  //   chartSetting: {
  //     chartType: ChartDetailsEnum.parallelCoordinates,
  //     chartConfig: {
  //       groups: [],
  //       keys: allFields.filter(f => f.type === 'categorical').slice(0, 1),
  //       values: allFields,
  //       colorCount: 1
  //     },
  //   },
  // }]
}


/**
 * fp-growth 频繁模式挖掘
 * @param allFields 用于可视化推荐的字段
 */
function FPGROWTHRecommendParameters(allFields: DataField[]): RecommendParameters[] {
  // 柱状图表示每个频繁项的频数大小（或支持度大小）,频繁项:pattern,  支持度: support; 频次: frequency
  return [{
    chartSetting: {
      chartType: ChartDetailsEnum.barChart,
      chartConfig: {
        groups: [],
        keys: allFields.filter(item => item.id === 'pattern'), // 频繁项
        values: allFields.filter(item => item.id === 'support') // 支持度
      },
    }
  }, {
    chartSetting: {
      chartType: ChartDetailsEnum.barChart,
      chartConfig: {
        groups: [],
        keys: allFields.filter(item => item.id === 'pattern'), // 频繁项
        values: allFields.filter(item => item.id === 'frequency'), // 频次
      },
    }

  }]
}

function PREFIXSPANRecommendParameters(allFields: DataField[]): RecommendParameters[] {
  return FPGROWTHRecommendParameters(allFields)
}

/**
 * TSNE 降维，目前处理方式同PCA
 * @param allFields 用于可视化推荐的字段
 */
function TSNERecommendParameters(allFields: DataField[]): RecommendParameters[] {
  return PCADENSERecommendParameters(allFields)
}

/**
 * LLE 降维，目前处理方式同PCA
 * @param allFields 用于可视化推荐的字段
 */
function LLERecommendParameters(allFields: DataField[]): RecommendParameters[] {
  return PCADENSERecommendParameters(allFields)
}

/**
 * DBSCAN，目前处理方式同KMEANS
 * @param allFields 用于可视化推荐的字段
 */
function DBSCANRecommendParameters(allFields: DataField[]): RecommendParameters[] {
  return KMEANSRecommendParameters(allFields)
}


/**
 * ISO_FOREST
 * @param allFields 用于可视化推荐的字段
 */
function ISOFORESTRecommendParameters(allFields: DataField[]): RecommendParameters[] {
  const labelFieldId = 'label'
  const quantitativeFields = allFields.filter(field => field.id !== labelFieldId && field.type === 'quantitative')
  if (quantitativeFields.length === 2) {  // 二维数据 + cluster_id 画散点图
    return [{
      chartSetting: {
        chartType: ChartDetailsEnum.scatterplot,
        chartConfig: {
          groups: [],
          keys: [quantitativeFields[0]],
          values: [quantitativeFields[1]],
          colorField: labelFieldId, // 用label区分着色
        },
      }
    }]
  }
  return []
  // 否则画平行坐标，平行坐标先禁用掉
  // return [{
  //   chartSetting: {
  //     chartType: ChartDetailsEnum.parallelCoordinates,
  //     chartConfig: {
  //       groups: [],
  //       keys: allFields.filter(f => f.type === 'categorical').slice(0, 1),
  //       values: allFields,
  //       colorField: labelFieldId
  //     },
  //   },
  // }]
}

function STATANOMALYRecommendParameters(allFields: DataField[], algData: KeyValueStore): RecommendParameters[] {
  const {feature_col: featureCol} = algData
  if (!featureCol) {
    return []
  }
  return [{
    chartSetting: {
      chartType: ChartDetailsEnum.densityDataAreaChart,
      chartConfig: {
        groups: [],
        keys: [],
        values: allFields.filter(f => f.id === featureCol),
      },
    },
  }]

}


export default {
  DBSCANRecommendParameters,
  KMEANSRecommendParameters,
  PCADENSERecommendParameters,
  TSNERecommendParameters,
  LLERecommendParameters,
  FPGROWTHRecommendParameters,
  PREFIXSPANRecommendParameters,
  ISOFORESTRecommendParameters,
  STATANOMALYRecommendParameters,
}
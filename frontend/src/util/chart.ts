interface KV {
  [key: string]: any
}

// interface ChartsInterface {
//   PieChart:any,
//   BarChart:any,
//   GroupBarChart:any,
//   LineChart:any,
//   Scatterplot:any,
//   AreaChart:any,
//   ParallelCoordinates:any,
//   HeatmapMatrix:any,
//   ChoroplethMap:any,
//   ScalableBarChart: any,
//   Watcher:any
// }
interface ChartsInterface {
  PieChart: any
  DountChart: any
  RoseChart: any

  BarChart: any
  GroupBarChart: any
  StackBarChart: any
  PercentageStackBarChart: any

  LineChart: any
  StepLineChart: any
  PathChart: any

  ScatterChart: any
  MatrixChart: any

  AreaChart: any
  StackedAreaChart: any
  PercentStackedAreaChart: any

  ParallelCoordinates: any

  ChoroplethMap: any
  ScalableBarChart: any
  Watcher: any

  HeatmapMatrixChart: any
  DensityHeatmapChart: any

  DualAxesChart: any

  SmartChart: any
}

interface VisComponentsInterface {
  default: ChartsInterface
}

export default interface SelectionInfo {
  // 起始索引
  startIndex: number
  // 结束索引
  endIndex: number
  // 是否符合以XXX开头的条件
  isStartWith: boolean
  // 是否符合以XXX结尾的条件
  isEndWidth: boolean
  // 是否符合单词位置推荐的条件（前后有空格），这里位置指的是单词位置
  isWordPos: boolean
  // 单词位置
  wordPos: number
  // 是否符合选中位置一样的规则,这里位置指的是索引位置
  isSelectionPos: boolean
  // 是否符合以xx单词开头的规则，刷选内容在最前面且后面有空格
  isStartWithWord: boolean
  // 前置单词
  startWithWord: string
  // 是否符合以xx单词结尾的规则，刷选内容在最后面且前面有空格
  isEndWithWord: boolean
  // 后置单词
  endWithword: string
  // 真实行号
  _record_id_: string
  // 显示行号
  rowIndex: number
  // 列名
  columnName: string
  // 选中的字符
  selection: string
  // 是否为刷选多个的情况
  isMutiple?: boolean
  // 多选模式下 刷选内容是否都一样
  isSameSelection?: boolean
  // 多选模式下 刷选内容位置是否都一样
  isSamePos?: boolean
  // 所选模式下所有选中信息
  mutipleSelectionInfo?: { [key: string]: SelectionInfo }
  // 多选模式下行索引集合
  rowIndexArray?: Array<number>
  // 多选模式下行号集合
  _record_id_Array?: Array<string>
  // 当前列类型
  columnDesc?: string
  // 当前完整值
  fullValue?: any
}

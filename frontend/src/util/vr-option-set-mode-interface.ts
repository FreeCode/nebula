import OptionPreViewAction from './data-selection-actions'
import SelectionType from './selection-type'

/**
 * transform推荐点击确定后的操作
 */
export default interface VROptionSetModeInteface {
  /**
   * 点击确定时的模式
   */
  sureMode: OptionPreViewAction,
  /**
   * dataTable 回填选中模式
   */
  selectionType: SelectionType,
  
  /**
   * 需要回写的列
   */
  column?:string,

  /**
   * condition 单元格回填触发条件
   */
  conditionTrigger( scope: any, dataStore: any, ...subArgs:Array<any> ): any
}
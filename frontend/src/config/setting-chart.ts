/* eslint-disable */
import { KeyValueStore } from '@/components/vueGridLayout/interfate/grid-Item-data-inteface'
import { isArray, cloneDeep } from 'lodash'
import {
  ChartDetailsEnum,
  chartTypes,
  filterFormTypeName,
  filterTypeName,
} from './contant'
import getDataBindSettings from './setting-data-bind'

export const commonConfig = {
  title: [
    {
      type: 'text',
      label: '标题内容',
      value: '',
      name: 'title',
      props: {},
    },
    // {
    //   type: 'switch',
    //   label: '显示',
    //   value: true,
    //   name: 'titleIsShow',
    //   props: {}
    // },
    // {
    //   type: 'colorPicker',
    //   label: '背景色',
    //   value: '#FFF',
    //   name: 'titleBackground',
    // },
    // {
    //   type: 'colorPicker',
    //   label: '文字颜色',
    //   value: '#373B52',
    //   name: 'titleFontColor',
    // },
    // {
    //   type: 'select',
    //   label: '对齐方式',
    //   name: 'titleTextPosition',
    //   value: 'center',
    //   props: {
    //     options:[{
    //       text: 'left',
    //       value: 'left',
    //     },{
    //       text: 'center',
    //       value: 'center',
    //     },{
    //       text: 'right',
    //       value: 'right',
    //     }]
    //   }
    // },
    // {
    //   type: 'inputNumber',
    //   label: '高度',
    //   value: 44,
    //   bind: {
    //     min: 24,
    //     max: 60,
    //     step: 1
    //   },
    //   name: 'titleHeight',
    // },input
    // {
    //   type: 'select',
    //   label: '位置',
    //   name: 'titlePosition',
    //   value: 'top',
    //   props: {
    //     options:[{
    //       text: 'left',
    //       value: 'left',
    //     },{
    //       text: 'top',
    //       value: 'top',
    //     },{
    //       text: 'right',
    //       value: 'right',
    //     },{
    //       text: 'bottom',
    //       value: 'bottom',
    //     }]
    //   }
    // },
  ],
  legend: [
    // {
    //   type: 'switch',
    //   label: '显示',
    //   value: false,
    //   name: 'legendIsShow',
    //   prop: {}
    // },
    // {
    //   type: 'colorPicker',
    //   label: '图例字色',
    //   value: '#373B52',
    //   name: 'legendFontColor',
    //   prop: {}
    // }
  ],
  axis: [
    {
      type: 'text',
      label: 'x轴标题',
      value: '',
      name: 'xAxisTitle',
      props: {},
    },
    {
      type: 'text',
      label: 'y轴标题',
      value: '',
      name: 'yAxisTitle',
      props: {},
    },
    {
      type: 'switch',
      label: '网格线',
      value: false,
      name: 'axisGridIsShow',
      props: {},
    },
    {
      type: 'iconRadio',
      label: '坐标轴布局',
      value: 'horizontal',
      name: 'axisLayout',
      chartType: ['barChart', 'groupBarChart', 'stackBarChart'],
      props: {
        name: 'axisLayout',
        optionType: 'qualitative',
        optionStyle: {
          wrapper: {
            width: '73px',
            height: '65px',
          },
          item: {
            width: '32px',
            height: '32px',
            'padding-left': '2.56px',
          },
          icon: {
            width: '24.89px',
            height: '24.89px',
            top: '-1px',
          },
        },
        default: 0,
        options: [
          {
            text: 'horizontal',
            name: '水平',
            type: 'qualitative',
            value: 'horizontal',
            imgSrc: '/icon/horizontal.svg',
          },
          {
            text: 'vertical',
            name: '垂直',
            type: 'qualitative',
            value: 'vertical',
            imgSrc: '/icon/vertical.svg',
          },
        ],
      },
    },
    {
      type: 'iconRadio',
      label: '线条样式',
      value: false,
      name: 'lineSmooth',
      chartType: ['lineChart'],
      props: {
        name: 'lineSmooth',
        optionType: 'qualitative',
        optionStyle: {
          wrapper: {
            width: '73px',
            height: '65px',
          },
          item: {
            width: '32px',
            height: '32px',
            'padding-left': '2.56px',
          },
          icon: {
            width: '24.89px',
            height: '24.89px',
            top: '-1px',
          },
        },
        default: 1,
        options: [
          {
            text: 'curve',
            name: '曲线',
            type: 'qualitative',
            value: true,
            imgSrc: '/icon/curve.svg',
          },
          {
            text: 'polyline',
            name: '折线',
            type: 'qualitative',
            value: false,
            imgSrc: '/icon/polyline.svg',
          },
        ],
      },
    },
    {
      type: 'iconRadio',
      label: '比例尺',
      value: 'linear',
      name: 'axisScaleType',
      props: {
        name: 'axisScaleType',
        optionType: 'qualitative',
        optionStyle: {
          wrapper: {
            width: '73px',
            height: '65px',
          },
          item: {
            width: '32px',
            height: '32px',
            'padding-left': '2.56px',
          },
          icon: {
            width: '24.89px',
            height: '24.89px',
            top: '-1px',
          },
        },
        default: 0,
        options: [
          {
            text: 'linear',
            name: '线性',
            type: 'qualitative',
            value: 'linear',
            imgSrc: '/icon/linear-proportion.svg',
          },
          {
            text: 'log',
            name: 'Log变换',
            type: 'qualitative',
            value: 'log',
            imgSrc: '/icon/log-proportion.svg',
          },
        ],
      },
    },
    // {
    //   type: 'switch',
    //   label: 'x轴',
    //   value: true,
    //   name: 'xAxisIsShow',
    //   prop: {}
    // },
    // {
    //   type: 'select',
    //   label: 'x轴位置',
    //   name: 'xAxisPosition',
    //   value: 'bottom',
    //   props: {
    //     options:[{
    //       text: '下方',
    //       value: 'bottom',
    //     },{
    //       text: '上方',
    //       value: 'top',
    //     }]
    //   }
    // },
    // {
    //   type: 'switch',
    //   label: 'x轴网格',
    //   value: false,
    //   name: 'xAxisGridIsShow',
    //   props: {}
    // },
    // {
    //   type: 'colorPicker',
    //   label: 'x轴网格色',
    //   value: '#e9e9e9',
    //   name: 'xAxisGridColor',
    //   props: {}
    // },
    // {
    //   type: 'switch',
    //   label: 'x轴刻度',
    //   value: false,
    //   name: 'xAxisTickIsShow',
    //   props: {}
    // },
    // {
    //   type: 'colorPicker',
    //   label: 'x轴刻度色',
    //   value: '#e9e9e9',
    //   name: 'xAxisTickColor',
    //   props: {}
    // },{
    //   type: 'switch',
    //   label: 'y轴',
    //   value: true,
    //   name: 'yAxisIsShow',
    //   props: {}
    // },
    // {
    //   type: 'select',
    //   label: 'y轴位置',
    //   name: 'yAxisPosition',
    //   value: 'left',
    //   props: {
    //     options:[{
    //       text: '左边',
    //       value: 'left',
    //     },{
    //       text: '右边',
    //       value: 'right',
    //     }]
    //   }
    // },
    // {
    //   type: 'switch',
    //   label: 'y轴网格',
    //   value: false,
    //   name: 'yAxisGridIsShow',
    //   props: {}
    // },
    // {
    //   type: 'colorPicker',
    //   label: 'y轴网格色',
    //   value: '#e9e9e9',
    //   name: 'yAxisGridColor',
    //   props: {}
    // },
    // {
    //   type: 'switch',
    //   label: 'y轴刻度',
    //   value: false,
    //   name: 'yAxisTickIsShow',
    //   props: {}
    // },
    // {
    //   type: 'colorPicker',
    //   label: 'y轴刻度色',
    //   value: '#e9e9e9',
    //   name: 'yAxisTickColor',
    //   props: {}
    // },
    // {
    //   type: 'select',
    //   label: '分类数据轴',
    //   name: 'axisCategoricalDataSelection',
    //   value: 'xAxis',
    //   props: {
    //     options:[{
    //       text: 'x轴',
    //       value: 'xAxis',
    //     },{
    //       text: 'y轴',
    //       value: 'yAxis',
    //     }
    //     // ,{
    //     //   text: '无',
    //     //   value: 'none',
    //     // },{
    //     //   text: '全部',
    //     //   value: 'all',
    //     // }
    //   ]
    //   }
    // },
  ],
  tooltip: [
    // {
    //   type: 'switch',
    //   label: '显示',
    //   value: true,
    //   name: 'tooltipIsShow',
    // }
  ],
  chart: [
    {
      type: 'iconRadio',
      label: '主题色',
      value: [
        '#7B88F2',
        '#FBC94B',
        '#65DCB8',
        '#EF7D52',
        '#F596C6',
        '#687B9D',
        '#5EC6D7',
        '#FFA65E',
        '#CC69D9',
        '#46A9A8',
        // "#7fc97f","#beaed4","#fdc086","#ffff99","#386cb0","#f0027f","#bf5b17","#666666",
      ],
      name: 'colors',
      props: {
        name: 'colors',
        optionType: 'qualitative',
        optionStyle: {
          wrapper: {
            width: '73px',
            height: '73px',
          },
          item: {
            width: '44px',
            height: '44px',
            padding: '5px',
          },
          icon: {
            width: '32px',
            height: '32px',
          },
        },
        default: 0,
        options: [
          {
            text: 'Accent',
            name: '浅色',
            type: 'qualitative',
            value: [
              '#7B88F2',
              '#FBC94B',
              '#65DCB8',
              '#EF7D52',
              '#F596C6',
              '#687B9D',
              '#5EC6D7',
              '#FFA65E',
              '#CC69D9',
              '#46A9A8',
            ],
            // value: ["#7fc97f","#beaed4","#fdc086","#ffff99","#386cb0","#f0027f","#bf5b17","#666666"],
            imgSrc: '/icon/themecolor-light.svg',
          },
          {
            text: 'Dark2',
            name: '深色',
            type: 'qualitative',
            value: [
              '#1b9e77',
              '#d95f02',
              '#7570b3',
              '#e7298a',
              '#66a61e',
              '#e6ab02',
              '#a6761d',
              '#666666',
            ],
            imgSrc: '/icon/themecolor-dark.svg',
          },
          {
            text: 'Paired',
            name: '其他',
            other: 'other',
            type: 'qualitative',
            value: [
              '#a6cee3',
              '#1f78b4',
              '#b2df8a',
              '#33a02c',
              '#fb9a99',
              '#e31a1c',
              '#fdbf6f',
              '#ff7f00',
              '#cab2d6',
              '#6a3d9a',
              '#ffff99',
              '#b15928',
            ],
            imgSrc: '/icon/themecolor-other.svg',
            props: {
              optionType: 'qualitative',
              options: [
                {
                  text: 'Paired',
                  type: 'qualitative',
                  value: [
                    '#5B8FF9',
                    '#CDDDFD',
                    '#5AD8A6',
                    '#CDF3E4',
                    '#5D7092',
                    '#CED4DE',
                    '#F6BD16',
                    '#FCEBB9',
                    '#6F5EF9',
                    '#D3CEFD',
                    '#6DC8EC',
                    '#D3EEF9',
                    '#945FB9',
                    '#DECFEA',
                    '#FF9845',
                    '#FFE0C7',
                    '#1E9493',
                    '#BBDEDE',
                    '#FF99C3',
                    '#FFE0ED',
                  ],
                  opacity: 0.3,
                },
                {
                  text: 'Pastel1',
                  type: 'qualitative',
                  value: [
                    '#fbb4ae',
                    '#b3cde3',
                    '#ccebc5',
                    '#decbe4',
                    '#fed9a6',
                    '#ffffcc',
                    '#e5d8bd',
                    '#fddaec',
                    '#f2f2f2',
                  ],
                  opacity: 0.6,
                },
                // {
                //   text: 'Paired',
                //   type: 'qualitative',
                //   value: ["#a6cee3","#1f78b4","#b2df8a","#33a02c","#fb9a99","#e31a1c","#fdbf6f","#ff7f00","#cab2d6","#6a3d9a","#ffff99","#b15928"],
                //   opacity: 0.3,
                // }, {
                //   text: 'Pastel1',
                //   type: 'qualitative',
                //   value: ["#fbb4ae","#b3cde3","#ccebc5","#decbe4","#fed9a6","#ffffcc","#e5d8bd","#fddaec","#f2f2f2"],
                //   opacity: 0.6,
                // },
                {
                  text: 'Pastel2',
                  type: 'qualitative',
                  value: [
                    '#b3e2cd',
                    '#fdcdac',
                    '#cbd5e8',
                    '#f4cae4',
                    '#e6f5c9',
                    '#fff2ae',
                    '#f1e2cc',
                    '#cccccc',
                  ],
                  opacity: 0.6,
                },
                {
                  text: 'Set1',
                  type: 'qualitative',
                  value: [
                    '#e41a1c',
                    '#377eb8',
                    '#4daf4a',
                    '#984ea3',
                    '#ff7f00',
                    '#ffff33',
                    '#a65628',
                    '#f781bf',
                    '#999999',
                  ],
                  opacity: 0.3,
                },
                {
                  text: 'Set2',
                  type: 'qualitative',
                  value: [
                    '#66c2a5',
                    '#fc8d62',
                    '#8da0cb',
                    '#e78ac3',
                    '#a6d854',
                    '#ffd92f',
                    '#e5c494',
                    '#b3b3b3',
                  ],
                  opacity: 0.3,
                },
                {
                  text: 'Set3',
                  type: 'qualitative',
                  value: [
                    '#8dd3c7',
                    '#ffffb3',
                    '#bebada',
                    '#fb8072',
                    '#80b1d3',
                    '#fdb462',
                    '#b3de69',
                    '#fccde5',
                    '#d9d9d9',
                    '#bc80bd',
                    '#ccebc5',
                    '#ffed6f',
                  ],
                  opacity: 0.3,
                },
                {
                  text: '主题色1',
                  type: 'qualitative',
                  value: [
                    '#5760e6',
                    '#FBC94B',
                    '#65DCB8',
                    '#EF7D52',
                    '#F596C6',
                    '#687B9D',
                    '#5EC6D7',
                    '#FFA65E',
                  ],
                  opacity: 0.3,
                },
                {
                  text: '主题色2',
                  type: 'sequential',
                  value: [
                    '#99ccff',
                    '#9999ff',
                    '#6699ff',
                    '#6666ff',
                    '#6666cc',
                    '#3366cc',
                    '#3333cc',
                    '#3333bb',
                  ],
                  opacity: 0.3,
                },
                {
                  text: 'YlGnBu',
                  type: 'sequential',
                  value: [
                    '#ffffd9',
                    '#edf8b1',
                    '#c7e9b4',
                    '#7fcdbb',
                    '#41b6c4',
                    '#1d91c0',
                    '#225ea8',
                    '#253494',
                    '#081d58',
                  ],
                  opacity: 0.3,
                },
                {
                  text: 'YlGn',
                  type: 'sequential',
                  value: [
                    '#ffffe5',
                    '#f7fcb9',
                    '#d9f0a3',
                    '#addd8e',
                    '#78c679',
                    '#41ab5d',
                    '#238443',
                    '#006837',
                    '#004529',
                  ],
                  opacity: 0.3,
                },
                {
                  text: 'OrRd',
                  type: 'sequential',
                  value: [
                    '#fff7ec',
                    '#fee8c8',
                    '#fdd49e',
                    '#fdbb84',
                    '#fc8d59',
                    '#ef6548',
                    '#d7301f',
                    '#b30000',
                    '#7f0000',
                  ],
                  opacity: 0.3,
                },
                {
                  text: 'Blues',
                  type: 'sequential',
                  value: [
                    '#f7fbff',
                    '#deebf7',
                    '#c6dbef',
                    '#9ecae1',
                    '#6baed6',
                    '#4292c6',
                    '#2171b5',
                    '#08519c',
                    '#08306b',
                  ],
                  opacity: 0.3,
                },
                {
                  text: 'Greens',
                  type: 'sequential',
                  value: [
                    '#f7fcf5',
                    '#e5f5e0',
                    '#c7e9c0',
                    '#a1d99b',
                    '#74c476',
                    '#41ab5d',
                    '#238b45',
                    '#006d2c',
                    '#00441b',
                  ],
                  opacity: 0.3,
                },
                {
                  text: 'Greys',
                  type: 'sequential',
                  value: [
                    '#ffffff',
                    '#f0f0f0',
                    '#d9d9d9',
                    '#bdbdbd',
                    '#969696',
                    '#737373',
                    '#525252',
                    '#252525',
                    '#000000',
                  ],
                  opacity: 0.3,
                },
              ],
            },
          },
        ],
      },
    },
    // {
    //   type: 'paddingSetter',
    //   label: '内边距',
    //   value: [10, 20, 20, 10],
    //   name: 'padding',
    // },
    // {
    //   type: 'colorPicker',
    //   label: '背景色',
    //   value: '#FFF',
    //   name: 'chartBackground',
    // },
    // {
    //   type: 'switch',
    //   label: '标签显示',
    //   value: false,
    //   name: 'labelIsShow',
    // },
    // {
    //   type: 'colorPicker',
    //   label: '标签字色',
    //   value: '#373B52',
    //   name: 'labelFontColor',
    // },
    // {
    //   type: 'inputNumber',
    //   label: '标签字号',
    //   value: 12,
    //   name: 'labelFontSize',
    // },
    // {
    //   type: 'inputNumber',
    //   label: '标签长度',
    //   value: 8,
    //   bind: {
    //     min: 5,
    //     max: 100,
    //     step: 1
    //   },
    //   name: 'labelTextMaxlength',
    // },
    // {
    //   type: 'themeColorRadio',
    //   label: '主题色',
    //   value: [
    //     "#7fc97f","#beaed4","#fdc086","#ffff99","#386cb0","#f0027f","#bf5b17","#666666",
    //   ],
    //   name: 'colors',
    //   props: {
    //     optionType: 'qualitative',
    //     options: [{
    //       text: 'Accent',
    //       type: 'qualitative',
    //       value: ["#7fc97f","#beaed4","#fdc086","#ffff99","#386cb0","#f0027f","#bf5b17","#666666"]
    //     }, {
    //       text: 'Dark2',
    //       type: 'qualitative',
    //       value: ["#1b9e77","#d95f02","#7570b3","#e7298a","#66a61e","#e6ab02","#a6761d","#666666"]
    //     }, {
    //       text: 'Paired',
    //       type: 'qualitative',
    //       value: ["#a6cee3","#1f78b4","#b2df8a","#33a02c","#fb9a99","#e31a1c","#fdbf6f","#ff7f00","#cab2d6","#6a3d9a","#ffff99","#b15928"]
    //     }, {
    //       text: 'Pastel1',
    //       type: 'qualitative',
    //       value: ["#fbb4ae","#b3cde3","#ccebc5","#decbe4","#fed9a6","#ffffcc","#e5d8bd","#fddaec","#f2f2f2"]
    //     }, {
    //       text: 'Pastel2',
    //       type: 'qualitative',
    //       value: ["#b3e2cd","#fdcdac","#cbd5e8","#f4cae4","#e6f5c9","#fff2ae","#f1e2cc","#cccccc"]
    //     }, {
    //       text: 'Set1',
    //       type: 'qualitative',
    //       value: ["#e41a1c","#377eb8","#4daf4a","#984ea3","#ff7f00","#ffff33","#a65628","#f781bf","#999999"]
    //     }, {
    //       text: 'Set2',
    //       type: 'qualitative',
    //       value: ["#66c2a5","#fc8d62","#8da0cb","#e78ac3","#a6d854","#ffd92f","#e5c494","#b3b3b3"]
    //     }, {
    //       text: 'Set3',
    //       type: 'qualitative',
    //       value: ["#8dd3c7","#ffffb3","#bebada","#fb8072","#80b1d3","#fdb462","#b3de69","#fccde5","#d9d9d9","#bc80bd","#ccebc5","#ffed6f"]
    //     }, {
    //       text: '主题色1',
    //       type: 'qualitative',
    //       value: [
    //         '#5760e6','#FBC94B','#65DCB8', '#EF7D52', '#F596C6', '#687B9D', '#5EC6D7', '#FFA65E'
    //       ]
    //     },{
    //       text: '主题色2',
    //       type: 'sequential',
    //       value: ['#99ccff','#9999ff','#6699ff','#6666ff','#6666cc','#3366cc','#3333cc','#3333bb']
    //     }, {
    //       text: 'YlGnBu',
    //       type: 'sequential',
    //       value: ["#ffffd9","#edf8b1","#c7e9b4","#7fcdbb","#41b6c4","#1d91c0","#225ea8","#253494","#081d58"]
    //     }, {
    //       text: 'YlGn',
    //       type: 'sequential',
    //       value: ["#ffffe5","#f7fcb9","#d9f0a3","#addd8e","#78c679","#41ab5d","#238443","#006837","#004529"]
    //     }, {
    //       text: 'OrRd',
    //       type: 'sequential',
    //       value: ["#fff7ec","#fee8c8","#fdd49e","#fdbb84","#fc8d59","#ef6548","#d7301f","#b30000","#7f0000"]
    //     }, {
    //       text: 'Blues',
    //       type: 'sequential',
    //       value: ["#f7fbff","#deebf7","#c6dbef","#9ecae1","#6baed6","#4292c6","#2171b5","#08519c","#08306b"]
    //     }, {
    //       text: 'Greens',
    //       type: 'sequential',
    //       value: ["#f7fcf5","#e5f5e0","#c7e9c0","#a1d99b","#74c476","#41ab5d","#238b45","#006d2c","#00441b"]
    //     }, {
    //       text: 'Greys',
    //       type: 'sequential',
    //       value: ["#ffffff","#f0f0f0","#d9d9d9","#bdbdbd","#969696","#737373","#525252","#252525","#000000"]
    //     }]
    //   }
    // }
  ],
  map: [
    {
      type: 'select',
      label: '地图主题',
      value: 'normal',
      name: 'mapTheme',
      props: {
        options: [
          {
            text: 'normal',
            value: 'normal',
          },
          {
            text: 'macaron',
            value: 'macaron',
          },
          {
            text: 'graffiti',
            value: 'graffiti',
          },
          {
            text: 'whitesmoke',
            value: 'whitesmoke',
          },
          {
            text: 'dark',
            value: 'dark',
          },
          {
            text: 'fresh',
            value: 'fresh',
          },
          {
            text: 'darkblue',
            value: 'darkblue',
          },
          {
            text: 'blue',
            value: 'blue',
          },
          {
            text: 'light',
            value: 'light',
          },
          {
            text: 'grey',
            value: 'grey',
          },
        ],
      },
    },
  ],
}
/**
 
    {
      type: 'labelIsShow',
      label: '标签显示',
      value: false,
      name: 'tooltipIsShow',
    },
 */
// 饼图
export const pieChart = {
  title: [
    {
      type: 'label',
      label: '标题',
    },
    ...commonConfig.title,
  ],
  legend: [
    {
      type: 'label',
      label: '图例',
      labelbutton: true,
      props: [
        {
          type: 'switch',
          label: '显示',
          value: false,
          name: 'legendIsShow',
          props: {},
        },
      ],
    },
    ...commonConfig.legend,
  ],
  tooltip: [
    {
      type: 'label',
      label: '提示',
      labelbutton: true,
      props: [
        {
          type: 'switch',
          label: '显示',
          value: true,
          name: 'tooltipIsShow',
        },
      ],
    },
    ...commonConfig.tooltip,
  ],
  chart: [
    {
      type: 'label',
      label: '主题',
    },
    ...commonConfig.chart,
  ],
}
// 圆环图
export const donutChart = {
  title: [
    {
      type: 'label',
      label: '标题',
    },
    ...commonConfig.title,
  ],
  legend: [
    {
      type: 'label',
      label: '图例',
      labelbutton: true,
      props: [
        {
          type: 'switch',
          label: '显示',
          value: false,
          name: 'legendIsShow',
          props: {},
        },
      ],
    },
    ...commonConfig.legend,
  ],
  tooltip: [
    {
      type: 'label',
      label: '提示',
      labelbutton: true,
      props: [
        {
          type: 'switch',
          label: '显示',
          value: true,
          name: 'tooltipIsShow',
        },
      ],
    },
    ...commonConfig.tooltip,
  ],
  chart: [
    {
      type: 'label',
      label: '主题',
    },
    ...commonConfig.chart,
  ],
}
// 普通柱状图
export const barChart = {
  title: [
    {
      type: 'label',
      label: '标题',
    },
    ...commonConfig.title,
  ],
  legend: [
    {
      type: 'label',
      label: '图例',
      labelbutton: true,
      props: [
        {
          type: 'switch',
          label: '显示',
          value: false,
          name: 'legendIsShow',
          props: {},
        },
      ],
    },
    ...commonConfig.legend,
  ],
  axis: [
    {
      type: 'label',
      label: '布局',
    },
    ...commonConfig.axis,
  ],
  tooltip: [
    {
      type: 'label',
      label: '提示',
      labelbutton: true,
      props: [
        {
          type: 'switch',
          label: '显示',
          value: true,
          name: 'tooltipIsShow',
        },
      ],
    },
    ...commonConfig.tooltip,
  ],
  chart: [
    {
      type: 'label',
      label: '主题',
    },
    ...commonConfig.chart,
  ],
}
// 分组柱状图
export const groupBarChart = {
  title: [
    {
      type: 'label',
      label: '标题',
    },
    ...commonConfig.title,
  ],
  legend: [
    {
      type: 'label',
      label: '图例',
      labelbutton: true,
      props: [
        {
          type: 'switch',
          label: '显示',
          value: false,
          name: 'legendIsShow',
          props: {},
        },
      ],
    },
    ...commonConfig.legend,
  ],
  axis: [
    {
      type: 'label',
      label: '布局',
    },
    ...commonConfig.axis,
  ],
  tooltip: [
    {
      type: 'label',
      label: '提示',
      labelbutton: true,
      props: [
        {
          type: 'switch',
          label: '显示',
          value: true,
          name: 'tooltipIsShow',
        },
      ],
    },
    ...commonConfig.tooltip,
  ],
  chart: [
    {
      type: 'label',
      label: '主题',
    },
    ...commonConfig.chart,
  ],
}
// 堆栈柱状图
export const stackBarChart = {
  title: [
    {
      type: 'label',
      label: '标题',
    },
    ...commonConfig.title,
  ],
  legend: [
    {
      type: 'label',
      label: '图例',
      labelbutton: true,
      props: [
        {
          type: 'switch',
          label: '显示',
          value: false,
          name: 'legendIsShow',
          props: {},
        },
      ],
    },
    ...commonConfig.legend,
  ],
  axis: [
    {
      type: 'label',
      label: '布局',
    },
    ...commonConfig.axis,
  ],
  tooltip: [
    {
      type: 'label',
      label: '提示',
      labelbutton: true,
      props: [
        {
          type: 'switch',
          label: '显示',
          value: true,
          name: 'tooltipIsShow',
        },
      ],
    },
    ...commonConfig.tooltip,
  ],
  chart: [
    {
      type: 'label',
      label: '主题',
    },
    ...commonConfig.chart,
  ],
}
// 折线图
export const lineChart = {
  title: [
    {
      type: 'label',
      label: '标题',
    },
    ...commonConfig.title,
  ],
  legend: [
    {
      type: 'label',
      label: '图例',
      labelbutton: true,
      props: [
        {
          type: 'switch',
          label: '显示',
          value: false,
          name: 'legendIsShow',
          props: {},
        },
      ],
    },
    ...commonConfig.legend,
  ],
  axis: [
    {
      type: 'label',
      label: '布局',
    },
    ...commonConfig.axis,
  ],
  tooltip: [
    {
      type: 'label',
      label: '提示',
      labelbutton: true,
      props: [
        {
          type: 'switch',
          label: '显示',
          value: true,
          name: 'tooltipIsShow',
        },
      ],
    },
    ...commonConfig.tooltip,
  ],
  chart: [
    {
      type: 'label',
      label: '主题',
    },
    ...commonConfig.chart,
  ],
}
// 散点图
export const scatterplot = {
  title: [
    {
      type: 'label',
      label: '标题',
    },
    ...commonConfig.title,
  ],
  legend: [
    {
      type: 'label',
      label: '图例',
      labelbutton: true,
      props: [
        {
          type: 'switch',
          label: '显示',
          value: false,
          name: 'legendIsShow',
          props: {},
        },
      ],
    },
    ...commonConfig.legend,
  ],
  axis: [
    {
      type: 'label',
      label: '布局',
    },
    ...commonConfig.axis,
  ],
  tooltip: [
    {
      type: 'label',
      label: '提示',
      labelbutton: true,
      props: [
        {
          type: 'switch',
          label: '显示',
          value: true,
          name: 'tooltipIsShow',
        },
      ],
    },
    ...commonConfig.tooltip,
  ],
  chart: [
    {
      type: 'label',
      label: '主题',
    },
    ...commonConfig.chart,
  ],
}
// 面积图
export const areaChart = {
  title: [
    {
      type: 'label',
      label: '标题',
    },
    ...commonConfig.title,
  ],
  legend: [
    {
      type: 'label',
      label: '图例',
      labelbutton: true,
      props: [
        {
          type: 'switch',
          label: '显示',
          value: false,
          name: 'legendIsShow',
          props: {},
        },
      ],
    },
    ...commonConfig.legend,
  ],
  axis: [
    {
      type: 'label',
      label: '布局',
    },
    ...commonConfig.axis,
  ],
  tooltip: [
    {
      type: 'label',
      label: '提示',
      labelbutton: true,
      props: [
        {
          type: 'switch',
          label: '显示',
          value: true,
          name: 'tooltipIsShow',
        },
      ],
    },
    ...commonConfig.tooltip,
  ],
  chart: [
    {
      type: 'label',
      label: '主题',
    },
    ...commonConfig.chart,
  ],
}
// 平行坐标轴图
export const parallelCoordinates = {
  title: [
    {
      type: 'label',
      label: '标题',
    },
    ...commonConfig.title,
  ],
  legend: [
    {
      type: 'label',
      label: '图例',
      labelbutton: true,
      props: [
        {
          type: 'switch',
          label: '显示',
          value: false,
          name: 'legendIsShow',
          props: {},
        },
      ],
    },
    ...commonConfig.legend,
  ],
  axis: [
    {
      type: 'label',
      label: '布局',
    },
    ...commonConfig.axis,
  ],
  tooltip: [
    {
      type: 'label',
      label: '提示',
      labelbutton: true,
      props: [
        {
          type: 'switch',
          label: '显示',
          value: true,
          name: 'tooltipIsShow',
        },
      ],
    },
    ...commonConfig.tooltip,
  ],
  chart: [
    {
      type: 'label',
      label: '主题',
    },
    ...commonConfig.chart,
  ],
}

// 地图
export const geographicMap = {
  title: [
    {
      type: 'label',
      label: '标题',
    },
    ...commonConfig.title,
    {
      type: 'label',
      label: '地图',
    },
    ...commonConfig.map,
  ],
  // tooltip:[{
  //   type: 'label',
  //   label: '提示',
  //   labelbutton: true,
  //   props: [{
  //     type: 'switch',
  //     label: '显示',
  //     value: true,
  //     name: 'tooltipIsShow',
  //   }]
  // }, ...commonConfig.tooltip],
  // chart:[{
  //   type: 'label',
  //   label: '主题'
  // }, ...commonConfig.chart]
}
// 热力图
export const heatmapMatrix = {
  title: [
    {
      type: 'label',
      label: '标题',
    },
    ...commonConfig.title,
  ],
  legend: [
    {
      type: 'label',
      label: '图例',
      labelbutton: true,
      props: [
        {
          type: 'switch',
          label: '显示',
          value: false,
          name: 'legendIsShow',
          props: {},
        },
      ],
    },
    ...commonConfig.legend,
  ],
  axis: [
    {
      type: 'label',
      label: '布局',
    },
    ...commonConfig.axis.filter((item) => {
      return item.label !== '比例尺'
    }),
  ],
  tooltip: [
    {
      type: 'label',
      label: '提示',
      labelbutton: true,
      props: [
        {
          type: 'switch',
          label: '显示',
          value: true,
          name: 'tooltipIsShow',
        },
      ],
    },
    ...commonConfig.tooltip,
  ],
  chart: [
    {
      type: 'label',
      label: '主题',
    },
    ...cloneDeep(commonConfig.chart),
    // 由于heatMatMtraix暂时不支持颜色log比例尺先注释
    // ...commonConfig.axis.filter(item => {return item.label === '比例尺'})
  ],
}
heatmapMatrix.chart.forEach((item: any) => {
  if (item.type === 'themeColorRadio') {
    item.props.optionType = 'sequential'
  }
})

export const table = {}

const chartSettingMaps: KeyValueStore = {
  pieChart,
  donutChart,
  barChart,
  groupBarChart,
  stackBarChart,
  lineChart,
  areaChart,
  scatterplot,
  parallelCoordinates,
  heatmapMatrix,
  geographicMap,
  table,
  densityDataBarChart: {},
  densityDataAreaChart: {},
}

export const chartSetting = {
  setChartSetting(
    formItems: Array<KeyValueStore>,
    chartType: string | undefined,
    formData?: KeyValueStore
  ) {
    if (chartType) {
      const chartConfig = chartSettingMaps[chartType]
      Object.values(chartConfig).forEach((value) => {
        // @ts-ignore
        value.forEach((v) => {
          const find = formItems.find((item) => {
            if (item.type === 'label') {
              return item.label === v.label
            }
            return item.name === v.name
          })
          if (!find) {
            formItems.push(v)
          }
          /* eslint-disable */
          if (v.name && formData) {
            formData[v.name] = v.value
          }
        })
      })
    }
  },
}

/**
 * 根据chartType获取配置表单项
 * @param chartType
 */
export function getSettingConfig(
  chartType: Array<string> | string,
  isAggr?: boolean
): Array<any> {
  let chartTypeUsed: any = chartType
  if (isArray(chartType)) {
    ;[, chartTypeUsed] = chartType
  } else if (/\|/.test(chartType)) {
    ;[, chartTypeUsed] = chartType.split('|')
  }

  const formItems = getDataBindSettings(chartTypeUsed, isAggr)
  Object.values(chartSettingMaps[chartTypeUsed]).forEach((value) => {
    // @ts-ignore
    value.forEach((v) => {
      const find = formItems.find((item) => {
        if (item.type === 'label') {
          return item.label === v.label
        }
        return item.name === v.name
      })
      if (!find) {
        if (!v.chartType || v.chartType.includes(chartTypeUsed)) {
          formItems.push(v)
        }
      }
    })
  })
  return cloneDeep(formItems)
}

/**
 * 根据chartType 获取默认配置
 * @param chartType
 */
export function getDefaultFormData(chartType: Array<string> | string) {
  const formItems = getSettingConfig(chartType)
  const formData: KeyValueStore = {}
  formItems.forEach((item) => {
    if (item.type !== 'label') {
      formData[item.name] = item.value
    } else if (item.labelbutton) {
      formData[item.props[0].name] = item.props[0].value
    }
  })
  return formData
}

/**
 * 适用于geomap处理逻辑的widgetJson
 * @param dataSetting chartOptions
 * @returns widgetJson
 */
function getGeoWidgetJson(dataSetting: KeyValueStore) {
  const keys: any[] = []
  const values: any[] = []
  const { labelKey, valueKey, labelSort } = dataSetting
  ;(Array.isArray(labelKey) ? labelKey : [labelKey]).forEach((key) => {
    keys.push({
      col: key,
      values: '',
      filterType: '',
      sort: labelSort ?? '',
    })
  })
  ;(Array.isArray(valueKey) ? valueKey : [valueKey]).forEach((key) => {
    values.push({
      col: key,
      sort: 'asc',
      aggregate_type: dataSetting.aggregateType,
      topN: dataSetting.topK ? dataSetting.topK : '',
    })
  })
  return {
    isAggr: !!dataSetting.isAggr,
    config: {
      keys: keys,
      values: values,
      groups: dataSetting.groupBy
        ? [
            {
              col: dataSetting.groupBy,
              values: [],
            },
          ]
        : undefined,
      filters: [],
      topN: dataSetting.topK,
    },
  }
}

export function getWidgetJson(
  dataSetting: KeyValueStore,
  chartType?: string[] | string
) {
  if (
    (Array.isArray(chartType) && chartType[0] === 'filterForm') ||
    chartType === 'filterForm'
  ) {
    return {}
  }
  if (
    (Array.isArray(chartType) &&
      chartType[1] === ChartDetailsEnum.geographicMap) ||
    chartType === ChartDetailsEnum.geographicMap
  ) {
    return getGeoWidgetJson(dataSetting)
  }
  /* 度量 */
  const values = []
  if (isArray(dataSetting.valueKey)) {
    dataSetting.valueKey.forEach((valueItem: KeyValueStore) => {
      if (valueItem.value !== '') {
        values.push({
          col: valueItem.value,
          sort: valueItem.sort,
          aggregate_type: valueItem.func ? valueItem.func : 'sum',
          topN: dataSetting.topK ? dataSetting.topK : '',
        })
      }
    })
  } else {
    values.push({
      col: dataSetting.valueKey,
      sort: '',
      aggregate_type: dataSetting.aggregateType,
      topN: dataSetting.topK ? dataSetting.topK : '',
    })
  }
  const keys = [
    {
      col: dataSetting.labelKey,
      values: '',
      filterType: '',
      sort: dataSetting.labelSort ? dataSetting.labelSort : 'asc',
    },
  ]
  if (dataSetting.labelKeyY) {
    keys.push({
      col: dataSetting.labelKeyY,
      values: '',
      filterType: '',
      sort: dataSetting.labelSortY ? dataSetting.labelSortY : '',
    })
  }
  return {
    isAggr: !!dataSetting.isAggr,
    config: {
      keys: keys,
      values: values,
      groups: dataSetting.groupBy
        ? [
            {
              col: dataSetting.groupBy,
              values: [],
            },
          ]
        : undefined,
      filters: [],
      topN: dataSetting.topK,
    },
  }
}

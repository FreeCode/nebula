/**
 * 数据绑定formConfig 配置
 */
import { cloneDeep } from 'lodash'
import { KeyValueStore } from '@/components/vueGridLayout/interfate/grid-Item-data-inteface'
import { ChartDetailsEnum } from './contant'

export const OneKeyOneValue: Array<KeyValueStore> = [
  {
    type: 'label',
    label: '数据管理',
    groupName: 'sjgl',
  },
  {
    type: 'select',
    label: '数据集',
    value: '',
    name: 'dataId',
    props: {
      options: [],
    },
    groupName: 'sjgl',
  },
  {
    type: 'switch',
    label: '聚合查询',
    value: true,
    name: 'isAggr',
    prop: {},
  },
  {
    type: 'select',
    label: '维度',
    value: '',
    name: 'labelKey',
    optionsSourse: 'columns',
    optionFilter(/* column:KeyValueStore */) {
      return true // !column.isNumber
    },
    subStyle: {
      display: 'inline-block',
      width: '20px',
      height: '28px',
      position: 'absolute',
      left: '152px',
      top: '-5px',
    },
    bind: {
      style: {
        width: '150px',
      },
    },
    subItems: [
      {
        type: 'sort',
        value: '',
        name: 'labelSort',
      },
    ],
    props: {
      // mode: 'multiple',
      options: [],
    },
    groupName: 'sjgl',
  },
  {
    type: 'select',
    label: '格式化',
    value: '',
    name: 'labelKeyFormat',
    isHide: true,
    bind: {
      style: {
        // width: '150px'
      },
    },
    props: {
      options: [
        {
          text: 'YYYY-MM-DD',
          value: 'YYYY-MM-DD',
        },
        {
          text: 'YYYY/MM/DD',
          value: 'YYYY/MM/DD',
        },
        {
          text: 'YYYY-MM-DD HH:mm:ss',
          value: 'YYYY-MM-DD HH:mm:ss',
        },
        {
          text: 'YYYY/MM/DD HH:mm:ss',
          value: 'YYYY/MM/DD HH:mm:ss',
        },
        {
          text: 'MM/DD',
          value: 'MM/DD',
        },
        {
          text: 'MM-DD',
          value: 'MM-DD',
        },
      ],
    },
  },
  {
    type: 'multipleSelect',
    label: '度量',
    value: [cloneDeep({ value: '', sort: 'asc', func: 'sum' })],
    name: 'valueKey',
    domClass: 'label-key',
    optionsSourse: 'columns',
    optionFilter(column: KeyValueStore) {
      return column.isNumber
    },
    props: {
      isSingle: true,
      options: [{}],
    },
    groupName: 'sjgl',
  },
  {
    type: 'select',
    label: '分组字段',
    value: '',
    name: 'groupBy',
    optionsSourse: 'columns',
    optionFilter(/* column:KeyValueStore */) {
      return true
    },
    bind: {
      allowClear: true,
    },
    props: {
      allowClear: true,
      options: [],
    },
    groupName: 'sjgl',
  },
  {
    type: 'inputNumber',
    label: 'TopK',
    tip: 'TopK',
    value: 0,
    name: 'topK',
    required: true,
    bind: {
      max: 9999,
      step: 1,
      min: 1,
    },
    groupName: 'sjgl',
  },
  {
    type: 'inputNumber',
    label: '显示阈值',
    tip: '显示阈值',
    value: 16,
    isHide: true,
    name: 'showNumber',
    required: true,
    bind: {
      allowClear: true,
      max: 9999,
      step: 1,
      min: 0,
    },
  },
]
export const OneKeyMultipleValue: Array<KeyValueStore> = [
  {
    type: 'label',
    label: '数据管理',
    groupName: 'sjgl',
  },
  {
    type: 'select',
    label: '数据集',
    value: '',
    name: 'dataId',
    props: {
      options: [],
    },
    groupName: 'sjgl',
  },
  {
    type: 'switch',
    label: '聚合查询',
    value: true,
    name: 'isAggr',
    prop: {},
  },
  {
    type: 'select',
    label: '维度',
    value: '',
    name: 'labelKey',
    optionsSourse: 'columns',
    subStyle: {
      display: 'inline-block',
      width: '20px',
      height: '28px',
      position: 'absolute',
      left: '152px',
      top: '-5px',
    },
    bind: {
      style: {
        width: '150px',
      },
    },
    subItems: [
      {
        type: 'sort',
        value: '',
        name: 'labelSort',
      },
    ],
    optionFilter(/* column:KeyValueStore */) {
      return true // !column.isNumber
    },
    props: {
      options: [],
    },
    groupName: 'sjgl',
  },

  {
    type: 'select',
    label: '格式化',
    value: '',
    name: 'labelKeyFormat',
    isHide: true,
    bind: {
      style: {
        // width: '150px'
      },
    },
    props: {
      options: [
        {
          text: 'YYYY-MM-DD',
          value: 'YYYY-MM-DD',
        },
        {
          text: 'YYYY/MM/DD',
          value: 'YYYY/MM/DD',
        },
        {
          text: 'YYYY-MM-DD HH:mm:ss',
          value: 'YYYY-MM-DD HH:mm:ss',
        },
        {
          text: 'YYYY/MM/DD HH:mm:ss',
          value: 'YYYY/MM/DD HH:mm:ss',
        },
        {
          text: 'MM/DD',
          value: 'MM/DD',
        },
        {
          text: 'MM-DD',
          value: 'MM-DD',
        },
      ],
    },
  },
  {
    type: 'multipleSelect', // 'select',
    label: '度量',
    value: [{ value: '', sort: 'asc', func: 'sum' }],
    name: 'valueKey',
    domClass: 'label-key',
    optionsSourse: 'columns',
    optionFilter(column: KeyValueStore) {
      return column.isNumber
    },
    props: {
      mode: 'multiple',
      options: [{}],
    },
    groupName: 'sjgl',
  },
  {
    type: 'select',
    label: '分组字段',
    value: '',
    name: 'groupBy',
    optionsSourse: 'columns',
    // domStyle: 'margin-top: -40px;',
    optionFilter(/* column:KeyValueStore */) {
      return true
    },
    bind: {
      allowClear: true,
    },
    props: {
      options: [],
    },
    groupName: 'sjgl',
  },
  {
    type: 'inputNumber',
    label: 'TopK',
    tip: 'TopK',
    value: 0,
    name: 'topK',
    required: true,
    bind: {
      max: 9999,
      min: 1,
    },
    props: {},
    groupName: 'sjgl',
  },
]

export const MultipleKeyOneValue: Array<KeyValueStore> = [
  {
    type: 'label',
    label: '数据管理',
    groupName: 'sjgl',
  },
  {
    type: 'select',
    label: '数据集',
    value: '',
    name: 'dataId',
    props: {
      options: [],
    },
    groupName: 'sjgl',
  },
  {
    type: 'switch',
    label: '聚合查询',
    value: true,
    name: 'isAggr',
    prop: {},
  },
  {
    type: 'select',
    label: '维度(x)',
    value: '',
    name: 'labelKey',
    optionsSourse: 'columns',
    subStyle: {
      display: 'inline-block',
      width: '20px',
      height: '28px',
      position: 'absolute',
      left: '152px',
      top: '-5px',
    },
    bind: {
      style: {
        width: '150px',
      },
    },
    subItems: [
      {
        type: 'sort',
        value: '',
        name: 'labelSort',
      },
    ],
    optionFilter(/* column:KeyValueStore */) {
      return true // !column.isNumber
    },
    props: {
      // mode: 'multiple',
      options: [],
    },
    groupName: 'sjgl',
  },
  {
    type: 'select',
    label: '维度(y)',
    value: '',
    name: 'labelKeyY',
    optionsSourse: 'columns',
    subStyle: {
      display: 'inline-block',
      width: '20px',
      height: '28px',
      position: 'absolute',
      left: '152px',
      top: '-5px',
    },
    bind: {
      style: {
        width: '150px',
      },
    },
    subItems: [
      {
        type: 'sort',
        value: '',
        name: 'labelSortY',
      },
    ],
    optionFilter(/* column:KeyValueStore */) {
      return true // !column.isNumber
    },
    props: {
      options: [],
    },
    groupName: 'sjgl',
  },
  {
    type: 'multipleSelect',
    label: '度量',
    value: [{ value: '', sort: 'asc', func: 'sum' }],
    name: 'valueKey',
    domClass: 'label-key',
    optionsSourse: 'columns',
    optionFilter(column: KeyValueStore) {
      return column.isNumber
    },
    props: {
      isSingle: true,
      mode: 'multiple',
      options: [{}],
    },
    groupName: 'sjgl',
  },
  {
    type: 'select',
    label: '分组字段',
    value: '',
    name: 'groupBy',
    optionsSourse: 'columns',
    // domStyle: 'margin-top: -40px;',
    optionFilter(/* column:KeyValueStore */) {
      return true
    },
    bind: {
      allowClear: true,
    },
    props: {
      options: [],
    },
    groupName: 'sjgl',
  },
  {
    type: 'inputNumber',
    label: 'TopK',
    tip: 'TopK',
    value: 0,
    name: 'topK',
    required: true,
    bind: {
      max: 9999,
      min: 1,
    },
    props: {},
    groupName: 'sjgl',
  },
]

const geographicMapSetting = [
  {
    type: 'label',
    label: '数据管理',
    groupName: 'sjgl',
  },

  {
    type: 'select',
    label: '数据集',
    value: '',
    name: 'dataId',
    props: {
      options: [],
    },
    groupName: 'sjgl',
  },
  {
    type: 'select',
    value: 'point',
    label: 'Glyph',
    name: 'glyph',
    isHide: false,
    bind: {
      allowClear: false,
      disabled: true,
    },
    props: {
      disabled: true,
      options: [
        // 先暂时支持1种
        {
          text: 'point',
          value: 'point',
        },
        {
          text: 'heatmap',
          value: 'heatmap',
        },
        {
          text: 'line',
          value: 'line',
        },
        {
          text: 'polygon',
          value: 'polygon',
        },
        {
          text: 'world',
          value: 'world',
        },
        // {
        //   text: 'text',
        //   value: 'text'
        // },
        // {
        //   text: 'complexGlyph',
        //   value: 'complexGlyph'
        // },
        // {
        //   text: 'link',
        //   value: 'link'
        // },
        // {
        //   text: 'contour',
        //   value: 'contour'
        // },
        // {
        //   text: 'district',
        //   value: 'district'
        // }
      ],
    },
  },

  {
    type: 'select',
    label: '坐标列',
    isHide: false,
    value: '',
    name: 'labelKey',
    optionsSourse: 'columns',
    optionFilter(/* column:KeyValueStore */) {
      return true // !column.isNumber
    },
    props: {
      // mode: 'multiple',
      options: [],
    },
    groupName: 'sjgl',
  },
  {
    type: 'multipleSelect',
    label: '数据列',
    value: [cloneDeep({ value: '', sort: 'asc', func: 'sum' })],
    name: 'valueKey',
    domClass: 'label-key',
    isHide: false,
    optionsSourse: 'columns',
    optionFilter(column: KeyValueStore) {
      return column.isNumber
    },
    props: {
      isSingle: true,
      options: [{}],
    },
    groupName: 'sjgl',
  },
  // {
  //   type: 'select',
  //   label: '映射方式',
  //   value: 'none',
  //   name: 'mappingType',
  //   bind: {
  //     allowClear: false,
  //   },
  //   props: {
  //     options: [
  //       {
  //         text: 'none',
  //         value: 'none'
  //       },
  //       {
  //         text: 'country',
  //         value: 'country'
  //       },
  //       {
  //         text: 'china',
  //         value: 'china' },
  //       {
  //         text: 'zhejiang',
  //         value: 'zhejiang'
  //       },
  //     ]
  //   },
  //   groupName: 'sjgl'
  // },
  // {
  //   type: 'select',
  //   label: '编码方式',
  //   value: 'none',
  //   name: 'encodingType',
  //   bind: {
  //     allowClear: false,
  //   },
  //   props: {
  //     options: [
  //       {
  //         text: 'none',
  //         value: 'none'
  //       },
  //       {
  //         text: 'color',
  //         value: 'color'
  //       },
  //       {
  //         text: 'area',
  //         value: 'area'
  //       }
  //     ]
  //   },
  //   groupName: 'sjgl'
  // },
  // {
  //   type: 'select',
  //   label: '点类型',
  //   value: 'RoundPoint',
  //   name: 'pointType',
  //   bind: {
  //     allowClear: false,
  //   },
  //   props: {
  //     options: [
  //       {
  //         text: 'RoundPoint',
  //         value: 'RoundPoint'
  //       },
  //       {
  //         text: 'ScatterPoint',
  //         value: 'ScatterPoint'
  //       },
  //       {
  //         text: 'PointCloud',
  //         value: 'PointCloud'
  //       }
  //     ]
  //   },
  //   groupName: 'sjgl'
  // },
  //
  {
    type: 'colorPicker',
    label: '多边形颜色',
    value: '#FF0000',
    name: 'polygonColor',
  },
  {
    type: 'colorPicker',
    label: '边框颜色',
    value: '#FF0000',
    name: 'borderColor',
  },
  {
    type: 'text',
    label: '划分区间',
    value: '[1000,10000,50000,100000,500000,1000000,2000000,5000000]',
    name: 'valueSeparator',
  },
  {
    type: 'inputNumber',
    label: '边框宽度',
    value: 1,
    bind: {
      min: 1,
      max: 10,
      step: 1,
    },
    name: 'borderWidth',
  },

  {
    type: 'colorPicker',
    label: '点颜色',
    value: '#FF0000',
    name: 'pointColor',
  },
  {
    type: 'inputNumber',
    label: '点半径',
    value: 5,
    bind: {
      min: 1,
      max: 1000,
      step: 1,
    },
    name: 'pointRadius',
  },
  {
    type: 'inputNumber',
    label: '点半径',
    value: 5,
    bind: {
      min: 1,
      max: 1000,
      step: 1,
    },
    name: 'heatmapRadius',
  },
  {
    type: 'switch',
    label: '聚合查询',
    value: false,
    isHide: true,
    name: 'isAggr',
    prop: {},
  },
  {
    type: 'select',
    label: '分组字段',
    value: '',
    isHide: true,
    name: 'groupBy',
    optionsSourse: 'columns',
    optionFilter(/* column:KeyValueStore */) {
      return true
    },
    bind: {
      allowClear: true,
    },
    props: {
      allowClear: true,
      options: [],
    },
    groupName: 'sjgl',
  },
  {
    type: 'inputNumber',
    label: 'TopK',
    tip: 'TopK',
    value: 1000,
    name: 'topK',
    required: true,
    bind: {
      max: 9999,
      min: 1,
    },
    groupName: 'sjgl',
  },
]

export const tableConfig = [
  {
    type: 'label',
    label: '表格配置',
    groupName: 'sjgl',
  },
  {
    type: 'text',
    label: '标题内容',
    value: '',
    name: 'title',
    bind: {
      allowClear: true,
    },
    props: {},
  },
  {
    type: 'select',
    label: '数据集',
    value: '',
    name: 'dataId',
    props: {
      options: [],
    },
    groupName: 'sjgl',
  },
  {
    type: 'columnSelector',
    label: '展示字段',
    value: [],
    name: 'column',
    isHide: true,
    domClass: 'column-key',
    optionsSourse: 'columns',
    optionFilter() {
      return true
    },
    bind: {
      class: 'table-columns',
      disabled: false,
      options: [],
    },
    groupName: 'sjgl',
  },
]

/**
pieChart : 'PieChart',
donutChart : 'pieChart',
barChart : 'barChart',
groupBarChart : 'groupBarChart',
stackBarChart : 'groupBarChart',
lineChart : 'lineChart',
scatterplot : 'scatterplot',
areaChart : 'areaChart',
heatmapMatrix : 'heatmapMatrix',
parallelCoordinates : 'parallelCoordinates'
 */

export default function getDataBindSettings(
  chartType: string,
  isAggr?: boolean
): Array<KeyValueStore> {
  if (isAggr) {
    return []
  }
  let result: Array<KeyValueStore>
  switch (chartType) {
    case 'table':
      result = tableConfig
      break
    case ChartDetailsEnum.pieChart:
    case ChartDetailsEnum.donutChart:
    case ChartDetailsEnum.barChart:
    case ChartDetailsEnum.scatterplot:
      result = OneKeyOneValue
      break

    case ChartDetailsEnum.groupBarChart:
    case ChartDetailsEnum.areaChart:
    case ChartDetailsEnum.lineChart:
    case ChartDetailsEnum.stackBarChart:
    case ChartDetailsEnum.parallelCoordinates:
      result = OneKeyMultipleValue
      break
    case ChartDetailsEnum.heatmapMatrix:
      result = MultipleKeyOneValue
      break
    case ChartDetailsEnum.geographicMap:
      result = geographicMapSetting
      break
    default:
      result = []
  }

  return cloneDeep(result)
}

export const dataBindKeys = [
  'labelKey',
  'labelKeyY',
  'valueKey',
  'groupBy',
  'isAggr',
  'dataId',
  'mapName',
  'topK',
  'labelKeyFormat',
  'showNumber',
]

/**
 * 算子图标, 背景色 映射关系， pipeline 节点胶囊共用
 * @author houjinhui
 */

interface KeyValue{
  [key:number] : any
}
// 算子列表 图标  graph 画布算子节点图标
export const AlgTypeEnum: KeyValue =  {
  1 : 'iconjulei-DBSCAN',  // DBSCAN
  2 : 'iconjulei-Kjulei',  // KMEANS
  3 : 'iconxianxinghuigui',  // 线性回归
  4 : 'iconjiangwei-PCA',  //  降维 PCA
  5 : 'iconjiangwei-T-SNE',  // 降维 TSNE
  6 : 'iconjiangwei-LLE',  // 降维 LLE
  7 : 'iconyichangjiance-tongjijiashejianyan',  // 异常检测 LT
  8 : 'iconyichangjiance-LsolationForest',  // 独立森林
  9 : 'iconluojihuigui',  // LOGREGRE
  10 : 'iconpinfanwajue-FP-growth',  // 频繁挖掘 FP_GROWTH
  11 : 'iconpinfanwajue-Prefixspan',  // 频繁挖掘 PREFIX_SPAN
}
// ETL 图标
export const DataIcon = 'iconxitongshujubeifen3'
// ETL 图标
export const EtlIcon = 'iconETL'
// 清洗节点 icon
export const ClearNodeIcon = 'iconqingxi-copy'
// 自定义 icon
export const SelfDefinedIcon = 'iconzidingyi'

// 算子列表分组图标
export const OperatorGroupIconEnum: KeyValue =  {
  0 : 'iconETL' ,  // ETL 操作
  1 : 'iconjulei',  // 聚类
  2 : 'iconhuigui',  // 回归
  3 : 'iconjiangwei',  // 降维
  4 : 'iconyichangjiance',  // 异常检测
  5 : 'iconpinfanmoshiwajue',  //  频繁模式挖掘
  6 : 'iconshoucang',  // 收藏
  7 : 'iconzidingyi',  // 自定义
  8 : 'iconqingxi-copy',  // 清洗
}


// ETL 编辑弹窗分类映射
export const ETLAlgType: KeyValue =  {
  1 : 'etl-ranks-filter',
  2 : 'etl-join',
  3 : 'etl-sampling',
  4 : 'etl-union',
  5 : 'etl-remap',
  6 : 'etl-sql',
}

export const DataIconBackGround = '#a4abfb'

// 算子列表分组图标 背景色
export const OperatorGroupIconBackGround: KeyValue =  {
  0 : {color: '#74a0fa', opacity: 0.8} ,  // ETL 操作
  1 : {color: '#aed495', opacity: 1} ,  // 聚类
  2 : {color: '#f5a4ac', opacity: 1} ,  // 回归
  3 : {color: '#f5a4ac', opacity: 1} ,  // 降维
  4 : {color: '#fbaf85', opacity: 1} ,  // 异常检测
  5 : {color: '#aed495', opacity: 1} ,  //  频繁模式挖掘
  7 : {color: '#a2dced', opacity: 1} ,  // 自定义
  8 : {color: '#74a0fa', opacity: 0.8} ,  // 清洗
}





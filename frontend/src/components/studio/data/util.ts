/* eslint-disable import/no-dynamic-require */
/* eslint-disable global-require */
import { cloneDeep } from 'lodash'

interface KeyValueStore {
  [key: string]: any
}

/**
 * 处理地图数据
 * @param chartData 地图组件配置
 * @param data 图表数据 value
 * @param transformData 地图中需要转换的字段（城市、ip等需要映射成经纬度）
 */
export function dealMapData(
  chartData?: KeyValueStore,
  data?: KeyValueStore[],
  transformData?: KeyValueStore[]
) {
  const { geoTransform } = chartData?.chartOptions
  const { geoCombineField = [] } =
    chartData?.chartOptions.glyphConfig || chartData?.chartOptions // || 后面是兼容老数据
  if (geoCombineField.length > 0) {
    // 需要单独的经度和纬度字段进行组合
    data?.forEach((item) => {
      geoCombineField.forEach((combineField: string[]) => {
        const targetField = combineField[2]
        item[targetField] = `${item[combineField[0]]},${item[combineField[1]]}`
      })
    })
  }
  if (geoTransform && geoTransform.fields.length > 0 && transformData) {
    data?.forEach((item, index: number) => {
      geoTransform.fields.forEach(
        (field: { semanticColumn: any; semantic: string }) => {
          const transformValue = transformData[field.semanticColumn][index]
          item[`$old$-${field.semanticColumn}`] = item[field.semanticColumn] // 记录原数据
          item[field.semanticColumn] =
            transformValue.length === 0 ? null : transformValue // 转换成对应的坐标, 如果是空值，则设置为null(画图不会出错)
        }
      )
    })
  }

  return data
}

/* eslint-disable  @typescript-eslint/no-unused-vars   */
function dealPieChartData(chartData: KeyValueStore, data: KeyValueStore[]) {
  const { formData, chartOptions } = chartData as KeyValueStore
  if (formData.showNumber > 0 && data.length > formData.showNumber) {
    // 饼图有显示阈值的设置
    const newValue = data.slice(0, formData.showNumber)
    newValue[formData.showNumber - 1].groupValue = [
      { ...data[formData.showNumber - 1] },
    ]
    // eslint-disable-next-line no-plusplus
    for (let i = formData.showNumber; i < data.length; i++) {
      newValue[formData.showNumber - 1][chartOptions.valueKey] +=
        data[i][chartOptions.valueKey]
      newValue[formData.showNumber - 1].groupValue.push(data[i])
    }
    newValue[formData.showNumber - 1][chartOptions.labelKey] = '其他'
    return newValue
  }
  return data
}

/**
 * 节点可视化对比
 * @param dataResult
 */
export function dealData(
  dataResult: any,
  chartType?: string,
  chartData?: KeyValueStore
) {
  const data = cloneDeep(dataResult)
  const result = [] as Array<KeyValueStore>
  ;(data.data as Array<Array<any>>).forEach((dataItem: any) => {
    const setItem: KeyValueStore = {}
    data.columns.forEach((column: KeyValueStore, index: number) => {
      const columnQuery = data.columns.find(
        (col: KeyValueStore) => col.text === column.name
      )
      setItem[column.name] =
        columnQuery && columnQuery.isNumber
          ? Number.parseFloat(dataItem[index])
          : dataItem[index]
    })
    result.push({
      ...setItem,
    })
  })
  if (chartType === 'geographicMap') {
    // 地图组件数据需要额外处理
    return dealMapData(chartData, result, data.extraData?.coordinates)
  }

  // if (['donutChart', 'pieChart'].includes(chartType as string)) {
  //   return dealPieChartData(chartData as KeyValueStore, result || [])
  // }
  return result
}

export const chartHeight = 190

/**
 * 判断类型
 * @param ParameterType
 */
export function judgeParameterType(ParameterType: string) {
  let typeBoolean = true
  typeBoolean =
    ParameterType === 'text' ||
    ParameterType === 'char' ||
    ParameterType === 'varchar'
  return typeBoolean
}

/**
 * 添加 编辑 算子 处理参数
 * @param inData
 */
export function dealDataForOperator(inData: any) {
  const data: any = {
    inputParamsObj: {
      input: [],
    },
    outputParamsObj: {
      output: [],
    },
  }
  // 参数
  inData.operatorParameters.forEach((item: any) => {
    if (item.type === 'select' || item.type === 'checkbox') {
      data.inputParamsObj.input.push({
        default: judgeParameterType(item.paramType)
          ? item.default
          : Number(item.default),
        english_name: item.english_name,
        name: item.name,
        paramType: item.paramType,
        type: item.type,
        value: item.value,
        items: item.items.map((option: { value: any }) =>
          judgeParameterType(item.paramType)
            ? option.value
            : Number(option.value)
        ),
      })
    } else {
      data.inputParamsObj.input.push({
        default: item.default,
        english_name: item.english_name,
        name: item.name,
        paramType: item.paramType,
        type: item.type,
        value: item.value,
      })
    }
  })
  // 输出
  inData.outputInterfaces.forEach((item: any) => {
    data.outputParamsObj.output.push({
      meta: item.meta,
      tableName: item.tableName,
    })
  })
  return data
}

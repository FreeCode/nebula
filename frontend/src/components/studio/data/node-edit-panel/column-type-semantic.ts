/**
 * （1）数据类型 menu
 * （2）数据类型 中文描述
 * （3）语义 menu
 * 2021.07.27
 * Hou Jinhui
 */

import { ISemantic } from '@/components/studio/data/node-edit-panel/interface'

/**
 * （1）数据类型修改下拉菜单 menu
 * 与 ./interface.ts 中的 ColumnDataType 对应
 * 使用文件:
 *  components/data/DatasetPreviewTable.vue
 */
export const columnTypeMenu: { [key: string]: any }[] = [
  { value: 'decimal', text: '小数' },
  { value: 'int', text: '整数' },
  { value: 'date', text: '日期' },
  // { value: 'text', text: '文本'},  // 删除
  { value: 'varchar', text: '字符串' },
  { value: 'json', text: '对象' },
  { value: 'array', text: '数组' },
]

/**
 * （2）数据类型中文描述
 *  使用文件:
 *    components/studio/data/node-edit-panel/DataColumn.vue
 *  注：
 *    单独出来， 与 DataColumn.vue 中的 onColumnDataTypeClick()  method 共用
 */
export const columnTypeMapping = {
  decimal: '小数',
  int: '整数',
  // text: '文本',
  date: '日期',
  varchar: '字符串',
  json: '对象',
  array: '数组',
}

// 语义中文描述
export const semanticMapping = {
  coordinate: '经纬度',
  time: '时间',
  city: '城市',
  ip: 'IP',
  country: '国家',
  latitude: '纬度',
  longitude: '经度',
  province: '省份',
  postcode: '邮编',
  url: 'URL',
  httpstatus: 'HTTP状态码',
  email: '邮件地址',
  telephone: '电话',
  id: '身份证',
  passport: '护照',
  disorder: '无序',
  order: '有序',
  null: '无',
}

/**
 * （3）数据语义修改下拉菜单 menu
 * detail 中 与 src/config/contant.ts 对应
 * 使用文件:
 *  components/data/DatasetPreviewTable.vue
 */
export const columnSemanticMenu: ISemantic[] = [
  {
    index: 1,
    name: 'geography',
    icon: 'icondili1',
    text: '地理',
    haveSub: true,
    detail: [
      { value: 'longitude', chineseName: '经度' },
      { value: 'latitude', chineseName: '纬度' },
      { value: 'coordinate', chineseName: '经纬度' },
      { value: 'country', chineseName: '国家' },
      { value: 'province', chineseName: '省份' },
      { value: 'city', chineseName: '城市' },
      { value: 'postcode', chineseName: '邮编' },
    ],
  },
  {
    index: 2,
    name: 'network',
    icon: 'iconwangluo2',
    text: '网络',
    haveSub: true,
    detail: [
      { value: 'ip', chineseName: 'IP' },
      { value: 'url', chineseName: 'URL' },
      { value: 'httpstatus', chineseName: 'HTTP状态码' },
    ],
  },
  {
    index: 3,
    name: 'information',
    icon: 'iconxinxi1',
    text: '信息',
    haveSub: true,
    detail: [
      { value: 'email', chineseName: '邮件地址' },
      { value: 'telephone', chineseName: '电话' },
      { value: 'id', chineseName: '身份证' },
      { value: 'passport', chineseName: '护照' },
    ],
  },
  {
    index: 4,
    name: 'category',
    icon: 'iconleibie1',
    text: '类别',
    haveSub: true,
    detail: [
      { value: 'disorder', chineseName: '无序' },
      { value: 'order', chineseName: '有序' },
    ],
  },
  {
    index: 4,
    name: 'null',
    text: '无',
    haveSub: false,
    detail: [],
  },
]

/**
 * 语义转换映射
 * prd： https://cf.zjvis.org/pages/viewpage.action?pageId=29567573
 */

export const semanticTransformationLimitation = {
  // 地理
  longitude: ['int', 'decimal', 'varchar'], // 经度
  latitude: ['int', 'decimal', 'varchar'], // 纬度
  coordinate: ['varchar'], // 经纬度
  country: ['int', 'varchar'], // 国家
  province: ['int', 'varchar'], // 省份
  city: ['int', 'varchar'], // 城市
  postcode: ['int'], // 邮编
  // 网络
  ip: ['varchar'], // IP
  url: ['varchar'], // URL
  httpstatus: ['int'], // HTTP状态码
  // 信息
  email: ['varchar'], // 邮件地址
  telephone: ['int', 'varchar'], // 电话
  id: ['int', 'varchar'], // 身份证
  passport: ['varchar'], // 护照
  // 类别
  disorder: ['int', 'varchar'], // 无序
  order: ['int', 'varchar'], // 有序
  // 无
  null: ['decimal', 'int', 'date', 'varchar', 'json', 'array'], // 无
}

export const columnTypeForm: { [key: string]: any }[] = [
  { value: 'percentage', text: '百分比' },
  { value: 'scientific', text: '科学计数' },
]

export const columnTypeFormMapping = {
  percentage: '百分比',
  scientific: '科学计数',
}

// type和subtype为-1时，表示未选
export const filterMapType: any = {
  节点类型: { type: 0, subType: 0 },
  边的类型: { type: 1, subType: 1 },
  边的权重范围: { type: 1, subType: 2 },
  仅保留有向边: { type: 1, subType: 3 },
  仅保留无向边: { type: 1, subType: 4 },
  仅保留双向边: { type: 1, subType: 5 },
  移除自环边: { type: 1, subType: 6 },
  等于: { type: 2, subType: 7 },
  范围: { type: 2, subType: 8 },
  非空: { type: 2, subType: 9 },
  类别: { type: 2, subType: 10 },
  分类统计: { type: 2, subType: 11 },
  内部边: { type: 2, subType: 12 },
  外部边: { type: 2, subType: 13 },
  'K-核心': { type: 3, subType: 14 },
  具有自环: { type: 3, subType: 15 },
  度范围: { type: 3, subType: 16 },
  入度范围: { type: 3, subType: 17 },
  出度范围: { type: 3, subType: 18 },
  并集: { type: 4, subType: 19 },
  交集: { type: 4, subType: 20 },
  非: { type: 4, subType: 21 },
}

// 过滤流程数据结构
export interface FilterPipeline {
  // gmtCreator: string,
  // gmtModifier: string,
  // gmtCreate: string,
  // gmtModify: string,
  id: number
  name: string
  projectId: number
  graphId: number
  userId: number
  data: any
}

// 过滤器数据结构
export interface Filter {
  id: number
  name: number
  projectId: number
  graphId: number
  graphFilterPipelineId: number
  type: number
  subType: number
  parentId: number | null
  userId: number
  data: FilterData
}

// 过滤器数据
export interface FilterData {
  output: any[]
  input: any[]
  subTypeName: string
  count?: any
  attr?: string
  setParams: any[]
}

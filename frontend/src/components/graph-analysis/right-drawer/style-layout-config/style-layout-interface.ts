// 图数据节点分类 categories
export interface ICategories {
  attrs?: any[]
  id: any
  keyAttr?: any
  label: any
  labelCfg?: any
  originLabel?: any
  size?: number
  style?: any
  tableInfo?: any
  value?: any
  x?: number | null
  y?: number | null
}

// 属性着色 item
export interface IAttributeColorContentListItem {
  color: string
  count: number
  nodeIds: any[]
  proportion: number
  value: any
}

export interface IComponentConfig {
  type: string
  label: string
  tip?: string
  name: string
  value?: any
  required?: boolean
  props?: object
  onDataChange?: Function
  domClass?: string
  domStyle?: string
}

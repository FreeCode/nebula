/**
 *  graph style/layout
 * @author houjinhui
 */

interface KeyValue {
  [key: string]: any
}

// 力导布局
export const FORCE = 'force'
export const FORCE_ATLAS_2 = 'forceAtlas2'
export const FRUCHTERMAN = 'fruchterman'
// 环形布局
export const CIRCULAR = 'circular'
export const RADIAL = 'radial'
// 随机布局
export const RANDOM = 'random'
// 弧线布局
export const BASIC_ARC = 'basic-arc'
export const CIRCULAR_ARC = 'circular-arc'
//
// 工具 - 旋转
export const ROTATING = 'rotating'
// 工具 - 扩展 / 收缩
export const SCALE = 'scale'
// 工具 - 避免节点重叠
export const NOVERLAP = 'noverlap'
// 工具 - 避免 label 重叠
export const LABEL_ADJUST = 'Label-adjust'

// graph analysis 各个布局对应的参数
// key: {  // key 如果 isG6cfg === true 则为G6
//   paramName: string,   参数名称
//   isG6cfg: boolean,   // 是否是G6参数
//   value: number | string | boolean,  参数值类型
//   min: number,
//   max: number,
//   type: string,  eg: 'number' | 'string' | 'boolean',  // 参数类型
//   show: boolean,   // 是否显示
//   g6LayoutCfg?: string,  // g6 对应的参数
//   key?: string  // 非 g6 参数
//   introduction: string   // 参数简介
// }

export const layoutArray: { [key: string]: any } = {
  1: [
    {
      id: 1,
      name: '基本力导布局',
      imageSrc: '/layout/icon-force.png',
      classId: 1,
      popoverGif: '/layout/force.gif',
    },
    {
      id: 2,
      name: 'Force Atlas 2',
      imageSrc: '/layout/icon-force-atlas-2.png',
      classId: 1,
      popoverGif: '/layout/force-atlas-2.gif',
    },
    {
      id: 3,
      name: 'Fruchterman-Reingold Layout',
      imageSrc: '/layout/icon-fruchterman-reingold.png',
      classId: 1,
      popoverGif: '/layout/fruchterman-reingold.gif',
    },
  ],
  2: [
    {
      id: 4,
      name: '基本环形布局',
      imageSrc: '/layout/icon-circular.png',
      classId: 2,
      popoverGif: '/layout/icon-circular.png',
    },
  ],
  3: [
    {
      id: 5,
      name: '辐射布局',
      imageSrc: '/layout/icon-radial.png',
      classId: 3,
      popoverGif: '/layout/icon-radial.png',
    },
  ],
  4: [
    {
      id: 6,
      name: '随机布局',
      imageSrc: '/layout/icon-random.png',
      classId: 4,
      popoverGif: '/layout/icon-random.png',
    },
  ],
  5: [
    {
      id: 7,
      name: '基本弧线布局',
      imageSrc: '/layout/icon-basic-arc.png',
      classId: 5,
      popoverGif: '/layout/icon-basic-arc.png',
    },
    {
      id: 8,
      name: '环形弧线布局',
      imageSrc: '/layout/icon-circular-arc.png',
      classId: 5,
      popoverGif: '/layout/icon-circular-arc.png',
    },
  ],
  6: [
    {
      id: 9,
      name: '旋转',
      imageSrc: '/layout/icon-rotation-angle.png',
      classId: 6,
      popoverGif: '/layout/icon-rotation-angle.png',
    },
    // 暂时隐藏
    {
      id: 10,
      name: '扩展/收缩',
      imageSrc: '/layout/icon-scale.png',
      classId: 6,
      popoverGif: '/layout/icon-scale.png',
    },
    {
      id: 11,
      name: '避免节点重叠',
      imageSrc: '/layout/icon-prevent-overlap.png',
      classId: 6,
      popoverGif: '/layout/icon-prevent-overlap.png',
    },
    // {
    //   id: 12,
    //   name: '避免标签重叠',
    //   imageSrc: 'iconxianxinghuigui1',
    //   classId: 6,
    //   popoverGif: 'iconxianxinghuigui1',
    // },
  ],
}

export const layoutParameters: any = {
  1: {
    name: 'Force Atlats',
    g6LayoutType: FORCE,
    params: [
      {
        paramName: '节点作用力',
        isG6cfg: true,
        value: 0,
        max: 1000,
        min: -1000,
        type: 'number',
        show: true,
        g6LayoutCfg: 'nodeStrength',
        introduction:
          '正数代表节点之间的引力作用，负数代表节点之间的斥力作用。',
      },
      {
        paramName: '边作用力',
        isG6cfg: true,
        value: 0,
        max: 1,
        min: 0,
        type: 'number',
        show: true,
        g6LayoutCfg: 'edgeStrength',
        introduction: '范围是 0 到 1，默认根据节点的出入度自适应。',
      },
      {
        paramName: '防止重叠的力强度',
        isG6cfg: true,
        value: 1,
        max: 1,
        min: 0,
        type: 'number',
        show: true,
        g6LayoutCfg: 'collideStrength',
        introduction: '防止重叠的力强度，范围 [0, 1]。',
      },
      {
        paramName: '是否防止重叠',
        isG6cfg: true,
        value: true,
        type: 'boolean',
        show: true,
        g6LayoutCfg: 'preventOverlap',
        introduction:
          '是否防止重叠，必须配合下面属性 nodeSize 或节点数据中的 size 属性，只有在数据中设置了 size 或在该布局中配置了与当前图节点大小相同的 nodeSize 值，才能够进行节点重叠的碰撞检测。',
      },
      {
        paramName: '节点边缘间距',
        isG6cfg: true,
        value: 2,
        max: 1000,
        min: 0,
        type: 'number',
        show: true,
        g6LayoutCfg: 'nodeSpacing',
        introduction: '防止重叠时节点边缘间距的最小值。',
      },
      {
        paramName: '迭代收敛阈值',
        isG6cfg: true,
        value: 0.4,
        max: 100,
        min: 0.1,
        type: 'number',
        show: true,
        g6LayoutCfg: 'alpha',
        introduction: '当前的迭代收敛阈值。',
      },
      {
        paramName: '迭代阈值的衰减率',
        isG6cfg: true,
        value: 0.04,
        max: 1,
        min: 0.01,
        type: 'number',
        show: true,
        g6LayoutCfg: 'alphaDecay',
        introduction: '迭代阈值的衰减率。范围 [0, 1]。0.028 对应迭代数为 300。',
      },
      {
        paramName: '停止迭代的阈值',
        isG6cfg: true,
        value: 0.005,
        max: 100,
        min: 0.001,
        type: 'number',
        show: true,
        g6LayoutCfg: 'alphaMin',
        introduction: '停止迭代的阈值。',
      },
    ],
  },
  2: {
    name: 'Force Atlas 2',
    g6LayoutType: FORCE_ATLAS_2,
    params: [
      {
        paramName: '斥力系数',
        isG6cfg: true,
        value: 5,
        max: 1000,
        min: 1,
        type: 'number',
        show: true,
        g6LayoutCfg: 'kr',
        introduction:
          '可用于调整布局的紧凑程度。斥力系数 kr 越大，布局越松散。',
      },
      {
        paramName: '重力系数',
        isG6cfg: true,
        value: 5,
        max: 1000,
        min: 1,
        type: 'number',
        show: true,
        g6LayoutCfg: 'kg',
        introduction: '重力系数 kg 越大，布局越聚集在中心。',
      },
      {
        paramName: '移动速度',
        isG6cfg: true,
        value: 1,
        max: 100,
        min: 0.1,
        type: 'number',
        show: true,
        g6LayoutCfg: 'ks',
        introduction: '控制迭代过程中，节点移动的速度。',
      },
      {
        paramName: '最大迭代次数',
        isG6cfg: true,
        value: 0,
        max: 10000,
        min: 0,
        type: 'number',
        show: true,
        g6LayoutCfg: 'maxIteration',
        introduction: '最大迭代次数，若为 0 则将自动调整。',
      },
      {
        paramName: '收敛震荡容忍度',
        isG6cfg: true,
        value: 0.2,
        max: 100,
        min: 0.1,
        type: 'number',
        show: false,
        g6LayoutCfg: 'tao',
        introduction: '迭代接近收敛时停止震荡的容忍度。',
      },
      {
        paramName: '防止节点重叠',
        isG6cfg: true,
        value: true,
        type: 'boolean',
        show: true,
        g6LayoutCfg: 'preventOverlap',
        introduction:
          '必须配合下面属性 nodeSize，只有设置了与当前图节点大小相同的 nodeSize 值，才能够进行节点重叠的碰撞检测。',
      },
      {
        paramName: '打开 hub 模式',
        isG6cfg: true,
        value: false,
        type: 'boolean',
        show: false,
        g6LayoutCfg: 'dissuadeHubs',
        introduction:
          '是否打开 hub 模式。若为 true，相比与出度大的节点，入度大的节点将会有更高的优先级被放置在中心位置开。',
      },
      {
        paramName: '打开 barnes hut 加速',
        isG6cfg: true,
        value: false,
        type: 'boolean',
        show: false,
        g6LayoutCfg: 'barnesHut',
        introduction:
          '是否打开 barnes hut 加速，即四叉树加速。由于每次迭代需要更新构建四叉树，建议在较大规模图上打开。',
      },
      {
        paramName: '自动剪枝',
        isG6cfg: true,
        value: false,
        type: 'boolean',
        show: true,
        g6LayoutCfg: 'prune',
        introduction:
          '是否开启自动剪枝模式。默认情况下，当节点数量大于 100 时它将会被激活。注意，剪枝能够提高收敛速度，但可能会降低图的布局质量。',
      },
    ],
  },
  3: {
    name: 'Fruchterman-Reingold Layout',
    g6LayoutType: FRUCHTERMAN,
    params: [
      {
        paramName: '最大迭代次数',
        isG6cfg: true,
        value: 300,
        max: 10000,
        min: 1,
        type: 'number',
        show: true,
        g6LayoutCfg: 'maxIteration',
        introduction: '最大迭代次数。',
      },
      {
        paramName: '重力大小',
        isG6cfg: true,
        value: 10,
        max: 1000,
        min: 0,
        type: 'number',
        show: true,
        g6LayoutCfg: 'gravity',
        introduction: '重力的大小，影响布局的紧凑程度。',
      },
      {
        paramName: '移动速度',
        isG6cfg: true,
        value: 5,
        max: 1000,
        min: 1,
        type: 'number',
        show: true,
        g6LayoutCfg: 'speed',
        introduction: '每次迭代节点移动的速度。速度太快可能会导致强烈震荡。',
      },
      // {paramName: '是否按照聚类布局', isG6cfg: true, value: false, type: 'boolean', show: false, g6LayoutCfg: 'clustering', introduction: '是否按照聚类布局。'},
      // {paramName: '聚类重力', isG6cfg: true, value: 10, type: 'number', show: true, g6LayoutCfg: 'clusterGravity', introduction: '聚类内部的重力大小，影响聚类的紧凑程度，在 clustering 为 true 时生效。'},
    ],
  },
  4: {
    name: 'Circular环形布局',
    g6LayoutType: CIRCULAR,
    params: [
      {
        paramName: '圆半径',
        isG6cfg: true,
        value: 0,
        max: 100000,
        min: 0,
        type: 'number',
        show: true,
        g6LayoutCfg: 'radius',
        introduction: '圆的半径， 0表示不生效',
      },
      {
        paramName: '排序的依据',
        isG6cfg: true,
        value: 'degree',
        type: 'string',
        show: true,
        g6LayoutCfg: 'ordering',
        introduction:
          '节点在环上排序的依据。默认 degree 按照度数大小排序。可选项：topology按照拓扑排序；degree按照度数大小排序；null代表直接使用数据中的顺序。',
      },
      {
        paramName: '顺时针排列',
        isG6cfg: true,
        value: true,
        type: 'boolean',
        show: true,
        g6LayoutCfg: 'clockwise',
        introduction: '节点在环上排序是否顺时针排列。',
      },
    ],
  },
  5: {
    name: 'Radial辐射布局',
    g6LayoutType: RADIAL,
    params: [
      {
        paramName: '边长度',
        isG6cfg: true,
        value: 50,
        max: 10000,
        min: 1,
        type: 'number',
        show: true,
        g6LayoutCfg: 'linkDistance',
        introduction: '边长度',
      },
      {
        paramName: '辐射半径差',
        isG6cfg: true,
        value: 100,
        max: 10000,
        min: 1,
        type: 'number',
        show: true,
        g6LayoutCfg: 'unitRadius',
        introduction:
          '每一圈距离上一圈的距离。默认填充整个画布，即根据图的大小决定',
      },
      {
        paramName: '节点边缘间距',
        isG6cfg: true,
        value: 0,
        max: 1000,
        min: 0,
        type: 'number',
        show: true,
        g6LayoutCfg: 'nodeSpacing',
        introduction: '防止重叠时节点边缘间距的最小值。',
      },
      {
        paramName: '防止节点重叠',
        isG6cfg: true,
        value: true,
        type: 'boolean',
        show: true,
        g6LayoutCfg: 'preventOverlap',
        introduction:
          '必须配合下面属性 nodeSize，只有设置了与当前图节点大小相同的 nodeSize 值，才能够进行节点重叠的碰撞检测。',
      },
      {
        paramName: '严格布局',
        isG6cfg: true,
        value: false,
        type: 'boolean',
        show: true,
        g6LayoutCfg: 'strictRadial',
        introduction:
          '是否必须是严格的 radial 布局，及每一层的节点严格布局在一个环上。开启防止节点重叠时生效。',
      },
      // maxPreventOverlapIteration
    ],
  },
  6: {
    name: '随机布局',
    g6LayoutType: RANDOM,
    params: [],
  },
  // g6 中没有 弧线布局 需要手动实现
  7: {
    name: '基本弧线布局',
    g6LayoutType: BASIC_ARC,
    params: [
      // {
      //   paramName: '节点边缘间距',
      //   isG6cfg: true,
      //   value: 3,
      //   type: 'number',
      //   show: true,
      //   g6LayoutCfg: 'nodeSpacing',
      //   introduction: '防止重叠时节点边缘间距的最小值。',
      // },
    ],
  },
  8: {
    name: '环形弧线布局',
    g6LayoutType: CIRCULAR_ARC,
    params: [
      // {
      //   paramName: '半径自适应',
      //   isG6cfg: true,
      //   value: true,
      //   type: 'boolean',
      //   g6LayoutCfg: 'isAutoRadius',
      //   show: true,
      //   introduction: '根据画布尺寸自适应圆的半径',
      // },
      {
        paramName: '圆半径',
        isG6cfg: true,
        value: 0,
        max: 100000,
        min: 0,
        type: 'number',
        show: true,
        g6LayoutCfg: 'radius',
        introduction: '圆的半径，0表示不生效',
      },
      {
        paramName: '排序的依据',
        isG6cfg: true,
        value: 'degree',
        type: 'string',
        show: true,
        g6LayoutCfg: 'ordering',
        introduction:
          '节点在环上排序的依据。默认 degree 按照度数大小排序。可选项：topology按照拓扑排序；degree按照度数大小排序；null代表直接使用数据中的顺序。',
      },
    ],
  },
  9: {
    name: '旋转',
    g6LayoutType: ROTATING,
    params: [
      {
        paramName: '旋转角度',
        isG6cfg: true,
        value: 90,
        max: 360,
        min: 0,
        type: 'number',
        g6LayoutCfg: 'angle',
        show: true,
        introduction: '根据图中心旋转的角度。当值为空时默认值: 90',
      },
    ],
  },
  10: {
    name: '扩展/收缩',
    g6LayoutType: SCALE,
    params: [
      {
        paramName: '比例',
        isG6cfg: true,
        value: 1.1,
        max: 1000,
        min: 0.001,
        type: 'number',
        g6LayoutCfg: 'scale',
        show: true,
        introduction:
          '取值范围[0.001, 1000], (1, 100] 代表扩展作用，（0.001，1）代表收缩作用。',
      },
    ],
  },
  11: {
    name: '避免节点重叠',
    g6LayoutType: NOVERLAP,
    params: [],
  },
  // 12: {
  //   name: '避免标签重叠',
  //   g6LayoutType: LABEL_ADJUST,
  //   params: [],
  // },
}

// 排序设置初始色阶  -- 基础色为 '#1971b8'
export const initGradientColor: string[] = [
  '#e3dcff',
  '#d6caff',
  '#c8b9ff',
  '#bba8ff',
  '#ad96ff',
  '#9f85ff',
  '#9273ff',
  '#8462ff',
]

// 系统默认颜色  -- 属性着色
export const systemDefaultColor: string[] = [
  // '#99a3f4', 目前分析节点默认色
  '#a4abfb',
  '#f5a4ac',
  '#a2dced',
  '#f9d26f',
  '#73a0fa',
  '#aed495',
  '#6e80a2',
  '#ddb6ff',
  '#fdb69e',
  '#ababab',
  '#16a37e',
  '#7751ff',
]

// 图分析 节点可选颜色
export const nodeColorSelection: string[] = [
  '#a4abfb',
  '#f5a4ac',
  '#a2dced',
  '#f9d26f',
  '#73a0fa',
  '#aed495',
  '#6e80a2',
  '#ddb6ff',
  '#fdb69e',
  '#ababab',
  '#16a37e',
  '#7751ff',
]

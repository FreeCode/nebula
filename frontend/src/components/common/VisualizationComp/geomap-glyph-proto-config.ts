/**
 * geomap glyph的配置
 * 不同于其他的chart机制，单独放
 */

interface IKeyValue {
  [key: string]: any
}

const commonProtoConfig = [
  {
    type: 'input',
    label: '标题',
    name: 'mapTitle',
  },
  {
    type: 'geoMapThemeSelect',
    label: '主题色',
    name: 'mapTheme',
  },
  {
    type: 'switch',
    label: '记住当前中心',
    name: 'rememberMapCenter',
    defaultValue: false,
  },
]

// 根据地图上的glyph不同，区分不同的配置
const glyphProtoConfig: IKeyValue = {
  point: (isRange = false) => {
    return [
      {
        type: 'slider',
        label: '点半径',
        name: 'pointRadius',
        defaultValue: isRange ? [3, 5] : 4,
        props: {
          min: 1,
          max: 20,
          range: isRange,
        },
      },
      {
        type: 'colorPicker',
        label: '点的颜色',
        name: 'pointColor',
        defaultValue: '#ff0000',
      },
      ...commonProtoConfig,
    ]
  },
  link: (isRange = false) => {
    return [
      // polygon 目前只用于画线
      {
        type: 'slider',
        label: '线条宽度',
        name: 'lineWidth',
        defaultValue: isRange ? [1, 5] : 1,
        props: {
          min: 1,
          max: 10,
          range: isRange,
        },
      },
      {
        type: 'slider',
        label: '线条弧度',
        name: 'lineCurverness',
        defaultValue: 0,
        props: {
          min: -10,
          max: 10,
          step: 1,
        },
      },
      {
        type: 'colorPicker',
        label: '线条颜色',
        name: 'lineColor',
        defaultValue: '#ff0000',
      },
      ...commonProtoConfig,
    ]
  },
  world: [
    {
      type: 'select',
      label: '图例位置',
      name: 'legendPosition',
      default: 'null',
      props: {
        options: [
          {
            label: '隐藏',
            value: 'null',
          },
          {
            label: '上',
            value: 'top',
          },
          {
            label: '右',
            value: 'right',
          },
          {
            label: '下',
            value: 'bottom',
          },
          {
            label: '左',
            value: 'left',
          },
        ],
      },
    },
    {
      type: 'colorPicker',
      label: '填充颜色',
      name: 'fillColor',
      defaultValue: '#ff0000',
    },
    {
      type: 'colorPicker',
      label: '边框颜色',
      name: 'borderColor',
      defaultValue: '#ffffff',
    },
    {
      type: 'input',
      label: '截断值',
      name: 'valueSeparator',
      defaultValue: '[]',
    },
    ...commonProtoConfig,
  ],
  'multi-subchart': [
    {
      type: 'select',
      label: '图例位置',
      name: 'legendPosition',
      default: 'null',
      props: {
        options: [
          {
            label: '隐藏',
            value: 'null',
          },
          {
            label: '上',
            value: 'top',
          },
          {
            label: '右',
            value: 'right',
          },
          {
            label: '下',
            value: 'bottom',
          },
          {
            label: '左',
            value: 'left',
          },
        ],
      },
    },
    ...commonProtoConfig,
  ],
}

export default glyphProtoConfig

import { THEME_COLORS } from '@/components/common/VisualizationComp/constants'
import ChartProtoConfig from '@/components/common/VisualizationComp/proto-config'
import { getDefaultValueFromProto } from '@/components/common/VisualizationComp/ChartConfig/common'
import { parseSettingToChartOption } from '@/components/common/VisualizationDashboard/util'

// const commonDefultOptions = {
//   titleIsShow: true,
//   axisGridIsShow: false,
//   xAxisRenderMode: 'all', //
//   yAxisRenderMode: 'all',
//   xAxisGridIsShow: false,
//   yAxisGridIsShow: false,
//   tooltipIsShow: true,
// }

// const barChartDefaultOptions = {
//   ...commonDefultOptions,
// }

// const pieChartDefaultOptions = {
//   visibleItemsNumber: 16,
// }

// export default {
//   barChart: barChartDefaultOptions,
//   pieChart: pieChartDefaultOptions,
// } as {
//   [key: string]: any
// }

/**
 * 详细版本的默认配置，默认主要素都展示齐全
 */
export const detailDefaultOptions = {
  title: '',
  titleIsShow: true,
  xAxisIsShow: true,
  yAxisIsShow: true,
  axisGridIsShow: true,
  xAxisGridIsShow: true,
  yAxisGridIsShow: true,
  visibleItemsNumber: 16,
  tooltipIsShow: true,
  colors: THEME_COLORS[0].value,
  xAxisGridStyle: 'solid',
  yAxisGridStyle: 'solid',
  xAxisTickIsShow: false,
  yAxisTickIsShow: false,
  xAxisTitleIsShow: true,
  xAxisTitle: '',
  yAxisTitleIsShow: true,
  yAxisTitle: '',
  legendPosition: 'right',
  labelIsShow: false,
  interactions: [{ type: 'element-single-selected' }],
  maxBarWidth: 28,
  // axisLayout: 'horizontal',
  // axisScaleType: 'linear',
  // lineSmooth: false,
}

/**
 * 简版图表的默认配置，适用于小区域展示
 * 无坐标轴标题、无网格线展示等
 */
export const simpleCommonDefultOptions = {
  titleIsShow: true,
  xAxisIsShow: false,
  yAxisIsShow: false,
  axisGridIsShow: false,
  xAxisGridIsShow: false,
  yAxisGridIsShow: false,
  visibleItemsNumber: 16,
}

/*
 * 根据chartType获取默认配置
 */
export const getDetailDefaultOptionsByChartType = (
  chartType: string | Array<string>
) => {
  const chartTypeString = Array.isArray(chartType) ? chartType[1] : chartType // 兼容性获取chartType

  let options = { ...detailDefaultOptions }
  if (ChartProtoConfig[chartTypeString]) {
    options = {
      ...options,
      ...getDefaultValueFromProto(ChartProtoConfig[chartTypeString]),
    }
  }
  return parseSettingToChartOption(options)
}

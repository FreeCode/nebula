/**
 * 共同的配置
 */

import { THEME_COLORS } from '@/components/common/VisualizationComp/constants'
/**
 * 标题panel下的配置
 */
const titleConfig = [
  {
    type: 'input',
    label: '图表标题',
    value: '',
    name: 'title',
    props: {
      placeholder: '请填写标题内容',
      maxLength: 50,
    },
    bind: {
      style: {
        // width: '150px'
      },
    },
  },
]

/**
 * 图例panel下的配置
 */
const legendConfig = [
  // {
  //   type: 'switch',
  //   label: '开启图例',
  //   name: 'legendIsShow',
  //   defaultValue: true,
  //   props: {
  //     placeholder: '请填写标题内容',
  //     maxLength: 150,
  //   },
  //   bind: {
  //     style: {
  //       // width: '150px'
  //     },
  //   },
  // },
  {
    type: 'select',
    label: '图例',
    name: 'legendPosition',
    defaultValue: 'null',
    props: {
      options: [
        {
          label: '隐藏',
          value: 'null',
        },
        {
          label: '上',
          value: 'top',
        },
        {
          label: '右',
          value: 'right',
        },
        {
          label: '下',
          value: 'bottom',
        },
        {
          label: '左',
          value: 'left',
        },
      ],
    },
  },
]
const rightLegendConfig = legendConfig.map((c: any) => ({
  ...c,
  defaultValue: 'right',
}))

const xAxis = [
  {
    type: 'group',
    label: 'X轴',
    children: [
      {
        type: 'input',
        label: '标题',
        name: 'xAxisTitle',
      },
      {
        type: 'select',
        label: '网格线',
        name: 'xAxisGridStyle',
        defaultValue: 'null',
        props: {
          options: [
            {
              label: '无',
              value: 'null',
            },
            {
              label: '实线',
              value: 'solid',
            },
            {
              label: '虚线',
              value: 'dash',
            },
            {
              label: '点',
              value: 'dot',
            },
          ],
        },
      },
      {
        type: 'switch',
        label: '刻度',
        name: 'xAxisTickIsShow',
        defaultValue: false,
      },
      {
        type: 'switch',
        label: '标签旋转',
        name: 'xAxisLabelIsRotate',
        defaultValue: false,
      },
    ],
  },
]

const yAxis = [
  {
    type: 'group',
    label: 'Y轴',
    children: [
      {
        type: 'input',
        label: '标题',
        name: 'yAxisTitle',
      },
      {
        type: 'select',
        label: '网格线',
        name: 'yAxisGridStyle',
        defaultValue: 'null',
        props: {
          options: [
            {
              label: '无',
              value: 'null',
            },
            {
              label: '实线',
              value: 'solid',
            },
            {
              label: '虚线',
              value: 'dash',
            },
            {
              label: '点',
              value: 'dot',
            },
          ],
        },
      },
      {
        type: 'inputNumber',
        label: '最大边界',
        name: 'yAxisMax',
        defaultValue: '',
      },
      {
        type: 'inputNumber',
        label: '最小边界',
        name: 'yAxisMin',
        defaultValue: '',
      },
      {
        type: 'switch',
        label: '刻度',
        name: 'yAxisTickIsShow',
        defaultValue: false,
      },
    ],
  },
]

const labelConfig = [
  {
    type: 'switch-group',
    label: '数据标签',
    name: 'labelIsShow',
    defaultValue: false,
    children: [
      {
        type: 'inputNumber',
        label: '小数位数',
        name: 'labelPrecision',
        defaultValue: 2,
        props: {
          min: 0,
          max: 9,
        },
      },
      {
        type: 'switch',
        label: '百分比',
        name: 'labelIsPercent',
        defaultValue: false,
      },
    ],
  },
]

// const xAxisTitle = [
//   {
//     type: 'input',
//     label: 'X轴标题',
//     name: 'xAxisTitle',
//   },
// ]

// const yAxisTitle = [
//   {
//     type: 'input',
//     label: 'Y轴标题',
//     name: 'yAxisTitle',
//   },
// ]

// const xAxisTick = [
//   {
//     type: 'switch',
//     label: 'X轴刻度',
//     name: 'xAxisTickIsShow',
//     defaultValue: false,
//   },
// ]

// const yAxisTick = [
//   {
//     type: 'switch',
//     label: 'Y轴刻度',
//     name: 'yAxisTickIsShow',
//     defaultValue: false,
//   },
// ]

// const gridLayoutConfig = [
//   {
//     type: 'switch',
//     label: 'X轴网格线',
//     name: 'xAxisGridIsShow',
//     defaultValue: false,
//   },
//   {
//     type: 'switch',
//     label: 'Y轴网格线',
//     name: 'yAxisGridIsShow',
//     defaultValue: false,
//   },
//   {
//     type: 'select',
//     label: '网格线样式',
//     name: 'AxisGridStyle',
//     defaultValue: 'solid',
//     props: {
//       options: [
//         {
//           label: '实线',
//           value: 'solid',
//         },
//         {
//           label: '虚线',
//           value: 'dash',
//         },
//         {
//           label: '点',
//           value: 'dot',
//         },
//       ],
//     },
//   }
// ]

const legendBasicColorConfig = [
  {
    type: 'themeColorPicker',
    label: '基础颜色',
    name: 'colors',
    defaultValue: THEME_COLORS[0].value,
    props: {
      options: THEME_COLORS,
    },
  },
]

const themeColorAllConfig = [
  // 全部颜色
  {
    type: 'themeColorPicker',
    label: '主题色',
    name: 'colors',
    defaultValue: THEME_COLORS[0].value,
    props: {
      options: THEME_COLORS,
    },
  },
]

const themeColorDiscreteConfig = [
  // 离散数值适用的颜色（颜色对比大）
  {
    type: 'themeColorPicker',
    label: '主题色',
    name: 'colors',
    defaultValue: THEME_COLORS.filter((item) => item.type === 'discrete')[0]
      .value,
    props: {
      options: THEME_COLORS.filter((item) => item.type === 'discrete'),
    },
  },
]

const themeColorSequentialConfig = [
  // 连续数值适用的颜色（颜色是同色系由浅到深）
  {
    type: 'themeColorPicker',
    label: '主题色',
    name: 'colors',
    defaultValue: THEME_COLORS.filter((item) => item.type === 'sequential')[0]
      .value,
    props: {
      options: THEME_COLORS.filter((item) => item.type === 'sequential'),
    },
  },
]

const tooltipConfig = [
  {
    type: 'group',
    label: 'tooltip提示',
    children: [
      {
        type: 'switch',
        label: '开启',
        name: 'tooltipIsShow',
        defaultValue: true,
      },
      {
        type: 'switch',
        label: '百分比',
        name: 'tooltipIsPercent',
        defaultValue: false,
      },
      {
        type: 'inputNumber',
        label: '小数位数',
        name: 'tooltipPrecision',
        defaultValue: 2,
        props: {
          min: 0,
          max: 9,
        },
      },
    ],
  },
]

const scaleConfig = [
  {
    type: 'iconRadio',
    label: '比例尺',
    defaultValue: 'linear',
    name: 'axisScaleType',
    props: {
      optionType: 'qualitative',
      optionStyle: {
        wrapper: {
          width: '80px',
          height: '65px',
        },
        item: {
          width: '32px',
          height: '32px',
          'padding-left': '2.56px',
        },
        icon: {
          width: '24.89px',
          height: '24.89px',
          top: '-1px',
        },
      },
      default: 0,
      options: [
        {
          text: 'linear',
          name: '线性',
          type: 'qualitative',
          value: 'linear',
          imgSrc: '/icon/linear-proportion.svg',
        },
        {
          text: 'log',
          name: 'Log变换',
          type: 'qualitative',
          value: 'log',
          imgSrc: '/icon/log-proportion.svg',
        },
      ],
    },
  },
]

const lineSmoothConfig = [
  {
    type: 'switch',
    label: 'lineSmooth',
    name: 'lineSmooth',
    optionType: 'qualitative',
    defaultValue: false,
    optionStyle: {
      wrapper: {
        width: '73px',
        height: '65px',
      },
      item: {
        width: '32px',
        height: '32px',
        'padding-left': '2.56px',
      },
      icon: {
        width: '24.89px',
        height: '24.89px',
        top: '-1px',
      },
    },
    default: 1,
    options: [
      {
        text: 'curve',
        name: '曲线',
        type: 'qualitative',
        value: true,
        imgSrc: '/icon/curve.svg',
      },
      {
        text: 'polyline',
        name: '折线',
        type: 'qualitative',
        value: false,
        imgSrc: '/icon/polyline.svg',
      },
    ],
  },
]

const layoutConfig = [
  {
    type: 'select',
    label: '布局方向',
    name: 'axisLayout',
    defaultValue: 'horizontal',
    props: {
      options: [
        {
          label: '水平方向',
          value: 'horizontal',
        },
        {
          label: '垂直方向',
          value: 'vertical',
        },
      ],
    },
  },
]

export const commonProtoConfig = {
  titleConfig,
  legendConfig,
  rightLegendConfig,
  // gridLayoutConfig,
  tooltipConfig,
  xAxis,
  yAxis,
  labelConfig,
  layoutConfig,
  lineSmoothConfig,
  scaleConfig,
  themeColorAllConfig,
  themeColorDiscreteConfig,
  themeColorSequentialConfig,
  legendBasicColorConfig,
}

export const defaultChartOptions = {
  titleIsShow: true,
  axisGridIsShow: false,
  xAxisRenderMode: 'all', //
  yAxisRenderMode: 'all',
  xAxisGridIsShow: false,
  yAxisGridIsShow: false,
  tooltipIsShow: true,
}

/**
 * 从 proto config 中提取defaultValue
 * @param panelList proto config 配置
 * @returns {key: value}
 */
export function getDefaultValueFromProto(panelList: any[]) {
  // eslint-disable-next-line consistent-return,unicorn/consistent-function-scoping
  const getDefault = (config: any): any => {
    if (config.defaultValue !== undefined) {
      return config.defaultValue
    }
    if (config.type === 'switch' || config.type === 'switch-group') {
      return config.options[config.default].value
    }
  }

  const data: { [key: string]: any } = {}
  panelList.forEach((panel) => {
    panel.children.forEach((config: { [key: string]: any }) => {
      if (config.type === 'group') {
        config.children.forEach((childConfig: { [key: string]: any }) => {
          const defaultValue = getDefault(childConfig)
          if (defaultValue !== undefined) {
            data[childConfig.name] = defaultValue
          }
        })
      } else if (config.type === 'switch-group') {
        const switchDefaultValue = getDefault(config)
        if (switchDefaultValue !== undefined) {
          data[config.name] = switchDefaultValue
        }
        config.children.forEach((childConfig: { [key: string]: any }) => {
          const defaultValue = getDefault(childConfig)
          if (defaultValue !== undefined) {
            data[childConfig.name] = defaultValue
          }
        })
      } else {
        const defaultValue = getDefault(config)
        if (defaultValue !== undefined) {
          data[config.name] = defaultValue
        }
      }
    })
  })
  return data
}

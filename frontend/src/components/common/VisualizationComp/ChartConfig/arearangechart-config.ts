/**
 * 根据共同的配置修改或者直接自定义个性化的配置
 */

import {
  commonProtoConfig,
  defaultChartOptions,
} from '@/components/common/VisualizationComp/ChartConfig/common'
import { defaultColors } from '@/components/common/VisualizationComp/constants'

const {
  titleConfig,
  tooltipConfig,
  rightLegendConfig,
  xAxis,
  yAxis,
  labelConfig,
  themeColorDiscreteConfig,
  lineSmoothConfig,
  scaleConfig,
} = commonProtoConfig

const confidenceSecondColorConfig = [
  // 面积颜色
  {
    type: 'colorPicker',
    label: '区间颜色',
    name: 'secondColor',
    defaultValue: defaultColors[0],
  },
]

const realChartTypeConfig = [
  {
    type: 'select',
    label: '样式',
    name: 'realChartType',
    defaultValue: 'AreaRangeChart',
    props: {
      options: [
        {
          label: '面积',
          value: 'AreaRangeChart',
        },
        {
          label: '误差棒',
          value: 'BoxLineChart',
        },
      ],
    },
  },
]

const areaRangeLabelConfig = [
  {
    ...labelConfig[0],
    children: [
      {
        type: 'select',
        label: '标签类型',
        name: 'labelShowChartType',
        defaultValue: [],
        props: {
          mode: 'multiple',
          options: [
            {
              label: '折线信息',
              value: 'main',
            },
            {
              label: '区域信息',
              value: 'second',
            },
          ],
        },
      },
      ...labelConfig[0].children,
    ],
  },
]

export const protoConfig = [
  {
    label: '',
    type: 'property',
    children: [
      ...titleConfig,
      ...xAxis,
      ...yAxis,
      ...areaRangeLabelConfig,
      ...rightLegendConfig,
      ...tooltipConfig,
      ...lineSmoothConfig,
      ...scaleConfig,
      ...themeColorDiscreteConfig,
      ...realChartTypeConfig,
      ...confidenceSecondColorConfig,
    ],
  },
]
/**
 * 图表渲染的默认配置
 */
export const defaultOptions = {
  ...defaultChartOptions,
}

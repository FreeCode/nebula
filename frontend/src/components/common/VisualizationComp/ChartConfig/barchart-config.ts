/**
 * 根据共同的配置修改或者直接自定义个性化的配置
 */

import {
  commonProtoConfig,
  defaultChartOptions,
} from '@/components/common/VisualizationComp/ChartConfig/common'

const {
  titleConfig,
  // legendConfig,
  tooltipConfig,
  xAxis,
  yAxis,
  labelConfig,
  layoutConfig,
  themeColorDiscreteConfig,
} = commonProtoConfig

const barLabelConfig = [
  {
    ...labelConfig[0],
    children: [
      {
        type: 'select',
        label: '标签位置',
        name: 'labelPosition',
        defaultValue: 'middle',
        props: {
          options: [
            {
              label: '上',
              value: 'top',
            },
            {
              label: '中',
              value: 'middle',
            },
            {
              label: '下',
              value: 'bottom',
            },
          ],
        },
      },
      ...labelConfig[0].children,
    ],
  },
]

export const barProtoConfig = {
  labelConfig: barLabelConfig,
}

export const protoConfig = [
  {
    label: '',
    type: 'property',
    children: [
      ...titleConfig,
      ...xAxis,
      ...yAxis,
      ...barLabelConfig,
      {
        type: 'switch',
        label: '显示Bar差值',
        name: 'diffBar',
        defaultValue: false,
      },
      // ...legendConfig,
      ...tooltipConfig,
      ...layoutConfig,
      ...themeColorDiscreteConfig,
    ],
  },
]
/**
 * 图表渲染的默认配置
 */
export const defaultOptions = {
  ...defaultChartOptions,
}

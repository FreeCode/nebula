/**
 * 根据共同的配置修改或者直接自定义个性化的配置
 */

import {
  commonProtoConfig,
  defaultChartOptions,
} from '@/components/common/VisualizationComp/ChartConfig/common'
import { pieProtoConfig } from '@/components/common/VisualizationComp/ChartConfig/piechart-config'

const {
  titleConfig,
  rightLegendConfig,
  tooltipConfig,
  themeColorDiscreteConfig,
} = commonProtoConfig

const { labelConfig } = pieProtoConfig

export const protoConfig = [
  {
    type: 'fetch', // fetch 属于和获取数据挂钩的属性
    children: [
      {
        type: 'inputNumber',
        label: 'topN',
        name: 'topN',
      },
    ],
  },
  {
    label: '',
    type: 'property',
    children: [
      {
        type: 'inputNumber',
        label: '最多块数',
        name: 'visibleItemsNumber',
      },
      ...titleConfig,
      ...labelConfig,
      ...rightLegendConfig,
      ...tooltipConfig,
      ...themeColorDiscreteConfig,
    ],
  },
]
/**
 * 图表渲染的默认配置
 */
export const defaultOptions = {
  ...defaultChartOptions,
}

const userGuideTextMap: { [key: number]: any } = {
  1: {
    title: '数据导入(1/4)',
    introduced: '点击创建数据分类',
  },
  2: {
    title: '数据导入(2/4)',
    introduced: '点击导入数据',
  },
  3: {
    title: '数据导入(3/4)',
    introduced: '选择导入数据的文件类型',
  },
  4: {
    title: '数据导入(4/4)',
    introduced: '点击确定，完成数据导入',
  },
  5: {
    title: '点击项目管理，开始项目',
    introduced: '创建教程',
  },
  6: {
    title: '项目创建(1/4)',
    introduced: '点击新建项目',
  },
  7: {
    title: '项目创建(2/4)',
    introduced: '点击进入项目，开始分析',
  },
  8: {
    title: '项目创建(3/4)',
    introduced: '加载刚导入的数据',
  },
  9: {
    title: '项目创建(4/4)',
    introduced: '将数据拖入画布，开始数据清洗',
  },
}

export default userGuideTextMap

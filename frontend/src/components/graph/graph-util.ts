/**
 *  graph 网络构建 节点 边 设置颜色
 * @author houjinhui
 */

interface KeyValue {
  [key: string]: any
}

// 设置颜色集合
export const SwitchColor: KeyValue = [
  { color: '#a4abfb', border: '#8690fb' },
  { color: '#8fb2fa', border: '#6194e9' },
  { color: '#a2dced', border: '#78bfcf' },
  { color: '#aed495', border: '#85c16d' },
  { color: '#fbaf85', border: '#e9996d' },
  { color: '#f5a4ac', border: '#e6838f' },
  { color: '#ddb6ff', border: '#c099d3' },
  { color: '#bdd3fb', border: '#8cafe4' },
  { color: '#bfe8ef', border: '#91cbd2' },
  { color: '#c2edad', border: '#93d383' },
  { color: '#ffd985', border: '#e9bf55' },
  { color: '#ffc5ac', border: '#f2a07d' },
  { color: '#7751ff', border: '#4713c3' },
  { color: '#3464ff', border: '#1b43a0' },
  { color: '#5ab7da', border: '#0a91ae' },
  { color: '#16a37e', border: '#248169' },
  { color: '#f3832a', border: '#cb6d15' },
  { color: '#f3470d', border: '#b12d10' },
  { color: '#ececec', border: '#d9d9d9' },
  { color: '#d2d2d2', border: '#979797' },
  { color: '#ababab', border: '#979797' },
  { color: '#818181', border: '#ababab' },
  { color: '#5b5b5b', border: '#979797' },
  { color: '#000000', border: '#979797' },
]
// 数据 图标
export const colors: KeyValue = [
  { color: '#a4abfb', isInUse: false },
  { color: '#73a0fa', isInUse: false },
  { color: '#a2dced', isInUse: false },
  { color: '#aed495', isInUse: false },
  { color: '#fbaf85', isInUse: false },
  { color: '#f5a4ac', isInUse: false },
  { color: '#ddb6ff', isInUse: false },
  { color: '#bdd3fb', isInUse: false },
  { color: '#bfe8ef', isInUse: false },
  { color: '#c2edad', isInUse: false },
  { color: '#ffd985', isInUse: false },
  { color: '#ffc5ac', isInUse: false },
  { color: '#7751ff', isInUse: false },
  { color: '#3464ff', isInUse: false },
  { color: '#5ab7da', isInUse: false },
  { color: '#16a37e', isInUse: false },
  { color: '#f3832a', isInUse: false },
  { color: '#f3470d', isInUse: false },
  { color: '#ececec', isInUse: false },
  { color: '#d2d2d2', isInUse: false },
  { color: '#ababab', isInUse: false },
  { color: '#818181', isInUse: false },
  { color: '#5b5b5b', isInUse: false },
  { color: '#000000', isInUse: false },
]

// graph 社团检测
export const graphHullColorScheme: KeyValue = [
  { fill: '#a4abfb', stroke: '#8690fb' },
  { fill: '#8fb2fa', stroke: '#6194e9' },
  { fill: '#a2dced', stroke: '#78bfcf' },
  { fill: '#aed495', stroke: '#85c16d' },
  { fill: '#fbaf85', stroke: '#e9996d' },
  { fill: '#f5a4ac', stroke: '#e6838f' },

  { fill: '#ddb6ff', stroke: '#c099d3' },
  { fill: '#bdd3fb', stroke: '#8cafe4' },
  { fill: '#bfe8ef', stroke: '#91cbd2' },
  { fill: '#c2edad', stroke: '#93d383' },
  { fill: '#ffd985', stroke: '#e9bf55' },
  { fill: '#ffc5ac', stroke: '#f2a07d' },

  { fill: '#7751ff', stroke: '#4713c3' },
  { fill: '#3464ff', stroke: '#1b43a0' },
  { fill: '#5ab7da', stroke: '#0a91ae' },
  { fill: '#16a37e', stroke: '#248169' },
  { fill: '#f3832a', stroke: '#cb6d15' },
  { fill: '#f3470d', stroke: '#b12d10' },
]

// 对比两个对象 返回值 true/false
export function isObjectValueEqual(a: any, b: any) {
  return JSON.stringify(a) === JSON.stringify(b)
}

// hex颜色转rgb颜色
function HexToRgb(color: string) {
  color = color.replace('#', '') // replace替换查找的到的字符串
  const hxs: any = color.match(/../g) // match得到查询数组
  for (let i = 0; i < 3; i += 1) {
    hxs[i] = Number.parseInt(hxs[i], 16)
  }
  return hxs
}
// GRB颜色转Hex颜色
function RgbToHex(a: number, b: number, c: number) {
  const hexes = [a.toString(16), b.toString(16), c.toString(16)]
  for (let i = 0; i < 3; i += 1) {
    if (hexes[i].length === 1) {
      hexes[i] = `0${hexes[i]}`
    }
  }
  return `#${hexes.join('')}`
}
// 得到hex颜色值为color的加深颜色值，level为加深的程度，限0-1之间
export function getDarkColor(color: string, level: number) {
  const rgba = HexToRgb(color)
  for (let i = 0; i < 3; i += 1) {
    rgba[i] = Math.floor(rgba[i] * (1 - level))
  } // floor 向下取整
  return RgbToHex(rgba[0], rgba[1], rgba[2])
}
// 得到hex颜色值为color的减淡颜色值，level为减浅的程度，限0-1之间
export function getLightColor(color: string, level: number) {
  const rgba = HexToRgb(color)
  for (let i = 0; i < 3; i += 1) {
    rgba[i] = Math.floor((255 - rgba[i]) * level + rgba[i])
  }
  return RgbToHex(rgba[0], rgba[1], rgba[2])
}

// 得到hex颜色值为color的减淡颜色值，限0-1之间， level为以基础色 color 相对深浅度， 1 为 原色, 0 为 白色  (图分析使用)
export function getLightColorFromBasedColor(color: string, level: number) {
  const rgba = HexToRgb(color)
  for (let i = 0; i < 3; i += 1) {
    rgba[i] = Math.floor((255 - rgba[i]) * (1 - level) + rgba[i])
  }
  return RgbToHex(rgba[0], rgba[1], rgba[2])
}

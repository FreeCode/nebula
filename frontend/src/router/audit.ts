/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { RouteConfig } from 'vue-router'

// 作为 home 下级路由
const AUDIT_ROUTE_CONFIG: RouteConfig = {
  path: 'audit',
  name: 'Audit',
  redirect: 'audit/user-info',
  component: () =>
    import(
      /* webpackChunkName: "studio-view" */ '../views/audit/AuditView.vue'
    ),
  children: [
    {
      path: 'record',
      name: '审计数据',
      component: () =>
        import(
          /* webpackChunkName: "data-view" */ '../views/audit/AuditData.vue'
        ),
    },
    {
      path: 'user-info',
      name: '用户列表',
      component: () => import('../views/audit/UserInfo.vue'),
    },
    {
      path: 'user-dataset-info',
      name: '用户数据',
      component: () => import('../views/audit/UserDatasetInfo.vue'),
    },
    {
      path: 'task-info',
      name: '任务监测',
      component: () => import('../views/audit/TaskMonitor.vue'),
    },
    {
      path: 'visit-history',
      name: '访问历史',
      component: () => import('../views/audit/VisitHistory.vue'),
    },
    {
      path: 'service',
      name: '客服服务',
      component: () => import('../views/audit/AuditService.vue'),
    },
  ],
}

export default AUDIT_ROUTE_CONFIG

import { RouteConfig } from 'vue-router'

const PROJECTS_ROUTE_CONFIG: RouteConfig = {
  path: '/management/project/:id',
  redirect: '/management/project/:id/data',
  name: 'Project',
  component: () => import('../views/projects/ProjectView.vue'),
  children: [
    {
      path: 'info',
      name: '项目信息',
      component: () => import('../views/Test.vue'),
    },
    {
      path: 'auth',
      name: '权限编辑',
      component: () => import('../components/projects/ProjectAuthPage.vue'),
    },
    {
      path: 'details',
      name: '详细信息',
      component: () => import('../components/projects/ProjectDetailsPage.vue'),
    }
  ]
}

export default PROJECTS_ROUTE_CONFIG
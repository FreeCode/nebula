/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { RouteConfig } from 'vue-router'
import AUDIT_ROUTE_CONFIG from './audit'

const HOME_ROUTE_CONFIG: RouteConfig = {
  path: '/management',
  redirect: '/management/projects',
  name: 'Home',
  component: () =>
    import(/* webpackChunkName: "home-view" */ '../views/home/HomeView.vue'),
  children: [
    {
      path: 'projects',
      name: '项目管理',
      component: () =>
        import(
          /* webpackChunkName: "project-view" */ '../components/projects/ProjectsPage.vue'
        ),
      children: [
        {
          path: 'cooperate',
          name: '我参与的',
          component: () =>
            import(
              /* webpackChunkName: "project-view" */ '../components/projects/ProjectsPage.vue'
            ),
        },
        {
          path: 'template',
          name: '模板项目',
          component: () =>
            import(
              /* webpackChunkName: "project-view" */ '../components/projects/ProjectsPage.vue'
            ),
        },
      ],
    },
    {
      path: 'data',
      name: '数据管理',
      component: () =>
        import(
          /* webpackChunkName: "project-view" */ '../views/data/DataView.vue'
        ),
      children: [
        {
          path: 'import',
          name: '数据导入',
          component: () => import('../views/data/DataView.vue'),
        },
        {
          path: 'sqlSearch',
          name: 'sql查询',
          component: () => import('../views/data/DataView.vue'),
        },
        {
          path: 'status',
          name: '数据统计',
          component: () => import('../views/data/DataView.vue'),
        },
      ],
    },
    AUDIT_ROUTE_CONFIG,
  ],
}

export default HOME_ROUTE_CONFIG

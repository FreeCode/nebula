import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'

import HOME_ROUTE_CONFIG from './home'
import STUDIO_ROUTE_CONFIG from './studio'
import PROJECTS_ROUTE_CONFIG from './projects'
import USER_ROUTE_CONFIG from './user'
import LOGIN_ROUTE_CONFIG from './login'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  HOME_ROUTE_CONFIG,
  LOGIN_ROUTE_CONFIG,
  STUDIO_ROUTE_CONFIG,
  PROJECTS_ROUTE_CONFIG,
  USER_ROUTE_CONFIG,
  {
    path: '/privacyAgreement',
    name: '天枢可视平台服务协议',
    component: () => import('../views/login/PrivacyAgreement.vue'),
  },
  {
    path: '/publish/:token',
    name: '可视化发布',
    component: () =>
      import(
        /* webpackChunkName: "visualization-view" */ '../views/studio/PublishView.vue'
      ),
  },
  {
    path: '/formulaGuide',
    name: '公式功能函数指南',
    component: () => import('../views/studio/FormulaGuide.vue'),
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

export default router

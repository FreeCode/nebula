/**
 * api for mathematical statistics in Intelligent Recommendation
 */

import axios from '@/api/http'

export const computeStats = (options?: any) =>
  axios.request({
    ...options,
    url: 'recommend/computeStats',
  })

export default {}

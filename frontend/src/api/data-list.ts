/**
 * 项目数据源 api
 * @author houjinhui
 */

import axios from '@/api/http'
import { message } from 'ant-design-vue'

message.config({
  top: '100px',
  duration: 2,
  maxCount: 1,
})

/**
 * 项目数据列表
 * @param options
 */
export const queryDatalist = (options?: any) =>
  axios.request({
    ...options,
    url: '/dataset/queryByProjectId',
  })

/**
 * 项目数据列表
 * @param options
 */
export const queryDatalist2 = (options?: any) =>
  axios.request({
    ...options,
    url: '/projects/datasets/queryAllDataset',
  })

/**
 * 根据pipeline id获取表信息
 * @returns
 */
export const getNodeTableById = (options?: any) =>
  axios.request({
    ...options,
    url: '/task/queryById',
  })

/**
 * 加载用户数据
 * @param options
 */
export const queryUserDatalist = (options?: any) => {
  return axios.request({
    ...options,
    url: '/category/queryDatasetCategoryByProjectId',
  })
}
/**
 * 数据搜索
 * @param options
 */
export const searchUserDatalist = (options?: any) =>
  axios.request({
    ...options,
    url: '/category/searchInProject',
  })

/**
 * 导入数据
 * @param options
 */
export const importData = (options?: any) =>
  axios.request({
    ...options,
    url: '/projects/datasets/create',
  })

/**
 * 数据预览
 * @param options
 */
export const infoData = (options?: any) =>
  axios.request({
    ...options,
    url: '/projects/datasets/query',
  })

/**
 * 删除数据
 * @param options
 */
export const deleteData = (options?: any) => {
  return axios.request({
    ...options,
    url: '/projects/datasets/delete',
  })
}

/**
 * 数据源
 * @param options
 */
export const queryProjectDataset = (options?: any) => {
  return axios.request({
    ...options,
    url: '/projects/datasets/queryProjectDataset',
  })
}

/**
 * 数据源定位
 * @param options
 */
export const locateDataSource = (options?: any) => {
  return axios.request({
    ...options,
    url: '/task/locateDataSource',
  })
}

/**
 * 保存至数据管理
 * @param options
 */
export const saveToDataset = (options?: any) => {
  return axios.request({
    ...options,
    url: '/task/saveToDataset',
  })
}

/**
 * 保存至数据源
 * @param options
 */
export const saveToDataSource = (options?: any) => {
  return axios.request({
    ...options,
    url: '/task/saveToDataSource',
  })
}

/**
 * 检测同名数据集
 * @param options
 */
export const checkDatasetName = (options?: any) => {
  return axios.request({
    ...options,
    url: '/dataset/checkDatasetName',
  })
}

export const getTaskByDataset = (options?: any) => {
  return axios.request({
    ...options,
    url: '/task/getTaskByDataset',
  })
}

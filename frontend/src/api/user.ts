import axios from '@/api/http'
import filehttp from '@/api/file-http'

export const queryInfoByName = (options?: any) =>
  axios.request({
    ...options,
    url: '/users/queryByName',
  })

export const userLogin = (options?: any) =>
  axios.request({
    ...options,
    url: '/auth/login',
  })

export const userRegister = (options?: any) =>
  axios.request({
    ...options,
    url: '/users/create',
  })

export const userLogout = (options?: any) =>
  axios.request({
    ...options,
    url: '/auth/logout',
  })

export const getUserInfo = (options?: any) =>
  axios.request({
    ...options,
    url: '/auth/info',
  })

export const updateUserInfo = (options?: any) =>
  filehttp.request({
    ...options,
    url: '/users/update',
  })

export const saveFeedback = (options?: any) =>
  filehttp.request({
    ...options,
    url: '/users/saveFeedback',
  })

export const getUserInfoById = (options?: any) =>
  axios.request({
    ...options,
    url: '/users/queryById',
  })

export const getUserConfig = (options?: any) =>
  axios.request({
    ...options,
    url: '/users/queryUserConfig',
  })

export const modifyPassword = (options?: any) =>
  axios.request({
    ...options,
    url: '/users/psdModify',
  })

export const queryNoticeReceiveLevel = (options?: any) =>
  axios.request({
    ...options,
    url: '/users/queryNoticeReceiveLevel',
  })

export const updateNoticeReceiveLevel = (options?: any) =>
  axios.request({
    ...options,
    url: '/users/updateNoticeReceiveLevel',
  })

export const updateNodeDeletionSecondaryConfirmation = (options?: any) =>
  axios.request({
    ...options,
    url: '/users/updateNodeDeletionSecondaryConfirmation',
  })

export const bind = (options?: any) =>
  axios.request({
    ...options,
    url: '/users/bind',
  })

export const bindChange = (options?: any) =>
  axios.request({
    ...options,
    url: '/users/bindChange',
  })

export default {}

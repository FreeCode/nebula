/**
 * pipeline 相关 api
 * @author houjinhui
 */

import axios from '@/api/http'

/**
 * pipeline  流程运行
 * @param options
 */
export const pipelineExecute = (options?: any) =>
  axios.request({
    ...options,
    url: '/pipeline/execute',
  })

/**
 * pipeline  流程停止
 * @param options
 */
export const pipelineStop = (options?: any) =>
  axios.request({
    ...options,
    url: '/pipeline/stop',
  })

/**
 * pipeline  列表
 * @param options
 */
export const pipelineList = (options?: any) =>
  axios.request({
    ...options,
    url: '/pipeline/queryByProject',
  })
/**
 * 新建 pipeline
 * @param options
 */
export const addPipeline = (options?: any) =>
  axios.request({
    ...options,
    url: '/pipeline/save',
  })

/**
 * 复制 pipeline
 * @param options
 */
export const copyPipeline = (options?: any) =>
  axios.request({
    ...options,
    url: '/pipeline/copy',
  })

/**
 * 删除 pipeline
 * @param options
 */
export const deletePipeline = (options?: any) =>
  axios.request({
    ...options,
    url: '/pipeline/deleteById',
  })

/**
 * 编辑 pipeline name
 * @param options
 */
export const editPipeline = (options?: any) =>
  axios.request({
    ...options,
    url: '/pipeline/update',
  })

/**
 * 获取当前用户在制定project的角色信息
 * @param options
 */
export const getUserRole = (options?: any) =>
  axios.request({
    ...options,
    url: '/projects/role/getUserRole',
  })

// 可视化对比
export const queryVisualById = (options?: any) =>
  axios.request({
    ...options,
    url: '/widget/queryByTaskId',
  })

/**
 * 历史版本查看-检查快照容量阈值
 */
export const checkSnapshotThreshold = (options?: any) =>
  axios.request({
    ...options,
    url: '/pipeline/snapshot/checkSnapshotThreshold',
  })

/**
 * 历史版本查看-保存快照
 */
export const saveSnapshot = (options?: any) =>
  axios.request({
    ...options,
    url: '/pipeline/snapshot/saveSnapshot',
  })

/**
 * 历史版本查看-删除快照
 */
export const deleteSnapshot = (options?: any) =>
  axios.request({
    ...options,
    url: '/pipeline/snapshot/deleteSnapshot',
  })

/**
 * 历史版本查看-恢复快照
 */
export const recoverSnapshot = (options?: any) =>
  axios.request({
    ...options,
    url: '/pipeline/snapshot/recoverSnapshot',
  })

/**
 * 历史版本查看-获取快照
 */
export const querySnapshot = (options?: any) =>
  axios.request({
    ...options,
    url: '/pipeline/snapshot/querySnapshot',
  })

/**
 * 预览节点数据
 */
export const queryColumn = (options?: any) =>
  axios.request({
    ...options,
    url: '/widget/render/queryColumn',
  })

/**
 * 获取数据透视表方法
 */
export const queryPivotTableMethod = (options?: any) =>
  axios.request({
    ...options,
    url: '/pipeline/queryPivotTableMethod',
  })

/**
 * api user operator
 * @author houjinhui
 */

import axios from '@/api/http'

/**
 * 查询算子
 * @param options
 */
export const queryOperator = (options?: any) =>
  axios.request({
    ...options,
    url: '/operator/query',
  })

/**
 * 保存接口:
 * @param options
 */
export const saveOperator = (options?: any) =>
  axios.request({
    ...options,
    url: '/operator/save',
  })

/**
 * 更新
 * @param options
 */
export const updateOperator = (options?: any) =>
  axios.request({
    ...options,
    url: '/operator/update',
  })

/**
 * 删除
 * @param options
 */
export const deleteOperator = (options?: any) =>
  axios.request({
    ...options,
    url: '/operator/deleteById',
  })

/**
 * debug
 * @param options
 */
export const debugOperator = (options?: any) =>
  axios.request({
    ...options,
    url: '/operator/debug',
  })

/**
 * copy
 * @param options
 */
export const copyOperator = (options?: any) =>
  axios.request({
    ...options,
    url: '/operator/copy',
  })

/**
 * create code Paragraph  //  新建，粘贴code cell
 * @param options
 */
export const createParagraph = (options?: any) =>
  axios.request({
    ...options,
    url: '/operator/createNewParagraph',
  })

/**
 * update code Paragraph // 保存 all code cells; 用作全局保存
 * @param options
 */
export const updateParagraphs = (options?: any) =>
  axios.request({
    ...options,
    url: '/operator/updateParagraphs',
  })

/**
 * query notebook // 编辑时读取noteBook
 * @param options
 */
export const queryNotebook = (options?: any) =>
  axios.request({
    ...options,
    url: '/operator/query',
  })

/**
 * delete paragraph // cut or delete cell
 * @param options
 */
export const deleteParagraph = (options?: any) =>
  axios.request({
    ...options,
    url: '/operator/deleteParagraph',
  })

/**
 * run paragraph (sync) //  运行之前，后端会自动保存cell
 * @param options
 */
export const runParagraph = (options?: any) =>
  axios.request({
    ...options,
    url: '/operator/runParagraphSync',
  })

/**
 * run notebook (async) run all cells
 * @param options   //  #运行之前前端逻辑里做好全局保存
 */
export const runNotebook = (options?: any) =>
  axios.request({
    ...options,
    url: '/operator/runNotebook',
  })

/**
 * query running cells status
 * @param options //   run Notebook之后调用
 */
export const queryRunningStatus = (options?: any) =>
  axios.request({
    ...options,
    url: '/operator/queryRunningStatus',
  })

/**
 * move paragraph
 * @param options //   移动cell位置
 */
export const moveParagraph = (options?: any) =>
  axios.request({
    ...options,
    url: '/operator/moveParagraph',
  })

/**
 * rename notebook
 * @param options //   id, name
 */
export const renameNotebook = (options?: any) =>
  axios.request({
    ...options,
    url: '/operator/renameNotebook',
  })

/**
 * export operator
 * @param id //   task表中operator_id字段
 */
export const exportOperator = (options?: any) =>
  axios.request({
    ...options,
    url: '/operator/exportOperator',
  })

/**
 * stop All paras
 * @param notebookId  //  notebookId
 */
export const stopAllPara = (options?: any) =>
  axios.request({
    ...options,
    url: '/operator/stopAllParagraphs',
  })

/**
 * stop the specific para
 * @param notebookId  //  notebookId
 * @param paragraphId //  paragraphId
 */
export const stopPara = (options?: any) =>
  axios.request({
    ...options,
    url: '/operator/stopParagraph',
  })

/**
 * run code cells upwards / downwards
 * @param notebookId  //  notebookId
 * @param id //   task表中operator_id字段
 * @param index  // code cell index
 * @param up // 1 upwards, -1 downwards [down 包括当前cell]
 */
export const runUpOrDownPara = (options?: any) =>
  axios.request({
    ...options,
    url: '/operator/runUpOrDownParagraphs',
  })

/**
 * undo operation for cells
 * @param notebookId  //  notebookId
 * @param id //  operatorId
 */
export const undo = (options?: any) =>
  axios.request({
    ...options,
    url: '/operator/undoParagraphSync',
  })

/**
 * redo operation for cells
 * @param notebookId  //  notebookId
 * @param id //  operatorId
 */
export const redo = (options?: any) =>
  axios.request({
    ...options,
    url: '/operator/redoParagraphSync',
  })

export default {}

import axios from '@/api/http'

export const queryModelList = (options?: any) =>
  axios.request({
    ...options,
    url: '/model/queryByProjectId',
  })

export const querySource = (options?: any) =>
  axios.request({
    ...options,
    url: '/projects/datasets/query',
  })

export const createModel = (options?: any) =>
  axios.request({
    ...options,
    url: '/model/create',
  })

export const queryModelById = (options?: any) =>
  axios.request({
    ...options,
    url: '/model/queryMetricsById',
  })

export const updateModel = (options?: any) =>
  axios.request({
    ...options,
    url: '/model/update',
  })

export const trainModel = (options?: any) =>
  axios.request({
    ...options,
    url: '/model/train',
  })

export const killTraining = (options?: any) =>
  axios.request({
    ...options,
    url: '/model/killTraining',
  })

// 查询模型训练状态
export const queryTrainigStatus = (options?: any) =>
  axios.request({
    ...options,
    url: '/model/queryTrainingStatus',
  })

// 查询模型训练状态
export const queryByStatus = (options?: any) =>
  axios.request({
    ...options,
    url: '/model/queryByStatus',
  })

// 导出模型
export const exportToFolder = (options?: any) =>
  axios.request({
    ...options,
    url: '/model/exportToFolder',
  })

// 删除已导出的模型
export const deleteModelInFolder = (options?: any) =>
  axios.request({
    ...options,
    // url: '/model/delete',
    url: '/model/deleteModelInFolder',
  })

// 删除模型
export const deleteModel = (options?: any) =>
  axios.request({
    ...options,
    url: '/model/delete',
  })

// 删除文件夹
export const deleteFolder = (options?: any) =>
  axios.request({
    ...options,
    url: '/model/deleteFolder',
  })

// 执行模型
export const predictModel = (options?: any) =>
  axios.request({
    ...options,
    url: '/model/predict',
  })

// 查询输出结果
export const queryOutputById = (options?: any) =>
  axios.request({
    ...options,
    url: '/model/queryOutputById',
  })

/**
 * 新建文件夹
 * @param options
 */
export const createFolder = (options?: any) =>
  axios.request({
    ...options,
    url: '/model/createFolder',
  })

/**
 * 保存模型结果到数据集
 * @param options
 */
export const saveOutputToDataSource = (options?: any) =>
  axios.request({
    ...options,
    url: '/model/saveOutputToDataSource',
  })

/**
 * 查找模型
 * @param options
 */
export const searchModel = (options?: any) =>
  axios.request({
    ...options,
    url: '/model/search',
  })

export default {}

import axios from '@/api/http'
import { hashWidgetQuery } from '@/util/chart-widget'

/**
 * 获取widget数据 --获取画图的数据
 * @param options
 */
export const getWidgetData = (options?: any) =>
  axios.request({
    ...hashWidgetQuery(options),
    url: '/widget/render/getData',
  })

/**
 * 获取全局可视化收藏列表
 * @param options
 */
export const getGlobalWidgets = (options?: any) =>
  axios.request({
    ...options,
    url: '/widget/getGlobalWidgets',
  })

/**
 * 保存chart信息
 */
export const saveWidget = (options?: any) =>
  axios.request({
    ...options,
    url: '/widget/save',
  })

/**
 * 更新chart信息
 */
export const updateWidget = (options?: any) =>
  axios.request({
    ...options,
    url: '/widget/update',
  })

/**
 * 删除chart信息
 */
export const deleteWidget = (options?: any) =>
  axios.request({
    ...options,
    url: '/widget/deleteById',
  })

export const widgetQueryByTaskId = (options?: any) =>
  axios.request({
    ...options,
    url: '/widget/queryByTaskId',
  })

export const widgetQueryById = (options?: any) =>
  axios.request({
    ...options,
    url: '/widget/queryById',
  })
export const widgetQueryByIds = (options?: any) =>
  axios.request({
    ...options,
    url: '/widget/queryByIds',
  })

/**
 * 可视化系统 - 删除公式
 */
export const deleteFormula = (options?: any) =>
  axios.request({
    ...options,
    url: '/widget/deleteFormula',
  })

/**
 * 可视化系统 - 新增公式
 */
export const addFormula = (options?: any) =>
  axios.request({
    ...options,
    url: '/widget/addFormula',
  })

/**
 * 可视化系统 - 更新公式
 */
export const updateFormula = (options?: any) =>
  axios.request({
    ...options,
    url: '/widget/updateFormula',
  })

/**
 * 可视化系统 - 获取公式列表 【TBD 目前后端没有对同一个数据集做归类】
 */
export const queryFormulaList = (options?: any) =>
  axios.request({
    ...options,
    url: '/widget/queryFormulaList',
  })

export const widgetQueryByProject = (options?: any) =>
  axios.request({
    ...options,
    url: '/widget/queryByProject',
  })

/**
 * 查询widget 的数据集下的列名list
 */
// @ts-ignore
export const queryWidgetColumn = (options?: any) =>
  axios.request({
    ...options,
    url: '/widget/render/queryColumn',
  })

/**
 * 加载图数据
 */
export const loadGraphData = (options?: any) =>
  axios.request({
    ...options,
    url: '/widget/render/loadGraphData',
  })

/**
 * 可视化系统 - 获取某个chart 联动关系的chartList
 */
export const getRelateWidgets = (options?: any) =>
  axios.request({
    ...options,
    url: '/widget/getRelateWidgets',
  })

/** 获取字段信息 */
export const queryColumnInfo = (options?: any) => {
  return axios.request({
    ...options,
    url: '/widget/render/queryStat',
  })
}

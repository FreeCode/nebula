import axios from '@/api/http'
import filehttp from '@/api/file-http'

export const queryCurrentUserDatasetCategory = (options?: any) =>
  axios.request({
    ...options,
    url: '/category/queryCurrentUserDatasetCategory',
  })

export const queryDataUploadProgress = (options?: any) =>
  axios.request({
    ...options,
    url: '/dataset/database/importProgress',
  })

export const cancelDatabaseImport = (options?: any) =>
  axios.request({
    ...options,
    url: '/dataset/database/importCancel',
  })

export const deleteDatasetCategory = (options?: any) =>
  axios.request({
    ...options,
    url: '/category/delete',
  })

export const deleteDatasetById = (options?: any) =>
  axios.request({
    ...options,
    url: '/dataset/delete',
  })

export const renameCategory = (options?: any) =>
  axios.request({
    ...options,
    url: '/category/update',
  })

export const createNewCategory = (options?: any) =>
  axios.request({
    ...options,
    url: '/category/save',
  })

export const updateDataset = (options?: any) =>
  axios.request({
    ...options,
    url: '/dataset/update',
  })

export const queryDatasetById = (options?: any) =>
  axios.request({
    ...options,
    url: '/dataset/queryDataById',
  })

export const checkFileMd5 = (options?: any) =>
  filehttp.request({
    ...options,
    url: '/dataset/checkFileMd5',
  })

export const uploadDataset = (options?: any) =>
  filehttp.request({
    ...options,
    url: '/dataset/uploadFiles',
  })

export const checkProgress = (options?: any) =>
  filehttp.request({
    ...options,
    url: '/dataset/checkProgress',
  })

export const repreviewData = (options?: any) =>
  axios.request({
    ...options,
    url: '/dataset/repreviewData',
  })

export const writeDataset = (options?: any) =>
  axios.request({
    ...options,
    url: '/dataset/fileWrite',
  })

export const testDatabaseConnection = (options?: any) =>
  axios.request({
    ...options,
    url: '/dataset/database/connectionTest',
  })

export const getDatabaseTables = (options?: any) =>
  axios.request({
    ...options,
    url: '/dataset/database/getTables',
  })

export const queryTableData = (options?: any) =>
  axios.request({
    ...options,
    url: '/dataset/database/queryTableData',
  })

export const importDatabaseData = (options?: any) =>
  axios.request({
    ...options,
    url: '/dataset/database/importData',
  })

// 脱敏设置  脱敏类型
export const listDataLevel = (options?: any) =>
  axios.request({
    ...options,
    url: '/dataset/listDataLevel',
  })

// 脱敏设置  脱敏方式
export const listMaskingType = (options?: any) =>
  axios.request({
    ...options,
    url: '/dataset/listMaskingType',
  })

// 获取表头  外部数据库
export const getTablesHead = (options?: any) =>
  axios.request({
    ...options,
    url: '/dataset/database/getTablesHead',
  })

// 获取 外部数据库类型列表
// export const getListExternalDatabaseType = (options?: any) =>
//   axios.request({
//     ...options,
//     url: '/dataset/database/listExternalDatabaseType',
//   })

export const uploadGraphDataset = (options?: any) =>
  filehttp.request({
    ...options,
    url: '/graphAnalysis/previewFile',
  })

export const writeGraphDataset = (options?: any) =>
  filehttp.request({
    ...options,
    url: '/graphAnalysis/uploadFile',
    headers: {
      post: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    },
  })

export const listGraphDataset = (options?: any) =>
  filehttp.request({
    ...options,
    url: '/graphAnalysis/listFiles',
  })

export const deleteGraphDataset = (options?: any) =>
  filehttp.request({
    ...options,
    url: '/graphAnalysis/deleteFile',
    method: 'post',
    headers: {
      'Content-Type': 'application/json;charset=utf-8',
    },
  })

// 图文件重命名
export const updateGraphDataset = (options?: any) =>
  filehttp.request({
    ...options,
    url: '/graphAnalysis/updateFileName',
    method: 'post',
    headers: {
      'Content-Type': 'application/json;charset=utf-8',
    },
  })

/**
 * 预览图文件
 */
export const queryGraphFileByName = (options?: any) =>
  axios.request({
    ...options,
    url: '/graphAnalysis/queryFileByName',
  })

export const httpUpdateDataStatus = (options?: any) =>
  axios.request({
    ...options,
    url: '/dataset/updateHttpDataStatus',
  })

export const selectDatasets = (options?: any) =>
  axios.request({
    ...options,
    url: 'datasetsinfo/selectBy',
  })

export const selectActionByDataId = (options?: any) =>
  axios.request({
    ...options,
    url: '/datasetaction/selectActionByDtaId',
  })

// sql 查询
export const queryDataBySql = (options: any) =>
  axios.request({
    ...options,
    url: '/dataset/queryDataBySql',
  })

export const querySqlCategory = (options: any) =>
  axios.request({
    ...options,
    url: '/dataset/sqlCategory',
  })

export const deleteSqlCategory = (options: any) =>
  axios.request({
    ...options,
    url: '/dataset/deleteSqlCategory',
  })

export const deleteAllSqlCategory = (options: any) =>
  axios.request({
    ...options,
    url: '/dataset/deleteAllSqlCategory',
  })

export const exportToCsv = (options: any) =>
  axios.request({
    ...options,
    url: '/dataset/exportToCsv',
  })

export const exportToXlsx = (options: any) =>
  axios.request({
    ...options,
    responseType: 'blob',
    url: '/dataset/exportToXlsx',
  })

export const updateConfig = (options: any) =>
  axios.request({
    ...options,
    url: '/dataset/database/updateConfig',
  })

export const checkSchedule = (options: any) =>
  axios.request({
    ...options,
    url: '/dataset/database/checkSchedule',
  })

export const queryUnread = (options?: any) =>
  axios.request({
    ...options,
    url: '/datasetaction/selectUnread',
  })

// 数据导入失败接口
export const parseError = (options?: any) =>
  axios.request({
    ...options,
    url: '/dataset/parseErrorFile',
  })

// 数据类型图片
export const getDataTypeImg = (options?: any) =>
  axios.request({
    ...options,
    url: '/dataset/listExternalDataSource',
  })

export const generateMask = (options?: any) =>
  axios.request({
    ...options,
    url: '/dataset/generateMask',
  })

export const queryDatasetUsedInfo = (options?: any) =>
  axios.request({
    ...options,
    url: '/dataset/queryDatasetUsedInfo',
  })
export default {}

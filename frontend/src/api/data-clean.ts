/* eslint-disable import/prefer-default-export */
import axios from '@/api/http'

/**
 * 查询操作历史列表
 */
export const apiGetHistoryList = (options?: any) =>
  axios.request({
    ...options,
    url: '/tcolumn/queryAction',
  })

/**
 * 删除操作历史
 */
export const apiDelHistory = (options?: any) =>
  axios.request({
    ...options,
    url: '/tcolumn/deleteAction',
  })

/**
 * 增加操作历史
 */
export const apiAddHistory = (options?: any) =>
  axios.request({
    ...options,
    url: '/tcolumn/addAction',
  })

/**
 * 更新操作历史
 */
export const apiUpdateHistory = (options?: any) =>
  axios.request({
    ...options,
    url: '/tcolumn/updateAction',
  })

/**
 * 获取表格明细数据
 */
export const apiGetTableData = (options?: any) =>
  axios.request({
    ...options,
    url: '/tcolumn/queryDetail',
  })

/**
 * 获取字段统计数据
 */
export const apiGetColumnStat = (options?: any) =>
  axios.request({
    ...options,
    url: '/tcolumn/queryStat',
  })

export const apiGetRecommendSplit = (options?: any) =>
  axios.request({
    ...options,
    url: '/tcolumn/recommendSeparator',
  })

export const apiUpdateSemantic = (options?: any) =>
  axios.request({
    ...options,
    url: '/tcolumn/updateSemantic',
  })

export const apiGetRecommendValue = (options?: any) =>
  axios.request({
    ...options,
    url: '/tcolumn/recommendValue',
  })

/**
 * check if it's ok to run formula content
 */
export const checkFormula = (options?: any) =>
  axios.request({
    ...options,
    url: '/tcolumn/checkFormula',
  })

/**
 * 对象解析 - 获取该字段（列）所有 key, 数组解析获取 index 列表
 */
export const apiGetJsonKeysOrArrayIndex = (options?: any) =>
  axios.request({
    ...options,
    url: '/tcolumn/queryIndex',
  })

/**
 * 语义转化验证
 */
export const apiValidateTransform = (options?: any) =>
  axios.request({
    ...options,
    url: '/tcolumn/validateTransform',
  })

/**
 * 有序序列的顺序， 获取
 */
export const apiQueryOrder = (options?: any) =>
  axios.request({
    ...options,
    url: '/tcolumn/queryOrder',
  })

/**
 * 节点数据全局搜索
 */
export const apiMultiColumnSearch = (options?: any) =>
  axios.request({
    ...options,
    url: '/tcolumn/multiColumnSearch',
  })

/**
 * 百分比和数值的对应关系
 */
export const apiGetPercentageValue = (options?: any) =>
  axios.request({
    ...options,
    url: '/tcolumn/queryPercentageData',
  })

/**
 * api for visualization recommendation
 * @author Jiahui Chen
 */

import axios from '@/api/http'

export const viewData = (options?: any) => axios.request({
  ...options,
  url: '/recommend/viewData'
})

export const preCalculate = (options?: any) => axios.request({
  ...options,
  url: '/recommend/preCalculate'
})

export const calculatePca = (options?: any) => axios.request({
  ...options,
  url: '/recommend/operator'
})

export default {}


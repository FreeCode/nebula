/**
 * audit 审核 相关 api
 * @author houjinhui
 */

import axios from '@/api/http'

/**
 * 审计数据 list
 * @param options
 */
export const adminAudit = (options?: any) =>
  axios.request({
    ...options,
    url: '/admin/audit',
  })

// 管理员查看用户信息
export const queryAllUserInfo = (options?: any) =>
  axios.request({
    ...options,
    url: '/admin/queryAllUserDTO',
  })

// 管理员查看用户的数据表信息
export const queryDataInfoById = (options?: any) =>
  axios.request({
    ...options,
    url: '/admin/queryDataInfoById',
  })

// 模糊搜索数据集名
export const searchDatasetNames = (options?: any) =>
  axios.request({
    ...options,
    url: '/admin/selectLikeDataName',
  })

// 查看数据集信息
export const queryAllDatasetInfo = (options?: any) =>
  axios.request({
    ...options,
    url: '/admin/queryAllUserDataSetDTO',
  })

// 查询任务检测操作类型
export const queryTaskType = (options?: any) =>
  axios.request({
    ...options,
    url: '/admin/queryTaskType',
  })

// 查询任务检测列表
export const queryTaskInstanceInfo = (options?: any) =>
  axios.request({
    ...options,
    url: '/admin/queryTaskInstanceInfo',
  })

// 查看所有用户反馈
export const queryUserFeedback = (options?: any) =>
  axios.request({
    ...options,
    url: '/admin/selectFeedbacks',
  })

// 根据id查看私信内容
export const queryFeedbackById = (options?: any) =>
  axios.request({
    ...options,
    url: '/admin/queryFeedbackById',
  })

// 发送私信
export const sendNotice = (options?: any) =>
  axios.request({
    ...options,
    url: '/admin/sendNotice',
  })

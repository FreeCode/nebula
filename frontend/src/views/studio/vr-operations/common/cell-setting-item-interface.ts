import { CellInfo, OptionPreViewAction } from '@/store/modules/dataview'

export default interface CellSettingItem  {
  /**
   * 是否展示
   */
  showFunc(info: Array<CellInfo>|null, ...subArgs: Array<any>): boolean,
  /**
   * 选中模式
   */
  selectionType: OptionPreViewAction,
  /**
   * 选中模式名称
   */
  name(info: Array<CellInfo>|null, ...subArgs: Array<any>): string

  /**
   * 操作选项
   */
  actions: Array<{
    label: string,
    bindKey: string,
    value: OptionPreViewAction
  }>,

  /**
   * 是否展示操作选项面板
   */
  isShowSub: boolean

}

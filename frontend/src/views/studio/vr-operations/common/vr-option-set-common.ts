import DataStore, {
  IColumn,
  OptionPreViewAction,
} from '@/store/modules/dataview'
import SelectionType from '@/util/selection-type'
import VROptionSetModeInteface from '@/util/vr-option-set-mode-interface'
import { isNull, isNumber } from 'lodash'
import ColumnSettingItem from './column-setting-item-interface'
import CompareInfoInterface from './compare-info-interface'

// 触发表格回显操作静态类
export default class OptionSettingCommon {
  static getCompareEvalString(value: any, compareInfo: CompareInfoInterface) {
    if (compareInfo.compareMode === '=') {
      return `${value} ${
        compareInfo.compareMode === '=' ? '===' : compareInfo.compareMode
      } ${compareInfo.delimiter}`
    }
    return `${value} ${compareInfo.compareMode} ${compareInfo.delimiter}`
  }
  // 设置表格选中当前列中为Null的单元格
  static setColumnFilterSelectNullCells(modeInfo: ColumnSettingItem) {
    const vrOptionInfo: VROptionSetModeInteface = {
      sureMode: modeInfo.selectionType,
      selectionType: SelectionType.cells,
      column: DataStore.tableSelectColumns[0].name,
      conditionTrigger(scope: any, store: any, column: IColumn) {
        return [null, undefined, ''].includes((scope.row as any)[column.name])
      },
    }
    DataStore.setTableVrCurrenSetMode(vrOptionInfo)
  }
  // 设置表格选中该列匹配第一个单元格中的值
  static setColumnFilterContains(modeInfo: ColumnSettingItem) {
    const vrOptionInfo: VROptionSetModeInteface = {
      sureMode: modeInfo.selectionType,
      selectionType: SelectionType.cells,
      column: DataStore.tableSelectColumns[0].name,
      conditionTrigger(scope: any, store: any, column: IColumn) {
        return (store.tableValueContains || []).includes(
          (scope.row as any)[column.name]
        )
      },
    }
    DataStore.setTableVrCurrenSetMode(vrOptionInfo)
  }
  // 设置表格选中符合compare条件的单元格
  static setColumnFilterCompare(modeInfo: ColumnSettingItem) {
    const compareInfo = DataStore.vrCompareInfo
    if (compareInfo) {
      const vrOptionInfo: VROptionSetModeInteface = {
        sureMode: modeInfo.selectionType,
        selectionType: SelectionType.cells,
        column: DataStore.tableSelectColumns[0].name,
        conditionTrigger(scope: any, store: any, column: IColumn) {
          const value = scope.row[column.name]
          if (!['', null, undefined].includes(value)) {
            const stringCompare = OptionSettingCommon.getCompareEvalString(
              value,
              store.vrCompareInfo
            )
            /* eslint-disable */
            return eval(stringCompare)
          }
          return value
        },
      }
      DataStore.setTableVrCurrenSetMode(vrOptionInfo)
    }
  }

  // 设置表格选中range范围内的单元格
  static setColumnFilterRange(modeInfo: ColumnSettingItem) {
    const rangeInfo = DataStore.vrRangeInfo
    if (rangeInfo && isNumber(rangeInfo[0]) && isNumber(rangeInfo[1])) {
      const vrOptionInfo: VROptionSetModeInteface = {
        sureMode: modeInfo.selectionType,
        selectionType: SelectionType.cells,
        column: DataStore.tableSelectColumns[0].name,
        conditionTrigger(scope: any, store: any, column: IColumn) {
          const value = scope.row[column.name]
          return (
            !isNull(value) &&
            rangeInfo[0] <= (value as number) &&
            rangeInfo[1] >= (value as number)
          )
        },
      }
      DataStore.setTableVrCurrenSetMode(vrOptionInfo)
    }
  }

  // 设置表格选中包含缺失值的行
  static setRowFilterSelectNullRows(selectionType: OptionPreViewAction) {
    const vrOptionInfo: VROptionSetModeInteface = {
      sureMode: selectionType,
      selectionType: SelectionType.rows,
      conditionTrigger(scope: any /*, store: any */) {
        return new Set(Object.values(scope.row)).has(null)
      },
    }
    DataStore.setTableVrCurrenSetMode(vrOptionInfo)
  }

  // 设置表格选中包含已选值得单元格
  static setCellFilterSelectContainsCell(selectionType: OptionPreViewAction) {
    const tableSelectCells = DataStore.tableSelectCells
    if (tableSelectCells) {
      const contains = new Set(
        tableSelectCells.map((item) => {
          return item.cellValue
        })
      )
      const vrOptionInfo: VROptionSetModeInteface = {
        sureMode: selectionType,
        selectionType: SelectionType.cells,
        column: DataStore.tableSelectCells[0].columnName,
        conditionTrigger(scope: any, store: any, column: IColumn) {
          const value = scope.row[column.name]
          return contains.has(value)
        },
      }
      DataStore.setTableVrCurrenSetMode(vrOptionInfo)
    }
  }

  // 设置表格选中该列中为缺省值得单元格
  static setCellFilterSelectNullCells(selectionType: OptionPreViewAction) {
    const tableSelectCells = DataStore.tableSelectCells
    if (tableSelectCells.length > 0) {
      const vrOptionInfo: VROptionSetModeInteface = {
        sureMode: selectionType,
        selectionType: SelectionType.cells,
        column: DataStore.tableSelectCells[0].columnName,
        conditionTrigger(scope: any, store: any, column: IColumn) {
          const value = scope.row[column.name]
          return [null, '', undefined].includes(value)
        },
      }
      DataStore.setTableVrCurrenSetMode(vrOptionInfo)
    }
  }

  // 设置选中符合指定条件的单元格
  static setCellFilterSelectCompareCell(selectionType: OptionPreViewAction) {
    const compareInfo = DataStore.vrCompareInfo
    if (compareInfo) {
      const vrOptionInfo: VROptionSetModeInteface = {
        sureMode: selectionType,
        selectionType: SelectionType.cells,
        column: DataStore.tableSelectCells[0].columnName,
        conditionTrigger(scope: any, store: any, column: IColumn) {
          const value = scope.row[column.name]
          if (!['', null, undefined].includes(value)) {
            const stringCompare = OptionSettingCommon.getCompareEvalString(
              value,
              store.vrCompareInfo
            )
            /* eslint-disable */
            return eval(stringCompare)
          }
          return value
        },
      }
      DataStore.setTableVrCurrenSetMode(vrOptionInfo)
    }
  }

  // 设置选中表格中该列在区间中的单元格
  static setCellFilterSelectRangeCell(selectionType: OptionPreViewAction) {
    const rangeInfo = DataStore.vrRangeInfo
    if (rangeInfo && isNumber(rangeInfo[0]) && isNumber(rangeInfo[1])) {
      const vrOptionInfo: VROptionSetModeInteface = {
        sureMode: selectionType,
        selectionType: SelectionType.cells,
        column: DataStore.tableSelectCells[0].columnName,
        conditionTrigger(scope: any, store: any, column: IColumn) {
          const value = scope.row[column.name]
          return (
            !isNull(value) &&
            rangeInfo[0] <= (value as number) &&
            rangeInfo[1] >= (value as number)
          )
        },
      }
      DataStore.setTableVrCurrenSetMode(vrOptionInfo)
    }
  }

  // 选中包含关键字的单元格
  static setSelectionFilterSelectCellsByKeyword(
    selectionType: OptionPreViewAction
  ) {
    const keyword = DataStore.tableSelctionText
    const columnName = DataStore.tableSelectionInfo?.columnName
    if (keyword && columnName) {
      const vrOptionInfo: VROptionSetModeInteface = {
        sureMode: selectionType,
        selectionType: SelectionType.selection,
        column: columnName,
        conditionTrigger(scope: any, store: any, column: IColumn) {
          let value = scope.row[column.name]
          if (column.name === columnName) {
            value = [undefined, null, ''].includes(scope.row[column.name])
              ? ''
              : scope.row[column.name]
            if (value.indexOf(keyword) !== -1) {
              return value.replace(
                keyword,
                `<span class="select-word">${keyword}</span>`
              )
            }
          }
          return value
        },
      }
      DataStore.setTableVrCurrenSetMode(vrOptionInfo)
    }
  }
  // 选中以关键字开头的单元格
  static setSelectionFilterSelectCellsStartWithKeyword(
    selectionType: OptionPreViewAction
  ) {
    const keyword = DataStore.tableSelctionText
    const columnName = DataStore.tableSelectionInfo?.columnName
    if (keyword && columnName) {
      const vrOptionInfo: VROptionSetModeInteface = {
        sureMode: selectionType,
        selectionType: SelectionType.selection,
        column: columnName,
        conditionTrigger(scope: any, store: any, column: IColumn) {
          let value = scope.row[column.name]
          if (column.name === columnName) {
            value = [undefined, null, ''].includes(scope.row[column.name])
              ? ''
              : scope.row[column.name]
            if (value.indexOf(keyword) === 0) {
              return value.replace(
                keyword,
                `<span class="select-word">${keyword}</span>`
              )
            }
          }
          return value
        },
      }
      DataStore.setTableVrCurrenSetMode(vrOptionInfo)
    }
  }

  // 选中以关键字结束的单元格
  static setSelectionFilterSelectCellsEndWithKeyword(
    selectionType: OptionPreViewAction
  ) {
    const keyword = DataStore.tableSelctionText
    const columnName = DataStore.tableSelectionInfo?.columnName
    if (keyword && columnName) {
      const vrOptionInfo: VROptionSetModeInteface = {
        sureMode: selectionType,
        selectionType: SelectionType.selection,
        column: columnName,
        conditionTrigger(scope: any, store: any, column: IColumn) {
          let value = scope.row[column.name]
          if (column.name === columnName) {
            value = [undefined, null, ''].includes(scope.row[column.name])
              ? ''
              : scope.row[column.name]
            if (value.slice(-keyword.length) === keyword) {
              return `${value.substr(
                0,
                value.length - keyword.length
              )}<span class="select-word">${keyword}</span>`
            }
            return value
          }
          return value
        },
      }
      DataStore.setTableVrCurrenSetMode(vrOptionInfo)
    }
  }
  // 选中表格中相同位置有值的单元格
  static setSelectionFilterSelectCellsSamePosSelection(
    selectionType: OptionPreViewAction
  ) {
    const keyword = DataStore.tableSelctionText
    const selectionInfo = DataStore.tableSelectionInfo
    if (keyword && selectionInfo) {
      const columnName = selectionInfo.columnName
      const vrOptionInfo: VROptionSetModeInteface = {
        sureMode: selectionType,
        selectionType: SelectionType.selection,
        column: columnName,
        conditionTrigger(scope: any, store: any, column: IColumn) {
          let value = scope.row[column.name]
          if (column.name === columnName) {
            value = [undefined, null, ''].includes(scope.row[column.name])
              ? ''
              : scope.row[column.name]
            // if(selectionInfo.startIndex < value.length) {
            // if(value.length >= selectionInfo.endIndex) {
            if (value.length > selectionInfo.startIndex) {
              return `${value.substr(
                0,
                selectionInfo.startIndex
              )}<span class="select-word">${value.slice(
                selectionInfo.startIndex,
                selectionInfo.endIndex
              )}</span>${
                selectionInfo.endIndex > value.length - 1
                  ? value.slice(selectionInfo.endIndex, value.length)
                  : ''
              }`
              // }
              // return value
            }
          }
          return value
        },
      }
      DataStore.setTableVrCurrenSetMode(vrOptionInfo)
    }
  }

  // 设置选中与所选内容单词位置一致的内容
  static setSelectionFilterSelectCellsSamePosWord(
    selectionType: OptionPreViewAction
  ) {
    const keyword = DataStore.tableSelctionText
    const selectionInfo = DataStore.tableSelectionInfo
    if (keyword && selectionInfo) {
      const columnName = selectionInfo.columnName
      const vrOptionInfo: VROptionSetModeInteface = {
        sureMode: selectionType,
        selectionType: SelectionType.selection,
        column: columnName,
        conditionTrigger(scope: any, store: any, column: IColumn) {
          let value = scope.row[column.name]
          if (column.name === columnName) {
            value = [undefined, null, ''].includes(scope.row[column.name])
              ? ''
              : scope.row[column.name]
            if (selectionInfo.startIndex < value.length) {
              let whiteSpaceSplitArray = (value as string).split(' ')
              const wordPos = selectionInfo.wordPos - 1
              if (wordPos >= 0 && whiteSpaceSplitArray.length > 0) {
                // 把位置相同的单词替换为dom标识
                whiteSpaceSplitArray = whiteSpaceSplitArray.map(
                  (currentValue, index) => {
                    return wordPos === index
                      ? `<span class="select-word">${currentValue}</span>`
                      : currentValue
                  }
                )
                return whiteSpaceSplitArray.join(' ')
              }
              return value
            }
          }
          return value
        },
      }
      DataStore.setTableVrCurrenSetMode(vrOptionInfo)
    }
  }
  // 设置表格选中以xxx开头的单词
  static setSelectionFilterSelectCellsStartWithSameWord(
    selectionType: OptionPreViewAction
  ) {
    const keyword = DataStore.tableSelctionText
    const selectionInfo = DataStore.tableSelectionInfo
    if (keyword && selectionInfo) {
      const columnName = selectionInfo.columnName
      const vrOptionInfo: VROptionSetModeInteface = {
        sureMode: selectionType,
        selectionType: SelectionType.selection,
        column: columnName,
        conditionTrigger(scope: any, store: any, column: IColumn) {
          const valueOrigin = scope.row[column.name]
          const value = [null, undefined].includes(valueOrigin)
            ? ''
            : (valueOrigin as string)
          const isStart = value.startsWith(`${selectionInfo.startWithWord} `)
          const splitString = isStart
            ? `${selectionInfo.startWithWord} `
            : ` ${selectionInfo.startWithWord} `
          if (value.indexOf(splitString) !== -1) {
            const reg = new RegExp(`(${splitString})(\\\S+)`)
            return value.replace(reg, '$1<span class="select-word">$2</span>')
          }
          return value
        },
      }
      DataStore.setTableVrCurrenSetMode(vrOptionInfo)
    }
  }
  // 设置表格选中以xxx结尾的单词
  static setSelectionFilterSelectCellsEndWithSameWord(
    selectionType: OptionPreViewAction
  ) {
    const keyword = DataStore.tableSelctionText
    const selectionInfo = DataStore.tableSelectionInfo
    if (keyword && selectionInfo) {
      const columnName = selectionInfo.columnName
      const vrOptionInfo: VROptionSetModeInteface = {
        sureMode: selectionType,
        selectionType: SelectionType.selection,
        column: columnName,
        conditionTrigger(scope: any, store: any, column: IColumn) {
          let value = [null, undefined].includes(scope.row[column.name])
            ? ''
            : (scope.row[column.name] as string)
          const tableSelectionInfo = DataStore.tableSelectionInfo
          const regExp = tableSelectionInfo?.endWithword
            ? new RegExp(`(\\S+)(\\s${tableSelectionInfo.endWithword})(\\s|$)`)
            : null
          if (
            column.name === columnName &&
            tableSelectionInfo !== null &&
            regExp &&
            regExp.test(value)
          ) {
            return value.replace(
              regExp,
              `<span class="select-word">$1</span>$2$3`
            )
          }
          return value
        },
      }
      DataStore.setTableVrCurrenSetMode(vrOptionInfo)
    }
  }

  // 设置选中xxx yyy单词之间的单词
  static setSelectionFilterSelectCellsStartWithAndEndWithSameWord(
    selectionType: OptionPreViewAction
  ) {
    const keyword = DataStore.tableSelctionText
    const selectionInfo = DataStore.tableSelectionInfo
    if (keyword && selectionInfo) {
      const columnName = selectionInfo.columnName
      const vrOptionInfo: VROptionSetModeInteface = {
        sureMode: selectionType,
        selectionType: SelectionType.selection,
        column: columnName,
        conditionTrigger(scope: any, store: any, column: IColumn) {
          // 这里需要考虑头部、尾部边界情况
          const value = [undefined, null, ''].includes(scope.row[column.name])
            ? ''
            : scope.row[column.name]
          const regExp = new RegExp(
            `(^|\\s)(${store.tableSelectionInfo.startWithWord}\\s)(\\S+)(\\s${store.tableSelectionInfo.endWithword})(\\s|$)`
          )
          if (column.name === columnName && regExp.test(value)) {
            return value.replace(
              regExp,
              '$1$2<span class="select-word">$3</span>$4$5'
            )
          }
          return value
        },
      }
      DataStore.setTableVrCurrenSetMode(vrOptionInfo)
    }
  }
}

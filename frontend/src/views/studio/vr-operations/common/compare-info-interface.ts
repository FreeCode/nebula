export default interface CompareInfoInterface {
  /** 对比模式 */
  compareMode: string,
  /** 限定值 */
  delimiter: number
}
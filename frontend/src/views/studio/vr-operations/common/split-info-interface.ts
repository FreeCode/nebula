export default interface SplitInfo {
  /** 拆分关键字 */
  separator: '',
  /** 拆分类型 */
  splitType: 1,
  /** 拆分数量 */
  splitNum: 1,
  /** 描述 */
  desc: string,
  /** 拆分后新的列名称集合 */
  newCol: Array<string>
}

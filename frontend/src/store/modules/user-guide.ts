/**
 * New user orientation 新用户引导
 */
import {
  Action,
  getModule,
  Module,
  Mutation,
  VuexModule,
} from 'vuex-module-decorators'
import store from '@/store'
import { guideChange, guideCheck } from '@/api/guide'

@Module({ dynamic: true, namespaced: true, name: 'UserGuide', store })
class UserGuideStore extends VuexModule {
  // 用户引导模式
  public _newUserGuideMode: boolean = false
  public get newUserGuideMode(): boolean {
    return this._newUserGuideMode
  }

  // 开始 / 结束的遮罩
  public _overlayModal: boolean = false
  public get overlayModal(): boolean {
    return this._overlayModal
  }

  // 引导模态框状态 start 开始， end 结束
  public _guideModalType: 'start' | 'end' = 'start'
  public get guideModalType(): string {
    return this._guideModalType
  }

  // 当前步骤, 默认 0， 无步骤
  public _currentStep: number = 0
  public get currentStep() {
    return this._currentStep
  }

  // 当前步骤状态, 每调到下一步， 都重置状态
  public _currentGuideStepPopover: boolean = false // 每一步 popover 显示状态
  public _currentGuideStepInProgress: boolean = false // 正在进行

  public get currentGuideStepPopover() {
    return this._currentGuideStepPopover
  }
  public get currentGuideStepInProgress() {
    return this._currentGuideStepInProgress
  }
  // 修改进行时
  @Mutation
  public setCurrentGuideStepInProgress(status: boolean) {
    // 当处于引导模式允许修改
    if (this._newUserGuideMode) {
      this._currentGuideStepInProgress = status
    }
  }

  /**
   * 关闭新用户引导, 请求接口, 更改后端记录
   * 场景：
   * 跳过 - 开始提示引导跳过
   * 中间关闭 - "user-guide/user-guide.ts" userGuideTextMap 中途关闭用户引导
   * 完成引导
   */
  @Action
  public async ignoreUserGuide(userId: number) {
    this.context.commit('ignoreUserGuideClearStatus')
    await guideChange({ data: userId })
  }

  /**
   * 新用户登陆 进入引导模式 初始化引导
   */
  @Mutation
  public intoGuide() {
    this._currentStep = 0
    this._newUserGuideMode = true
    this._guideModalType = 'start'
    this._overlayModal = true
  }

  // 关闭引导
  @Mutation
  public ignoreUserGuideClearStatus() {
    this._newUserGuideMode = false
    this._currentStep = 0
    this._currentGuideStepPopover = false
    this._overlayModal = false
  }

  /**
   * modal 点击 开始探索 新人引导
   */
  @Mutation
  public startNewUserGuide() {
    this._newUserGuideMode = true
    this._currentStep = 1
    this._currentGuideStepPopover = true
    this._overlayModal = false
  }

  /**
   * 进入指定引导步骤
   */
  @Mutation
  public toSpecifiedGuideStep(step: number) {
    if (this._newUserGuideMode) {
      this._currentStep = step
      this._currentGuideStepPopover = true
      this._currentGuideStepInProgress = false
    }
  }

  /**
   * 所有引导步骤完成，进入用户引导完成弹窗
   */
  @Mutation
  public finishGuideStepModal() {
    if (this._newUserGuideMode) {
      this._currentStep = 0
      this._currentGuideStepPopover = false
      this._currentGuideStepInProgress = false
      this._guideModalType = 'end'
      this._overlayModal = true
    }
  }

  /**
   * 检测是否完成引导 / 跳过
   */
  @Action
  public async guideCheck(userId: number) {
    const response = await guideCheck({ data: userId })
    return response.data.result
  }
}

export default getModule(UserGuideStore)

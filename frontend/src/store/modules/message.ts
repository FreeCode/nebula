/**
 * message store
 * @author Junjie Jin
 */
import {
  Module,
  Mutation,
  VuexModule,
  getModule,
  Action,
} from 'vuex-module-decorators'
import store from '@/store'
import {
  queryNotice,
  queryFilterType,
  queryProjectList,
  deleteNotice,
  deleteAllNotice,
  processNotice,
  processAllNotice,
} from '@/api/message'

@Module({ dynamic: true, namespaced: true, name: 'MessageStore', store })
class MessageStore extends VuexModule {
  // For HomeHeader.vue 快捷消息中心的消息队列
  private _noticeListInShortcut = {
    unread: [],
    read: [],
  }

  public get unreadNoticeList() {
    return this._noticeListInShortcut.unread
  }
  public get readNoticeList() {
    return this._noticeListInShortcut.read
  }

  // 获取信息列表
  @Action
  public async queryNoticeListInShortcut(passData: any) {
    const response = await queryNotice({ data: passData })

    if (passData.status === 0) {
      this.context.commit('setUnreadNoticeList', response.data.result.data)
    } else if (passData.status === 1) {
      this.context.commit('setReadNoticeList', response.data.result.data)
    }

    return response.data.result.data
  }

  @Mutation
  private setUnreadNoticeList(value: any) {
    this._noticeListInShortcut.unread = value
  }

  @Mutation
  private setReadNoticeList(value: any) {
    this._noticeListInShortcut.read = value
  }

  // For MessageCenter.vue 消息中心页面的消息队列
  private _noticeListInCenter: any[] = [] // 当前页消息列表
  private _totalElements: number = 0 // 当前状态消息总数
  private _filterType: any[] = [] // 当前消息过滤类型
  private _projectList: any[] = [] // 用户所在的项目索引及名称

  public get noticeList() {
    return this._noticeListInCenter
  }
  public get totalElements() {
    return this._totalElements
  }
  public get filterType() {
    return this._filterType
  }
  public get projectList() {
    return this._projectList
  }

  // 获取信息列表
  @Action
  public async queryNoticeListInCenter(passData: any) {
    const response = await queryNotice({ data: passData })

    // checked属性: 绑定Message Center 的复选框
    response.data.result.data.forEach(function addChecked(item: any) {
      item.checked = false
    })

    this.context.commit('setNoticeList', response.data.result.data)
    this.context.commit(
      'setAllTotalElements',
      response.data.result.totalElements
    )
  }

  // 获取消息类型
  @Action({ commit: 'setFilterType' })
  public async queryFilterType() {
    const response = await queryFilterType({})
    return response.data.result
  }

  // 获取项目列表
  @Action({ commit: 'setProjectList' })
  public async queryProjectList() {
    const response = await queryProjectList({})
    return response.data.result
  }

  // 已读信息
  @Action
  public async processNotice(passData: any) {
    const response = await processNotice({ data: passData })
    return response.data.result
  }

  // 删除信息
  @Action
  public async deleteNotice(passData: any) {
    const response = await deleteNotice({ data: passData })
    return response.data.result
  }

  // 已读所有信息
  @Action
  public async processAllNotice(passData: any) {
    const response = await processAllNotice({ data: passData })
    return response.data.result
  }

  // 删除所有信息
  @Action
  public async deleteAllNotice(passData: any) {
    const response = await deleteAllNotice({ data: passData })
    return response.data.result
  }

  // 更新消息队列
  @Mutation
  private setNoticeList(value: any) {
    this._noticeListInCenter = value
  }

  // 更新消息总数
  @Mutation
  private setAllTotalElements(value: any) {
    this._totalElements = value
  }

  // 更新过滤消息类型
  @Mutation
  private setFilterType(value: any) {
    this._filterType = value
  }

  // 更新消息项目名
  @Mutation
  private setProjectList(value: any) {
    this._projectList = value
  }
}

export default getModule(MessageStore)

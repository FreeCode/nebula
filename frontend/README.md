## ⌨️ Development

You will need [Node.js](http://nodejs.org) **version 10+**.

After cloning the repo, run:

``` bash
$ npm install # install the dependencies of the project
```

Common NPM scripts:

``` bash
# watch and auto re-build the project
$ npm run serve

# run unit tests
$ npm run test:unit

# compile and minify for production
$ npm run build

# lint and fix files
$ npm run lint
```


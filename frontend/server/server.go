package main

import (
	"git.zjvis.org/nebula-platform/nebula/server/controller"
	model "git.zjvis.org/nebula-platform/nebula/server/model"
)

func main() {
	model.Setup()
	controller.Setup().Run()
}

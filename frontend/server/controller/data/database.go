package data

import (
	"github.com/gin-gonic/gin"

	model "git.zjvis.org/nebula-platform/nebula/server/model"
)

// GetAllDatabases returns the database connections owned by the current user
func GetAllDatabases(c *gin.Context) {
	testDatabases := []model.Database{
		{
			ID: 0,
			Name: "Test Database",
		},
	}

	c.JSON(200, gin.H{
		"data": gin.H{
			"items": testDatabases,
		},
	})
}
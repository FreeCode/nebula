/* eslint-disable */
const StyleLintPlugin = require('stylelint-webpack-plugin')

module.exports = {
  publicPath: '/',
  lintOnSave: process.env.NODE_ENV === 'production' ? false : 'default',
  productionSourceMap: false,
  configureWebpack: (config) => {
    const plugins = [
      new StyleLintPlugin({
        files: ['src/**/*.{vue,less}'],
        fix: true,
      }),
    ]
    config.optimization = {
      splitChunks: {
        cacheGroups: {
          vendors: {
            name: 'chunk-vendors',
            test: /[\/]node_modules[\/]/,
            priority: -10,
            chunks: 'initial',
          },
          common: {
            name: 'chunk-common',
            minChunks: 2,
            priority: -20,
            chunks: 'initial',
            reuseExistingChunk: true,
          },
          visChartsPlus: {
            name: 'chunk-vischarts-plus',
            test: /[\/]node_modules[\/]vis-charts-plus[\/]/,
            chunks: 'all',
            priority: -5,
            reuseExistingChunk: true,
          },
          antv: {
            name: 'antv',
            test: /[\/]node_modules[\/]@antv[\/]/,
            chunks: 'all',
            priority: -6,
            reuseExistingChunk: true,
          },
        },
      },
    }
    return {plugins}
  },
  css: {
    loaderOptions: {
      less: {
        lessOptions: {
          modifyVars: {
            'primary-color': '#5561ff',
            'link-color': '#5561ff',
            'border-radius-base': '2px',
          },
          javascriptEnabled: true,
        },
      },
    },
  },
  devServer: {
    proxy: {
      '/api': {
        target: 'https://10.5.24.17:8443', // dev
        // target: 'https://10.11.32.16:8444', // yb
        // target: 'https://10.11.32.16:8443', // yb
        // target: 'https://10.11.32.28:8443',// dzm
        // target: 'https://10.11.16.118:8443/', // zj
        // target: 'https://10.11.16.245:8443',// taotao
        // target: 'https://10.11.32.50:8443',// qcy
        // target: 'https://10.11.32.101:8443',// lwm
        // target: 'https://10.11.24.162:8443',// zh
        // target: 'http://10.11.16.95:8080',// ff
        // target: 'https://10.0.105.191:8443',
        // target: 'https://10.11.24.216:8443', // 元数据管理
        // target: 'http://10.11.16.241:8080', // 图片获取
        // target: 'https://10.11.16.243:8443', // hjh
        // target: 'https://10.11.32.101:8443', // lwm
        // target: 'https://10.11.16.241:8443', // dev-message-center
        // target: 'https://10.11.32.48:8443', // zhilin
        ws: true,
        changeOrigin: true,
        pathRewrite: {
          '^/api/(.*)': '/$1',
        },
      },
    },
  },
}

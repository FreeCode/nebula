import {mount} from '@vue/test-utils'
import { wrap } from 'lodash'
import ColumnStatistics from '../../src/components/studio/data/node-edit-panel/ColumnStatistics.vue'

const testData1 = {
  mode: 'continuous',
  width: 160,
  height: 180,
  barHeight: 10,
  barGap: 0,
  totalCount: 501,
  totalValue: 29,
  value: [{"label":"0 - 100","index":0,"yAxisValue":0,"value":[498,0],"low":0,"up":100},{"label":"100 - 200","index":1,"yAxisValue":100,"value":[2,0],"low":100,"up":200},{"label":"200 - 300","index":2,"yAxisValue":200,"value":[0,0],"low":200,"up":300},{"label":"300 - 400","index":3,"yAxisValue":300,"value":[0,0],"low":300,"up":400},{"label":"400 - 500","index":4,"yAxisValue":400,"value":[0,0],"low":400,"up":500},{"label":"500 - 600","index":5,"yAxisValue":500,"value":[0,0],"low":500,"up":600},{"label":"600 - 700","index":6,"yAxisValue":600,"value":[0,0],"low":600,"up":700},{"label":"700 - 800","index":7,"yAxisValue":700,"value":[0,0],"low":700,"up":800},{"label":"800 - 900","index":8,"yAxisValue":800,"value":[0,0],"low":800,"up":900},{"label":"900 - 1000","index":9,"yAxisValue":900,"value":[0,0],"low":900,"up":1000},{"label":"1000 - 1100","index":10,"yAxisValue":1000,"value":[1,0],"low":1000,"up":1100}]
}

const testData2 = {
  mode: 'discrete',
  width: 160,
  height: 180,
  barHeight: 20,
  barGap: 1,
  totalCount: 72,
  totalValue: 28,
  overviewOptions: [[4,0],[1,0],[1,0],[2,0],[33,0],[1,0],[1,0],[1,0],[1,0],[1,0],[1,0],[1,0],[1,0],[1,0],[1,0],[1,0],[1,0],[1,0],[1,0],[1,0],[1,0],[1,0],[1,0],[1,0],[1,0],[1,0],[9,0],[1,0]],
  value: [{'label':'Australia','index':0,'yAxisValue':'Australia','value':[4,0]},{'label':'Belgium','index':1,'yAxisValue':'Belgium','value':[1,0]},{'label':'Cambodia','index':2,'yAxisValue':'Cambodia','value':[1,0]},{'label':'Canada','index':3,'yAxisValue':'Canada','value':[2,0]},{'label':'China','index':4,'yAxisValue':'China','value':[33,0]},{'label':'Cruise Ship','index':5,'yAxisValue':'Cruise Ship','value':[1,0]},{'label':'Egypt','index':6,'yAxisValue':'Egypt','value':[1,0]},{'label':'Finland','index':7,'yAxisValue':'Finland','value':[1,0]},{'label':'France','index':8,'yAxisValue':'France','value':[1,0]},{'label':'Germany','index':9,'yAxisValue':'Germany','value':[1,0]},{'label':'India','index':10,'yAxisValue':'India','value':[1,0]},{'label':'Italy','index':11,'yAxisValue':'Italy','value':[1,0]},{'label':'Japan','index':12,'yAxisValue':'Japan','value':[1,0]},{'label':'Korea, South','index':13,'yAxisValue':'Korea, South','value':[1,0]},{'label':'Malaysia','index':14,'yAxisValue':'Malaysia','value':[1,0]},{'label':'Nepal','index':15,'yAxisValue':'Nepal','value':[1,0]},{'label':'Philippines','index':16,'yAxisValue':'Philippines','value':[1,0]},{'label':'Russia','index':17,'yAxisValue':'Russia','value':[1,0]},{'label':'Singapore','index':18,'yAxisValue':'Singapore','value':[1,0]},{'label':'Spain','index':19,'yAxisValue':'Spain','value':[1,0]},{'label':'Sri Lanka','index':20,'yAxisValue':'Sri Lanka','value':[1,0]},{'label':'Sweden','index':21,'yAxisValue':'Sweden','value':[1,0]},{'label':'Taiwan*','index':22,'yAxisValue':'Taiwan*','value':[1,0]},{'label':'Thailand','index':23,'yAxisValue':'Thailand','value':[1,0]},{'label':'United Arab Emirates','index':24,'yAxisValue':'United Arab Emirates','value':[1,0]},{'label':'United Kingdom','index':25,'yAxisValue':'United Kingdom','value':[1,0]},{'label':'US','index':26,'yAxisValue':'US','value':[9,0]},{'label':'Vietnam','index':27,'yAxisValue':'Vietnam','value':[1,0]}]
}

describe('ColumnStatistics.vue', () => {
  const wrapper = mount(ColumnStatistics, {
    propsData: {
      ...testData1
    }
  })

  it('continuous render and click event', () => {
    expect(wrapper.vm.tickPoints.length).toBe(3)
    expect(wrapper.vm.tickPoints[0].labelStyle.top).toBe('-8px')
    expect(wrapper.vm.tickPoints[0].tickStyle.top).toBe('0px')
    expect(wrapper.vm.tickPoints[2].labelStyle.top).toBe('72px')
    expect(wrapper.find('.range-box').exists()).toBe(false)

    const bar = wrapper.find('.detail-outside')
    expect(wrapper.vm.tooltipVisible).toBe(false)
    bar.trigger('mouseenter')
    expect(wrapper.vm.tooltipVisible).toBe(true)
    expect(wrapper.vm.tooltipData.value).toBe(498)
    expect(wrapper.vm.tooltipData.percent).toBe('99%')
  })

  it('discrete render and click event', async () => {
    await wrapper.setProps(testData2)
    await wrapper.setData({
      tooltipVisible: false
    })
    expect(wrapper.find('.range-box').exists()).toBe(true)

    const bar = wrapper.find('.detail-outside')
    bar.trigger('click')
    expect(wrapper.emitted()['bar-click']).toBeTruthy()
    expect(wrapper.emitted()['bar-click'].length).toBe(1)

    expect(wrapper.vm.tooltipVisible).toBe(false)
    bar.trigger('mouseenter')
    expect(wrapper.vm.tooltipVisible).toBe(true)
    expect(wrapper.vm.tooltipData.label).toBe('Australia')
    expect(wrapper.vm.tooltipData.value).toBe(4)
    expect(wrapper.vm.tooltipData.percent).toBe('5%')
    

    const rangeBar = wrapper.findAll('.range-item').at(0)
    rangeBar.trigger('click')
    expect(wrapper.emitted()['range-bar-click']).toBeTruthy()
    expect(wrapper.emitted()['range-bar-click'].length).toBe(1)

  })
})
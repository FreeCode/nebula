import { createLocalVue, shallowMount } from "@vue/test-utils"
import DataFilter from '../../../src/components/graph-analysis/right-drawer/DataFilter.vue'
import Vue from 'vue'
import Vuex from 'vuex'
import {Icon, Tooltip} from 'ant-design-vue'
import {queryFilterPipelineByGraphId, addFilterPipeline} from '../../../src/api/graph-analysis'

const data0 = {
    gmtCreator: '135',
    gmtModifier: '135',
    gmtCreate: '2021-07-06T10:53:28',
    gmtModify: '2021-07-06T10:53:28',
    id: 853,
    name: '过滤1',
    projectId: 1010,
    graphId: 2388,
    userId: 135
}

const data1 = {
    gmtCreator: "135",
    gmtModifier: "135",
    gmtCreate: null,
    gmtModify: null,
    id: 909,
    name: "过滤2",
    projectId: 1010,
    graphId: 2388,
    userId: 135,
    data: null
}

let data = [data0]

jest.mock('../../../src/api/graph-analysis')
queryFilterPipelineByGraphId.mockImplementation(()=>{
    return {
        data:{
            code: 100,
            success: true,
            result:data
        }
    }
})

addFilterPipeline.mockImplementation(()=>{
    data.push(data1)
    return {
        data:{
            code: 100,
            success: true,
            result:data
        }
    }
})

const localVue = createLocalVue()
localVue.use(Icon)
localVue.use(Tooltip)
localVue.use(Vuex)

Vue.component(
    'AIconFont',
    Icon.createFromIconfontCN({
      scriptUrl: '//at.alicdn.com/t/font_1566179_nlydkel94un.js', // at.alicdn.com/t/font_1566179_1skd5tg1lja.js',
    })
  )


describe('DataFilter.vue', ()=>{
    let wrapper
    let store

    beforeEach(()=>{
        store = new Vuex.Store({
            modules:{
                namespaced: 'GraphAnalysis',
                actions:{
                    addGraphFilterPipline: jest.fn(()=>{})
                }
            }
        })
        wrapper = shallowMount(DataFilter, {
            propsData:{

            },
            localVue,
            store
        })
    })

    afterEach(()=>{
        wrapper.destroy()
    })

    it('初始化，1个过滤组', async ()=>{
        await Vue.nextTick()
        expect(wrapper.findAll('.filter-groups filter-group-stub').length).toBe(1)
        expect(wrapper.findAll('.filter-groups').length).toBe(1)
    })

    it('添加一个过滤组', async ()=>{
        await wrapper.find('.add-filter').trigger('click')
        expect(wrapper.findAll('.filter-groups filter-group-stub').length).toBe(2)
    })
})
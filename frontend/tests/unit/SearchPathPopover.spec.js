import { shallowMount, createLocalVue } from '@vue/test-utils'
import SearchPathPopover from '../../src/components/graph/SearchPathPopover.vue'
import { Vue } from 'vue-property-decorator'

import {
  Popover,
  Button,
  InputNumber,
  Radio,
  Tooltip,
  Icon
} from 'ant-design-vue'

Vue.component(
  'AIconFont',
  Icon.createFromIconfontCN({
    scriptUrl: '//at.alicdn.com/t/font_1566179_nlydkel94un.js',
  })
)

const localVue = createLocalVue()

localVue.use(Popover)
localVue.use(Button)
localVue.use(InputNumber)
localVue.use(Radio)
localVue.use(Radio.Group)
localVue.use(Tooltip)
localVue.use(Icon)

// 没有选择节点的情况
const propData = {  //  正常打开情况下
  selectNodesInfo: [], // 选择的节点
  show: false,  // 显示
  queryMode: 1 // 查询模式
}

// 选择了两个节点
const propData2 = {  // 编辑状态下
  selectNodesInfo: [
    {
      _cfg: {
        model: {
          style: {
            fill: '#372891'
          },
          label: 'node0'
        }
      }
    },
    {
      _cfg: {
        model: {
          style: {
            fill: '#783722'
          },
          label: 'node1'
        }
      }
    }

  ], // 选择的节点
  show: true,  // 显示
  queryMode: 1 // 查询模式
}

describe('SearchPathPopover.vue', () => {
  let wrapper = null
  beforeEach(() => {
    wrapper = shallowMount(SearchPathPopover, {
      // 伪造传入的 props
      propsData: {
        ...propData
      },
      data() {
        return {
          searchPathPattern: 1, // 配置项 1-最短， 2-全部
          searchPathStepLength: 2, // 最大步长
        }
      },
      localVue,
    })
  })

  afterEach(() => {
    wrapper.destroy()
  })

  it('点击取消',  async () => {
    await wrapper.setProps(propData2)
    const cancel = wrapper.find('.search-path-btn').findAll('.set-btn').at(0)
    await cancel.trigger('click')
    wrapper.vm.$emit('visible-change', false)
    expect(wrapper.emitted('visible-change')).toBeTruthy()
    expect(wrapper.emitted('visible-change')[0]).toEqual([false]);

  })

  it('选择节点', async () => {
    await wrapper.setProps(propData2)
    const node0 = wrapper.find('.selected-node').findAll('.node-itme').at(0).find('.node-name')
    expect(node0.text()).toBe('node0')

    const node1 = wrapper.find('.selected-node').findAll('.node-itme').at(1).find('.node-name')
    expect(node1.text()).toBe('node1')
  })

  it('正常点击确认',  async () => {
    // 切换正常选择节点后
    await wrapper.setProps(propData2)
    const confirm = wrapper.find('.search-path-btn').findAll('.set-btn').at(1)
    await confirm.trigger('click')
    wrapper.vm.$emit('confirm', {type: wrapper.vm.searchPathPattern, maxStep: wrapper.vm.searchPathStepLength})
    expect(wrapper.emitted('confirm')).toBeTruthy()
    expect(wrapper.emitted('confirm')[0]).toEqual([{type: 1, maxStep: 2}]);   // 默认情况就是
    expect(wrapper.vm.selectNodesInfo.length).toBe(2);  // 正常节点数量2
  })

  it('点击确认（无输入步长）',  async () => {
    // 有选择节点
    await wrapper.setProps(propData2)
    // 设置步长为空
    await wrapper.setData({ searchPathStepLength: '' })
    const confirm = wrapper.find('.search-path-btn').findAll('.set-btn').at(1)
    await confirm.trigger('click')
    wrapper.vm.$emit('confirm', {type: wrapper.vm.searchPathPattern, maxStep: wrapper.vm.searchPathStepLength})
    expect(wrapper.emitted('confirm')).toBeTruthy()
    expect(wrapper.emitted('confirm')[0]).toEqual([{type: 1, maxStep: ''}]);
  })

  it('没有选择节点提示空状态', () => {
    const empty = wrapper.find('.selected-node').find('.empty-node')
    expect(empty.exists()).toBe(true);
    expect(empty.text()).toBe('--请先选择2个节点--');
  })

  it('点击确认（无选择节点）',  async () => {
    // 直接确认
    const confirm = wrapper.find('.search-path-btn').findAll('.set-btn').at(1);
    await confirm.trigger('click')
    expect(wrapper.vm.selectNodesInfo.length).toBe(0);  // 节点长度 0
  })

})

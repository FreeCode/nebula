import { shallowMount, createLocalVue } from '@vue/test-utils'
import AggregateFunction from '../../src/components/studio/data/node-edit-panel/AggregateFunction.vue'
import {
  Select
} from 'ant-design-vue'
//  看dev 功能点
//  准备输入； prop, computed
//  思考输出，挑选可以测试的点；

//  createLocalVue 返回一个 Vue 的类供你添加组件、混入和安装插件而不会污染全局的 Vue 类。
//  例如 vue-router, vuex等

const localVue = createLocalVue()

localVue.use(Select)

const colList = ["小数","字符串","单词","特殊字符","时间格式"]

const propData = {  //  正常打开情况下
  aggreType: 'sum',
  column: {
    desc: 'int',
    name: '整数',
    semantic: null,
    type: -5
  },
  editData: {},
  isEdit: false
}

const propData2 = { //   编辑状态下
  editData: {"projectId":942,"taskId":14424,"taskName":"清洗","data":{"groupByCol":"类别","col":"整数","actionType":"aggreFunc","description":"avg","action":"GROUP_BY","label":"分组聚合","id":103274,"gmtCreate":"2021-05-19 14:53:02","resultType":"2","aggrfun":"avg","table":"pipeline.view_tclean_14424_1621407182967","status":"SUCCESS"}},
  isEdit: true
}

describe('AggregateFunction.vue', () => {
  let wrapper = null
  beforeEach(() => {
    wrapper = shallowMount(AggregateFunction, {
      propsData: {
        ...propData
      },
      localVue,
      computed: { //  用 computed 来模拟 getters
        tableColumns: () => [{"semantic":null,"name":"整数","type":-5,"desc":"int","_stamp":1621391462027},{"semantic":null,"name":"小数","type":2,"desc":"decimal","_stamp":1621391462027},{"semantic":null,"name":"字符串","type":-1,"desc":"varchar","_stamp":1621391462027},{"semantic":null,"name":"单词","type":-1,"desc":"varchar","_stamp":1621391462027},{"semantic":null,"name":"特殊字符","type":-1,"desc":"varchar","_stamp":1621391462027},{"semantic":null,"name":"时间格式","type":93,"desc":"date","_stamp":1621391462027}]
      },
    })
  })

  afterEach(() => {
    wrapper.destroy()
  })


  it('aggrefunc data correct', () => {
    //  测试funcType是否正确
    expect(wrapper.vm.funcType).toBe('sum')
    //  测试columnsList是否正确
    expect(wrapper.vm.columnsList).toEqual(colList)
    //  测试select value 是否正确
    expect(wrapper.find('.func-box').find('a-select-stub').attributes().value).toBe('sum')
  })

  it('render correctly', () => {
    //  test snapshot html structure
    expect(wrapper).toMatchSnapshot()
  })

  it('test edit mode for aggreFunc.vue', async () => {
    await wrapper.setProps(propData2)

    expect(wrapper.vm.funcType).toBe('avg')
    expect(wrapper.vm.generateType).toBe('2')
    expect(wrapper.vm.groupbyCol).toBe('类别')

    //  aggrefunc value 是否正确
    expect(wrapper.find('.func-box').find('a-select-stub').attributes().value).toBe('avg')
    //  type value 是否正确
    expect(wrapper.find('.type-box').find('a-select-stub').attributes().value).toBe('2')
    //  groupby value 是否正确
    expect(wrapper.find('.groupby-box').find('a-select-stub').attributes().value).toBe('类别')
  })
})
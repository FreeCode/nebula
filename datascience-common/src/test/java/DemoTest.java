import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

import static com.alibaba.fastjson.serializer.SerializerFeature.QuoteFieldNames;
import static com.alibaba.fastjson.serializer.SerializerFeature.WriteNonStringValueAsString;

public class DemoTest {
    private static List<String> errorPatterns = new ArrayList<>();

    static {

        //全小写
//        errorPatterns.add("fail\\s{1,}to\\s{1,}execute\\s{1,}line\\s{1,}\\d{1,}");
//        errorPatterns.add("\\s{1,}file\\s{1,}\\\"([^\\\"]*)\\\",\\s{1,}line\\s{1,}\\d{1,}");
        errorPatterns.add("\\s{1,}line\\s{1,}(\\d{1,})");
    }

    private static String splitAndMinus(String line){
        //you will get ["", "line", "3"]
        String[] parts = line.split(" ");
        if (parts.length >= 2){
            int parseInt = Integer.parseInt(parts[parts.length - 1].trim());
            parseInt -= 1;
            return " " + parts[1] + " " + parseInt;
        }
        return line;
    }

    /**
     * 自定义替换，
     *
     * @param matcher
     * @param replacement
     * @param isUnique
     * @return
     */
    private static String replaceAll(Matcher matcher, String replacement, Boolean isUnique) {
        matcher.reset();
        boolean result = matcher.find();
        if (result) {
            StringBuffer sb = new StringBuffer();
            do {
                if (isUnique) {
                    //输入unique 可以部指定replacement
                    replacement = splitAndMinus(matcher.group());
                }
                matcher.appendReplacement(sb, replacement);
                result = matcher.find();
            } while (result);
            matcher.appendTail(sb);
            return sb.toString();
        }
        return matcher.toString();
    }
    private String extractErrorInfo(String paragraph) {
        String lowerScript = paragraph.toLowerCase();
        String result = "";
        for (String pattern : errorPatterns) {
            Pattern p = Pattern.compile(pattern);
            Matcher m = p.matcher(lowerScript);
            while (m.find()) {
                result = m.group().trim();
            }
        }
        return result;
    }

    private static String wrapperErrorMsg(String script) {
        Pattern p = Pattern.compile(errorPatterns.get(0));
        Matcher m = p.matcher(script);
        script = replaceAll(m, "", Boolean.TRUE);
        return script;
    }

    public static void main(String[] args) {
//        String text = " def  myname(a, b, c): {\n print(1)}\n def test2():{ print(2) }";
//        List<String> lis = ToolUtil.extractPythonFunctionName(text);
//        for (String item: lis) {
//            System.out.println(item);
//            List<String> strings = ToolUtil.extractParamsForFunc(item);
//            if (strings != null) {
//                for (String e: strings) {
//                    System.out.println(e);
//                }
//            }
//        }

//        String HEADER_SNIPPET = "%pyspark\n";
//
//        String text = "from utilTool import *\n"
//            + "import sys\n"
//            + "\n"
//            + "\n"
//            + "# use dollar sign to get input data stream\n"
//            + "sourceTable = \"$$$convert$dataset.h_133_1614582535808_m\";\n"
//            + "# 注意调试的时候修改该变量的名字，否则可能报表已经存储错误\n"
//            + "targetTable = \"pipeline.solid_demo_temp_table123123213ccxvc222x22\"\n"
//            + "# '_record_id_' is a necessary column\n"
//            + "featureCols = \"_record_id_,area\"\n"
//            + "# 1. spark session环境准备\n"
//            + "spark = getOrCreateSparkSession()\n"
//            + "# 2. 读取数据, 从greenplum数据库中读取数据为spark dataset格式\n"
//            + "dataset = readFromGreenPlum(spark, sourceTable)\n"
//            + "cols = featureCols.split(\",\")\n"
//            + "# 3. 处理数据\n"
//            + "newDataset = dataset.select(cols)\n"
//            + "# 4. 存储数据, 存储数据到greenplum表中\n"
//            + "saveTableForGreenPlum(newDataset, targetTable)\n"
//            + "# 5. 返回结果信息\n"
//            + "ret = getResultMeta([targetTable], {\"feature_cols\": featureCols}, 0, \"success\")\n"
//            + "\n"
//            + "print(ret)\n";
//
//        String res = String.format("%s%s", HEADER_SNIPPET, text).replaceAll("\\$.*?(?=\\$)", "").replaceAll("\\$", "");
//        System.out.println(res);

//        DemoTest instance = new DemoTest();
//        ArrayList<String> list = new ArrayList<>();
//        list.add("Fail to execute line 12: print(ay)\n"
//                + "Traceback (most recent call last):\n"
//                + "  File \"/mnt/data1/yarn/nm/usercache/root/appcache/application_1601361253087_3649/container_1601361253087_3649_01_000001/tmp/1625106614865-0/zeppelin_python.py\", line 158, in \n"
//                + "    exec(code, _zcUserQueryNameSpace)\n"
//                + "  File \"\", line 3, in \n"
//                + "NameError: name 'ay' is not defined");
//
//        list.add("Traceback (most recent call last):\n"
//                + "  File \"<stdin>\", line 1, in <module>\n"
//                + "NameError: HiThere)");
//
//        list.add("Traceback (most recent call last):\n"
//                + "  File \"<stdin>\", line 2, in <module>\n"
//                + "  File \"<stdin>\", line 2, in func\n"
//                + "OSError\n"
//                + "\n"
//                + "The above exception was the direct cause of the following exception:\n"
//                + "\n"
//                + "Traceback (most recent call last):\n"
//                + "  File \"<stdin>\", line 4, in <module>\n"
//                + "RuntimeError: Failed to open database");
//
//        list.add("Traceback (most recent call last):\n"
//                + "  File \"<stdin>\", line 1, in <module>\n"
//                + "  File \"<stdin>\", line 3, in divide\n"
//                + "TypeError: unsupported operand type(s) for /: 'str' and 'str'");
//
//        list.add("Traceback (most recent call last):\n"
//                + "  File \"E:/PycharmProjects/ProxyPool-master/proxypool/test.py\", line 4, in <module>\n"
//                + "    1/0\n"
//                + "ZeroDivisionError: division by zero");
//        list.add("Traceback (most recent call last):\n"
//                + "  File \"D:\\Projects\\test_py\\auto_completed.py\", line 91, in <module>\n"
//                + "    test_AutoComplete()\n"
//                + "  File \"D:\\Projects\\test_py\\auto_completed.py\", line 74, in test_AutoComplete\n"
//                + "    ac.add('allen', 'wind')\n"
//                + "  File \"D:\\Projects\\test_py\\auto_completed.py\", line 14, in add\n"
//                + "    pipeline.lrem(ac_list, value)\n"
//                + "TypeError: lrem() missing 1 required positional argument: 'value'");
//
//
//        list.stream().map(str -> {
//            String wrapperMsg = wrapperErrorMsg(str);
//            System.out.println(wrapperMsg);
//            System.out.println();
//            String matchedResult = instance.extractErrorInfo(wrapperMsg);
//            System.out.println(matchedResult.split(" ")[1]);
//            return str;
//        }).collect(Collectors.toList());

//        JSONObject mappingJson = new JSONObject();
//        Map<Long, Long> map = new HashMap<Long, Long>();
//        map.put(111L, 222L);
//        map.put(222l, 444l);
//        map.put(333l, 555l);
//        mappingJson.put("test", MapUtil.reMap(map, String.class));
//        System.out.println(mappingJson.toJSONString());


    }
}

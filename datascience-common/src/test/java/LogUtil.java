//
//package org.zjvis.datascience.common.util;
//
//import ch.qos.logback.classic.Level;
//import cn.hutool.core.util.ArrayUtil;
//import com.alibaba.fastjson.JSON;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.commons.lang3.exception.ExceptionUtils;
//import org.slf4j.MDC;
//import org.slf4j.helpers.MessageFormatter;
//import org.springframework.util.StringUtils;
//import org.zjvis.datascience.common.aspect.LogAspect;
//import org.zjvis.datascience.common.dto.LogInfo;
//import org.zjvis.datascience.common.enums.LogEnum;
//
//import java.util.Arrays;
//import java.util.UUID;
//
///**
// * @description 日志工具类
// * @date 2020-06-29
// */
//@Slf4j
//public class LogUtil {
//
//    public static final int LOG_STACK_TRACE_SIZE = 6;
//    public static final int LOG_STRING_SIZE = 2000;
//
//    /**
//     * info级别的日志
//     *
//     * @param logType 日志类型
//     * @param object  打印的日志参数
//     * @return void
//     */
//
//    public static void info(LogEnum logType, Object... object) {
//        logHandle(logType, Level.INFO, object);
//    }
//
//    /**
//     * debug级别的日志
//     *
//     * @param logType 日志类型
//     * @param object  打印的日志参数
//     * @return void
//     */
//    public static void debug(LogEnum logType, Object... object) {
//        logHandle(logType, Level.DEBUG, object);
//    }
//
//    /**
//     * error级别的日志
//     *
//     * @param logType 日志类型
//     * @param object  打印的日志参数
//     * @return void
//     */
//    public static void error(LogEnum logType, Object... object) {
//        errorObjectHandle(object);
//        logHandle(logType, Level.ERROR, object);
//    }
//
//    /**
//     * warn级别的日志
//     *
//     * @param logType 日志类型
//     * @param object  打印的日志参数
//     * @return void
//     */
//    public static void warn(LogEnum logType, Object... object) {
//        logHandle(logType, Level.WARN, object);
//    }
//
//    /**
//     * trace级别的日志
//     *
//     * @param logType 日志类型
//     * @param object  打印的日志参数
//     * @return void
//     */
//    public static void trace(LogEnum logType, Object... object) {
//        logHandle(logType, Level.TRACE, object);
//    }
//
//    /**
//     * 日志处理
//     *
//     * @param logType 日志类型
//     * @param level   日志级别
//     * @param object  打印的日志参数
//     * @return void
//     */
//    private static void logHandle(LogEnum logType, Level level, Object[] object) {
//        LogInfo logInfo = generateLogInfo(logType, level, object);
//        String logInfoJsonStr = logJsonStringLengthLimit(logInfo);
//        switch (Level.toLevel(logInfo.getLevel()).levelInt) {
//            case Level.TRACE_INT:
//                log.trace(logInfoJsonStr);
//                break;
//            case Level.DEBUG_INT:
//                log.debug(logInfoJsonStr);
//                break;
//            case Level.INFO_INT:
//                log.info(logInfoJsonStr);
//                break;
//            case Level.WARN_INT:
//                log.warn(logInfoJsonStr);
//                break;
//            case Level.ERROR_INT:
//                log.error(logInfoJsonStr);
//                break;
//            default:
//        }
//
//    }
//
//    /**
//     * 日志信息组装的内部方法
//     *
//     * @param logType 日志类型
//     * @param level   日志级别
//     * @param object  打印的日志参数
//     * @return LogInfo
//     */
//    private static LogInfo generateLogInfo(LogEnum logType, Level level, Object[] object) {
//        LogInfo logInfo = new LogInfo();
//        // 日志类型检测
//        if (!LogEnum.isLogType(logType)) {
//            level = Level.ERROR;
//            object = new Object[1];
//            object[0] = "log type【".concat(String.valueOf(logType)).concat("】error！");
//            logType = LogEnum.SYS_ERR;
//        }
//
//        // 获取trace_id
//        if (StringUtils.isEmpty(MDC.get(LogAspect.TRACE_ID))) {
//            MDC.put(LogAspect.TRACE_ID, UUID.randomUUID().toString());
//        }
//        // 设置logInfo的level,type,traceId属性
//        logInfo.setLevel(level.levelStr).setType(logType.toString()).setTraceId(MDC.get(LogAspect.TRACE_ID));
//        // 设置logInfo的堆栈信息
//        setLogStackInfo(logInfo);
//        // 设置logInfo的info信息
//        setLogInfo(logInfo, object);
//        // 截取loginfo的长度并转换成json字符串
//        return logInfo;
//    }
//
//    /**
//     * 设置loginfo的堆栈信息
//     *
//     * @param logInfo 日志对象
//     */
//    private static void setLogStackInfo(LogInfo logInfo) {
//        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
//        if (elements.length >= LOG_STACK_TRACE_SIZE) {
//            logInfo.setCName(elements[LOG_STACK_TRACE_SIZE - 1].getClassName())
//                    .setMName(elements[LOG_STACK_TRACE_SIZE - 1].getMethodName())
//                    .setLine(String.valueOf(elements[LOG_STACK_TRACE_SIZE - 1].getLineNumber()));
//        }
//    }
//
//    /**
//     * 限制log日志的长度并转换成json
//     *
//     * @param logInfo 日志对象
//     * @return String
//     */
//    private static String logJsonStringLengthLimit(LogInfo logInfo) {
//        try {
//            if (logInfo.getInfo()!=null&&logInfo.getInfo().toString().length() <= LOG_STRING_SIZE) {
//                return JSON.toJSONString(logInfo);
//            }
//            if (logInfo.getType().equals(LogEnum.SYS_ERR.toString())) {
//                logInfo.setInfo(
//                        logInfo.getInfo().toString().substring(0, LOG_STRING_SIZE));
//                return JSON.toJSONString(logInfo);
//            }
//            logInfo.setInfo(logInfo.getInfo().toString().substring(0, LOG_STRING_SIZE));
//            return JSON.toJSONString(logInfo);
//        } catch (Exception e) {
//            logInfo.setLevel(Level.ERROR.levelStr).setType(LogEnum.SYS_ERR.toString())
//                    .setInfo("cannot serialize exception: " + ExceptionUtils.getStackTrace(e));
//            return JSON.toJSONString(logInfo);
//        }
//    }
//
//    /**
//     * 设置日志对象的info信息
//     *
//     * @param logInfo 日志对象
//     * @param object  打印的日志参数
//     */
//    private static void setLogInfo(LogInfo logInfo, Object[] object) {
//        if (object.length > 1) {
//            logInfo.setInfo(MessageFormatter.arrayFormat(object[0].toString(),
//                    Arrays.copyOfRange(object, 1, object.length)).getMessage());
//            return;
//        }
//        if (ArrayUtil.isEmpty(object)) {
//            logInfo.setInfo("");
//            return;
//        }
//        if (object.length == 1 && object[0] instanceof Exception) {
//            logInfo.setInfo((ExceptionUtils.getStackTrace((Exception) object[0])));
//            log.error((ExceptionUtils.getStackTrace((Exception) object[0])));
//            return;
//        }
//        logInfo.setInfo(object[0] == null ? "" : object[0]);
//    }
//
//    /**
//     * 处理Exception的情况
//     *
//     * @param object 打印的日志参数
//     */
//    private static void errorObjectHandle(Object[] object) {
//        if (object.length >= 2) {
//            object[0] = String.valueOf(object[0]).concat("{}");
//        }
//
//        if (object.length == 2 && object[1] instanceof Exception) {
//            log.error(String.valueOf(object[0]), (Exception) object[1]);
//            object[1] = ExceptionUtils.getStackTrace((Exception) object[1]);
//            return;
//        }
//        if (object.length < 3) {
//            return;
//        }
//
//        log.error(String.valueOf(object[0]),
//                Arrays.copyOfRange(object, 1, object.length));
//        for (int i = 0; i < object.length; i++) {
//            if (object[i] instanceof Exception) {
//                object[i] = ExceptionUtils.getStackTrace((Exception) object[i]);
//            }
//        }
//    }
//
//}

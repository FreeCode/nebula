package org.zjvis.datascience.common.constant;

/**
 * @description 节点JSON数据常量类
 * @date 2020-07-23
 */
public class DataJsonConstant {
    //Task

    public static final String PARENT_TIMESTAMP_ARRAY_HEADER = "parentTimeStamps";

    public static final String LAST_TIMESTAMP_HEADER = "lastTimeStamp";

    public static final String TASK_ALG_TYPE = "algType";

    public static final String FORBIDDEN_FLAG = "forbidden";

    //TaskInstance
    public static final String INSTANCE_JSON_HEADER = "inputInfo";

    public static final String INSTANCE_SET_PARAM_HEADER = "setParams";

    //input & output
    public static final String INPUT_HEADER = "input";

    public static final String OUTPUT_HEADER = "output";

    public static final String TABLE_COLS_IDS = "tableCols";

    public static final String COL_TYPES_IDS = "columnTypes";

    public static final String TABLE_NAME_IDX = "tableName";


}

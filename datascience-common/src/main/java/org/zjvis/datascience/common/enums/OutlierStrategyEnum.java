package org.zjvis.datascience.common.enums;

/**
 * @description 用异常值检测枚举类
 * @date 2021-09-15
 */
public enum OutlierStrategyEnum {

    KNN("knn", ""),
    AVG("avg", ""),
    MANUAL("manual", ""),
    MULTIPLE_IMPUTATION("multiple_imputation", "");

    private String name;

    private String desc;

    OutlierStrategyEnum(String name, String desc) {
        this.desc = desc;
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public String getVal() {
        return name;
    }
}

package org.zjvis.datascience.common.vo.project;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

/**
 * @description 渲染项目数据集相关VO
 * @date 2020-07-31
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoadDatasetVO {

    @NotNull(message = "数据集不能为空")
    @Size(min = 1, message = "数据集不能为空")
    private Set<Long> datasetIds;

    @NotNull(message = "项目id不能为空")
    @Min(value = 1, message = "请输入有效项目id")
    private Long projectId;

    private static final long serialVersionUID = 1L;

}

package org.zjvis.datascience.common.vo.notice;

import java.time.LocalDateTime;
import lombok.Data;

/**
 * @description 用户反馈查询相关VO
 * @date 2021-09-15
 */
@Data
public class NoticeQueryVO {
    private Long id;
    private String title;
    private Long senderId;
    private Long receiverId;
    private String content;
    private Long projectId;
    private String projectName;
    private Integer type;
    private Integer status;
    protected LocalDateTime gmtCreate;
    protected LocalDateTime gmtModify;
}

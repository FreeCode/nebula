package org.zjvis.datascience.common.exception;

/**
 * @description union联结操作通用异常类
 * @date 2021-08-03
 */
public class UnionStatementParserException extends Exception {

    public UnionStatementParserException(String msg) {
        super("[UnionStatementParserException]" + msg);
    }
}

package org.zjvis.datascience.common.enums;

import lombok.Getter;

/**
 * @description 用户提醒类别枚举类
 * @date 2021-09-02
 */
@Getter
public enum NoticeFilterTypeEnum {
    all(1, "全部类别"),
    system(2, "系统消息"),
    owner(3, "与我相关"),
    project(4, "项目消息");

    private int type;
    private String name;

    NoticeFilterTypeEnum(int type, String name) {
        this.type = type;
        this.name = name;
    }
}

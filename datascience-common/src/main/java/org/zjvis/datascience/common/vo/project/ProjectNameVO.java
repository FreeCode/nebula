package org.zjvis.datascience.common.vo.project;

import cn.weiguangfu.swagger2.plus.annotation.ApiRequestInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @description 项目数据集Name查询相关VO
 * @date 2021-11-03
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProjectNameVO {

    @ApiRequestInclude(groups = {ProjectNameVO.ProjectId.class})
    @NotNull(message = "项目id不能为空", groups = ProjectNameVO.ProjectId.class)
    @Min(value = 1, message = "请输入有效项目id", groups = ProjectNameVO.ProjectId.class)
    private Long projectId;

    private String searchName;

    private Long userId;

    public ProjectNameVO(Long projectId){
        this.setProjectId(projectId);
        this.setUserId(null);
        this.setSearchName(null);
    }

    public interface ProjectId {
    }
}

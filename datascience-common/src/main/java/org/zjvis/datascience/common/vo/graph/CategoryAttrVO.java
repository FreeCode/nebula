package org.zjvis.datascience.common.vo.graph;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CategoryAttrVO implements Serializable {
    private String column;

    private String name;

    //according to java.sql.Types
    private int type;

    private String id;
}

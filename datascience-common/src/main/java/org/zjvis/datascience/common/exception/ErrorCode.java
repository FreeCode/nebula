package org.zjvis.datascience.common.exception;

/**
 * @description 通用异常code接口
 * @date 2020-03-26
 */
public interface ErrorCode {

    /**
     * 错误码
     *
     * @return code
     */
    Integer getCode();

    /**
     * error info
     *
     * @return
     */
    String getMsg();

}

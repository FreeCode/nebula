package org.zjvis.datascience.common.enums;

import lombok.Getter;

/**
 * @description 数据透视表查看方式枚举类
 * @date 2021-11-11
 */
@Getter
public enum PivotTableMethodEnum {
    SUM("求和", "sum"),
    AVG("平均值", "avg"),
    MAX("最大值", "max"),
    MAIN("最小值",  "min"),
    COUNT("计数",  "count"),
    VARIANCE_SAMP("样本方差",  "var_samp"),
    STANDARD_DEVIATION_SAMP("样本标准差",  "stddev_samp"),
    VARIANCE_POP("总体方差",  "var_pop"),
    STANDARD_DEVIATION_POP("总体标准差",  "stddev_pop")
    ;

    private String name;
    private String method;

    PivotTableMethodEnum(String name, String method) {
        this.name = name;
        this.method = method;
    }
}

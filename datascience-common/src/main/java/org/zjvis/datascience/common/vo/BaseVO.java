package org.zjvis.datascience.common.vo;

import cn.weiguangfu.swagger2.plus.annotation.ApiRequestExclude;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @description 通用父类VO （语序子类继承创建和修改时间）
 * @date 2021-07-23
 */
@Data
public class BaseVO implements Serializable {

    private static final long serialVersionUID = -5086608872601561541L;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((gmtCreator == null) ? 0 : gmtCreator.hashCode());
        result = prime * result + ((gmtModifier == null) ? 0 : gmtModifier.hashCode());
        result = prime * result + ((gmtCreate == null) ? 0 : gmtCreate.hashCode());
        result = prime * result + ((gmtModify == null) ? 0 : gmtModify.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        BaseVO other = (BaseVO) obj;
        if (gmtCreator == null) {
            if (other.gmtCreator != null) {
                return false;
            }
        } else if (!gmtCreator.equals(other.gmtCreator)) {
            return false;
        }

        if (gmtModifier == null) {
            if (other.gmtModifier != null) {
                return false;
            }
        } else if (!gmtModifier.equals(other.gmtModifier)) {
            return false;
        }

        if (gmtCreate == null) {
            if (other.gmtCreate != null) {
                return false;
            }
        } else if (!gmtCreate.equals(other.gmtCreate)) {
            return false;
        }

        if (gmtModify == null) {
            if (other.gmtModify != null) {
                return false;
            }
        } else if (!gmtModify.equals(other.gmtModify)) {
            return false;
        }
        return true;
    }

    @ApiRequestExclude(groups = {Ignore.class})
    private String gmtCreator;
    @ApiRequestExclude(groups = {Ignore.class})
    private String gmtModifier;
    @ApiRequestExclude(groups = {Ignore.class})
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime gmtCreate;
    @ApiRequestExclude(groups = {Ignore.class})
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime gmtModify;

    public interface Ignore {
    }

}

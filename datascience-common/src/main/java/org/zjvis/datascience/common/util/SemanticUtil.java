package org.zjvis.datascience.common.util;

import java.util.ArrayList;
import java.util.List;

/**
 * @description 字段语义工具类
 * @date 2021-09-15
 */
public class SemanticUtil {
    public static List<String> semanticItems(String key, List<String> semanticList, String aPrefix,
                                             String bPrefix) {
        List<String> sqlItems = new ArrayList<>();
        for (String item : semanticList) {
            sqlItems.add(String.format("%s%s = %s%s", aPrefix, item, bPrefix, key));
        }
        return sqlItems;
    }
}

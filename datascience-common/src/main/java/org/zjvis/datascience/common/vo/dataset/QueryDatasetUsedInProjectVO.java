package org.zjvis.datascience.common.vo.dataset;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * @description 数据结果相关VO
 * @date 2021-12-23
 */
@Data
public class QueryDatasetUsedInProjectVO {

    @NotNull(message = "datasetId不能为空")
    @Min(value = 1, message = "请输入有效id")
    private Long datasetId;

    private Long userId;
}

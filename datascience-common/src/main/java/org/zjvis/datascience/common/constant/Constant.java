package org.zjvis.datascience.common.constant;

/**
 * @description 通用常量类
 * @date 2020-07-23
 */
public class Constant {

    public static final String CREATOR_FIELD = "gmtCreator";
    public static final String MODIFIER_FIELD = "gmtModifier";
    /**
     * 换行符
     */
    public static final String NEW_LINE_CHARACTER = "\n";

    /* excel */
    public static final String EXCEL_XLS = "xls";
    public static final String EXCEL_XLSX = "xlsx";

    /**错误的算子的logInfo的json*/
    public static String errorTpl = "{\"status\":500, \"error_msg\":\"%s\"}";

    /**
     * pipeline快照容量
     */
    public static final int PIPELINE_SNAPSHOT_CAPACITY = 10;

    /**
     * pipeline快照存放在minio中的bucket名
     */
    public static final String PIPELINE_SNAPSHOT_MINIO_BUCKET = "pipeline-snapshot";

    public static final String PIPELINE_SNAPSHOT_PICTURE_PREFIX = "data:image/png;base64,";

    /**
     * 随机数范围
     */
    public static final long RANDOM = 100;
}

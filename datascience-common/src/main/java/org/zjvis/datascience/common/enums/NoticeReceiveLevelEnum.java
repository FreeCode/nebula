package org.zjvis.datascience.common.enums;

import lombok.Getter;

/**
 * @description 用户提醒接受级别枚举类
 * @date 2021-09-03
 */
@Getter
public enum NoticeReceiveLevelEnum {
    all(1,"所有消息"),
    owner(2,"与自己相关"),
    //系统消息总是会全部接收，无论用户设置什么消息级别，都不会被屏蔽
    system(3,"屏蔽消息");

    private int level;
    private String name;
    NoticeReceiveLevelEnum(int type, String name){
        this.level = type;
        this.name = name;
    }
}

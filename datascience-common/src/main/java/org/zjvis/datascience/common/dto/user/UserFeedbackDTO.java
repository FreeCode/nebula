package org.zjvis.datascience.common.dto.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.zjvis.datascience.common.dto.BaseDTO;

import java.util.List;


/**
 * @description 用户反馈DTO
 * @date 2021-12-24
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Setter
public class UserFeedbackDTO extends BaseDTO {

    private static final long serialVersionUID = 8988101607356944917L;

    private Long id;

    private String name;

    private String realName;

    /*
    用户反馈信息
     */
    private Long userId;


    private String feedback; //研究领域


//    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnore
    private String feedbackPic; //用户反馈图片编码

    private List<String> feedbackPicList;

    private Integer status; //反馈已读/未读


}

package org.zjvis.datascience.common.vo;

import com.alibaba.fastjson.JSONArray;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @description 数据预览渲染相关VO
 * @date 2020-08-20
 */
@Data
public class DataPreviewVO {
    private Long taskId;

    private Long pipelineId;

    private Long pipelineInstanceId;

    private LocalDateTime createTime;

    private String name;

    private Long count;

    private JSONArray head;

    private JSONArray data;

}

package org.zjvis.datascience.common.vo;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.zjvis.datascience.common.dto.PipelineDTO;
import org.zjvis.datascience.common.util.DozerUtil;

/**
 * @description 数据视图数据表 Pipeline 相关VO
 * @date 2021-06-11
 */
@Data
public class PipelineVO {

    private Long id;

    private String name;

    private Long projectId;

    private Long userId;

    private String userName;

    private JSONObject data;

    public PipelineDTO toPipeline() {
        PipelineDTO task = DozerUtil.mapper(this, PipelineDTO.class);
        task.setDataJson(data.toJSONString());
        return task;
    }


}

package org.zjvis.datascience.common.formula.dto;

import lombok.*;

import java.util.List;

/**
 * @description 公式数据集渲染-可视化构建 文本控件 公式与数据集绑定的渲染类
 * @date 2021-12-01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class FormulaDatasetDTO {
    private long taskId;
    private String name;
    private String type;
    List<FormulaDTO> formulaList;
}

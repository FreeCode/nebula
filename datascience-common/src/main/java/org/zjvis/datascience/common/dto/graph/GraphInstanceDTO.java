package org.zjvis.datascience.common.dto.graph;

import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.zjvis.datascience.common.dto.BaseDTO;

import java.time.LocalDateTime;

/**
 * @description Graph分析构建 instanceDTO
 * @date 2021-12-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GraphInstanceDTO extends BaseDTO {
    private Long id;

    private Long graphId;

    private Long taskId;

    private Long pipelineId;

    private Long projectId;

    private Long userId;

    private String dataJson;

    private String status;

    private Long duringTime;

    private String logInfo;

    private Integer runTimes;

    private LocalDateTime gmtRunning;

    public GraphInstanceDTO(GraphDTO graph){
        this.setGraphId(graph.getId());
        this.setTaskId(graph.getTaskId());
        this.setPipelineId(graph.getPipelineId());
        this.setProjectId(graph.getProjectId());
        this.setUserId(graph.getUserId());
        JSONObject data = new JSONObject();
        this.setDataJson(data.toJSONString());
        this.setStatus("CREATE");
    }
}

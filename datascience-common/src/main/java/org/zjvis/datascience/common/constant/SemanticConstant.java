package org.zjvis.datascience.common.constant;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @description 语义信息管理常量
 * @date 2021-12-24
 */
public class SemanticConstant {

    public final static List<String> COUNTRY_COL = new ArrayList<>(
            Arrays.asList("zh", "zh_full", "en", "en_full", "iso3", "iso2", "phone"));

    public final static List<String> PROVINCE_COL = new ArrayList<>(Arrays.asList("zh", "zh_full", "zh_abbr", "en", "en_full", "en_abbr"));

    public final static List<String> CITY_COL = new ArrayList<>(Arrays.asList("zh_full", "zh", "en"));
}

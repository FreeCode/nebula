package org.zjvis.datascience.common.dto.datax;

import lombok.Data;

import java.util.List;

@Data
public class ReaderConnection extends Connection {
    private List<String> jdbcUrl;
}

package org.zjvis.datascience.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

/**
 * @description 项目中引用数据集信息表，项目数据DTO
 * @date 2021-09-07
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class DatasetProjectDTO extends BaseDTO implements Serializable {
    private Long id;

    private Long projectId;

    private Long datasetId;

    private String datasetName;

    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        DatasetProjectDTO other = (DatasetProjectDTO) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getProjectId() == null ? other.getProjectId() == null : this.getProjectId().equals(other.getProjectId()))
                && (this.getDatasetId() == null ? other.getDatasetId() == null : this.getDatasetId().equals(other.getDatasetId()))
                && (this.getGmtCreate() == null ? other.getGmtCreate() == null : this.getGmtCreate().equals(other.getGmtCreate()))
                && (this.getGmtModify() == null ? other.getGmtModify() == null : this.getGmtModify().equals(other.getGmtModify()))
                && (this.getGmtModifier() == null ? other.getGmtModifier() == null : this.getGmtModifier().equals(other.getGmtModifier()))
                && (this.getGmtCreator() == null ? other.getGmtCreator() == null : this.getGmtCreator().equals(other.getGmtCreator()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getProjectId() == null) ? 0 : getProjectId().hashCode());
        result = prime * result + ((getDatasetId() == null) ? 0 : getDatasetId().hashCode());
        result = prime * result + ((getGmtCreate() == null) ? 0 : getGmtCreate().hashCode());
        result = prime * result + ((getGmtModify() == null) ? 0 : getGmtModify().hashCode());
        result = prime * result + ((getGmtModifier() == null) ? 0 : getGmtModifier().hashCode());
        result = prime * result + ((getGmtCreator() == null) ? 0 : getGmtCreator().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", projectId=").append(projectId);
        sb.append(", datasetId=").append(datasetId);
        sb.append("]");
        return sb.toString();
    }
}

package org.zjvis.datascience.common.dto.notice;

import java.time.LocalDateTime;
import lombok.Data;

/**
 * @description 用户消息查询DTO
 * @date 2021-12-24
 */
@Data
public class NoticeQueryDTO {
    private Long id;
    private String title;
    private Long senderId;
    private Long receiverId;
    private String content;
    private String process;
    private Integer type;
    private Integer status;
    protected LocalDateTime gmtCreate;
    protected LocalDateTime gmtModify;
}

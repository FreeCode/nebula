package org.zjvis.datascience.common.exception;

/**
 * @description 数组下标越界通用异常类
 * @date 2021-06-08
 */
public class IndexOutBoundException extends Exception {

    public IndexOutBoundException(String msg) {
        super("[IndexOutBoundException]" + msg);
    }
}

package org.zjvis.datascience.common.widget.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.zjvis.datascience.common.dto.BaseDTO;
import org.zjvis.datascience.common.util.DozerUtil;
import org.zjvis.datascience.common.widget.vo.WidgetFavouriteVO;

import java.io.Serializable;

/**
 * @description widget 图表收藏数据表 图表收藏组件
 * @date 2021-12-14
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class WidgetFavouriteDTO extends BaseDTO implements Serializable {
    private Long id;

    private Long pipelineId;

    private Long widgetId;

    private WidgetDTO widget;

    private static final long serialVersionUID = 1L;

    public WidgetFavouriteVO view() {
        WidgetFavouriteVO vo = DozerUtil.mapper(this, WidgetFavouriteVO.class);
        return vo;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        WidgetFavouriteDTO other = (WidgetFavouriteDTO) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getPipelineId() == null ? other.getPipelineId() == null : this.getPipelineId().equals(other.getPipelineId()))
                && (this.getWidgetId() == null ? other.getWidgetId() == null : this.getWidgetId().equals(other.getWidgetId()))
                && (this.getGmtCreate() == null ? other.getGmtCreate() == null : this.getGmtCreate().equals(other.getGmtCreate()))
                && (this.getGmtModify() == null ? other.getGmtModify() == null : this.getGmtModify().equals(other.getGmtModify()))
                && (this.getGmtModifier() == null ? other.getGmtModifier() == null : this.getGmtModifier().equals(other.getGmtModifier()))
                && (this.getGmtCreator() == null ? other.getGmtCreator() == null : this.getGmtCreator().equals(other.getGmtCreator()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getPipelineId() == null) ? 0 : getPipelineId().hashCode());
        result = prime * result + ((getWidgetId() == null) ? 0 : getWidgetId().hashCode());
        result = prime * result + ((getGmtCreate() == null) ? 0 : getGmtCreate().hashCode());
        result = prime * result + ((getGmtModify() == null) ? 0 : getGmtModify().hashCode());
        result = prime * result + ((getGmtModifier() == null) ? 0 : getGmtModifier().hashCode());
        result = prime * result + ((getGmtCreator() == null) ? 0 : getGmtCreator().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", pipelineId=").append(pipelineId);
        sb.append(", widgetId=").append(widgetId);
        sb.append(", gmtCreator=").append(gmtCreator);
        sb.append("]");
        return sb.toString();
    }
}

package org.zjvis.datascience.common.dto.datasetsinfo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.zjvis.datascience.common.dto.BaseDTO;

import java.io.Serializable;
import java.util.Objects;

/**
 * @description 数据信息DTO
 * @date 2021-12-24
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class DatasetsInfoDTO extends BaseDTO implements Serializable {
    private Long id;

    private String name;

    private Long userId;

    private Long categoryId;

    private String dataType;

    private  String dataSource; // 存储数据源

    private Long rowNum;

    private Long totalSize;

    private String totalSizeString;

    private Boolean update;

}

package org.zjvis.datascience.common.strategy.operation;

import org.zjvis.datascience.common.strategy.SetValueStrategy;

/**
 * @description : 平均插补策略接口
 * @date 2021-09-15
 */
public class AVGStrategy implements SetValueStrategy {

    @Override
    public int doOperation(int num1, int num2) {
        return (int) ((num1 + num2) / 2);
    }
}

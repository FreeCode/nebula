package org.zjvis.datascience.common.exception;

import lombok.Getter;
import org.zjvis.datascience.common.model.ApiResult;
import org.zjvis.datascience.common.model.ApiResultCode;

/**
 * @description ETL-自动执行异常类
 * @date 2021-12-27
 */
@Getter
public class AutoTriggerException extends RuntimeException {

    private ApiResult apiResult;

    public AutoTriggerException(String message) {
        super(message);
        this.apiResult = ApiResult.valueOf(ApiResultCode.SYS_ERROR, message);
    }

    public AutoTriggerException(String message, Throwable cause) {
        super(message, cause);
        this.apiResult = ApiResult.valueOf(ApiResultCode.SYS_ERROR, message);
    }

    public AutoTriggerException(Throwable cause) {
        super(cause);
        this.apiResult = ApiResult.valueOf(ApiResultCode.SYS_ERROR);
    }

    public AutoTriggerException(Integer code, String msg, String info, Throwable cause) {
        super(msg, cause);
        if (info == null) {
            this.apiResult = new ApiResult(code, msg);
        } else {
            this.apiResult = new ApiResult(code, msg + ":" + info);
        }
    }

    public AutoTriggerException(ErrorCode errorCode, Throwable cause) {
        this(errorCode.getCode(), errorCode.getMsg(), null, cause);
    }

    public AutoTriggerException(ErrorCode errorCode, String info, Throwable cause) {
        this(errorCode.getCode(), errorCode.getMsg(), info, cause);
    }

    public static AutoTriggerException of(ErrorCode errorCode, String info) {
        return new AutoTriggerException(errorCode, info, null);
    }

    public static AutoTriggerException of(ErrorCode errorCode, String info, String tips) {
        AutoTriggerException e = new AutoTriggerException(errorCode, info, null);
        e.getApiResult().setTips(tips);
        return e;
    }

    public AutoTriggerException(ErrorCode errorCode) {
        this(errorCode, null);
    }

    public AutoTriggerException(Integer code, String msg) {
        this.apiResult = new ApiResult(code, msg);
    }
}

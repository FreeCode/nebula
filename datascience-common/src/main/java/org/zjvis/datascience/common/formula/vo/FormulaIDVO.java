package org.zjvis.datascience.common.formula.vo;

import lombok.Data;

/**
 * @description 公式ID渲染类 用于Controller接受和验证参数
 * @date 2021-12-01
 */
@Data
public class FormulaIDVO {
    private Long formulaId;
}


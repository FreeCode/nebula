package org.zjvis.datascience.common.graph.model;

import lombok.Data;

import java.util.Map;

/**
 * @description GraphAttr
 * @date 2021-12-29
 */
@Data
public class GraphAttr {
    public String labelName;

    public Map<String, Object> attr;
}

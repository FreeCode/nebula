package org.zjvis.datascience.common.model;

import java.util.Iterator;

/**
 * @description FILTER复杂条件包装类
 * @date 2020-07-27
 */
public abstract class ConfigComponent {

    public void add(ConfigComponent configComponent) {
        throw new UnsupportedOperationException();
    }

    public void remove(ConfigComponent configComponent) {
        throw new UnsupportedOperationException();
    }

    public Iterator<ConfigComponent> getIterator() {
        throw new UnsupportedOperationException();
    }

}

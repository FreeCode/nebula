package org.zjvis.datascience.common.exception;

import lombok.Getter;
import org.zjvis.datascience.common.model.ApiResult;
import org.zjvis.datascience.common.model.ApiResultCode;

/**
 * @description 非法公式操作通用异常类
 * @date 2021-08-03
 */
@Getter
public class InvalidFormulaException extends RuntimeException {

    private ApiResult apiResult;

    public InvalidFormulaException(String message) {
        super(message);
        this.apiResult = ApiResult.valueOf(ApiResultCode.SYS_ERROR, message);
    }

    public InvalidFormulaException(String message, Throwable cause) {
        super(message, cause);
        this.apiResult = ApiResult.valueOf(ApiResultCode.SYS_ERROR, message);
    }

    public InvalidFormulaException(Throwable cause) {
        super(cause);
        this.apiResult = ApiResult.valueOf(ApiResultCode.SYS_ERROR);
    }

    public InvalidFormulaException(Integer code, String msg, String info, Throwable cause) {
        super(msg, cause);
        if (info == null) {
            this.apiResult = new ApiResult(code, msg);
        } else {
            this.apiResult = new ApiResult(code, msg + ":" + info);
        }
    }

    public InvalidFormulaException(ErrorCode errorCode, Throwable cause) {
        this(errorCode.getCode(), errorCode.getMsg(), null, cause);
    }

    public InvalidFormulaException(ErrorCode errorCode, String info, Throwable cause) {
        this(errorCode.getCode(), errorCode.getMsg(), info, cause);
    }

    public static InvalidFormulaException of(ErrorCode errorCode, String info) {
        return new InvalidFormulaException(errorCode, info, null);
    }

    public static InvalidFormulaException of(ErrorCode errorCode, String info, String tips) {
        InvalidFormulaException e = new InvalidFormulaException(errorCode, info, null);
        e.getApiResult().setTips(tips);
        return e;
    }

    public InvalidFormulaException(ErrorCode errorCode) {
        this(errorCode, null);
    }

    public InvalidFormulaException(Integer code, String msg) {
        this.apiResult = new ApiResult(code, msg);
    }
}

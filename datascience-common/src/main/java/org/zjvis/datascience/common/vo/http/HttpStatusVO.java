package org.zjvis.datascience.common.vo.http;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.zjvis.datascience.common.vo.category.DatasetCategoryVO;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @description HTTP数据上传状态相关VO
 * @date 2021-04-08
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HttpStatusVO {
    private static final long serialVersionUID = 1L;

    @NotNull(message = "分类目录id不能为空", groups = DatasetCategoryVO.Id.class)
    @Min(value = 1, message = "请输入有效分类目录id", groups = DatasetCategoryVO.Id.class)
    private Long id;

    @NotNull
    private String preIncrementalDataConfig;
    @NotNull
    private String incrementalDataConfig;
}

package org.zjvis.datascience.common.vo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.zjvis.datascience.common.dto.ZeppelinStatusDTO;
import org.zjvis.datascience.common.util.DozerUtil;

import javax.validation.constraints.NotNull;

/**
 * @description Zeppelin自定义算子任务执行结果 相关VO
 * @date 2021-12-28
 */
@Data
public class ZeppelinStatusVO {

    @NotNull
    private String status = "FAIL";

    private String message;

    private Object body;

    public ZeppelinStatusDTO toTask() {
        ZeppelinStatusDTO dto = DozerUtil.mapper(this, ZeppelinStatusDTO.class);
        String jsonString = JSON.toJSONString(body);
        JSONObject jsonObject = JSONObject.parseObject(jsonString);
        dto.setBody(jsonObject);
        return dto;
    }

    public JSONObject bodyToJSONObject() {
        try {
            String bodyStr = JSON.toJSONString(body);
            JSONObject bodyJson = JSONObject.parseObject(bodyStr);
            return bodyJson;
        } catch (Exception e) {
            return null;
        }
    }

}

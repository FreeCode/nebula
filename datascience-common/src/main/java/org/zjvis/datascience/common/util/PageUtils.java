package org.zjvis.datascience.common.util;

import com.github.pagehelper.PageInfo;

/**
 * @description 分页工具类
 * @date 2021-05-07
 */
public class PageUtils {

    /**
     * 统一返回数据格式为org.zjvis.datascience.common.util.Page
     *
     * @param pageInfo
     * @param <S>
     * @return
     */
    public static <S> Page<S> getPageResult(PageInfo<S> pageInfo) {
        Page<S> page = new Page<S>();
        page.setCurPage(pageInfo.getPageNum());
        page.setPageSize(pageInfo.getPageSize());
        page.setTotalElements((int) pageInfo.getTotal());
        page.setTotalPages(pageInfo.getPages());
        page.setData(pageInfo.getList());
        return page;
    }
}

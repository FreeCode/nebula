package org.zjvis.datascience.common.vo.project;

import cn.weiguangfu.swagger2.plus.annotation.ApiRequestInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.zjvis.datascience.common.vo.BaseVO;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @description 项目数据表 Project相关VO
 * @date 2021-08-10
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProjectVO extends BaseVO {

    public static final String NAME_NOT_BLANK = "项目名不能为空";
    public static final String NAME_SIZE = "项目名长度不能超过50";
    public static final String DESC_SIZE = "项目描述长度不能超过100";

    @ApiRequestInclude(groups = {Id.class})
    @NotNull(message = "项目id不能为空", groups = Id.class)
    @Min(value = 1, message = "请输入有效项目id", groups = Id.class)
    private Long id;

    @NotBlank(message = NAME_NOT_BLANK)
    @Length(max = 50, message = NAME_SIZE)
    @ApiModelProperty(value = "项目名", required = true)
    private String name;

    @Length(max = 100, message = DESC_SIZE)
    @ApiModelProperty(value = "项目描述")
    private String description;

    @ApiModelProperty(value = "权限：1读，2写，3管理员，创建项目时无需")
    private Byte auth;

    private String creatorName;

    private static final long serialVersionUID = 1L;

    public interface Id {
    }

}

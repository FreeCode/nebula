package org.zjvis.datascience.common.widget.enums;

/**
 * @description 关联图表之间的状态 枚举类
 * @date 2021-12-08
 */
public enum RelatedWidgetStatusEnum {

    AVAILABLE("available", 1),
    CONNECTED("connected", 2),
    LOSE_CONNECTION("lose_connection", 3),
    FILTER_CONNECTED("filter_connected", 4);

    private String desc;

    private int val;


    RelatedWidgetStatusEnum(String desc, int val) {
        this.desc = desc;
        this.val = val;
    }

    public String getDesc() {
        return desc;
    }

    public Integer getVal() {
        return val;
    }

}

package org.zjvis.datascience.common.dto;

import lombok.Data;

@Data
public class ProjectStatusDTO {
    private Long id;

    private Integer lock;

    private Integer type;
}

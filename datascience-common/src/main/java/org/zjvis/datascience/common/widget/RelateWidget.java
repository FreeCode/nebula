package org.zjvis.datascience.common.widget;

import com.alibaba.fastjson.JSONObject;
import javafx.util.Pair;
import lombok.Data;
import org.zjvis.datascience.common.util.JsonParseUtil;
import org.zjvis.datascience.common.widget.dto.WidgetDTO;
import org.zjvis.datascience.common.widget.enums.RelatedWidgetStatusEnum;

/**
 * @description 相互关联图表控件VO
 * @date 2021-12-08
 */
@Data
public class RelateWidget {

    private Long widgetId;

    private Long taskId;

    private String xAttrStr;

    private String xAttrValueType;

    private String tableName;

    private String chartName;

    private String status;

    public static RelateWidget syncProp(RelateWidget oldRelatedWidget, WidgetDTO newValue) {
        oldRelatedWidget.setChartName(JsonParseUtil.getValueByKey("chartOptions.title", JSONObject.parseObject(newValue.getDataJson()), String.class));
        Pair<String, String> xAttrInfo = newValue.getXAttrInfo();
        oldRelatedWidget.setXAttrStr(xAttrInfo.getKey());
        oldRelatedWidget.setXAttrValueType(xAttrInfo.getValue());
        return oldRelatedWidget;
    }


    public boolean isRelated(WidgetDTO widget) {
        Pair<String, String> xAttrInfo = widget.getXAttrInfo();
        if (!xAttrInfo.getKey().equals(this.getXAttrStr())) {
            return false;
        }
        if (!xAttrInfo.getValue().equals(this.getXAttrValueType())) {
            return false;
        }
        //数据类型还没有验证
        return true;
    }

    public boolean isConnected() {
        return this.getStatus().equals(RelatedWidgetStatusEnum.CONNECTED.getDesc()) || this.getStatus().equals(RelatedWidgetStatusEnum.FILTER_CONNECTED.getDesc());
    }

    public boolean isLoseConnected() {
        return this.getStatus().equals(RelatedWidgetStatusEnum.LOSE_CONNECTION.getDesc());
    }
}

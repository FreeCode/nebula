package org.zjvis.datascience.common.model;

import lombok.Data;

import java.util.List;

/**
 * @description SelectItem字段包装类
 * @date 2021-10-26
 */
@Data
public class DimensionConfig extends ConfigComponent {

    protected String fieldName;

    //key, group
    protected String type;

    protected String filterType;

    protected List<String> values;

    protected String sort;

    protected String alias;

    protected FunctionConfig fun;

    protected boolean includeNull;

}

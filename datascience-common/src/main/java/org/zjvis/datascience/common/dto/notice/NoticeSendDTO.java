package org.zjvis.datascience.common.dto.notice;

import lombok.Data;

/**
 * @description 用户消息发送DTO
 * @date 2021-12-24
 */
@Data
public class NoticeSendDTO {
    private Long id;
    private String title;
    private Long userId;
    private String content;
    private Integer type;
    private Integer status;
}

package org.zjvis.datascience.common.dto;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import lombok.Data;
import org.zjvis.datascience.common.enums.TaskInstanceStatus;
import org.zjvis.datascience.common.widget.vo.WidgetVO;

import java.util.List;

/**
 * @description 项目中数据集信息表，项目中数据集 DTO
 * @date 2021-12-01
 */
@Data
public class ProjectDatasetDTO {
    private Long id;

    private String datasetName;

    private String dataJson;

    private String userName;

    private Long userId;

    private String categoryName;

    public List<WidgetVO> toWidgets() {
        WidgetVO widget = new WidgetVO();
        widget.setTid(this.getId());
        widget.setType("dataset");
        widget.setStatus(TaskInstanceStatus.SUCCESS.toString());
        widget.setName(this.getDatasetName());
        JSONObject dataJson = new JSONObject();
        JSONObject extraJsonObj = JSONObject.parseObject(this.getDataJson());
        dataJson.put("table", extraJsonObj.getString("schema") + "." + extraJsonObj.getString("table"));
        widget.setData(dataJson);
        return Lists.newArrayList(widget);
    }
}

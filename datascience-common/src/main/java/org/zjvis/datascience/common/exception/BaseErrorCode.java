package org.zjvis.datascience.common.exception;

import lombok.Getter;

/**
 * @description 通用异常code枚举类
 * @date 2020-03-26
 */
@Getter
public enum BaseErrorCode implements ErrorCode {

    /**
     * undefined error
     */
    UNDEFINED(10000, "操作成功!"),
    ERROR(10001, "操作失败!"),
    ERROR_SYSTEM(10002, "系统繁忙!"),
    UNAUTHORIZED(401, "无权访问!"),
    DUPLICATED_DATA(501, "请勿重复添加!"),
    /**
     * 用户模块异常码
     */
    USER_NAME_ALREADY_EXISTS(20000, "账号已存在!"),
    USER_PASSWORD_ERROR(20001, "密码加密异常!"),
//    USER_IS_NOT_EXISTS(20002, "用户不存在!"),
    USER_ALREADY_EXIST(20003, "该用户名或手机号已存在!"),
    USER_SEND_MESSAGE_ERROR(20004, "短信验证码发送失败！"),
    USER_PHONE_CODE_EXPIRED(20005, "短信验证码已过期!"),
    USER_PHONE_CODE_CANNOT_EXCEED_TIMES(20006, "请求过于频繁，请稍后再试!"),
    USER_PHONE_CODE_ERROR(20007, "短信验证码错误，请重新输入!"),
    USER_IS_LOCKED(20008, "用户已锁定!"),
    USER_USERNAME_OR_PASSWORD_ERROR(20009, "账号或密码不正确!"),
    USER_USERNAME_IS_LOCKED(20010, "账号已锁定，请稍后再试!"),
    USER_CANNOT_UPDATE_ADMIN(20011, "仅超级管理员可操作!"),
    USER_TOKEN_INFO_IS_NULL(20012, "登录信息不存在!"),
    USER_INFO_IS_NULL(20013, "用户信息为空!"),
    USER_CANNOT_DELETE(20014, "系统默认用户不可删除!"),
    USER_CAPTCHA_CODE_ERROR(20015, "图片验证码错误或已过期！"),
    USER_NEED_CAPTCHA_CODE(20016, "需请求图片验证码"),
    USER_PASSWORD_NOT_EQUAL(20017, "两次密码不相同!"),
    USER_PHONE_NOT_VERIFIED(20018, "未通过手机验证！"),
    ADMIN_CANNOT_UPDATE(20019, "管理员不能对自己更改权限!"),
    USER_PHONE_NOT_CORRESPOND(20020, "该手机号不是当前账户所绑定的手机号!"),
    USER_LOGIN_TIMES_EXCEEDED(20022, "密码错误次数超限，请稍后再试！"),
    USER_PHONE_INCORRECT_FORMAT(20023, "输入手机号格式有误！"),
    USER_PHONE_ALREADY_BIND(20024, "该手机号已被绑定!"),

    PROJECT_AUTH_DELETE_ERROR(30000, "管理员权限不允许删除"),
    PROJECT_AUTH_CREATE_ERROR(30001, "管理员权限不允许添加"),
    PROJECT_AUTH_EXIST_ERROR(30002, "该用户已是项目成员，请勿重复添加"),
    PROJECT_AUTH_DUPLICATE_ERROR(30003, "存在重复数据"),
    PROJECT_NAME_DUPLICATE_ERROR(30004, "此项目名已存在"),
    PROJECT_NO_AUTH(31000, "无此项目操作权限"),
    PROJECT_LOCKED(31001, "项目已锁定无法修改"),
    PROJECT_IS_TEMPLATE(31002, "模板项目不能修改"),
    PROJECT_NOT_EXIST(31003, "项目不存在"),
    PROJECT_OPERATIONAL_USER_EXCEEDS_THRESHOLD(31004, "超出项目中可添加的高于访客权限用户的阈值"),
    PROJECT_ORDER_1(31005, "当指定排序规则时，排序字段不能为空"),
    PROJECT_ORDER_2(31006, "排序规则，只能是 asc 或 desc或空，为空时默认增序"),
    PROJECT_ADD_USER_NOT_EXIT(31007, "要添加的用户不存在"),
    PROJECT_AUTH_PERMISSION_DENIED(31008, "权限不足，无法操作"),
    PROJECT_COPY_FAILED(31009, "项目批量复制过程出错。"),
    DASHBOARD_COPY_FAILED(31010, "可视化视图复制过程出错。"),

    DATASET_NOT_AVAILABLE(40000, "无可用数据集"),
    DATASET_IMPORT_CANNOT_GET_COUNT(40001, "无法查询数据源数据量"),
    DATASET_IMPORT_ERROR(40002, "数据库连接或执行sql异常"),
    DATASET_IMPORT_TASK_EXIST(40003, "您已有数据导入任务，请先等待任务完成"),
    DATASET_TABLE_NOT_EXIST(40004, "需要导入的表不存在"),
    DATASET_TABLE_GP_CREATE_FAIL(40005, "GP表创建失败"),
    DATASET_TABLE_NO_FIELD(40006, "需要导入的表无字段"),
    DATASET_IMPORT_GP_INSERT_ERROR(40007, "GP数据插入失败"),
    DATASET_TABLE_EMPTY_IMPORT_ERROR(40008, "不允许导入空表"),
    DATASET_IMPORT_GP_COUNT_ERROR(40009, "GP数据统计失败"),
    DATASET_IMPORT_CANCEL_ERROR(40010, "没有进行中的数据导入任务"),
    DATASET_GP_DELETE_ERROR(40011, "GP数据集删除失败"),
    DATASET_IMPORT_DATA_TYPE_ERROR(40012, "不支持的数据类型"),
    DATASET_IMPORT_DATA_TRANSFORM_FAIL(40013, "数据类型转换失败"),
    DATASET_NAME_DUPLICATE_ERROR(40014, "同一分类下已有同名数据集"),
    DATASET_QUERY_ERROR(40015, "查询数据出现异常"),
    DATASET_GRAPH_FILE_EXT_CHANGE(40016, "无法修改图文件后缀"),

    DATASET_CSV_PREVIEW_ERROR(40105, "数据预览失败"),
    DATASET_IMPORT_CSV_GPLOAD_ERROR(40100, "导入数据失败"),
    DATASET_IMPORT_CONNECT_GP_SERVER_ERROR(40101, "连接GP服务器异常"),
    DATASET_IMPORT_MASKING_ERROR(40102, "数据导入脱敏异常"),
    DATASET_IMPORT_CSV_GPLOAD_ERROR_TYPENUM(40103, "导入数据失败(无法转换为数字类型)"),
    DATASET_IMPORT_CSV_GPLOAD_ERROR_TYPEDATE(40104, "导入数据失败(无法转换为日期类型)"),
    DATASET_IMPORT_CONNECT_GP_SERVER_CAT_ERROR(40106, "连接GP服务器, 合并文件失败"),
    DATASET_IMPORT_CSV_CREATETABLE_ERROR(40107, "创建表失败"),

    DATASET_PREVIEW_UNSUPPORTED(41000, "当前仅支持MySQL、Oracle及rds-mysql数据库"),
    DATASET_PREVIEW_SQL_ERROR(41001, "预览查询出现异常"),
    DATASET_USED_IN_PROJECT(41002, "该用户有数据集被加载到项目中，请先把载入数据从项目中移除再执行此操作"),

    WIDGET_NOT_EXIST(50000, "无可用可视化组件"),
    DASHBOARD_PUBLISHED_IN_PROJECT(50001, "存在已发布的可视化构建系统，请先把发布的系统下架移除再执行此操作"),
    DASHBOARD_ALREADY_BEEN_PULLED(50002, "这个仪表盘已经被下架"),
    DASHBOARD_PARAM_NOT_VALID(50003, "可视化构建传参不合理"),

    PIPELINE_NOT_EXIST(60000, "无可用pipeline"),
    PIPELINE_NOT_TASK_EXIST(60001, "当前pipeline没有节点，不能保存快照"),
    PIPELINE_IS_PERFORMING_SNAPSHOT(60002, "pipeline已存在快照操作，请稍后执行此操作"),
    PIPELINE_SNAPSHOT_NAME_EXIST(60003, "无法保存，已存在同名快照"),
    PIPELINE_SNAPSHOT_DELETE_EXCEPTION(60004, "旧版本清除异常，无法添加快照"),
    PIPELINE_SNAPSHOT_EMPTY(60005, "指定的pipeline没有视图，不需要删除"),
    PIPELINE_SNAPSHOT_ID_NOT_EXIST(60006, "要删除的快照在当前pipeline中不存在"),
    PIPELINE_SNAPSHOT_TASK_EMPTY(60007, "指定的pipeline视图为空，无法操作"),
    PIPELINE_SNAPSHOT_NOT_SUPPORT_GRAPH_BUILD(60008, "图构建节点不支持快照功能，请先移除掉pipeline中给图构建节点再保存快照"),
    PIPELINE_SNAPSHOT_PICTURE_NOT_NULL(60009, "上传的快照文件不能为空"),
    PIPELINE_SNAPSHOT_NAME_NOT_NULL(60009, "上传的快照名能为空"),
    PIPELINE_SNAPSHOT_PROJECT_INVALID(60010, "请输入有效project id"),
    PIPELINE_SNAPSHOT_PIPELINE_INVALID(60011, "请输入有效pipeline id"),
    PIPELINE_NO_OPERATION_PERMISSION(60012, "无权操作"),

    PARENTS_NOT_CONFIG(70000, "子节点依赖父节点的输出，请先配置父节点参数!"),
    TASK_NOT_CONFIG(70001, "请先配置好参数再运行！"),
    TASK_NOT_EXIST(70002, "节点不存在"),
    TASK_OUTPUT_TABLE_NOT_EXIST(70003, "节点缺少输出表"),
    TASK_SAVE_TO_DATASET_PARAM(70004, "category id和name均不能为空"),
    TASK_NO_OPERATION_PERMISSION(70005, "无权操作"),
    TASK_NOT_PROPER_INIT(70006, "父节点可能存在问题，建议删除后重试"),

    /** 文件上传 */
    UPLOAD_FILE_LINE_LESS_ERROR(80000,"上传文件内容不能少于两行!"),
    UPLOAD_EXCEL_FILE_LINE_LESS_ERROR(80001,"上传的excel文件必须满足：至少有一个sheet数据大于一行!"),
    UPLOAD_EXCEL_TOO_LARGE_ERROR(80002,"上传的excel文件过大无法处理"),
    UPLOAD_EXCEL_SHEETS_TOO_MANY_ERROR(80003,"上传的excel文件表格过多无法处理：sheet数量需小于10"),
    UPLOAD_EXCEL_MULTI_HEAD(80004,"上传的excel文件含有多级标题，无法上传"),
    UPLOAD_FILE_ERROR(80005,"上传文件失败"),

    /** graph */
    GRAPH_SUBMIT_REQUEST_ERROR(90000, "请求字段过长"),
    GRAPH_LOAD_ATTRS_TYPE_CONFLICT(90001, "属性类型冲突"),
    GRAPH_LOAD_REQUIRED_KEY_MISS(90002, "缺少必填字段"),
    GRAPH_LOAD_REQUIRED_INTEGER_ID(90003, "id标识必须为整数"),
    GRAPH_LOAD_NODE_NOT_FOUND(90004, "找不到节点"),
    GRAPH_LOAD_CSV_HEAD_ERROR(90005, "数据标题有误，无法识别"),
    GRAPH_FILTER_ERROR(90006, "部分过滤器已失效，无法运行"),
    GRAPH_FILE_PARSE_ERROR(90007, "图数据文件解析失败"),
    GRAPH_DEV_TIP(90008, "数据变动，请重新导入数据"),
    GRAPH_NO_PERMISSION(90009, "无权操作"),
    GRAPH_NOT_FOUND(90010, "找不到该视图"),
    GRAPH_ATTR_NOT_FOUND(90011, "找不到该属性"),
    GRAPH_ELEMENT_NOT_FOUND(90012, "找不到该元素"),
    GRAPH_NO_BUILD_RECORD(90013, "无构建记录"),
    GRAPH_CATEGORY_ID_ERROR(90014, "节点id与视图不匹配"),
    GRAPH_CATEGORY_CLEAN_ACTION_ID_NOT_FOUND(90015, "找不到该记录"),
    GRAPH_BUILD_ERROR(90016, "构建失败"),
    GRAPH_PROJECT_RELATION_ERROR(90017, "该视图不属于此项目"),
    GRAPH_FILTER_PIPELINE_RELATION_ERROR(90018, "该过滤器组不属于此视图"),
    GRAPH_FILTER_RELATION_ERROR(90019, "该过滤器不属于此过滤器组或视图"),

    /** notice */
    NOTICE_NOT_EXISTS(100000,"消息不存在"),
    NOTICE_ALREADY_READ(100001,"消息已经是已读状态，请勿重复操作"),

    /** socket */
    SOCKET_TOKEN_MISSING(110000,"缺少token"),
    SOCKET_TOKEN_USELESS(110001,"无效token"),
    SOCKET_TOKEN_EXPIRE(110002,"token过期"),

    /** feedback */
    FEEDBACK_PARAMETER_NOT_MATCHING(120000,"请求参数中反馈id与发送用户不匹配"),
    FEEDBACK_NOTICE_REPEAT_SEND(120001,"消息已经回复，请勿重复操作"),

    /**
     * formula
     */
    FORMULA_UPDATE_FAILED(130001, "创建或更新公式失败"),


    /**
     * 断点续传部分
     */
    SLICE_UPLOAD_FILE_NOT_FOUND(140000, "文件未找到"),
    SLICE_UPLOAD_FILE_WRITE_FAIL(140001, "写入文件失败"),
    SLICE_UPLOAD_PREVIEW_FAIL(140002, "预览数据失败"),
    UPLOAD_PREVIEW_DATA_ERROR(140003, "数据行解析有误，请检查数据格式"),
    SLICE_UPLOAD_FILE_SIZE_ZERO(140004, "传递的文件大小为0"),
    SLICE_UPLOAD_FAIL(140005, "文件上传失败"),
    SLICE_NOW_UPLOAD(140006, "该文件已在另一窗口上传"),
    ;

    Integer code;
    String msg;

    BaseErrorCode(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}

package org.zjvis.datascience.common.constant;

/**
 * @description 数据日期类型常量
 * @date 2021-12-24
 */
public class DateTimeFormatConstant {

    public static final String JDK_MINUTE_PATTERN = "mm";

    public static final String JDK_MILLISECONDS_PATTERN = "SSS";

    public static final String JDK_SECONDS_PATTERN = "ss";

    public static final String JDK_HOUR_PATTERN = "HH";

    public static final String GP_MINUTE_PATTERN = "MI";

    public static final String GP_MILLISECONDS_PATTERN = "MS";

    public static final String GP_HOUR_PATTERN = "HH24";

}

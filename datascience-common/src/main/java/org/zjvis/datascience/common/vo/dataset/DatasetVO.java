package org.zjvis.datascience.common.vo.dataset;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.zjvis.datascience.common.vo.category.DatasetCategoryVO;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @description 数据表Dataset 数据集VO
 * @date 2020-07-31
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DatasetVO {
    private static final long serialVersionUID = 1L;

    @NotNull(message = "分类目录id不能为空", groups = DatasetCategoryVO.Id.class)
    @Min(value = 1, message = "请输入有效分类目录id", groups = DatasetCategoryVO.Id.class)
    private Long id;

    @NotBlank(message = "分类目录名不能为空")
    @Length(max = 100, message = "分类目录名长度不能超过100")
    @ApiModelProperty(value = "分类目录名", required = true)
    private String name;

    @ApiModelProperty(value = "用户id", required = true)
    @NotNull(message = "用户不能为空")
    @Min(value = 1, message = "请输入有效用户id")
    private Long userId;

    @ApiModelProperty(value = "所属类别id", required = true)
    @NotNull(message = "所属类别id不能为空")
    @Min(value = 1, message = "请输入有效所属类别id")
    private Long categoryId;

    @NotBlank(message = "数据集元信息不能为空")
    @ApiModelProperty(value = "数据集元信息", required = true)
    private String dataJson;

    @NotBlank(message = "'描述不能为空")
    @Length(max = 255, message = "'描述长度不能超过255")
    @ApiModelProperty(value = "'描述", required = true)
    private String description;

    public interface Id {
    }
}

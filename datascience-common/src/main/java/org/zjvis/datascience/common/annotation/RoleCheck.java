package org.zjvis.datascience.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @description 用户角色校验鉴权注解
 * @date 2021-12-24
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface RoleCheck {

    /**
     * 角色值 0-普通用户，1-管理员。待明确角色需求后可以补充完整
     */
    //int[] roles() default {};
}

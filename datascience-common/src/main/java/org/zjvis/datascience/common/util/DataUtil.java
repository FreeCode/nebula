package org.zjvis.datascience.common.util;

import com.google.common.collect.Lists;
import org.zjvis.datascience.common.model.AggregateColumn;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @description 查询数据包装工具类
 * @date 2021-10-29
 */
public class DataUtil {

    public static List<Object[]> wrapDataFromAggHead(ResultSet rs, List<AggregateColumn> heads) throws SQLException {
        List<Object[]> data = Lists.newArrayList();
        while (rs.next()) {

            Object[] record = new Object[heads.size()];

            for (int i = 0; i < heads.size(); i++) {
                Object o = rs.getObject(heads.get(i).getIndex());
                record[i] = o;
            }
            data.add(record);
        }

        return data;
    }
}

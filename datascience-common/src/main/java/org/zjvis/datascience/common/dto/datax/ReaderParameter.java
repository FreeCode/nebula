package org.zjvis.datascience.common.dto.datax;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
public class ReaderParameter extends Parameter {
    private String where;
}

package org.zjvis.datascience.common.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @description DTO通用父类
 * @date 2021-10-09
 */
@SuperBuilder
@Data
public class BaseDTO implements Serializable {

    private static final long serialVersionUID = -4550929628899796970L;

    public BaseDTO() {

    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((gmtCreator == null) ? 0 : gmtCreator.hashCode());
        result = prime * result + ((gmtModifier == null) ? 0 : gmtModifier.hashCode());
        result = prime * result + ((gmtCreate == null) ? 0 : gmtCreate.hashCode());
        result = prime * result + ((gmtModify == null) ? 0 : gmtModify.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        BaseDTO other = (BaseDTO) obj;
        if (gmtCreator == null) {
            if (other.gmtCreator != null) {
                return false;
            }
        } else if (!gmtCreator.equals(other.gmtCreator)) {
            return false;
        }

        if (gmtModifier == null) {
            if (other.gmtModifier != null) {
                return false;
            }
        } else if (!gmtModifier.equals(other.gmtModifier)) {
            return false;
        }

        if (gmtCreate == null) {
            if (other.gmtCreate != null) {
                return false;
            }
        } else if (!gmtCreate.equals(other.gmtCreate)) {
            return false;
        }

        if (gmtModify == null) {
            if (other.gmtModify != null) {
                return false;
            }
        } else if (!gmtModify.equals(other.gmtModify)) {
            return false;
        }
        return true;
    }

    protected Long gmtCreator;

    protected Long gmtModifier;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    protected LocalDateTime gmtCreate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    protected LocalDateTime gmtModify;

}

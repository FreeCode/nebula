package org.zjvis.datascience.common.exception;

import lombok.Getter;
import org.zjvis.datascience.common.model.ApiResult;
import org.zjvis.datascience.common.model.ApiResultCode;

/**
 * @description 公式操作通用异常类
 * @date 2021-08-03
 */
@Getter
public class FormulaException extends RuntimeException {

    private ApiResult apiResult;

    public FormulaException(String message) {
        super(message);
        this.apiResult = ApiResult.valueOf(ApiResultCode.SYS_ERROR, message);
    }

    public FormulaException(String message, Throwable cause) {
        super(message, cause);
        this.apiResult = ApiResult.valueOf(ApiResultCode.SYS_ERROR, message);
    }

    public FormulaException(Throwable cause) {
        super(cause);
        this.apiResult = ApiResult.valueOf(ApiResultCode.SYS_ERROR);
    }

    public FormulaException(Integer code, String msg, String info, Throwable cause) {
        super(msg, cause);
        if (info == null) {
            this.apiResult = new ApiResult(code, msg);
        } else {
            this.apiResult = new ApiResult(code, msg + ":" + info);
        }
    }

    public FormulaException(ErrorCode errorCode, Throwable cause) {
        this(errorCode.getCode(), errorCode.getMsg(), null, cause);
    }

    public FormulaException(ErrorCode errorCode, String info, Throwable cause) {
        this(errorCode.getCode(), errorCode.getMsg(), info, cause);
    }

    public static FormulaException of(ErrorCode errorCode, String info) {
        return new FormulaException(errorCode, info, null);
    }

    public static FormulaException of(ErrorCode errorCode, String info, String tips) {
        FormulaException e = new FormulaException(errorCode, info, null);
        e.getApiResult().setTips(tips);
        return e;
    }

    public FormulaException(ErrorCode errorCode) {
        this(errorCode, null);
    }

    public FormulaException(Integer code, String msg) {
        this.apiResult = new ApiResult(code, msg);
    }
}

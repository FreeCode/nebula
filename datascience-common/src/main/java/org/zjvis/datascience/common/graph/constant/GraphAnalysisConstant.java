package org.zjvis.datascience.common.graph.constant;

/**
 * @description GraphAnalysisConstant
 * @date 2021-12-29
 */
public class GraphAnalysisConstant {
    public static final String GRAPH_DEFAULT_NAME = "图分析";

    public static final String GRAPH_DEFAULT_ID_STR = "graphId";

}

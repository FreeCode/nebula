package org.zjvis.datascience.common.dto;

import lombok.Data;

/**
 * @description 角色信息表，角色DTO
 * @date 2021-05-07
 */
@Data
public class RoleDTO {

    private Integer id;

    private String name;
}

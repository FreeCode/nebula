package org.zjvis.datascience.common.vo.http;

import lombok.Data;
import org.zjvis.datascience.common.dto.dataset.DatasetColumnDTO;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @description HTTP数据上传相关VO
 * @date 2021-04-25
 */
@Data
public class HttpDataCreateTableVO {
    @NotNull(message = "请选择数据分类")
    private Long categoryId;

    @NotBlank(message = "数据集名称不能为空")
    private String name;

    @NotBlank(message = "请选择您想导入的数据类型 excel.表格文件excel\\cvs , table.外部数据库 http数据")
    private String importType;

    @NotNull(message = "需要上传的数据字段不能为空")
    @Size(min = 1, message = "需要上传的数据字段不能为空")
    private List<@Valid DatasetColumnDTO> columnDTOList;

    @Size(message = "需要上传的数据字段属性值不能为空")
    private List<Object> data;
}

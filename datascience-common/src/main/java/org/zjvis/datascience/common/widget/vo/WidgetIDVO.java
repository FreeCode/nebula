package org.zjvis.datascience.common.widget.vo;

import lombok.Data;
import org.zjvis.datascience.common.validator.NotNullNumber;

/**
 * @description 用于图表控件渲染数据的请求POJO （单ID）
 * @date 2021-12-02
 */
@Data
public class WidgetIDVO {

    @NotNullNumber(message = "widgetId cannot be null")
    private Long id;

    private Long projectId;
}

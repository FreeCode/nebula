package org.zjvis.datascience.common.exception;

/**
 * @description sql 解析错误通用异常类
 * @date 2021-08-03
 */
public class SqlParserException extends Exception {

    public SqlParserException(){ super();}

    public SqlParserException(String msg) {
        super("[SqlParserException]" + msg);
    }

}

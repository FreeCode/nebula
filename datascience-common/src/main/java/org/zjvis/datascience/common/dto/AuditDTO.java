package org.zjvis.datascience.common.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @description 操作记录审计表，记录请求操作
 * @date 2021-01-08
 */
@Data
public class AuditDTO implements Serializable {

    private static final long serialVersionUID = 6597243550150450851L;

    private Long id;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime requestTime;

    private String url;

    private String params;

    private String requestType;

    private String requestData;

    private String username;

    private String ip;

    private String envInfo;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

}

package org.zjvis.datascience.common.vo.project;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.zjvis.datascience.common.annotation.EnumValue;
import org.zjvis.datascience.common.dto.UserProjectDTO;
import org.zjvis.datascience.common.dto.user.UserDTO;
import org.zjvis.datascience.common.enums.ProjectRoleAuthEnum;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @description 创建项目相关VO
 * @date 2021-05-11
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateUserProjectVO {

    @ApiModelProperty(value = "用户id", required = true)
    @NotNull(message = "用户不能为空")
    @Min(value = 1, message = "请输入有效用户id")
    private Long userId;

//    @ApiModelProperty(value = "权限，1：读，2：读写", required = true)
//    @EnumValue(enumClass = ProjectAuthEnum.class, message = "权限只能为1-读、2-写")
//    private Byte auth;

    @ApiModelProperty(value = "角色：2-项目管理员、3-项目开发者、4-项目访客", required = true)
    @EnumValue(enumClass = ProjectRoleAuthEnum.WithOutCreatorEnum.class, message = "角色只能为：2-项目管理员、3-项目开发者、4-项目访客")
    private Integer roleId;

    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append(", userId=").append(userId);
        sb.append(", roleId=").append(roleId);
        sb.append("]");
        return sb.toString();
    }

    public static UserProjectDTO toDto(CreateUserProjectVO vo, long projectId,
                                       UserDTO user) {
        return UserProjectDTO.builder()
                .userId(vo.getUserId())
                .roleId(vo.getRoleId())
                .projectId(projectId)
                .userName(user.getName())
                .build();
    }
}

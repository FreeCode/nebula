package org.zjvis.datascience.common.dto.datax;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
public class Parameter {
    private String username;
    private String password;
    private List<String> column;
    private List<Connection> connection;
}

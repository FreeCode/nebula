package org.zjvis.datascience.common.enums;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.zjvis.datascience.common.exception.BaseErrorCode;
import org.zjvis.datascience.common.exception.DataScienceException;

/**
 * @description 数据类型 枚举类
 * @date 2021-07-29
 */
@Getter
public enum DataTypeEnum {

    /**
     * 需要保证value值与枚举名一致，否则会取不到
     */
    VARCHAR("字符串", "varchar", "varchar"),
    DATE("日期", "date", "varchar"), //timestamp without time zone
    DECIMAL("小数", "decimal", "decimal"),
    INT("整数", "int", "bigint"),
    JSON("json", "json", "varchar"),
    ARRAY("array", "array", "varchar"),
    ;

    /**
     * 中文名
     */
    private String desc;
    private String value;
    /**
     * 数据在gp中的存储类型
     */
    private String gpType;

    DataTypeEnum(String desc, String value, String gpType) {
        this.desc = desc;
        this.value = value;
        this.gpType = gpType;
    }

    public static DataTypeEnum get(String value) {
        if (StringUtils.isEmpty(value)) {
            throw new DataScienceException(BaseErrorCode.DATASET_IMPORT_DATA_TYPE_ERROR);
        }
        return DataTypeEnum.valueOf(StringUtils.upperCase(value));
    }


}

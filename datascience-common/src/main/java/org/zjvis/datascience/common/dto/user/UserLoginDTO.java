package org.zjvis.datascience.common.dto.user;

import lombok.Data;

/**
 * @description 用户每日登录信息记录DTO
 * @date 2021-12-24
 */
@Data
public class UserLoginDTO {

    /**
     * 最后一次登录错误时间
     */
    int loginErrorTimes;

    /**
     * 5分钟内密码错误次数
     */
    int last5MinErrorTimes;

    /**
     * 最后一次登录时间
     */
    long lastLoginTime;

    /**
     * 5分钟内登录次数
     */
    long last5MinTime;
}

package org.zjvis.datascience.common.graph.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @description TupleWrapper
 * @date 2021-12-29
 */
@Data
public class TupleWrapper<T, P> implements Serializable {
    public T left;

    public P right;

    public TupleWrapper(T left, P right) {
        this.left = left;
        this.right = right;
    }
}

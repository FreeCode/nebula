package org.zjvis.datascience.common.enums;

/**
 * @description Task任务节点类型枚举类
 * @date 2021-11-15
 */
public enum TaskTypeEnum {
    TASK_TYPE_ALGO("系统算子节点", 1),
    TASK_TYPE_ETL("ETL操作节点", 2),
    TASK_TYPE_DATA("数据节点", 3),
//    TASK_TYPE_DEFINE("自定义算子节点", 4), 已移除
    TASK_TYPE_CLEAN("清洗节点", 5),
    TASK_TYPE_GRAPH("图网络节点", 6),
    TASK_TYPE_MODEL("模型节点",7),
    TASK_TYPE_ALGOPY("py算子节点",8),
    TASK_TYPE_JLAB("JLAB节点",9);

    private String desc;

    private int val;


    TaskTypeEnum(String desc, int val) {
        this.desc = desc;
        this.val = val;
    }

    public String getDesc() {
        return desc;
    }

    public Integer getVal() {
        return val;
    }
}

package org.zjvis.datascience.common.vo.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @description 用户登录相关VO
 * @date 2021-12-13
 */
@Data
public class UserModifyPasswordVO {

    @NotBlank(message = "手机号码不能为空")
    @Pattern(regexp = "^1[3|4|5|7|8|9][0-9]{9}$", message = "手机号码格式有误")
    @ApiModelProperty(value = "手机")
    private String phone;

    @ApiModelProperty(value = "图形验证码")
    private String captchaCode;

    // 密码由数字和字母特殊字符组成，并且要同时含有数字和字母，且长度要在8-16位之间。
    @Pattern(regexp = "^(?=.*\\d)^(?![0-9]+$)(?![a-zA-Z]+$).{8,16}$", message = "密码需同时含有数字和字母,可包含特殊字符，且长度在8-16位之间!")
    @ApiModelProperty(value = "密码")
    private String password1;

    @Pattern(regexp = "^(?=.*\\d)^(?![0-9]+$)(?![a-zA-Z]+$).{8,16}$", message = "密码需同时含有数字和字母,可包含特殊字符，且长度在8-16位之间!")
    @ApiModelProperty(value = "密码")
    private String password2;

    @ApiModelProperty(value = "最终密码")
    private String password;
}

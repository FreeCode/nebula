package org.zjvis.datascience.common.dto.graph;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.zjvis.datascience.common.dto.BaseDTO;
import org.zjvis.datascience.common.util.DozerUtil;
import org.zjvis.datascience.common.vo.graph.GraphActionVO;

/**
 * @description Graph分析构建清洗ActionDTO
 * @date 2021-12-24
 */
@Data
public class GraphActionDTO extends BaseDTO {
    private Long id;
    private String actionType;
    private Long projectId;
    private Long pipelineId;
    private Long taskId;
    private Long graphId;
    private Long userId;
    private String context;

    public GraphActionVO view() {
        GraphActionVO vo = DozerUtil.mapper(this, GraphActionVO.class);
        vo.setContextObj(JSONObject.parseObject(context));
        return vo;
    }
}

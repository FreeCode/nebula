package org.zjvis.datascience.common.enums;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.zjvis.datascience.common.exception.BaseErrorCode;
import org.zjvis.datascience.common.exception.DataScienceException;

/**
 * @description python日期类型格式
 * @date 2021-08-26
 */
@Getter
public enum PythonDateTypeFormatEnum {
    FORMAT_0("yyyy-mm-dd hh:mm:ss", "%Y-%m-%d %H:%M:%S"),
    FORMAT_1("yyyy-mm-dd hh:mm", "%Y-%m-%d %H:%M"),
    FORMAT_2("yyyy-mm-dd hh", "%Y-%m-%d %H"),
    FORMAT_3("yyyy-mm-dd", "%Y-%m-%d"),
    FORMAT_4("yyyy-mm", "%Y-%m"),
    FORMAT_5("yyyy", "%Y"),
    FORMAT_6("yyyy-mm-dd hh:mm:ss AM/PM", "%Y-%m-%d %H:%M:%S %p"),
    FORMAT_7("mm-dd-yyyy", "%m-%d-%Y"),
    FORMAT_8("yyyy/mm/dd", "%Y/%m/%d"),
    FORMAT_9("yyyy.mm.dd", "%Y.%m.%d"),

    FORMAT_10("hh:mm:ss", "%H:%M:%S"),
    FORMAT_11("hh:mm:ss AM/PM", "%H:%M:%S %p"),

    FORMAT_12("dd M, yyyy", "%d %b, %Y"),
    FORMAT_13("W dd M, yyyy", "%a %d %b, %Y"),
    FORMAT_14("dd M, yyyy W", "%d %b, %Y %a"),
    FORMAT_15("M dd, yyyy", "%b %d, %Y"),
    FORMAT_16("W M dd, yyyy", "%a %b %d, %Y"),
    FORMAT_17("M dd, yyyy W", "%b %d, %Y %a"),

    FORMAT_TIMESTAMP("timestamp", "timestamp"),
    ;

    private String desc;
    private String val;

    PythonDateTypeFormatEnum(String desc, String val) {
        this.desc = desc;
        this.val = val;
    }

    public String getDesc() {
        return desc;
    }

    public String getVal() {
        return val;
    }
}

package org.zjvis.datascience.common.util;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.zjvis.datascience.common.enums.EngineEnum;

/**
 * @description 计算引擎选择器
 * @date 2021-08-19
 */
@Data
@Component
public class EngineSelector {

    @Value("${engineselector.engine}")
    private String engineName;

    public EngineEnum getEngine() {
        return EngineEnum.valueOf(engineName.toUpperCase());
    }

    public EngineEnum getEngine(String engineName) {
        return EngineEnum.valueOf(engineName);
    }

    public boolean isSpark() {
        return getEngine().equals(EngineEnum.SPARK);
    }

    public boolean isMadlib() {
        return getEngine().equals(EngineEnum.MADLIB);
    }
}

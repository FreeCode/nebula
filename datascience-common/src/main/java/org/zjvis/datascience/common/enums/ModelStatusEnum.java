package org.zjvis.datascience.common.enums;

/**
 * @description 模型状态枚举类
 * 定义：
 * “我的模型” -》 场外
 * “模型训练” -》 场内
 * @date 2021-01-15
 */
public enum ModelStatusEnum {

    NORMAL("场内外均存在", 0),
    DELETED_IN_FIELD("场内已删除", 1),
    COPIED("复制创建出来的", -1);

    private String desc;

    private long val;


    ModelStatusEnum(String desc, int val) {
        this.desc = desc;
        this.val = val;
    }

    public String getDesc() {
        return desc;
    }

    public Long getVal() {
        return val;
    }

    public ModelStatusEnum valueOfs(int val) {
        switch (val) {
            case -1:
                return COPIED;
            case 0:
                return NORMAL;
            case 1:
                return DELETED_IN_FIELD;
            default:
                return null;
        }
    }
}

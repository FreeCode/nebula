package org.zjvis.datascience.common.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @description SQL查询数据表 sql_category 相关VO
 * @date 2021-04-25
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SqlCategoryVO{
    @NotNull(message = "日志ID不能为空",groups = Id.class)
    @Min(value = 1, message = "请输入有效分类目录id", groups = Id.class)
    @ApiModelProperty(value = "日志ID", required = true)
    private Long id;

    public interface Id {
    }
}

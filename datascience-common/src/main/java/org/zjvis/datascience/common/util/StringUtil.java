package org.zjvis.datascience.common.util;

import org.apache.commons.lang3.StringUtils;
import org.zjvis.datascience.common.constant.DatasetConstant;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @description 字符串工具类
 * @date 2021-10-29
 */
public class StringUtil {


    public static boolean hasChinese(String str) {
        Pattern p = Pattern.compile("[\u4e00-\u9fa5]");
        Matcher m = p.matcher(str);
        return m.find();
    }

    public static String generateKey(String... args) {
        return generateKey(Arrays.asList(args));
    }

    public static String generateKey(List<String> args) {
        StringBuilder builder = new StringBuilder();
        for (String arg : args) {
            if (arg.isEmpty()) {
                builder.append('#');
            } else {
                builder.append(arg);
            }
            builder.append("-");
        }

        return builder.toString();
    }

    private static String bytesToHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    public static String generateHashKey(String... args) {
        return generateHashKey(generateKey(Arrays.asList(args)));
    }

    public static String generateHashKey(String key) {
        String cacheKey;
        try {
            final MessageDigest mDigest = MessageDigest.getInstance("SHA-1");
            mDigest.update(key.getBytes(StandardCharsets.UTF_8));
            cacheKey = bytesToHexString(mDigest.digest());
        } catch (NoSuchAlgorithmException e) {
            cacheKey = String.valueOf(key.hashCode());
        }
        return cacheKey;
    }

    public static boolean isAllNumeric(String str) {
        return StringUtils.isNumeric(str);
    }

    /**
     * 按照oldName的格式拼装 新内容
     *
     * @param newName
     * @param oldName
     * @return
     */
    public static String replaceTableName(String newName, String oldName) {
        if (oldName.split("\\.").length > 1) {
            String[] oldParts = oldName.split("\\.");
            String[] newParts = newName.split("\\.");
            return oldParts[0] + "." + newParts[newParts.length - 1];
        } else {
            String[] newParts = newName.split("\\.");
            return newParts[newParts.length - 1];
        }
    }

    public static String addHtmlBoldLabel(String str) {
        if (StringUtils.isNotBlank(str)) {
            str = "<b>" + str + "</b>";
        }
        return str;
    }

    /**
     * 将中文双引号 替换成英文双引号
     *
     * @param str
     * @return
     */
    public static String removeChineseQuote(String str) {
        Pattern p = Pattern.compile("“(.*?)”");
        Matcher m = p.matcher(str);
        while (m.find()) {
            String group = m.group();
            String newIdentifier = group.substring(1, group.length() - 1).trim();
            str = str.replace(group, "\"" + newIdentifier + "\"");
        }
        return str;
    }

    public static boolean containsAllNumAndSpecial(String fieldName) {
        return fieldName.trim().matches("\\d+_") || fieldName.trim().matches("\\d+");
    }

    public static boolean hasSpecialCharacter(String fieldName) {
        return fieldName.trim().matches(DatasetConstant.SPECIAL_CHARACTER_REGEX);
    }
}

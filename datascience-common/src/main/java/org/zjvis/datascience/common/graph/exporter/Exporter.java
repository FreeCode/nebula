package org.zjvis.datascience.common.graph.exporter;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zjvis.datascience.common.graph.model.MetricResult;
import org.zjvis.datascience.common.vo.graph.AttrVO;
import org.zjvis.datascience.common.vo.graph.LinkVO;
import org.zjvis.datascience.common.vo.graph.NodeVO;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @description Exporter
 * @date 2021-12-29
 */
@Data
public class Exporter {
    protected static final Logger logger = LoggerFactory.getLogger(Exporter.class);
    protected Long graphId;
    protected List<NodeVO> nodes;
    protected List<LinkVO> links;
    protected Map<String, String> labelMap;
    protected JSONObject multiValueHelper;
    protected List<MetricResult> metricResults;


    public static void makeAttributes(List<AttrVO> attrs, JSONObject attributes, String id, String parentId, JSONObject multiValueHelper, List<MetricResult> metricResults){
        for (AttrVO attr: attrs) {
            Object v;
            String key;
            if (multiValueHelper != null
                    && multiValueHelper.getJSONObject(parentId) != null
                    && multiValueHelper.getJSONObject(parentId).containsKey(attr.getKey())) {
                Boolean flag = multiValueHelper.getJSONObject(parentId).getBoolean(attr.getKey());
                assert attr.getValue() instanceof Collection;
                if (flag) {
                    List<String> strs = (List<String>) ((Collection) attr.getValue()).stream().map(Object::toString).collect(Collectors.toList());
                    v = String.join(",", strs);
                } else {
                    v = ((Collection) attr.getValue()).iterator().next();
                }
                key = attr.getKey();
            } else {
                v = attr.getValue();
                key = attr.getKey();
                if (multiValueHelper != null) {
                    //检查主属性是否和其他副属性类型冲突
                    for (String _parentId: multiValueHelper.keySet()) {
                        JSONObject valueHelper = multiValueHelper.getJSONObject(_parentId);
                        Boolean flag = valueHelper.getBoolean(attr.getKey());
                        if (flag != null && flag) {
                            key = attr.getKey() + "(primary)";
                        }
                    }
                }
            }
            attributes.put(key, v);
        }

        //metric
        if (metricResults != null) {
            for (MetricResult metricResult: metricResults) {
                attributes.put(metricResult.getTag(), metricResult.getResult().get(id));
            }
        }
    }

    public static String fieldContentFormat(String content) {
        if (content == null) {
            return "";
        }
        if (content.contains("\"")) {
            content = content.replaceAll("\"", "\"\"");
        }
        return "\"" + content + "\"";
    }

    public void makeNodeCfg(JSONObject config, NodeVO node) {
        config.put("style", node.getStyle());
        config.put("labelCfg", node.getLabelCfg());
        config.put("type", node.getType());
        config.put("x", node.getX());
        config.put("y", node.getY());
        config.put("size", node.getSize());
    }

    public void makeLinkCfg(JSONObject config, LinkVO link) {
        config.put("style", link.getStyle());
        config.put("labelCfg", link.getLabelCfg());
        config.put("type", link.getType());
        config.put("controlPoints", link.getControlPoints());
        config.put("curveOffset", link.getCurveOffset());
    }
}

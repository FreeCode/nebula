package org.zjvis.datascience.common.graph.enums;

/**
 * @description GraphFilterSubTypeEnum
 * @date 2021-12-29
 */
public enum GraphFilterSubTypeEnum {
    NODE_CATEGORY("节点类别", 0, "node/node_category.json"),

    LINK_CATEGORY("边类别", 1, "link/link_category.json"),
    LINK_WEIGHT_RANGE("边权重范围", 2, "link/link_weight_range.json"),
    LINK_ONLY_DIRECTED("仅有向边", 3, "link/link_only_directed.json"),
    LINK_ONLY_UNDIRECTED("仅无向边", 4, "link/link_only_undirected.json"),
    LINK_ONLY_DOUBLE_DIRECTED("仅双向边", 5, "link/link_only_double_directed.json"),
    LINK_REMOVE_SELF_LOOP("移除自环边", 6,"link/link_remove_self_loop.json"),

    ATTR_EQUAL("属性等于", 7, "attr/attr_equal.json"),
    ATTR_RANGE("属性范围", 8, "attr/attr_range.json"),
    ATTR_NOT_NULL("属性非空", 9, "attr/attr_not_null.json"),
    ATTR_CATEGORY("属性类别", 10, "attr/attr_category.json"),
    ATTR_CATEGORY_COUNT("属性类别计数", 11, "attr/attr_category_count.json"),
    ATTR_INNER_LINK("内部边", 12, "attr/attr_inner_link.json"),
    ATTR_OUTER_LINK("外部边", 13, "attr/attr_outer_link.json"),

    TOPOLOGY_K_CORE("k核心", 14, "topology/topology_k_core.json"),
    TOPOLOGY_SELF_LOOP("具有自环", 15, "topology/topology_self_loop.json"),
    TOPOLOGY_DEGREE_RANGE("度范围", 16, "topology/topology_degree_range.json"),
    TOPOLOGY_IN_DEGREE_RANGE("入度范围", 17, "topology/topology_in_degree_range.json"),
    TOPOLOGY_OUT_DEGREE_RANGE("出度范围", 18, "topology/topology_out_degree_range.json"),

    OPERATION_OR("并集", 19),
    OPERATION_AND("交集", 20),
    OPERATION_NOT("非", 21),
    ;
    private final String desc;

    private final int val;

    private final String tplFileName;
    private static final String TPL_FILE_PREFIX = "template/graph_filter/";

    GraphFilterSubTypeEnum(String desc, int val, String tplFileName) {
        this.desc = desc;
        this.val = val;
        this.tplFileName = tplFileName;
    }

    GraphFilterSubTypeEnum(String desc, int val) {
        this.desc = desc;
        this.val = val;
        this.tplFileName = null;
    }


    public String getDesc() {
        return desc;
    }

    public int getVal() {
        return val;
    }

    public String getTplFileName() {
        return tplFileName != null ? TPL_FILE_PREFIX + tplFileName : null;
    }
}

package org.zjvis.datascience.common.vo.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @description 创建用户相关VO
 * @date 2021-12-13
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class CreateUserVO {

    private static final long serialVersionUID = -6347175492072993897L;

    @NotBlank(message = "用户名不能为空")
    @Length(max = 20, message = "用户名长度不能超过20")
    @Pattern(regexp = "^.*[^\\d].*$", message = "用户名不能为纯数字")
    @ApiModelProperty(value = "登录用户名", required = true)
    private String name;

    @NotBlank(message = "手机号码不能为空")
    @Pattern(regexp = "^1[3|4|5|7|8|9][0-9]{9}$", message = "手机号码格式有误")
    @ApiModelProperty(value = "手机", required = true)
    private String phone;

    // 密码由数字和字母特殊字符组成，并且要同时含有数字和字母，且长度要在8-16位之间。
    @Pattern(regexp = "^(?=.*\\d)^(?![0-9]+$)(?![a-zA-Z]+$).{8,16}$", message = "密码需同时含有数字和字母,可包含特殊字符，且长度在8-16位之间!")
    @ApiModelProperty(value = "密码", required = true)
    @NotBlank(message = "密码不能为空")
    private String password;

    @ApiModelProperty(value = "图形验证码")
    private String captchaCode;

    @Pattern(regexp = "^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$", message = "邮箱地址格式有误")
    //   @ApiModelProperty(value = "邮箱", required = true)
    private String email;

    @ApiModelProperty(value = "状态")
    private Byte status;

    private String remark;

}

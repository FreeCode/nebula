package org.zjvis.datascience.common.dto.user;

import lombok.Data;

/**
 * @description 用户配置DTO
 * @date 2021-12-24
 */
@Data
public class UserConfigDTO {
    private Long id;

    private Long userId;

    private Integer noticeReceiveLevel;//消息接收级别

    private Integer nodeDeletionSecondaryConfirmation;//数据视图中删除节点二次确认

}

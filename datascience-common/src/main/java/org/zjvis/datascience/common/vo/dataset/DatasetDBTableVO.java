package org.zjvis.datascience.common.vo.dataset;

import lombok.Data;
import org.zjvis.datascience.common.dto.dataset.DatasetScheduleDTO;

import javax.validation.constraints.NotBlank;

/**
 * @description 数据结果相关VO
 * @date 2021-08-24
 */
@Data
public class DatasetDBTableVO extends BaseTableConfigVO {

    @NotBlank(message = "表名不能为空")
    private String tableName;
    private DatasetScheduleDTO datasetScheduleConfig;
}

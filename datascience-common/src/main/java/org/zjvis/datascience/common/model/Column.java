package org.zjvis.datascience.common.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @description SQL查询结果 字段包装类
 * @date 2021-12-08
 */
@Data
public class Column implements Serializable {

    private String name;

    private String type;


    private String desc;

    public Column(String name, String type, String desc) {
        this.name = name;
        this.type = type;
        this.desc = desc;
    }

}

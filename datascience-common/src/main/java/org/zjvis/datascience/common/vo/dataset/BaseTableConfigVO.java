package org.zjvis.datascience.common.vo.dataset;

import lombok.Data;
import org.zjvis.datascience.common.dto.dataset.DatasetColumnDTO;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @description 数据上传相关基础VO
 * @date 2021-01-28
 */
@Data
public class BaseTableConfigVO {

    @NotNull(message = "需要上传的数据字段不能为空")
    @Size(min = 1, message = "需要上传的数据字段不能为空")
    private List<@Valid DatasetColumnDTO> data;

//    /**
//     * 数据集预览排序字段（暂时默认只支持单个字段升序排序）
//     */
//    private String sortColumnName;
//
//    /**
//     * 排序类型，升序：asc，降序：desc
//     */
//    private String sortType;
}

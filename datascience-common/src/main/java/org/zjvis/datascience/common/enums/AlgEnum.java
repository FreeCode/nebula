package org.zjvis.datascience.common.enums;

/**
 * @description 通用算子节点类型 枚举类
 * @date 2021-10-22
 */
public enum AlgEnum {
    DBSCAN("聚类-DBSCAN", 1),
    KMEANS("聚类-KMEANS", 2),
    LINEARREGRE("回归-LINEAR_REGRESSION", 3),
    PCA_DENSE("降维-PCA", 4),
    TSNE("降维-TSNE", 5),
    LLE("降维-LLE", 6),
    STAT_ANOMALY("异常值检测-STAT", 7),
    ISO_FOREST("异常值检测-ISO_FOREST", 8),
    LOGREGRE("分类-LOGISTIC_REGRESSION", 9),
    FP_GROWTH("频繁模式挖掘-FP_GROWTH", 10),
    PREFIX_SPAN("频繁模式挖掘-PREFIX_SPAN", 11),
    GRAPH_BUILD("图网络构建", 12),
    SELF_DEFINE("自定义算子", 13);


    private String desc;

    private int val;

    AlgEnum(String desc, int val) {
        this.desc = desc;
        this.val = val;
    }

    public String getDesc() {
        return desc;
    }

    public int getVal() {
        return val;
    }
}

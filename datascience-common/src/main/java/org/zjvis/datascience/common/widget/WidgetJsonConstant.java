package org.zjvis.datascience.common.widget;

/**
 * @description 可视化图表控件相关常量类
 * @date 2021-12-22
 */
public class WidgetJsonConstant {

    /**
     * 各种类型widget的描述信息
     */
    public static final String WIDGET_JSON = "widgetJson";

    public static final String CHART_OPTION = "chartOptions";

    public static final String IS_AGGR_QUERY = "isAggr";

    public static final String RELATIONSHIP_IDX = "relationship";

    public static final String PUBLISHED_FLAG = "published";

    public static final String WIDGET_ID = "widgetId";

    /**
     * 查询dataset类型widget 需要指定tableJson
     */
    public static final String TABLE_JSON = "tableJson";

    public static final String INTERACTION_JSON = "interactionJson";

    /**
     * chartOptions 下的子内容
     */

    public static final String X_ATTR_VALUE = "xAxisAttribute";

    public static final String DATA_TYPE = "dataType";

    /**
     * widgetJson 下的子内容
     */
    public static final String CONFIG = "config";

    public static final String FILTERS = "filters";

    public static final String KEYS = "keys";

    public static final String VALUES = "values";

    public static final String COL = "col";

    public static final String TOP_N = "topN";

    public static final String SORT = "sort";

    public static final String TYPE_DESC = "desc";

    public static final String GRID_ITEMS = "gridItems";

    public static final String TABLES = "tables";

    /**
     * 筛选器控件 指定 使用数据集类型
     */
    public static final String DATASET_FLAG = "dataset";

    public static final String PIPELINE_FLAG = "task";

    public static final String DATA_TABLE_FLAG = "table";
}

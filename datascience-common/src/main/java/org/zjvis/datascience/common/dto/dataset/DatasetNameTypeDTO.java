package org.zjvis.datascience.common.dto.dataset;

import lombok.Data;

/**
 * @description 数据集名称类型DTO
 * @date 2021-12-24
 */
@Data
public class DatasetNameTypeDTO {
  
  private Long id;
  
  private String name;
  
  private String type;
  
  private String tableName;
  
}

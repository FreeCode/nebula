package org.zjvis.datascience.common.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;
import lombok.Data;

/**
 * @description 任务节点实例 TaskInstance 信息表，任务节点实例 分页信息DTO
 * @date 2021-08-30
 */
@Data
public class TaskInstancePageQueryDTO {
    private String username;

    private Integer type;

    private String projectName;

    private String pipelineName;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endTime;
}

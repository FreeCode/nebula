package org.zjvis.datascience.common.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @description 平台左侧可选任务列表 POJO
 * @date 2020-07-29
 */
@Data
public class TaskItems {
    private String name;
    private List<TaskMeta> items = new ArrayList<>();
    private int subType;

    public void addItem(TaskMeta task) {
        items.add(task);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setItems(List<TaskMeta> metas) {
        this.items = metas;
    }

    public String getName() { return name; }

    public List<TaskMeta> getItems() { return items; }

}

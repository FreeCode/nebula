package org.zjvis.datascience.common.model;

import lombok.Data;

/**
 * @description SelectItem 聚合函数包装类
 * @date 2020-07-27
 */
@Data
public class FunctionConfig {

    private String name;

    private Object[] params;

    public FunctionConfig(String name, Object[] params) {
        this.name = name;
        this.params = params;
    }

}

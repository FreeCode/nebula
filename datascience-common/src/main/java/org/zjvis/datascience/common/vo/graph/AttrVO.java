package org.zjvis.datascience.common.vo.graph;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AttrVO implements Serializable {
    private String key;

    private Object value;

    private int type;
}

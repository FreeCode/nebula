package org.zjvis.datascience.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.SerializationUtils;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @description 数据集信息表，数据集DTO
 * @date 2021-10-19
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class DatasetDTO extends BaseDTO implements Serializable {
    private Long id;

    private String name;

    private Long userId;

    private Long categoryId;

    private String dataJson;

    private String description;

    private String dataConfig;

    private String incrementalDataConfig;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        DatasetCategoryDTO other = (DatasetCategoryDTO) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
                && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
                && (this.getCategoryId() == null ? other.getParentId() == null : this.getCategoryId().equals(other.getParentId()))
                && (this.getDataJson() == null ? other.getParentId() == null : this.getDataJson().equals(other.getParentId()))
                && (this.getDataConfig() == null ? other.getParentId() == null : this.getDataConfig().equals(other.getParentId()))
                && (this.getIncrementalDataConfig() == null ? other.getParentId() == null : this.getIncrementalDataConfig().equals(other.getParentId()))
                && (this.getDescription() == null ? other.getParentId() == null : this.getDescription().equals(other.getParentId()))
                && (this.getGmtCreate() == null ? other.getGmtCreate() == null : this.getGmtCreate().equals(other.getGmtCreate()))
                && (this.getGmtModify() == null ? other.getGmtModify() == null : this.getGmtModify().equals(other.getGmtModify()))
                && (this.getGmtModifier() == null ? other.getGmtModifier() == null : this.getGmtModifier().equals(other.getGmtModifier()))
                && (this.getGmtCreator() == null ? other.getGmtCreator() == null : this.getGmtCreator().equals(other.getGmtCreator()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getCategoryId() == null) ? 0 : getCategoryId().hashCode());
        result = prime * result + ((getDataJson() == null) ? 0 : getDataJson().hashCode());
        result = prime * result + ((getDataConfig() == null) ? 0 : getDataConfig().hashCode());
        result = prime * result + ((getIncrementalDataConfig() == null) ? 0 : getIncrementalDataConfig().hashCode());
        result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
        result = prime * result + ((getGmtCreate() == null) ? 0 : getGmtCreate().hashCode());
        result = prime * result + ((getGmtModify() == null) ? 0 : getGmtModify().hashCode());
        result = prime * result + ((getGmtModifier() == null) ? 0 : getGmtModifier().hashCode());
        result = prime * result + ((getGmtCreator() == null) ? 0 : getGmtCreator().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append(" id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", userId=").append(userId);
        sb.append(", categoryId=").append(categoryId);
        sb.append(", dataJson=").append(dataJson);
        sb.append(", dataConfig=").append(dataConfig);
        sb.append(", dataJson=").append(dataJson);
        sb.append(", incrementalDataConfig=").append(incrementalDataConfig);
        sb.append("]");
        return sb.toString();
    }

    public String formatTime(LocalDateTime date) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return dateTimeFormatter.format(date);
    }

    public DatasetDTO copyOne(Long userId){
        DatasetDTO clone = SerializationUtils.clone(this);
        clone.setId(null);
        clone.setUserId(userId);
        clone.setGmtCreator(userId);
        clone.setGmtModifier(userId);
        return clone;
    }
}

package org.zjvis.datascience.common.util;

import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.List;

/**
 * @description 分页POJO
 * @date 2021-10-14
 */
public class Page<T> implements Serializable {

    private static final long serialVersionUID = -8630833487512611990L;

    @Override
    public String toString() {
        return "Page [totalElements=" + totalElements + ", totalPages=" + totalPages + ", curPage=" + curPage
                + ", pageSize=" + pageSize + ", data=" + data + "]";
    }

    //总记录数
    private Integer totalElements;

    //总页数
    private Integer totalPages;

    //当前页
    private Integer curPage;

    //页大小
    private Integer pageSize;

    //结果集
    private List<T> data = Lists.newArrayList();

    public void setTotalElementsAndPage(Integer totalElements) {
        this.totalElements = totalElements;

        if (this.pageSize != null && this.pageSize > 0) {
            if (totalElements % this.pageSize == 0) {
                this.totalPages = totalElements / this.pageSize;
            } else {
                this.totalPages = totalElements / this.pageSize + 1;
            }
        }
    }

    public Integer getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(Integer totalElements) {
        this.totalElements = totalElements;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public Integer getCurPage() {
        return curPage;
    }

    public void setCurPage(Integer curPage) {
        this.curPage = curPage;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public <S> Page<S> transfer(List<S> data) {
        Page<S> page = new Page<S>();
        page.setCurPage(curPage);
        page.setPageSize(pageSize);
        page.setTotalElements(totalElements);
        page.setTotalPages(totalPages);
        page.setData(data);
        return page;
    }

}

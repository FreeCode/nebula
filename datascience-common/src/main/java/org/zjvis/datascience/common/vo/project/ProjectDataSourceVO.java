package org.zjvis.datascience.common.vo.project;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description 项目数据集相关VO
 * @date 2021-09-28
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProjectDataSourceVO {

    private String userName;

    private List<ProjectDataSourceTableVO> dataset;

    private List<ProjectDataSourceTableVO> pipeline;
}

package org.zjvis.datascience.common.model.stat;

import lombok.Data;
import org.zjvis.datascience.common.util.Page;

import java.util.List;

/**
 * @description 字段统计计数器，用于数据结果渲染
 * @date 2021-09-26
 */
@Data
public class ColumnStat<T extends Comparable> {

    private static final long serialVersionUID = 1630021059435163943L;

    private String table;

    private String name;

    private Integer type;

    //总记录数
    private Integer total;

    //distinct记录数
    private Integer distinctTotal;

    //频率出现最高的列
    private ColumnCounter topColumn;

    //最大值
    private T max;

    //最小值
    private T min;

    //分页详情统计
    private Page<ColumnCounter> pageCount;

    //区间概览统计
    private List<ColumnRange> rangeCount;

    public ColumnStat() {
    }

    public ColumnStat(String table, String name, Integer type) {
        this.table = table;
        this.name = name;
        this.type = type;
    }

}

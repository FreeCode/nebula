package org.zjvis.datascience.common.constant;

/**
 * @description URL信息管理常量
 * @date 2021-12-24
 */
public interface URLConstant {
    /**
     * 提交任务的url
     */
    String RUN_JOB_PARAMS = "jobs?appName=datascience-algo-1.0.0-SNAPSHOT.jar&classPath=org.zjvis.datascience.spark.AlgorithmAdaptor&context=%s&sync=true&timeout=1000000";
    /**
     * 创建context的url
     */
    String CREATE_CONTEXT = "contexts/%s?spark.executor.instances=10&spark.executor.memory=2g&spark.executor.cores=2&spark.driver.memory=4g&spark.driver.maxResultSize=16g&spark.driver.cores=16&spark.sql.crossJoin.enabled=true&context-factory=spark.jobserver.context.SessionContextFactory";
    /**
     * 停止jobServer上的context
     */
    String DELETE_CONTEXT = "contexts/%s";

    /**
     * 要创建的context名称
     */
    String CONTEXT_NAME = "contextName";
    /**
     * 每个节点分配的CPU核数
     */
    String NUM_CPU_CORES = "num-cpu-cores";
    /**
     * 每个节点分配的内存
     */
    String MEMORY_PER_NODE = "memory-per-node";

    String SLASH = "/";
}

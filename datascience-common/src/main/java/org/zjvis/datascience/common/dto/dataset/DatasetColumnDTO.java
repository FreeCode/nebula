package org.zjvis.datascience.common.dto.dataset;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @description 数据集列信息DTO
 * @date 2021-12-24
 */
@Data
public class DatasetColumnDTO {
    @NotBlank(message = "字段名不能为空")
    private String name;
    //@NotBlank(message = "字段类型不能为空")
    private String type;

    /**
     * 是否导入
     */
    private Boolean importColumn;

    /**
     * 数据分级id
     */
    private Integer dataLevelId;

    /**
     * 脱敏方式
     */
    private String dataMaskingType;

    /**
     * 数据语义
     */
    private String semantic;
}

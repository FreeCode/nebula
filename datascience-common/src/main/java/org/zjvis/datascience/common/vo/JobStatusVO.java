package org.zjvis.datascience.common.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @description 任务执行结果 相关VO
 * @date 2021-11-24
 */
@Data
public class JobStatusVO {
    @NotNull(message = "applicationId不能为空", groups = Id.class)
    @ApiModelProperty(value = "id", required = true)
    private String id;

    private String state;

    private String finalStatus;

    private Float progress;

    private String diagnostics;

    private Long startedTime;

    private Long finishedTime;

    public interface Id {
    }

}

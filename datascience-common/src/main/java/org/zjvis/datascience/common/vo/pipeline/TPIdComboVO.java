package org.zjvis.datascience.common.vo.pipeline;

import com.google.common.collect.Lists;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @description 数据视图多种ID请求VO
 * @date 2021-12-23
 */
@Data
public class TPIdComboVO {

    @NotBlank(message = "task ids cannot be null or empty")
    private String ids;

    @NotNull(message = "pipelineId cannot be null")
    @Min(value = 1, message = "请输入有效的pipelineId")
    private Long pipelineId;

    @NotNull(message = "projectId cannot be null")
    @Min(value = 1, message = "请输入有效的projectId")
    private Long projectId;

    /**
     * 是否跨项目 或者跨pipeline
     */
    private Boolean isCross;

    public List<Long> getTaskIds() {
        if (this.ids.contains(",")){
            return new ArrayList<String>(Arrays.asList(this.ids.split(","))).stream().map(Long::parseLong).collect(Collectors.toList());
        }
        return Lists.newArrayList(Long.parseLong(ids));
    }

    public Boolean getIsCross(){
        if (null == this.isCross){
            return false;
        }else {
            return this.isCross;
        }
    }
}

/**
 * @NotNull: a constrained CharSequence, Collection, Map, or Array is valid as long as it's not null, but it can be empty.
 * @NotEmpty: a constrained CharSequence, Collection, Map, or Array is valid as long as it's not null, and its size/length is greater than zero.
 * @NotBlank: a constrained String is valid as long as it's not null, and the trimmed length is greater than zero.
 */
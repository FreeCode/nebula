package org.zjvis.datascience.common.widget.request;

import lombok.Data;
import org.zjvis.datascience.common.widget.RelateWidget;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @description 用于关联图表渲染的请求POJO 用于图表联动和 筛选器控件 结果渲染
 * @date 2021-12-01
 */
@Data
public class RelateWidgetResultVO {

    private Long widgetId;

    @NotBlank(message = "xAttrStr cannot be null or empty")
    private String xAttrStr;

    private String xAttrValueType;

    private List<RelateWidget> relateWidgets;


}


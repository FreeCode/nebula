package org.zjvis.datascience.common.graph.annotation;

import java.lang.annotation.*;

/**
 * @description CheckNull
 * @date 2021-12-29
 */
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(CheckNulls.class)
public @interface CheckNull {
    String field() default "";
}

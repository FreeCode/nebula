package org.zjvis.datascience.common.vo;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.zjvis.datascience.common.dto.TaskDTO;
import org.zjvis.datascience.common.util.DozerUtil;

import static org.zjvis.datascience.common.constant.DataJsonConstant.FORBIDDEN_FLAG;

/**
 * @description 任务节点表 Task相关VO
 * @date 2021-11-26
 */
@Data
@NoArgsConstructor
public class TaskVO extends BaseVO {

    private Long id;

    private String name;

    private Long projectId;

    private Long pipelineId;

    private String parentId;

    // 多个用逗号隔开
    private String childId;

    private Long userId;

    private Integer type;

    private JSONObject data;

    private Long operatorId;

    private Exception exception;

    public TaskDTO toTask() {
        Exception tempException = null;
        if (this.getException() != null) {
            tempException = this.getException();
            this.setException(null);
        }
        TaskDTO task = DozerUtil.mapper(this, TaskDTO.class);
        task.setDataJson(data.toJSONString());
        task.setException(tempException);
        return task;
    }

    public TaskVO(Long taskId, Long pipelineId) {
        this.setId(taskId);
        this.setPipelineId(pipelineId);
    }

    public TaskVO initForbiddenInfo() {
        JSONObject json = new JSONObject();
        json.put(FORBIDDEN_FLAG, true);
        json.put("isSimple", true);
        this.setData(json);
        return this;
    }

    public TaskVO initUnForbiddenInfo() {
        JSONObject json = new JSONObject();
        json.put(FORBIDDEN_FLAG, false);
        json.put("isSimple", true);
        this.setData(json);
        return this;
    }
}

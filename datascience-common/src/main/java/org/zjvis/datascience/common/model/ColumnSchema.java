package org.zjvis.datascience.common.model;

import lombok.Data;

/**
 * @description SQL查询结果 字段包装类
 * @date 2020-08-06
 */
@Data
public class ColumnSchema {

    private String columnName;//字段名

    private String columnType;//字段类型

    private Integer columnSize;//数据长度（decimal不使用此字段）

    private Integer integerDigits;//decimal数据整数部分长度

    private Integer decimalPlaces;//decimal数据小数点后长度
}

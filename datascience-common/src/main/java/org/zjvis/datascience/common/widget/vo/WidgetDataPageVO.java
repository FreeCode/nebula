package org.zjvis.datascience.common.widget.vo;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.zjvis.datascience.common.widget.enums.WidgetTypeEnum;
import org.zjvis.datascience.common.validator.NotNullNumber;
import org.zjvis.datascience.common.vo.PageVO;

import javax.validation.constraints.NotNull;

/**
 * @description 用于图表控件的结果分页渲染POJO （比如收藏，和结果表）
 * @date 2021-12-01
 */
@Data
public class WidgetDataPageVO extends PageVO {

    @NotNullNumber(message = "taskId or datasetId cannot be null")
    private Long id; //taskId or datasetId

    private Long projectId;

    @NotNull(message = "widget type cannot be empty")
    private String type;

    public WidgetTypeEnum getTypeEnum() {
        return WidgetTypeEnum.valueOf(StringUtils.upperCase(this.getType()));
    }

    public Integer getCurPage() {
        return super.getCurPage() == null ? 1 : super.getCurPage();
    }

    public Integer getPageSize() {
        return super.getPageSize() == null ? 20 : super.getPageSize();
    }
}

package org.zjvis.datascience.common.enums;

/**
 * @description Spark任务执行状态枚举类
 * @date 2020-09-29
 */
public enum JobStatus {
    //job的运行状态
    NEW,
    NEW_SAVING,
    SUBMITTED,
    ACCEPTED,
    RUNNING,
    FINISHED,
    FAILED,
    KILLED,
    //下面是最终状态,还包含两个状态FAILED,KILLED
    UNDEFINED,
    SUCCEEDED;

    public static boolean jobIsEnd(String state) {
        return FINISHED.toString().equals(state) || FAILED.toString().equals(state) || KILLED.toString().equals(state);
    }
}

package org.zjvis.datascience.common.dto;

import lombok.Data;

/**
 * @description 任务节点Task信息表，任务节点快照DTO
 * @date 2021-09-06
 */
@Data
public class TaskSnapshotDTO extends BaseDTO {

    private Long id;

    private Long pipelineSnapshotId;

    private Long pipelineId;

    private Long projectId;

    private String taskJson;
}

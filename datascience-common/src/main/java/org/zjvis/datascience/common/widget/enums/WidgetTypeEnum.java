package org.zjvis.datascience.common.widget.enums;

/**
 * @description 图表控件类型 枚举类
 * @date 2021-12-01
 */
public enum WidgetTypeEnum {

    DATA("data", 1),
    CONFIG("configuration", 2),
    CHART("chart", 3),
    TASK("task", 4),
    TASK_COPY("task_copy", 5),
    RECOMMEND("recommend", 6),
    TCLEAN("clean", 7),
    TEXT("text", 8),
    DATASET("dataset", 9),
    NETWORK("network", 10),
    FILTER_FORM("filterForm", 11);

    private String desc;

    private int val;


    WidgetTypeEnum(String desc, int val) {
        this.desc = desc;
        this.val = val;
    }

    public String getDesc() {
        return desc;
    }

    public Integer getVal() {
        return val;
    }

    public Boolean isTaskNode() {
        return this.equals(TASK) || this.equals(TASK_COPY) || this.equals(TCLEAN);
    }

    public Boolean isDataNode() {
        return this.equals(DATA) || this.equals(DATASET);
    }
}

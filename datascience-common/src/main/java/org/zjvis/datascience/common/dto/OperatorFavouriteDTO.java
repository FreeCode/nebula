package org.zjvis.datascience.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

/**
 * @description 用户算子收藏信息表，用户算子收藏 DTO
 * @date 2021-01-14
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class OperatorFavouriteDTO extends BaseDTO implements Serializable {

    private Long id;

    private Integer taskType;

    private Integer algType;

    private String algName;

    private boolean starred;

    private boolean isEdited;

    private String descUrl;

    private static final long serialVersionUID = 1L;

    private Integer status;

    private String suggestion;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        OperatorFavouriteDTO other = (OperatorFavouriteDTO) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getTaskType() == null ? other.getTaskType() == null : this.getTaskType().equals(other.getTaskType()))
                && (this.getAlgType() == null ? other.getAlgType() == null : this.getAlgType().equals(other.getAlgType()))
                && (this.getAlgName() == null ? other.getAlgName() == null : this.getAlgName().equals(other.getAlgName()))
                && (this.getGmtCreate() == null ? other.getGmtCreate() == null : this.getGmtCreate().equals(other.getGmtCreate()))
                && (this.getGmtModify() == null ? other.getGmtModify() == null : this.getGmtModify().equals(other.getGmtModify()))
                && (this.getGmtModifier() == null ? other.getGmtModifier() == null : this.getGmtModifier().equals(other.getGmtModifier()))
                && (this.getGmtCreator() == null ? other.getGmtCreator() == null : this.getGmtCreator().equals(other.getGmtCreator()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getTaskType() == null) ? 0 : getTaskType().hashCode());
        result = prime * result + ((getAlgType() == null) ? 0 : getAlgType().hashCode());
        result = prime * result + ((getAlgName() == null) ? 0 : getAlgName().hashCode());
        result = prime * result + ((getGmtCreate() == null) ? 0 : getGmtCreate().hashCode());
        result = prime * result + ((getGmtModify() == null) ? 0 : getGmtModify().hashCode());
        result = prime * result + ((getGmtModifier() == null) ? 0 : getGmtModifier().hashCode());
        result = prime * result + ((getGmtCreator() == null) ? 0 : getGmtCreator().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append(", id=").append(id);
        sb.append(", taskType=").append(taskType);
        sb.append(", algType=").append(algType);
        sb.append(", algName=").append(algName);
        sb.append(", gmtCreator=").append(gmtCreator);
        sb.append("]");
        return sb.toString();
    }
}

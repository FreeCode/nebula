package org.zjvis.datascience.common.dto;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @description Zeppelin版本的自定义算子节点的请求返回POJO
 * @date 2021-04-19
 */
@Data
public class ZeppelinStatusDTO {
    @NotNull
    private String status = "FAIL";

    private String message;

    private JSONObject body;

}

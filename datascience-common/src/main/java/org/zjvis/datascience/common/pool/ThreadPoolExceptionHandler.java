
package org.zjvis.datascience.common.pool;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @description 异常线程池
 * @date 2020-04-10
 */
public class ThreadPoolExceptionHandler implements Thread.UncaughtExceptionHandler {
    private final static Logger logger = LoggerFactory.getLogger("ThreadPoolExceptionHandler");

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        logger.error(e.getMessage(), e);
    }

}

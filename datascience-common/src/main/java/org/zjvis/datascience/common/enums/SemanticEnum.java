package org.zjvis.datascience.common.enums;

/**
 * @description 语义推断类型枚举类
 * @date 2021-07-29
 */
public enum SemanticEnum {
    NULL("无", "null", 0),
    GEOGRAPHY("地理", "geography", 1),
    NETWORK("网络", "network", 2),
    INFO("信息", "info", 3),
    CATEGORY("类别", "category", 4),
    ;

    private final String desc;

    private final String val;

    private final int type;

    SemanticEnum(String desc, String val, int type) {
        this.desc = desc;
        this.val = val;
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public String getVal() {
        return val;
    }

    public int getType() {
        return type;
    }

}

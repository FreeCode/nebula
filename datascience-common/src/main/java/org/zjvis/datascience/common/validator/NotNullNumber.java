package org.zjvis.datascience.common.validator;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * @description 数字类型非空判断
 * @date 2021-11-26
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = NotNullNumberValidator.class)
@Documented
public @interface NotNullNumber {

    String message() default "";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}

class NotNullNumberValidator implements ConstraintValidator<NotNullNumber, Number> {

    @Override
    public boolean isValid(Number value, ConstraintValidatorContext context) {
        return value != null;
    }
}
package org.zjvis.datascience.common.vo;

import com.alibaba.fastjson.JSONArray;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * @description 图左侧面板 查询相关VO
 * @date 2020-08-07
 */
@Data
public class ParamItemVO {

    public static String[] typeOptions = new String[] {
            "string",
            "number",
            "select",
            "checkbox"
    };

    public static String[] paramTypeOptions = new String[] {
            "int2",
            "int4",
            "char",
            "varchar",
            "float4",
            "float8",
            "text"
    };

    private String defaultV;

    private String paramType;

    private String englishName;

    private String name;

    private String type;

    private String value;

    private JSONArray items;
}

package org.zjvis.datascience.common.dto;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.zjvis.datascience.common.util.DozerUtil;
import org.zjvis.datascience.common.vo.RecommendVO;

/**
 * @description 语义推荐信息表，语义推荐DTO
 * @date 2020-08-31
 */
@Data
public class RecommendDTO extends BaseDTO {
    private Long id;

    private String name;

    private Long userId;

    private Long categoryId;

    private String tableNames;

    private String dataJson;

    public RecommendVO view() {
        RecommendVO recommendVO = DozerUtil.mapper(this, RecommendVO.class);
        recommendVO.setData(JSONObject.parseObject(dataJson));
        return recommendVO;
    }

}

package org.zjvis.datascience.common.vo.dataset;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @description 数据上传相关基础VO （根据分类目录id获取数据集）
 * @date 2020-07-29
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CategoryIdOfTheDatasetVO {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "所属类别id", required = true)
    @NotNull(message = "所属类别id不能为空")
    @Min(value = 1, message = "请输入有效所属类别id")
    private Long categoryId;

    public interface CategoryId {
    }
}

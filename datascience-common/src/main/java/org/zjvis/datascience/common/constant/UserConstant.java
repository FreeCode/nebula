package org.zjvis.datascience.common.constant;


/**
 * @description 用户常量类
 * @date 2020-06-01
 */
public class UserConstant {

    public final static String SEX_MEN = "男";

    public final static String SEX_WOMEN = "女";

    /**
     * redis key
     */
    /**
     * 用户发送邮件限制次数
     */
    public final static int USER_PHONE_LIMIT_COUNT = 3;

    /**
     * 用户邮箱注册信息发送验证码
     */
    public final static String USER_EMAIL_REGISTER = "user:email:register:";

    /**
     * 用户邮箱激修改信息发送验证码
     */
    public final static String USER_EMAIL_UPDATE = "user:email:update:";

    /**
     * 用户邮箱忘记发送验证码
     */
    public final static String USER_EMAIL_RESET_PASSWORD = "user:email:reset-password:";

    /**
     * 用户其他操作发送验证码
     */
    public final static String USER_EMAIL_OTHER = "user:email:other:";

    /**
     * 一天的秒数 24x60x60
     */
    public final static long DATE_SECOND = 86400;

    /**
     * 发邮件次数
     */
    public final static int COUNT_SENT_EMAIL = 3;

    /**
     * 账号密码不正确登录失败次数
     */
    public final static int COUNT_LOGIN_FAIL = 100;

    /**
     * 账号5min内密码不正确登录失败次数
     */
    public final static int COUNT_LOGIN_FAIL_IN_5MIN = 6;

    /**
     * 账号锁定30min
     */
    public final static long USER_LOCKED_TIME = 60 * 30;

    /**
     * 用户登录限制次数
     */
    public final static String USER_LOGIN_LIMIT_COUNT = "user:login:limit:count:";

    /**
     * 初始化管理员ID
     */
    public final static int ADMIN_USER_ID = 1;

    /**
     * 管理员角色ID
     */
    public final static int ADMIN_ROLE_ID = 1;

    /**
     * 注册用户角色ID
     */
    public final static int REGISTER_ROLE_ID = 2;

    /**
     * 用户 token 请求头
     */
    public final static String USER_TOKEN_KEY = "Authorization";

    /**
     * 短信有效时间
     */
    public static final long EFFECTIVE_TIME = 60 * 5;

    /**
     * 禁发短信时间
     */
    public static final long PROHIBIT_TIME = 60;

    /**
     * 手机号记录有效时间
     */
    public static final long RECORD_EFFECTIVE_TIME = 60 * 60 * 24;

    /**
     * 注册手机号头部
     */
    public static final String REGISTER_PREFIX = "phone_register_";

    /**
     * 修改密码手机号头部
     */
    public static final String MODIFY_PREFIX = "phone_modify_";

    /**
     * 日登录记录头部
     */
    public static final String LOGIN_TIMES_PREFIX = "login_record_";

    /**
     * 图片验证手机号头部
     */
    public static final String CAPTCHA_PREFIX = "phone_captcha_";

    /**
     * 图片验证手机号头部
     */
    public static final String BIND_PREFIX = "phone_bind_";

    /**
     * 手机短信登录头部
     */
    public static final String LOGIN_PREFIX = "phone_login_";


    /**
     * 注册类型标记
     */
    public static final String TYPE_OF_REGISTER = "register";

    /**
     * 修改密码类型标记
     */
    public static final String TYPE_OF_MODIFY = "modify";

    /**
     * 手机绑定类型标记
     */
    public static final String TYPE_OF_BIND = "bind";

    /**
     * 短信登录类型标记
     */
    public static final String TYPE_OF_LOGIN = "login";


}

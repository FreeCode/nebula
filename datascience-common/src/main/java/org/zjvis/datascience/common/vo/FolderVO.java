package org.zjvis.datascience.common.vo;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.zjvis.datascience.common.dto.MLModelDTO;
import org.zjvis.datascience.common.dto.PipelineDTO;
import org.zjvis.datascience.common.dto.FolderDTO;
import org.zjvis.datascience.common.util.DozerUtil;
import springfox.documentation.spring.web.json.Json;

/**
 * @description 平台右侧文件夹数据表 Folder 相关VO
 * @date 2021-06-15
 */
@Data
public class FolderVO extends BaseVO{
    private Long id;

    private String name;

    private Long projectId;

    private Long userId;

    private Long modelNum;

    private JSONObject modelInfo;

    private Long unfold;

    public FolderDTO toFolder() {
        FolderDTO folder = DozerUtil.mapper(this, FolderDTO.class);
        folder.setModelInfo(modelInfo.toJSONString());
        return folder;
    }
}

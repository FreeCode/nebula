package org.zjvis.datascience.common.widget.vo;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.zjvis.datascience.common.vo.BaseVO;

import java.util.List;

/**
 * @description 用于来源于数据视图的图表控件的结果渲染POJO （比如收藏，和结果表）
 * @date 2021-12-01
 */
@Data
@NoArgsConstructor
public class PipelineWidgetVO extends BaseVO {

    private static final long serialVersionUID = 8118415883697921429L;

    private String groupName;

    private Long pipelineId;

    private List<WidgetPackageVO> taskWidgets;

    private List<WidgetPackageVO> othersWidgets;


    public PipelineWidgetVO(String groupName){
        this.setGroupName(groupName);
    }


}

package org.zjvis.datascience.common.graph.filter;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

/**
 * @description FilterResult
 * @date 2021-12-29
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FilterResult {
    private Boolean operation;
    private List<Long> filterPath;
    private Set<String> filterV;
    private Set<String> filterE;

    public FilterResult(List<Long> filterPath, Set<String> filterV, Set<String> filterE) {
        this.operation = false;
        this.filterPath = filterPath;
        this.filterV = filterV;
        this.filterE = filterE;
    }

    public FilterResult(List<Long> filterPath) {
        this.operation = true;
        this.filterPath = filterPath;
        this.filterV = null;
        this.filterE = null;
    }

    public void and(FilterResult filterResult) {
        this.filterV.retainAll(filterResult.getFilterV());
        this.filterE.retainAll(filterResult.getFilterE());
    }

    public void or(FilterResult filterResult) {
        this.filterV.addAll(filterResult.getFilterV());
        this.filterE.addAll(filterResult.getFilterE());
    }
}

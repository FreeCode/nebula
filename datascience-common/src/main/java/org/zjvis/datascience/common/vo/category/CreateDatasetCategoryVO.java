package org.zjvis.datascience.common.vo.category;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @description 数据管理相关VO
 * @date 2020-07-31
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateDatasetCategoryVO {
    private static final long serialVersionUID = 1L;

    @NotBlank(message = "分类目录名不能为空")
    @Length(max = 100, message = "分类目录名长度不能超过100")
    @ApiModelProperty(value = "分类目录名", required = true)
    private String name;

}

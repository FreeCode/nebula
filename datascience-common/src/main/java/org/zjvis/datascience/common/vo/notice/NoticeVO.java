package org.zjvis.datascience.common.vo.notice;

import cn.weiguangfu.swagger2.plus.annotation.ApiRequestInclude;
import lombok.Data;
import org.zjvis.datascience.common.dto.notice.NoticeDTO;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * @description 用户反馈表Notice
 * @date 2021-09-02
 */
@Data
public class NoticeVO {

    private Long id;

    @ApiRequestInclude(groups = {Query.class})
    @NotNull(message = "pageNum不能为空", groups = {Query.class})
    @Min(value = 1, message = "请输入有效的pageNum", groups = {Query.class})
    private Integer pageNum;

    @ApiRequestInclude(groups = {Query.class})
    @NotNull(message = "pageSize不能为空", groups = {Query.class})
    @Min(value = 1, message = "请输入有效的pageSize", groups = {Query.class})
    private Integer pageSize;

    @ApiRequestInclude(groups = {Query.class, DeleteByStatus.class})
    @Min(value = 0, message = "请输入有效的status", groups = {Query.class, DeleteByStatus.class})
    @Max(value = 255, message = "请输入有效的status", groups = {Query.class, DeleteByStatus.class})
    private Integer status;

    @ApiRequestInclude(groups = {Query.class})
    private Integer filterType;

    @ApiRequestInclude(groups = {Query.class})
    private Long projectId;

    @ApiRequestInclude(groups = {Ids.class})
    @NotNull(message = "id列表不能为空", groups = {Ids.class})
    private Set<Long> ids;

    private Long userId;

    public interface Query {
    }

    public interface Ids {
    }

    public interface DeleteByStatus {
    }

    public NoticeDTO toNoticeDTO(Long creator) {
        return NoticeDTO.builder()
                .userId(this.userId)
                .status(this.status)
                .filterType(this.filterType)
                .projectId(this.projectId)
                .gmtCreator(creator)
                .gmtModifier(creator)
                .build();
    }
}

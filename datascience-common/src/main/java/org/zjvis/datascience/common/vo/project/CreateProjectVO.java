package org.zjvis.datascience.common.vo.project;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @description 创建项目相关VO
 * @date 2020-07-23
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateProjectVO {

    public static final String NAME_NOT_BLANK = "项目名不能为空";
    public static final String NAME_SIZE = "项目名长度不能超过50";
    public static final String DESC_SIZE = "项目描述长度不能超过100";

    @NotBlank(message = NAME_NOT_BLANK)
    @Length(max = 50, message = NAME_SIZE)
    @ApiModelProperty(value = "项目名", required = true)
    private String name;

    @Length(max = 100, message = DESC_SIZE)
    @ApiModelProperty(value = "项目描述")
    private String description;

    private static final long serialVersionUID = 1L;

}

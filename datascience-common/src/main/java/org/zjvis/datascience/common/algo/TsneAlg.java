package org.zjvis.datascience.common.algo;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zjvis.datascience.common.constant.SqlTemplate;
import org.zjvis.datascience.common.enums.AlgEnum;
import org.zjvis.datascience.common.enums.SubTypeEnum;
import org.zjvis.datascience.common.sql.SqlHelper;
import org.zjvis.datascience.common.util.ToolUtil;
import org.zjvis.datascience.common.vo.TaskVO;

import java.util.List;

/**
 * @description TSNE 降维算子模板类
 * @date 2021-12-24
 */
public class TsneAlg extends BaseAlg {

    private final static Logger logger = LoggerFactory.getLogger("TsneAlg");

    private static String TPL_FILENAME = "template/algo/tsne.json";

    private static String SQL_TPL_MADLIB = "SELECT * FROM \"%s\".\"tsne\"('%s', '%s', '%s', '%s', %s)";
    private static String SQL_TPL_MADLIB_SAMPLE = "SELECT * FROM \"%s\".\"tsne\"('CREATE VIEW %s AS SELECT * from %s where \"%s\" <= %s', '%s', '%s', '%s', %s)";
    private static String SQL_TPL_SPARK = "tsne -s %s -f %s -t %s -k %d -m %d -uk %s -idcol %s";

    public void initTemplate(JSONObject data) {
        JSONArray jsonArray = getTemplateParamList(TPL_FILENAME);
        data.put("setParams", jsonArray);
        baseInitTemplate(data);
        JSONArray validate = new JSONArray();
        validate.add("feature_cols,number");
        data.put("validate", validate);
    }

    public TsneAlg() {
        super(AlgEnum.TSNE.name(), SubTypeEnum.DIMENSION_REDUCTION.getVal(), SubTypeEnum.DIMENSION_REDUCTION.getDesc());
        this.maxParentNumber = 1;
    }

    private String getTsneSql(String sourceTable, String outTable, String featureCols, int k,
                              long timeStamp, int maxIter, String sampleTable) {
        if (StringUtils.isNotEmpty(sampleTable)) {
            // 采样
            return String.format(SQL_TPL_MADLIB_SAMPLE, SqlTemplate.SCHEMA, sampleTable, sourceTable, ID_COL,
                    SAMPLE_NUMBER, outTable, ID_COL, featureCols, k);
        } else {
            // 全量, 根据配置
            if (getEngine().isMadlib()) {
                return String.format(SQL_TPL_MADLIB, SqlTemplate.SCHEMA, sourceTable, outTable, ID_COL, featureCols, k);
            } else if (getEngine().isSpark()) {
                return String.format(SQL_TPL_SPARK, sourceTable, featureCols, outTable, k, maxIter, timeStamp, ID_COL);
            }
        }
        return StringUtils.EMPTY;
    }

    public String initSql(JSONObject json, List<SqlHelper> sqlHelpers, long timeStamp,
                          String engineName) {
        this.engineName = engineName;
        String sourceTable = json.getString("source_table");
        sourceTable = ToolUtil.alignTableName(sourceTable, timeStamp);
        String outTable = json.getString("out_table_rename");
        outTable = ToolUtil.alignTableName(outTable, timeStamp);
        // String idCol = json.getString("id_col");
        int k = json.getInteger("dimension_num");
        JSONArray features = json.getJSONArray("feature_cols");
        String featureCols = this.getFeatureColsStr(features);
        int maxIter = json.getInteger("max_iter");
        String sampleTable = "";
        if (!json.containsKey("isSample") ||
                json.getString("isSample").equals("SUCCESS") ||
                json.getString("isSample").equals("FAIL")) {
            sampleTable = outTable.replace("solid_", "view_");
            json.put("isSample", "CREATE");
        }
        String sql = this.getTsneSql(sourceTable, outTable, featureCols, k, timeStamp, maxIter, sampleTable);
        logger.debug("sql={}", sql);
        return sql;
    }

    public void defineOutput(TaskVO vo) {
        JSONObject jsonObject = vo.getData();
        String outTablePrefix = jsonObject.getString("out_table");
        String tableName = String.format(SqlTemplate.OUT_TABLE_NAME, outTablePrefix, vo.getPipelineId(), vo.getId());
        jsonObject.put("out_table_rename", tableName);
        JSONArray input = jsonObject.getJSONArray("input");
        if (input == null || input.size() == 0) {
            logger.warn("input is empty");
            return;
        }
        this.checkBoxSelectFilter(jsonObject, "number", FEATURE_COLS);
        this.supplementForCheckbox(jsonObject, TPL_FILENAME, 1, vo);
        jsonObject.put("source_table", input.getJSONObject(0).getString("tableName"));

        JSONArray outputColTypes = new JSONArray();
        JSONArray outputCols = new JSONArray();
        outputCols.add(ID_COL);
        outputColTypes.add(ID_TYPE);
        if (!jsonObject.containsKey("dimension_num")) {
            vo.setData(jsonObject);
            return;
        }
        Integer k = jsonObject.getInteger("dimension_num");
        for (int i = 0; i < k; ++i) {
            outputCols.add(String.format("f%s", i + 1));
            outputColTypes.add("numeric");
        }
        JSONArray output = new JSONArray();
        JSONObject outItem = new JSONObject();
        outItem.put("tableName", tableName);
        outItem.put("tableCols", outputCols);
        outItem.put("nodeName", vo.getName() == null ? AlgEnum.TSNE.toString() : vo.getName());
        outItem.put("columnTypes", outputColTypes);
        this.setSubTypeForOutput(outItem);
        output.add(outItem);
        jsonObject.put("output", output);
        vo.setData(jsonObject);
    }
}

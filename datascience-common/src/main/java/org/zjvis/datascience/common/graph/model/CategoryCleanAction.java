package org.zjvis.datascience.common.graph.model;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.zjvis.datascience.common.vo.TaskInstanceVO;
import org.zjvis.datascience.common.vo.graph.CategoryVO;

/**
 * @description CategoryCleanAction
 * @date 2021-12-29
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CategoryCleanAction {
    private Long graphId;
    private String categoryId;
    private CategoryVO category;
    private String taskName;
    private JSONObject data;

    public CategoryCleanAction(TaskInstanceVO vo, CategoryVO category) {
        this.graphId = Long.parseLong(vo.getParentId().split("_")[1]);
        this.categoryId = vo.getParentId();
        this.category = category;
        this.taskName = vo.getTaskName();
        this.data = vo.getData();
    }

    public CategoryCleanAction(TaskInstanceVO vo) {
        this.graphId = Long.parseLong(vo.getParentId().split("_")[1]);
        this.categoryId = vo.getParentId();
        this.taskName = vo.getTaskName();
        this.data = vo.getData();
    }
}

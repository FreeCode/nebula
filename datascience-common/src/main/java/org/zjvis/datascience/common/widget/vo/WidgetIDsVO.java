package org.zjvis.datascience.common.widget.vo;

import lombok.Data;
import org.zjvis.datascience.common.validator.NotNullNumber;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @description 用于图表控件渲染数据的请求POJO  （多IDs）
 * @date 2021-12-01
 */
@Data
public class WidgetIDsVO {

    @NotNull(message = "the size of widget id list cannot less than zero")
    private List<Long> ids;
}

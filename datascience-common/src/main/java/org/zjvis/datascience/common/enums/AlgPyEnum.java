package org.zjvis.datascience.common.enums;

import org.zjvis.datascience.common.exception.DataScienceException;

/**
 * @description Python算子节点类型 枚举类
 * @date 2021-12-16
 */
public enum AlgPyEnum {
    ANOMALY_STAT("异常值检测-统计", 1, "anomaly_stat"),
    ANOMALY_KNN("异常值检测-近邻", 2, "anomaly_knn"),
    IMPUTATION_STAT("缺失值插补-通用", 3, "imputation_stat"),
    IMPUTATION_MULTI("缺失值插补-多重", 4, "imputation_multi"),
    FEATURE_SCALING("标准化", 5, "standardization"),
    TIMESERIES_DECOMPOSE("时间序列分解", 6, "timeseries_decompose"),
    FEATURE_SMOOTHING("数据平滑", 7, "smoothing"),
    SIMULATE("模拟推演", 8, "simulate"),
    SIMULATE_NEW("模拟推演-多参数", 9, "simulate_new"),
    TIMESERIES_SHIFT("时间序列移位", 10, "timeseries_shift"),
    SIMULATE_INVERSE("反向模拟推演", 11, "simulate_inverse"),
    ;

    private String desc;

    private int val;

    private String name;

    AlgPyEnum(String desc, int val, String name) {
        this.desc = desc;
        this.val = val;
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public int getVal() {
        return val;
    }

    public String getName() {
        return name;
    }

    public static AlgPyEnum getEnumByDesc(String desc) {
        for (AlgPyEnum alg : AlgPyEnum.values()) {
            if (alg.desc.equals(desc)) {
                return alg;
            }
        }
        throw new DataScienceException("AlgPyEnum does not exist!");
    }

    public static AlgPyEnum getEnumByVal(int val) {
        for (AlgPyEnum alg : AlgPyEnum.values()) {
            if (alg.val == val) {
                return alg;
            }
        }
        throw new DataScienceException("AlgPyEnum does not exist!");
    }

    public static AlgPyEnum getEnumByName(String name) {
        for (AlgPyEnum alg : AlgPyEnum.values()) {
            if (alg.name.equals(name)) {
                return alg;
            }
        }
        throw new DataScienceException("AlgPyEnum does not exist!");
    }
}

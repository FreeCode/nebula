package org.zjvis.datascience.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * @description 数据集导入结果DTO
 * @date 2021-08-23
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class DatasetImportResDTO {
    private Long datasetId;

    private Boolean successed;

    private Long inserted;

    private Long formatErrors;
}

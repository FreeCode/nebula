package org.zjvis.datascience.common.constant;

/**
 * @description 文件上传设置常量
 * @date 2021-12-24
 */
public class FileConstant {

    public static final String FILE_UPLOAD_COMPLETE = "FILE_UPLOAD_COMPLETE"; // 自定义设置不看

    public static final String FILE_UPLOAD_PREVIEW_DATA = "FILE_UPLOAD_PREVIEW_DATA"; // 不带块的自定义设置没有该信息

    public static final String FILE_UPLOAD_FILENAME = "FILE_UPLOAD_FILENAME"; // 所有统一

    public static final String FILE_HEADER = "FILE_HEADER"; // 自定义设置的最后删除

    public static final String FILE_ORIGIN = "FILE_ORIGIN"; //

    public static final String FILE_CHECK_PROGRESS = "FILE_CHECK_PROGRESS"; // 自定义的没用到

    public static final String FILE_CHUNK_SEG_INFO = "FILE_CHUNK_SEG_INFO"; // 自定义设置的直接覆盖set不需要删除

    public static final String FILE_IS_ING = "FILE_IS_ING"; // 自定义的没用到
}
package org.zjvis.datascience.common.formula.dto;

import lombok.*;
import org.zjvis.datascience.common.formula.dto.FormulaDTO;
import org.zjvis.datascience.common.util.DozerUtil;

/**
 * @description 公式执行历史 Formula_history表 FormulaHistoryDTO
 * @date 2021-12-08
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class FormulaHistoryDTO {
    private long id;
    private long taskId;
    private String tableName;
    private String name;
    private String formula;
    private String result;
    private String type;
    private long projectId;
    private Integer precisionNum;

    public FormulaDTO toFormulaDTO() {
        return DozerUtil.mapper(this, FormulaDTO.class);
    }
}

package org.zjvis.datascience.common.enums;

import cn.hutool.core.date.DatePattern;
import lombok.Getter;
import org.zjvis.datascience.common.constant.DateTimeFormatConstant;

import java.time.format.DateTimeFormatter;

/**
 * @description 日期时间格式枚举
 * @date 2021-07-29
 */
@Getter
public enum DateTimeFormatEnum {

    /**
     * dateTime
     */
    NORM_DATETIME_MS_PATTERN(DatePattern.NORM_DATETIME_MS_PATTERN, 1, DateTimeFormatTypeEnum.DATE_TIME),
    NORM_DATETIME_PATTERN(DatePattern.NORM_DATETIME_PATTERN, 1, DateTimeFormatTypeEnum.DATE_TIME),
    NORM_DATETIME_MINUTE_PATTERN(DatePattern.NORM_DATETIME_MINUTE_PATTERN, 1, DateTimeFormatTypeEnum.DATE_TIME),

    WEST_DATETIME_MS_PATTERN("MM/dd/yyyy HH:mm:ss.SSS", 1, DateTimeFormatTypeEnum.DATE_TIME),
    WEST_DATETIME_MS_PATTERN_DAY_HEAD("dd/MM/yyyy HH:mm:ss.SSS", 0, DateTimeFormatTypeEnum.DATE_TIME),
    WEST_DATETIME_PATTERN("MM/dd/yyyy HH:mm:ss", 1, DateTimeFormatTypeEnum.DATE_TIME),
    WEST_DATETIME_PATTERN_DAY_HEAD("dd/MM/yyyy HH:mm:ss", 0, DateTimeFormatTypeEnum.DATE_TIME),
    WEST_DATETIME_MINUTE_PATTERN("MM/dd/yyyy HH:mm", 1, DateTimeFormatTypeEnum.DATE_TIME),
    WEST_DATETIME_MINUTE_PATTERN_DAY_HEAD("dd/MM/yyyy HH:mm", 0, DateTimeFormatTypeEnum.DATE_TIME),

    ISO8601_PATTERN(DatePattern.ISO8601_PATTERN, 1, DateTimeFormatTypeEnum.DATE_TIME),

    PURE_DATETIME_MS_PATTERN(DatePattern.PURE_DATETIME_MS_PATTERN, 1, DateTimeFormatTypeEnum.DATE_TIME),
    PURE_DATETIME_PATTERN(DatePattern.PURE_DATETIME_PATTERN, 1, DateTimeFormatTypeEnum.DATE_TIME),

    UTC_MS_PATTERN(DatePattern.UTC_MS_PATTERN, 1, DateTimeFormatTypeEnum.DATE_TIME, "yyyy-MM-dd\"T\"HH:mm:ss.SSS\"Z\""),
    UTC_PATTERN(DatePattern.UTC_PATTERN, 1, DateTimeFormatTypeEnum.DATE_TIME, "yyyy-MM-dd\"T\"HH:mm:ss\"Z\""),

    CHINESE_DATE_TIME_PATTERN(DatePattern.CHINESE_DATE_TIME_PATTERN, 1, DateTimeFormatTypeEnum.DATE_TIME),
    /**
     * Date
     */
    NORM_DATE_PATTERN(DatePattern.NORM_DATE_PATTERN, 1, DateTimeFormatTypeEnum.DATE),

    WEST_DATE_PATTERN("MM/dd/yyyy", 1, DateTimeFormatTypeEnum.DATE),
    WEST_DATE_PATTERN_DAY_HEAD("dd/MM/yyyy", 0, DateTimeFormatTypeEnum.DATE),

    PURE_DATE_PATTERN(DatePattern.PURE_DATE_PATTERN, 1, DateTimeFormatTypeEnum.DATE),
    CHINESE_DATE_PATTERN(DatePattern.CHINESE_DATE_PATTERN, 1, DateTimeFormatTypeEnum.DATE),
    WEST_DATE_SINGLE_M_PATTERN("M/dd/yyyy", 1, DateTimeFormatTypeEnum.DATE, "MM/dd/yyyy"),
    WEST_DATE_SINGLE_M_PATTERN_DAY_HEAD("dd/M/yyyy", 0, DateTimeFormatTypeEnum.DATE, "dd/MM/yyyy"),
    WEST_DATE_SINGLE_D_PATTERN("MM/d/yyyy", 1, DateTimeFormatTypeEnum.DATE, "MM/dd/yyyy"),
    WEST_DATE_SINGLE_D_PATTERN_DAY_HEAD("d/MM/yyyy", 0, DateTimeFormatTypeEnum.DATE, "dd/MM/yyyy"),
    WEST_DATE_SINGLE_D_M_PATTERN("M/d/yyyy", 1, DateTimeFormatTypeEnum.DATE, "MM/dd/yyyy"),
    WEST_DATE_SINGLE_D_M_PATTERN_DAY_HEAD("d/M/yyyy", 0, DateTimeFormatTypeEnum.DATE, "dd/MM/yyyy"),
    YMD_SLASH("yyyy/MM/dd", 0, DateTimeFormatTypeEnum.DATE),
    YMD_SLASH_SINGLE_D("yyyy/MM/d", 0, DateTimeFormatTypeEnum.DATE, "yyyy/MM/dd"),
    YMD_SLASH_SINGLE_M("yyyy/M/dd", 0, DateTimeFormatTypeEnum.DATE, "yyyy/MM/dd"),
    YMD_SLASH_SINGLE_M_D("yyyy/M/d", 0, DateTimeFormatTypeEnum.DATE, "yyyy/MM/dd"),
    /**
     * time
     */
    NORM_TIME_PATTERN("HH:mm:ss.SSS", 1, DateTimeFormatTypeEnum.TIME),

    NORM_TIME_PATTERN_WITHOUT_PRE_ZERO("H:mm:ss", 1, DateTimeFormatTypeEnum.TIME, "HH:mm:ss"),
    NORM_TIME_PATTERN_SINGLE_H_M_WITHOUT_PRE_ZERO("H:m:ss", 1, DateTimeFormatTypeEnum.TIME, "HH:mm:ss"),
    NORM_TIME_PATTERN_SINGLE_H_M_S_WITHOUT_PRE_ZERO("H:m:s", 1, DateTimeFormatTypeEnum.TIME, "HH:mm:ss"),

    PURE_TIME_PATTERN(DatePattern.PURE_TIME_PATTERN, 1, DateTimeFormatTypeEnum.TIME),
    ;

    /**
     * 中文名
     */
    private String pattern;
    private DateTimeFormatter formatter;
    private int priority;
    private DateTimeFormatTypeEnum type;
    private String gpPattern;

    DateTimeFormatEnum(String pattern, int priority, DateTimeFormatTypeEnum type, String jdkNormalPattern) {
        this.pattern = pattern;
        this.formatter = DateTimeFormatter.ofPattern(pattern);
        this.priority = priority;
        this.type = type;
        if (jdkNormalPattern == null) {
            this.gpPattern = toGpPattern(pattern);
        } else {
            this.gpPattern = toGpPattern(jdkNormalPattern);
        }
    }

    DateTimeFormatEnum(String pattern, int priority, DateTimeFormatTypeEnum type) {
        this(pattern, priority, type, null);
    }

    private static String toGpPattern(String p) {
        return p.replaceAll(DateTimeFormatConstant.JDK_MINUTE_PATTERN, DateTimeFormatConstant.GP_MINUTE_PATTERN)
                .replaceAll(DateTimeFormatConstant.JDK_HOUR_PATTERN, DateTimeFormatConstant.GP_HOUR_PATTERN)
                .replaceAll(DateTimeFormatConstant.JDK_MILLISECONDS_PATTERN, DateTimeFormatConstant.GP_MILLISECONDS_PATTERN)
                .toUpperCase();
    }

}

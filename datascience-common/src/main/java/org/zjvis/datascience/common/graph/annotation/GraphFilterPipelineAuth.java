package org.zjvis.datascience.common.graph.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @description GraphFilterPipelineAuth
 * @date 2021-12-29
 */
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface GraphFilterPipelineAuth {
    String field() default "id";
}

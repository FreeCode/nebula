package org.zjvis.datascience.common.util.db;

import org.apache.commons.lang3.StringUtils;
import org.apache.coyote.http11.filters.IdentityOutputFilter;
import org.zjvis.datascience.common.enums.DataTypeEnum;
import org.zjvis.datascience.common.enums.DateTimeFormatEnum;
import org.zjvis.datascience.common.exception.BaseErrorCode;
import org.zjvis.datascience.common.exception.DataScienceException;

import java.math.BigDecimal;
import org.zjvis.datascience.common.util.DatasetUtil;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;

/**
 * @description 数据类型转换校验
 * @date 2020-09-04
 */
public class DataTypeValidator {

    /**
     * @param data
     * @param dataType
     * @return 校验正确则返回null，出错则抛异常。date的数据类型返回匹配的格式
     */
    public static DateTimeFormatEnum validateAndMatch(String data, String dataType) {
        if (StringUtils.isBlank(data)) {
            return null;
        }
        data = data.trim();
        DataTypeEnum dt = DataTypeEnum.get(dataType);
        switch (dt) {
            case INT:
                try {
                    Long.valueOf(data);
                } catch (Exception e) {
                    throw new DataScienceException(BaseErrorCode.DATASET_IMPORT_DATA_TRANSFORM_FAIL, e);
                }
            case JSON:
            case ARRAY:
            case VARCHAR:
            case DATE:
                return null;
//            case DATE:
//                if (data.matches(DatasetUtil.NORM_DATETIME_REGEX)) {
//                    return DateTimeFormatEnum.NORM_DATETIME_MS_PATTERN;F
//                }
//                if (data.matches(DatasetUtil.WEST_DATETIME_REGEX)) {
//                    return DateTimeFormatEnum.WEST_DATETIME_MS_PATTERN;
//                }
//                if (data.matches(DatasetUtil.TIME_REGEX)) {
//                    return DateTimeFormatEnum.NORM_TIME_PATTERN;
//                }
//                String lower = data.toLowerCase();
//                if (lower.contains("am") || lower.contains("pm") || lower.contains("a.m.") || lower.contains("p.m.")) {
//                    throw new DataScienceException(BaseErrorCode.DATASET_IMPORT_DATA_TRANSFORM_FAIL);
//                }
//                for (DateTimeFormatEnum formatEnum : DateTimeFormatEnum.values()) {
//                    try {
//                        switch (formatEnum.getType()) {
//                            case DATE_TIME:
//                                LocalDateTime.parse(data, formatEnum.getFormatter());
//                                break;
//                            case DATE:
//                                LocalDate.parse(data, formatEnum.getFormatter());
//                                break;
//                            case TIME:
//                                LocalTime.parse(data, formatEnum.getFormatter());
//                                break;
//                            default:
//                        }
//                        return formatEnum;
//                    } catch (Exception ignore) {
//                    }
//                }
//                throw new DataScienceException(BaseErrorCode.DATASET_IMPORT_DATA_TRANSFORM_FAIL);
            case DECIMAL:
                try {
                    new BigDecimal(data);
                } catch (Exception e) {
                    throw new DataScienceException(BaseErrorCode.DATASET_IMPORT_DATA_TRANSFORM_FAIL, e);
                }
                return null;
            default:
                throw new DataScienceException(BaseErrorCode.DATASET_IMPORT_DATA_TYPE_ERROR);
        }
    }

}

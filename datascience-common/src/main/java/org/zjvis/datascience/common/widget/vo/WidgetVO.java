package org.zjvis.datascience.common.widget.vo;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.zjvis.datascience.common.widget.dto.WidgetDTO;
import org.zjvis.datascience.common.util.DozerUtil;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * @description 用于可视化系统构建 可用图表控件的结果类VO
 * @date 2021-12-01
 */
@Data
public class WidgetVO {

    private Long id;

    private Long tid;

    private String type;

    private String name;

    @NotNull(message = "data cannot be null")
    @NotEmpty(message = "data cannot be empty")
    private JSONObject data;

    private String publishName;

    private JSONObject publishData;

    private LocalDateTime publishTime;
    //用于权限校验,前端传入
    private Long projectId;

    private String status;

    private Long dashboardId;

    public WidgetDTO toWidget() {
        WidgetDTO widget = DozerUtil.mapper(this, WidgetDTO.class);
        if (data != null) {
            widget.setDataJson(data.toJSONString());
        }
        if (publishData != null) {
            widget.setPublishDataJson(publishData.toJSONString());
        }
        return widget;
    }

}

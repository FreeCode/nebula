package org.zjvis.datascience.common.model.stat;

import java.io.Serializable;

/**
 * @description 字段范围统计计数器，用于数据结果渲染
 * @date 2021-09-26
 */
public class Range<C extends Comparable> implements Serializable {
    private final C lower;
    private final C upper;

    public Range(C lowerBound, C upperBound) {
        this.lower = lowerBound;
        this.upper = upperBound;
    }

    public static <C extends Comparable<?>> Range<C> closedOpen(C lower, C upper) {
        return new Range<>(lower, upper);
    }

    public C lowerEndpoint() {
        return lower;
    }

    public C upperEndpoint() {
        return upper;
    }

    @Override
    public String toString() {
        return "{" + lower +
                ", " + upper + '}';
    }
}

package org.zjvis.datascience.common.vo;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.zjvis.datascience.common.dto.RecommendDTO;
import org.zjvis.datascience.common.dto.TaskDTO;
import org.zjvis.datascience.common.util.DozerUtil;

/**
 * @description 结果推荐数据表 Recommend 相关VO
 * @date 2020-08-31
 */
@Data
public class RecommendVO extends BaseVO {

    private Long id;

    private String name;

    private Long userId;

    private Long categoryId;

    private String tableNames;

    private JSONObject data;

    public RecommendDTO toTask() {
        RecommendDTO task = DozerUtil.mapper(this, RecommendDTO.class);
        task.setDataJson(data.toJSONString());
        return task;
    }
}

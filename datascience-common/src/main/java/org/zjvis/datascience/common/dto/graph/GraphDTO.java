package org.zjvis.datascience.common.dto.graph;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.zjvis.datascience.common.dto.BaseDTO;
import org.zjvis.datascience.common.util.DozerUtil;
import org.zjvis.datascience.common.vo.graph.*;

import java.util.ArrayList;

/**
 * @description GraphDTO
 * @date 2021-12-24
 */
@Data
public class GraphDTO extends BaseDTO {
    private Long id;

    private String name;

    private Long projectId;

    private Long pipelineId;

    private Long userId;

    private Long taskId;

    private String dataJson;

    public GraphVO view() {
        GraphVO vo = DozerUtil.mapper(this, GraphVO.class);
        JSONObject jsonObject = JSONObject.parseObject(dataJson);
        vo.setCategories(jsonObject.getJSONArray("categories").toJavaList(CategoryVO.class));
        vo.setEdges(jsonObject.getJSONArray("edges").toJavaList(EdgeVO.class));
        vo.setNodes(jsonObject.getJSONArray("nodes").toJavaList(NodeVO.class));
        vo.setLinks(jsonObject.getJSONArray("links").toJavaList(LinkVO.class));
        vo.setGraphInfo(jsonObject.getJSONObject("graphInfo"));
        vo.setActionContext(jsonObject.getJSONObject("actionContext"));
        return vo;
    }

    public GraphVO initActionView() {
        GraphVO vo = DozerUtil.mapper(this, GraphVO.class);
        vo.setCategories(new ArrayList<>());
        vo.setEdges(new ArrayList<>());
        return vo;
    }

    public void initActionView(GraphVO vo) {
        vo.setId(this.getId());
        vo.setName(this.getName());
        vo.setProjectId(this.getProjectId());
        vo.setPipelineId(this.getPipelineId());
        vo.setTaskId(this.getTaskId());
        vo.setUserId(this.getUserId());
        vo.setCategories(new ArrayList<>());
        vo.setEdges(new ArrayList<>());
        vo.setNodes(new ArrayList<>());
        vo.setLinks(new ArrayList<>());
    }

    //仅返回左边元素
    public GraphVO leftView() {
        GraphVO vo = DozerUtil.mapper(this, GraphVO.class);
        JSONObject jsonObject = JSONObject.parseObject(dataJson);
        vo.setCategories(jsonObject.getJSONArray("categories").toJavaList(CategoryVO.class));
        vo.setEdges(jsonObject.getJSONArray("edges").toJavaList(EdgeVO.class));
        return vo;
    }

    public GraphVO tab() {
        GraphVO tab = new GraphVO();
        tab.setId(id);
        tab.setName(name);
        return tab;
    }

}

package org.zjvis.datascience.common.dto.dataset;

import lombok.Data;

import java.util.Date;

/**
 * @description HTTP数据上传定时配置DTO
 * @date 2021-12-24
 */
@Data
public class DatasetScheduleDTO {

    private String scheduleType;

    private Date beginDate;

    private Integer hourOfDay;

    private Integer minuteOfHour;

    private String daysOfWeek;

    private Integer intervalOfHour;

    private Boolean needSchedule;

    private String incrementColumn;

    public DatasetScheduleDTO(String scheduleType, Date beginDate, Integer hourOfDay, Integer minuteOfHour, String daysOfWeek,
                              Integer intervalOfHour, Boolean needSchedule, String incrementColumn) {
        this.scheduleType = scheduleType;
        this.beginDate = beginDate;
        this.hourOfDay = hourOfDay;
        this.minuteOfHour = minuteOfHour;
        this.daysOfWeek = daysOfWeek;
        this.intervalOfHour = intervalOfHour;
        this.needSchedule = needSchedule;
        this.incrementColumn = incrementColumn;
    }
}

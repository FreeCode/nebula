package org.zjvis.datascience.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

/**
 * @description 数据集操作信息表，数据集操作DTO
 * @date 2021-04-20
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class DatasetActionDTO extends BaseDTO implements Serializable {

    public Long id;

    public String actionType;

    private Long datasetId;

    private Long rowAffect;

    private Long rowFinal;

    private String context;

    private Long userId;

    private String status;

}

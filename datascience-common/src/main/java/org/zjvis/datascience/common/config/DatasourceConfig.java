package org.zjvis.datascience.common.config;

import org.zjvis.datascience.common.util.ConstantUtil;
import lombok.Data;

/**
 * @description 数据库驱动设置类
 * @date 2021-12-24
 */
@Data
public class DatasourceConfig {

    public enum Driver {
        Greenplum("com.pivotal.jdbc.GreenplumDriver"),
        Postgres("org.postgresql.Driver"),
        Tidb("com.mysql.jdbc.Driver"),
        Mysql("com.mysql.jdbc.Driver"),
        Hive("org.apache.hive.jdbc.HiveDriver"),
        Sqlserver("com.microsoft.sqlserver.jdbc.SQLServerDriver"),
        Oracle("oracle.jdbc.driver.OracleDriver"),
        Hana("com.sap.db.jdbc.Driver"),

        Druid("org.apache.calcite.avatica.remote.Driver"),
        ClickHouse("ru.yandex.clickhouse.ClickHouseDriver"),
        ElasticSearch("com.facebook.presto.jdbc.PrestoDriver");

        String driver;

        public String getDriver() {
            return driver;
        }

        Driver(String driver) {
            this.driver = driver;
        }

        public static String getDriver(int type) {
            if (type == ConstantUtil.DATASOURCE_TYPE_GREENPLUM) {
                return Driver.Greenplum.driver;
            } else if (type == ConstantUtil.DATASOURCE_TYPE_TIDB) {
                return Driver.Hive.driver;
            } else if (type == ConstantUtil.DATASOURCE_TYPE_MYSQL) {
                return Driver.Mysql.driver;
            } else if (type == ConstantUtil.DATASOURCE_TYPE_HIVE) {
                return Driver.Hive.driver;
            } else if (type == ConstantUtil.DATASOURCE_TYPE_SQLSERVER) {
                return Driver.Sqlserver.driver;
            } else if (type == ConstantUtil.DATASOURCE_TYPE_ORACLE) {
                return Driver.Oracle.driver;
            } else if (type == ConstantUtil.DATASOURCE_TYPE_HANA) {
                return Driver.Hana.driver;
            } else if (type == ConstantUtil.DATASOURCE_TYPE_DRUID) {
                return Driver.Druid.driver;
            } else if (type == ConstantUtil.DATASOURCE_TYPE_CLICKHOUSE) {
                return Driver.ClickHouse.driver;
            } else if (type == ConstantUtil.DATASOURCE_TYPE_ELASTICSEARCH) {
                return Driver.ElasticSearch.driver;
            } else if (type == ConstantUtil.DATASOURCE_TYPE_POSTGRES) {
                return Driver.Postgres.driver;
            }
            return null;
        }

    }

}

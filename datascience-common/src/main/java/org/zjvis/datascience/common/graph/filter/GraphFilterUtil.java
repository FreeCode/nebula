package org.zjvis.datascience.common.graph.filter;

import com.alibaba.fastjson.JSONArray;
import org.zjvis.datascience.common.graph.enums.GraphFilterSubTypeEnum;
import org.zjvis.datascience.common.util.ToolUtil;

/**
 * @description GraphFilterUtil
 * @date 2021-12-29
 */
public class GraphFilterUtil {
    public static JSONArray getTemplateParams(String fileName) {
        if (fileName == null) {
            return null;
        }
        String json = new ToolUtil().readJsonFile(fileName);
        if (json.length() == 0) {
            return null;
        }
        JSONArray jsonArray = JSONArray.parseArray(json);
        return jsonArray;
    }

    public static GraphFilterSubTypeEnum getGraphFilterSubTypeEnum(int subType) {
        if (subType == GraphFilterSubTypeEnum.NODE_CATEGORY.getVal()) {
            return GraphFilterSubTypeEnum.NODE_CATEGORY;
        } else if (subType == GraphFilterSubTypeEnum.LINK_CATEGORY.getVal()) {
            return GraphFilterSubTypeEnum.LINK_CATEGORY;
        } else if (subType == GraphFilterSubTypeEnum.LINK_WEIGHT_RANGE.getVal()) {
            return GraphFilterSubTypeEnum.LINK_WEIGHT_RANGE;
        } else if (subType == GraphFilterSubTypeEnum.LINK_ONLY_DIRECTED.getVal()) {
            return GraphFilterSubTypeEnum.LINK_ONLY_DIRECTED;
        } else if (subType == GraphFilterSubTypeEnum.LINK_ONLY_UNDIRECTED.getVal()) {
            return GraphFilterSubTypeEnum.LINK_ONLY_UNDIRECTED;
        } else if (subType == GraphFilterSubTypeEnum.LINK_ONLY_DOUBLE_DIRECTED.getVal()) {
            return GraphFilterSubTypeEnum.LINK_ONLY_DOUBLE_DIRECTED;
        } else if (subType == GraphFilterSubTypeEnum.LINK_REMOVE_SELF_LOOP.getVal()) {
            return GraphFilterSubTypeEnum.LINK_REMOVE_SELF_LOOP;
        } else if (subType == GraphFilterSubTypeEnum.ATTR_EQUAL.getVal()) {
            return GraphFilterSubTypeEnum.ATTR_EQUAL;
        } else if (subType == GraphFilterSubTypeEnum.ATTR_RANGE.getVal()) {
            return GraphFilterSubTypeEnum.ATTR_RANGE;
        } else if (subType == GraphFilterSubTypeEnum.ATTR_NOT_NULL.getVal()) {
            return GraphFilterSubTypeEnum.ATTR_NOT_NULL;
        } else if (subType == GraphFilterSubTypeEnum.ATTR_CATEGORY.getVal()) {
            return GraphFilterSubTypeEnum.ATTR_CATEGORY;
        } else if (subType == GraphFilterSubTypeEnum.ATTR_CATEGORY_COUNT.getVal()) {
            return GraphFilterSubTypeEnum.ATTR_CATEGORY_COUNT;
        } else if (subType == GraphFilterSubTypeEnum.ATTR_INNER_LINK.getVal()) {
            return GraphFilterSubTypeEnum.ATTR_INNER_LINK;
        } else if (subType == GraphFilterSubTypeEnum.ATTR_OUTER_LINK.getVal()) {
            return GraphFilterSubTypeEnum.ATTR_OUTER_LINK;
        } else if (subType == GraphFilterSubTypeEnum.TOPOLOGY_K_CORE.getVal()) {
            return GraphFilterSubTypeEnum.TOPOLOGY_K_CORE;
        } else if (subType == GraphFilterSubTypeEnum.TOPOLOGY_SELF_LOOP.getVal()) {
            return GraphFilterSubTypeEnum.TOPOLOGY_SELF_LOOP;
        } else if (subType == GraphFilterSubTypeEnum.TOPOLOGY_DEGREE_RANGE.getVal()) {
            return GraphFilterSubTypeEnum.TOPOLOGY_DEGREE_RANGE;
        } else if (subType == GraphFilterSubTypeEnum.TOPOLOGY_IN_DEGREE_RANGE.getVal()) {
            return GraphFilterSubTypeEnum.TOPOLOGY_IN_DEGREE_RANGE;
        } else if (subType == GraphFilterSubTypeEnum.TOPOLOGY_OUT_DEGREE_RANGE.getVal()) {
            return GraphFilterSubTypeEnum.TOPOLOGY_OUT_DEGREE_RANGE;
        } else if (subType == GraphFilterSubTypeEnum.OPERATION_OR.getVal()) {
            return GraphFilterSubTypeEnum.OPERATION_OR;
        } else if (subType == GraphFilterSubTypeEnum.OPERATION_AND.getVal()) {
            return GraphFilterSubTypeEnum.OPERATION_AND;
        } else if (subType == GraphFilterSubTypeEnum.OPERATION_NOT.getVal()) {
            return GraphFilterSubTypeEnum.OPERATION_NOT;
        }
        else {
            return null;
        }
    }
}

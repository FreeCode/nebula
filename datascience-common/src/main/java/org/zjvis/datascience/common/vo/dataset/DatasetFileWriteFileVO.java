package org.zjvis.datascience.common.vo.dataset;

import lombok.Data;

import javax.validation.constraints.NotBlank;


/**
 * @description 数据上传相关VO
 * @date 2021-08-23
 */
@Data
public class DatasetFileWriteFileVO extends BaseTableConfigVO {

    @NotBlank(message = "上传文件名不能为空")
    private String filename;
//    @NotNull(message = "需要上传的数据字段不能为空")
//    @Size(min = 1, message = "需要上传的数据字段不能为空")
//    private List<@Valid DatasetColumnDTO> data;
    /**
     * 第一行是否作为字段名
     */
    private Boolean firstLineAsFields;

    //@NotBlank(message = "sheet名不能为空")
    private String sheetName;
    // 唯一标识
    private String identifier;
}

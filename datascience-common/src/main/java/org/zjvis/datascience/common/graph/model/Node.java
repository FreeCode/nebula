package org.zjvis.datascience.common.graph.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @description Node
 * @date 2021-12-29
 */
public class Node extends Element{
    private String categoryId;
    private List<Link> headOut;
    private List<Link> headIn;

    public Node(String id, String categoryId) {
        super(id);
        this.categoryId = categoryId;
        this.headOut = new ArrayList<>();
        this.headIn = new ArrayList<>();
    }

    public List<Link> getHeadOut() {
        return headOut;
    }

    public List<Link> getHeadIn() {
        return headIn;
    }

    public String getCategoryId() {
        return categoryId;
    }
}

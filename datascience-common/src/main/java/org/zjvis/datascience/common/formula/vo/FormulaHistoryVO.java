package org.zjvis.datascience.common.formula.vo;

import lombok.*;
import org.zjvis.datascience.common.formula.dto.FormulaDatasetDTO;

import java.util.List;

/**
 * @description 公式执行历史 Formula_history表 FormulaHistoryVO 渲染对象
 * @date 2021-12-01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class FormulaHistoryVO {
    private Long projectId;

    private Long pipelineId;

    private List<FormulaDatasetDTO> datasetList;

    public FormulaHistoryVO(Long projectId, List<FormulaDatasetDTO> datasetList) {
        this.projectId = projectId;
        this.datasetList = datasetList;
    }
}

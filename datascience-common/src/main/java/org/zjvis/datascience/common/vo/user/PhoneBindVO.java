package org.zjvis.datascience.common.vo.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @description 手机绑定相关VO
 * @date 2021-11-23
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class PhoneBindVO {
    @NotBlank(message = "手机号码不能为空")
    @Pattern(regexp = "^1[3|4|5|7|8|9][0-9]{9}$", message = "手机号码格式有误")
    @ApiModelProperty(value = "手机")
    private String phone;

    @ApiModelProperty(value = "图形验证码")
    private String captchaCode;
}

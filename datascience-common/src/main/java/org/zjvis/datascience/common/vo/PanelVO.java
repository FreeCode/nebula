package org.zjvis.datascience.common.vo;

import org.zjvis.datascience.common.model.TaskItems;

import java.util.ArrayList;
import java.util.List;

/**
 * @description 图左侧面板 查询相关VO
 * @date 2020-08-07
 */
public class PanelVO {
    List<TaskItems> panel = new ArrayList<>();

    public void add(TaskItems taskItems) {
        panel.add(taskItems);
    }
}

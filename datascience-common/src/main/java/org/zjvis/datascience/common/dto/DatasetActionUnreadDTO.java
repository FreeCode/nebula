package org.zjvis.datascience.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.Objects;

/**
 * @description 数据集未读操作信息表，数据集未读操作DTO
 * @date 2021-04-20
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class DatasetActionUnreadDTO {
    private Long datasetId;

    private Long unreadNum;

//    private Long readNum;

    @Override
    public String toString() {
        return "DatasetActionUnreadDTO{" +
                "datasetId=" + datasetId +
                ", unreadNum=" + unreadNum +
//                ", readNum=" + readNum +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DatasetActionUnreadDTO that = (DatasetActionUnreadDTO) o;
        return Objects.equals(datasetId, that.datasetId) && Objects.equals(unreadNum, that.unreadNum);
    }

    @Override
    public int hashCode() {
        return Objects.hash(datasetId, unreadNum);
    }
}

package org.zjvis.datascience.common.dto;

import lombok.Data;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @description 数据查询筛选渲染结果DTO
 * @date 2021-09-22
 */
@Data
@Getter
@SuperBuilder
public class DataDto implements Serializable {
    private Map<String, String> head;
    List<Map<String, String>> data;

    public DataDto(Map<String, String> head, List<Map<String, String>> data) {
        this.head = head;
        this.data = data;
    }

}

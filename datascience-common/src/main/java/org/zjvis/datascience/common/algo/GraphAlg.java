package org.zjvis.datascience.common.algo;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zjvis.datascience.common.enums.AlgEnum;
import org.zjvis.datascience.common.enums.SubTypeEnum;
import org.zjvis.datascience.common.vo.TaskVO;

import java.util.ArrayList;

/**
 * @description 图分析模板类
 * @date 2021-12-24
 */
public class GraphAlg extends BaseAlg {
    private final static Logger logger = LoggerFactory.getLogger("GraphAlg");

    private final String TPL_FILENAME = "template/algo/graph.json";

    public GraphAlg() {
        super(AlgEnum.GRAPH_BUILD.name(), SubTypeEnum.GRAPH_NET.getVal(), SubTypeEnum.GRAPH_NET.getDesc());
    }

    public void initTemplate(JSONObject data) {
        JSONArray jsonArray = getTemplateParamList(TPL_FILENAME);
        data.put("setParams", jsonArray);
        data.put("categories", new ArrayList<>());
        data.put("edges", new ArrayList<>());
        baseInitTemplate(data);
    }

    public void defineOutput(TaskVO vo) {
    }
}

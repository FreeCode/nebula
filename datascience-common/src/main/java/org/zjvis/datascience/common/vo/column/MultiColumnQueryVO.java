package org.zjvis.datascience.common.vo.column;

import lombok.Data;
import org.zjvis.datascience.common.vo.PageVO;

import java.util.Set;

/**
 * @description 查询数据统计查询请求VO （多个col）
 * @date 2021-12-22
 */
@Data
public class MultiColumnQueryVO extends PageVO {
    private Long taskId;

    private String table;

    private Integer type;

    private String search;

    private Set<String> columns;

}

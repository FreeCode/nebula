package org.zjvis.datascience.common.vo.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.zjvis.datascience.common.dto.user.UserDTO;

import java.io.Serializable;
import java.util.Set;

/**
 * @description 用户登录相关VO
 * @date 2020-07-23
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserTokenVO implements Serializable {

    private static final long serialVersionUID = 1075295841294758563L;

    private UserVO user;

    private String token;

    private Set<String> permission;

}

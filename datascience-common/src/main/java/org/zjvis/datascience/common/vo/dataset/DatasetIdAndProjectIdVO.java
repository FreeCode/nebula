package org.zjvis.datascience.common.vo.dataset;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.zjvis.datascience.common.vo.PageVO;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @description 数据上传相关VO (获取指定项目中的数据集数据)
 * @date 2021-10-12
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DatasetIdAndProjectIdVO extends PageVO {
    @NotNull(message = "数据集id不能为空")
    @Min(value = 1, message = "请输入有效数据集id")
    private Long datasetId;

}

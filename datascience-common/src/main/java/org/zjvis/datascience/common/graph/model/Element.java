package org.zjvis.datascience.common.graph.model;

/**
 * @description Element
 * @date 2021-12-29
 */
public class Element {
    protected String id;
    protected String label;

    public Element(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}

package org.zjvis.datascience.common.pool;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.*;

/**
 * @description EXCEL文件上传线程
 * @date 2021-10-14
 */
@Component
public class ExcelPool {

    private ThreadPoolExecutor executorService;
    @Value("${excelPool.corePoolSize:20}")
    private Integer corePoolSize;
    @Value("${excelPool.maximumPoolSize:40}")
    private Integer maximumPoolSize;
    @Value("${excelPool.keepAliveTime:120}")
    private Integer keepAliveTime;
    @Value("${excelPool.blockQueueSize:10000}")
    private Integer blockQueueSize;

    @PostConstruct
    public void init() {
        BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<>(blockQueueSize);
        ThreadFactory threadFactory = new DefaultThreadFactoryImpl();
        RejectedExecutionHandler handler = new ThreadPoolExecutor.AbortPolicy();
        executorService = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime, TimeUnit.SECONDS,
                workQueue, threadFactory, handler);
    }

    public ThreadPoolExecutor getExecutor() {
        return executorService;
    }
}

package org.zjvis.datascience.common.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.zjvis.datascience.common.util.RedisUtil;

/**
 * @description JWT配置类
 * @date 2020-04-15
 */
@Configuration
public class JwtConfig {

    @Value("${jwt.online-key}")
    public String onlineKey;

    @Autowired
    public RedisUtil redisUtil;
    @Value("${jwt.token-validity-in-seconds}")

    public Long jwtExpiration;

    @Value("${jwt.base64-secret}")
    public String secret;

}

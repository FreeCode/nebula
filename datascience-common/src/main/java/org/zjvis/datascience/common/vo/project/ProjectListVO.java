package org.zjvis.datascience.common.vo.project;

import java.util.List;
import lombok.Data;

/**
 * @description 项目数据集渲染相关VO
 * @date 2021-05-07
 */
@Data
public class ProjectListVO {
    private Long id;

    private String name;

    private Integer roleId;

    private List<Long> permissionIds;

    private String creator;

    private String updateTime;

    private Integer type;
}

package org.zjvis.datascience.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @description 数据 接口审计相关VO
 * @date 2020-12-31
 */
@Data
@AllArgsConstructor
public class AuditVO {

    private LocalDateTime requestTime;

    private String url;

    private String params;

    private String requestType;

    private String requestData;

    private String username;

    private String ip;

    private String envInfo;
}

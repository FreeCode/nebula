package org.zjvis.datascience.common.vo.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @description 更新用户相关VO
 * @date 2021-12-13
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class UpdateUserVO {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "用户id不能为空")
    @Min(value = 1, message = "请输入有效用户id")
    @ApiModelProperty(value = "id", required = true)
    private Long id;

//    @ApiModelProperty(value = "登录用户名")
//    private String name;
//
//    @ApiModelProperty(value = "状态")
//    private Byte status;
//
//    @ApiModelProperty(value = "密码")
//    private String password;

//    private String phone;

    private String remark;

    /*
    注册后完善
     */
    private String email; // email

    private String nickName; //昵称

    private String realName; //真实姓名

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateBirth; //出生日期


    private String gender; //性别

    private String profession; //职业

    private String workOrg; //工作单位

    private String address; //通信地址

    private String researchField; //研究领域

//    private String pic_encode; //用户头像编码

}

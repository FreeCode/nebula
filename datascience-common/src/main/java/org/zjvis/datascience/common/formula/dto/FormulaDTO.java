package org.zjvis.datascience.common.formula.dto;

import lombok.*;

/**
 * @description 公式 FormulaPOJO
 * @date 2021-12-01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class FormulaDTO {
    private long id;
    private String formula;
    private String result;
    private String status;
    private Integer precisionNum;
}

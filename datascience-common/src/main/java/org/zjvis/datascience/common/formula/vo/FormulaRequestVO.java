package org.zjvis.datascience.common.formula.vo;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.zjvis.datascience.common.validator.NotNullNumber;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @description 公式查询渲染VO类
 * @date 2021-12-01
 */
@Data
public class FormulaRequestVO {

    private Long pipelineId;

    @NotNullNumber
    private Long projectId;

    private Object relationship;

    public Long getPipelineId() {
        return this.pipelineId != null ? this.pipelineId : null;
    }

    public Map<String, Long> getRelationshipMap() {
        if (null != this.relationship && StringUtils.isNotEmpty(this.relationship.toString())) {
            String relationshipJsonStr = this.relationship.toString().substring(1, this.relationship.toString().length() - 1);
            return Arrays.stream(relationshipJsonStr.split(",")).map(s -> s.split("=")).collect(Collectors.toMap(e -> e[0].trim(), e -> Long.parseLong(e[1])));
        }else {
            return null;
        }
    }
}

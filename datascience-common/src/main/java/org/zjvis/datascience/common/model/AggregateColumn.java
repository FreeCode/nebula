package org.zjvis.datascience.common.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @description 复杂聚合查询 字段包装类
 * @date 2021-12-23
 */
@Data
public class AggregateColumn implements Serializable {

    private static final long serialVersionUID = 4379348936813117804L;

    private int index;

    private String name;

    private String type;

    private String aggrType;

    public AggregateColumn() {
    }

}

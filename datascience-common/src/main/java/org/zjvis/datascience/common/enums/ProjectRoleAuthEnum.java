package org.zjvis.datascience.common.enums;

import lombok.Getter;

/**
 * @description 项目角色枚举类
 * @date 2021-05-11
 */
@Getter
public enum ProjectRoleAuthEnum {
    CREATOR("项目创建者", 1),
    ADMIN("项目管理员", 2),
    DEVELOPER("项目开发人员", 3),
    VISITOR("项目访客",  4)
    ;

    private String desc;
    private Integer value;

    ProjectRoleAuthEnum(String desc, Integer value) {
        this.desc = desc;
        this.value = value;
    }
    public enum WithOutCreatorEnum{
        ADMIN("项目管理员", 2),
        DEVELOPER("项目开发人员", 3),
        VISITOR("项目访客",  4)
        ;

        private String desc;
        private Integer value;

        WithOutCreatorEnum(String desc, Integer value) {
            this.desc = desc;
            this.value = value;
        }
    }
}

package org.zjvis.datascience.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

/**
 * @description 用户账号下项目信息表，用户项目 DTO
 * @date 2021-05-08
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class UserProjectDTO extends BaseDTO implements Serializable {

    private Long id;

    private Long userId;

    private String userName;

    private Long projectId;

    private Integer roleId;

    private String roleName;

    private Byte auth;

    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        UserProjectDTO other = (UserProjectDTO) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
                && (this.getProjectId() == null ? other.getProjectId() == null : this.getProjectId().equals(other.getProjectId()))
                && (this.getRoleId() == null ? other.getRoleId() == null : this.getRoleId().equals(other.getRoleId()))
                && (this.getRoleName() == null ? other.getRoleName() == null : this.getRoleName().equals(other.getRoleName()))
                && (this.getGmtCreate() == null ? other.getGmtCreate() == null : this.getGmtCreate().equals(other.getGmtCreate()))
                && (this.getGmtModify() == null ? other.getGmtModify() == null : this.getGmtModify().equals(other.getGmtModify()))
                && (this.getGmtCreator() == null ? other.getGmtCreator() == null : this.getGmtCreator().equals(other.getGmtCreator()))
                && (this.getGmtModifier() == null ? other.getGmtModifier() == null : this.getGmtModifier().equals(other.getGmtModifier()))
                && (this.getAuth() == null ? other.getAuth() == null : this.getAuth().equals(other.getAuth()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getProjectId() == null) ? 0 : getProjectId().hashCode());
        result = prime * result + ((getRoleId() == null) ? 0 : getRoleId().hashCode());
        result = prime * result + ((getRoleName() == null) ? 0 : getRoleName().hashCode());
        result = prime * result + ((getGmtCreate() == null) ? 0 : getGmtCreate().hashCode());
        result = prime * result + ((getGmtModify() == null) ? 0 : getGmtModify().hashCode());
        result = prime * result + ((getGmtCreator() == null) ? 0 : getGmtCreator().hashCode());
        result = prime * result + ((getGmtModifier() == null) ? 0 : getGmtModifier().hashCode());
        result = prime * result + ((getAuth() == null) ? 0 : getAuth().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append(" id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", projectId=").append(projectId);
        sb.append(", roleId=").append(roleId);
        sb.append(", roleName=").append(roleName);
        sb.append(", auth=").append(auth);
        sb.append("]");
        return sb.toString();
    }
}

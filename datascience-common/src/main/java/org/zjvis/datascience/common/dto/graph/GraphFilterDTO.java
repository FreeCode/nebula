package org.zjvis.datascience.common.dto.graph;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.zjvis.datascience.common.dto.BaseDTO;
import org.zjvis.datascience.common.util.DozerUtil;
import org.zjvis.datascience.common.vo.graph.GraphFilterVO;

/**
 * @description Graph分析构建清洗筛选DTO
 * @date 2021-12-24
 */
@Data
public class GraphFilterDTO extends BaseDTO {
    private Long id;

    private String name;

    private Long projectId;

    private Long graphId;

    private Long graphFilterPipelineId;

    private Integer type;

    private Integer subType;

    private Long parentId;

    private Long userId;

    private String dataJson;

    public GraphFilterVO view() {
        GraphFilterVO vo = DozerUtil.mapper(this, GraphFilterVO.class);
        vo.setData(JSONObject.parseObject(this.dataJson));
        return vo;
    }
}

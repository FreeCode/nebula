package org.zjvis.datascience.common.annotation;

import org.zjvis.datascience.common.enums.ProjectRoleAuthEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @description 项目角色权限校验鉴权注解
 * @date 2021-12-24
 */
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface ProjectRoleAuth {
    /**
     * 项目id字段
     * Note: 如果项目id非参数中的某一属性，即打上此注解的参数就是项目id，则令filed=StringUtils.EMPTY
     */
    String field() default "projectId";

    ProjectRoleAuthEnum role() default ProjectRoleAuthEnum.VISITOR;

    /**
     * 项目锁定校验
     *
     * @return
     */
    boolean checkLock() default false;
}

package org.zjvis.datascience.common.strategy;

/**
 * @description : 多种设值策略接口
 * @date 2021-09-15
 */
public interface SetValueStrategy {

    int doOperation(int num1, int num2);

}

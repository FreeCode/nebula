package org.zjvis.datascience.common.dto.dataset;

import lombok.Data;

/**
 * @description 数据集名称状态DTO
 * @date 2021-12-24
 */
@Data
public class DatasetNameStatusDTO {

    private Long id;

    private String name;

    private Integer status;
}

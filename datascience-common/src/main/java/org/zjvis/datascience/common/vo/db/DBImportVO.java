package org.zjvis.datascience.common.vo.db;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.zjvis.datascience.common.vo.dataset.DatasetDBTableVO;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DBImportVO {

    @ApiModelProperty(value = "所属类别id", required = true)
    @NotNull(message = "所属类别id不能为空")
    @Min(value = 1, message = "请输入有效所属类别id")
    private Long categoryId;

    @NotBlank(message = "导入数据类型")
    @ApiModelProperty(value = "导入数据类型", required = true)
    private String importType;

    @NotBlank(message = "数据库类型不能为空")
    @ApiModelProperty(value = "数据库类型", required = true)
    private String databaseType;

    @NotNull(message = "表配置信息不能为空")
    @Size(min = 1, message = "表配置信息不能为空")
    @ApiModelProperty(value = "表配置信息", required = true)
    private List<@Valid DatasetDBTableVO> tableConfigs;

    @NotBlank(message = "server不能为空")
    @ApiModelProperty(value = "server", required = true)
    private String server;

    @NotNull(message = "端口不能为空")
    @ApiModelProperty(value = "端口", required = true)
    @Min(value = 0, message = "请输入有效端口")
    private Integer port;

    @NotBlank(message = "数据库名不能为空")
    @ApiModelProperty(value = "数据库名", required = true)
    private String databaseName;

    @NotBlank(message = "用户名不能为空")
    @ApiModelProperty(value = "用户名", required = true)
    private String user;

    @NotBlank(message = "密码不能为空")
    @ApiModelProperty(value = "密码", required = true)
    private String password;

    @ApiModelProperty(value = "连接方式", required = true)
    private String connectType;

    @ApiModelProperty(value = "服务名或SID", required = true)
    private String connectValue;

}

package org.zjvis.datascience.common.vo.project;

import cn.weiguangfu.swagger2.plus.annotation.ApiRequestInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.zjvis.datascience.common.annotation.EnumValue;
import org.zjvis.datascience.common.enums.ProjectRoleAuthEnum;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;


/**
 * @description 用户项目数据表 user_project相关VO
 * @date 2021-12-27
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserProjectVO {

    @ApiRequestInclude(groups = {Id.class, Delete.class})
    @NotNull(message = "id不能为空", groups = {Id.class, Delete.class})
    @Min(value = 1, message = "请输入有效id", groups = {Id.class, Delete.class})
    private Long id;

    @ApiRequestInclude(groups = {Create.class, Update.class, Check.class})
    @ApiModelProperty(value = "用户id", required = true)
    @NotNull(message = "用户不能为空", groups = {Create.class, Update.class, Check.class})
    @Min(value = 1, message = "请输入有效用户id", groups = {Create.class, Update.class, Check.class})
    private Long userId;

    private String userName;

    @ApiRequestInclude(groups = {ProjectId.class, Create.class, Update.class, Delete.class, Check.class})
    @NotNull(message = "项目id不能为空", groups = {ProjectId.class, Create.class, Update.class, Delete.class, Check.class})
    @Min(value = 1, message = "请输入有效项目id", groups = {ProjectId.class, Create.class, Update.class, Delete.class, Check.class})
    private Long projectId;

//    @ApiRequestInclude(groups = {Create.class, Update.class})
//    @ApiModelProperty(value = "权限，1：读，2：读写", required = true)
//    @EnumValue(enumClass = ProjectAuthEnum.class, message = "权限只能为1-读、2-写"
//            , groups = {Update.class, Create.class})
//    private Byte auth;

    @ApiRequestInclude(groups = {Create.class, Update.class})
    @ApiModelProperty(value = "角色：2-项目管理员、3-项目开发者、4-项目访客", required = true)
    @EnumValue(enumClass = ProjectRoleAuthEnum.WithOutCreatorEnum.class, message = "角色只能为2-项目管理员、3-项目开发者、4-项目访客"
            , groups = {Update.class, Create.class})
    private Integer roleId;

    private LocalDateTime gmtModify;

    private String roleName;

    private static final long serialVersionUID = 1L;

    public interface Id {
    }

    public interface ProjectId {
    }

    public interface Create {
    }

    public interface Update {
    }

    public interface Delete {
    }

    public interface Check {
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append(" id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", projectId=").append(projectId);
        sb.append(", roleId=").append(roleId);
        sb.append("]");
        return sb.toString();
    }
}

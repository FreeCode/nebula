package org.zjvis.datascience.common.vo.project;

import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @description 创建项目相关VO
 * @date 2021-05-07
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class BatchCreateUserProjectVO {

//    @Valid
//    @NotNull(message = "需要添加的权限不能为空")
//    @Size(min = 1, message = "需要添加的权限不能为空")
//    private List<CreateUserProjectVO> userAuths;

    @Valid
    @NotNull(message = "需要添加的角色不能为空")
    @Size(min = 1, message = "需要添加的角色不能为空")
    private List<CreateUserProjectVO> userRoles;

    @NotNull(message = "项目id不能为空")
    @Min(value = 1, message = "请输入有效项目id")
    private Long projectId;

    private static final long serialVersionUID = 1L;

}

package org.zjvis.datascience.common.vo.project;

import lombok.Builder;
import lombok.Data;

/**
 * @description 项目数据集相关VO
 * @date 2021-05-25
 */
@Data
@Builder
public class ProjectDataSourceTableVO {
    private Long id;
    /**
     * dataset name
     */
    private String name;
    private String tableName;
    private String categoryName;
    private String type;

}

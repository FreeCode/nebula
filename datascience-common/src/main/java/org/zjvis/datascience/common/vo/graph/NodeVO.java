package org.zjvis.datascience.common.vo.graph;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NodeVO implements Serializable {
    private String id;

    private String label;

    private String type = "circle";

    private String categoryId;

    private Integer x;

    private Integer y;

    private Integer size;

    private List<AttrVO> attrs;

    private JSONObject labelCfg = new JSONObject();

    private JSONObject style = new JSONObject();

    private Integer orderId;
}

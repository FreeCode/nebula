package org.zjvis.datascience.common.vo.project;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * @description 项目数据集渲染相关VO
 * @date 2021-05-07
 */
@Data
public class ProjectPageVO {
    @NotNull(message = "pageNum不能为空")
    @Min(value = 1,message = "请输入有效的pageNum")
    private Integer pageNum;

    @NotNull(message = "pageSize不能为空")
    @Min(value = 1,message = "请输入有效的pageSize")
    private Integer pageSize;

    @NotNull(message = "projectType不能为空")
    private Integer projectType;

    private String  sortColumn;

    private String  sortMethod;
}

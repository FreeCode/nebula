package org.zjvis.datascience.common.util.db;

import cn.hutool.db.Entity;
import cn.hutool.db.handler.EntityListHandler;
import org.zjvis.datascience.common.dto.dataset.DatasetColumnDTO;
import org.zjvis.datascience.common.util.SqlUtil;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * @description 数据集列表结果处理器
 * @date 2020-08-27
 */
public class TransformEntityListHandler extends EntityListHandler {

    private String newTable;
    private String databaseType;
    private boolean preview;
    private List<DatasetColumnDTO> columnMessage;

    public TransformEntityListHandler(String newTable, String databaseType, boolean preview,
                                      List<DatasetColumnDTO> columnMessage) {
        super();
        this.newTable = newTable;
        this.databaseType = databaseType;
        this.preview = preview;
        this.columnMessage = columnMessage;
    }

    @Override
    public List<Entity> handle(ResultSet rs) throws SQLException {
        final ResultSetMetaData meta = rs.getMetaData();
        final int columnCount = meta.getColumnCount();
        List<Entity> data = new LinkedList<>();
        while (rs.next()) {
            data.add(HandleHelper.handleRow(columnCount, meta, rs, false, newTable, SqlUtil::formatFieldName, databaseType, preview, columnMessage));
        }

        return data;
    }

}

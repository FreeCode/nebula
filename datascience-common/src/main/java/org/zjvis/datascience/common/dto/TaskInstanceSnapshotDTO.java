package org.zjvis.datascience.common.dto;

import lombok.Data;

/**
 * @description 任务节点实例 TaskInstance 快照信息表，任务节点实例 快照DTO
 * @date 2021-05-25
 */
@Data
public class TaskInstanceSnapshotDTO extends BaseDTO {
    private Long id;

    private Long pipelineSnapshotId;

    private Long pipelineId;

    private Long projectId;

    private String taskInstanceJson;
}

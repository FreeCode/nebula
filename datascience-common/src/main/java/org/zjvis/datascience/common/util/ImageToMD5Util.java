package org.zjvis.datascience.common.util;


import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/**
 * @description 获取上传文件的md5
 * @date 2021-10-15
 */
public class ImageToMD5Util {
    public static String getMd5(MultipartFile file) throws IOException, NoSuchAlgorithmException {
        //获取文件的byte信息
        byte[] uploadBytes = file.getBytes();
        // 拿到一个MD5转换器
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        byte[] digest = md5.digest(uploadBytes);
        //转换为16进制
        return new BigInteger(1, digest).toString(16);

    }


}

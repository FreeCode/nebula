package org.zjvis.datascience.common.vo;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import lombok.Data;

/**
 * @description 机器学习算子实例数据表 Model_Instance 相关VO
 * @date 2021-06-15
 */
@Data
public class MLModelInstanceVO {
    private Long id;

    private Long projectId;

    private Long userId;

    private String algorithm;

    private String modelDesc;

    private String modelType;

    private String status;

    private Long duringTime;

    private LocalDateTime gmtRunning;

    private Map<Long, TaskInstanceVO> taskMap;

    private List<String> totalLogs;
}

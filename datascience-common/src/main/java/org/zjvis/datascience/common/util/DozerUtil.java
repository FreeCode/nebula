package org.zjvis.datascience.common.util;


import com.github.dozermapper.core.DozerBeanMapperBuilder;
import com.github.dozermapper.core.Mapper;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @description 对象转换工具类
 * @date 2020-08-12
 */
public class DozerUtil {

    private static Mapper mapper = DozerBeanMapperBuilder.buildDefault();

    public static <S, T> T mapper(S s, Class<T> t) {
        if (s == null) {
            return null;
        }
        return mapper.map(s, t);
    }

    public static <S, T> List<T> mapper(Collection<S> ss, Class<T> t) {
        if (CollectionUtils.isEmpty(ss)) {
            return Collections.emptyList();
        }
        return ss.stream().map(s -> mapper(s, t)).collect(Collectors.toList());
    }

    public static <S, T> List<T> mapper(Collection<S> ss, Function<? super S, ? extends T> customMapper) {
        if (CollectionUtils.isEmpty(ss)) {
            return Collections.emptyList();
        }
        return ss.stream().map(customMapper).collect(Collectors.toList());
    }

}

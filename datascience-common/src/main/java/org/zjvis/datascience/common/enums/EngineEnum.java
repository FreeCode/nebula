package org.zjvis.datascience.common.enums;

/**
 * @description 计算引擎枚举类
 * @date 2021-08-19
 */
public enum EngineEnum {
    MADLIB("madlib", 1),
    SPARK("spark", 2);

    private String desc;

    private int val;

    EngineEnum(String desc, int val) {
        this.desc = desc;
        this.val = val;
    }

    public String getDesc() {
        return desc;
    }

    public int getVal() {
        return val;
    }

    public boolean isSpark(){
        return this.getVal() == SPARK.getVal();
    }

    public boolean isMadlib(){
        return this.getVal() == MADLIB.getVal();
    }
}

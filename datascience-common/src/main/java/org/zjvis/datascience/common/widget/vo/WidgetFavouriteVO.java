package org.zjvis.datascience.common.widget.vo;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.zjvis.datascience.common.widget.dto.WidgetFavouriteDTO;
import cn.weiguangfu.swagger2.plus.annotation.ApiRequestInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description 用于来源于数据视图的图表控件的收藏结果渲染POJO （比如收藏，和结果表）
 * @date 2021-12-01
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WidgetFavouriteVO {

    @NotNull(message = "id不能为空")
    @Min(value = 1, message = "请输入有效id")
    private Long id;

    @ApiRequestInclude(groups = {Create.class})
    @NotNull(message = "可视化组件id不能为空", groups = {Create.class})
    private Long widgetId;

    @ApiRequestInclude(groups = {Query.class, Create.class})
    @NotNull(message = "pipeline id不能为空", groups = {Create.class, Query.class})
    private Long pipelineId;
    
    private String pipelineName;

    private WidgetVO widget;

    public interface Create {
    }

    public interface Query {
    }

    public static WidgetFavouriteVO from(WidgetFavouriteDTO dto) {
        if (dto == null) {
            return null;
        }
        WidgetFavouriteVO widgetFavouriteVO = new WidgetFavouriteVO();
        widgetFavouriteVO.setId(dto.getId());
        widgetFavouriteVO.setWidgetId(dto.getWidgetId());
        widgetFavouriteVO.setPipelineId(dto.getPipelineId());
        widgetFavouriteVO.setWidget(dto.getWidget().view());
        return widgetFavouriteVO;
    }

}

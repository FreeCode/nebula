package org.zjvis.datascience.common.enums;

/**
 * @description 重复文件上传状态枚举类
 * @date 2021-12-16
 */
public enum FileCheckMd5Status {

    FILE_UPLOADED(200, "文件已存在！"),
    FILE_NO_UPLOAD(404, "该文件没有上传过"),
    FILE_UPLOAD_SOME(206, "该文件已上传一部分"),
    CHECK_PROCESS_FAIL(405, "服务繁忙，请重新尝试！"),
    ;

    private final int value;

    private final String status;

    FileCheckMd5Status(int value, String status) {
        this.value = value;
        this.status = status;
    }

    public int getValue() {
        return value;
    }

    public String getStatus() {
        return status;
    }
}

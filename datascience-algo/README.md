# 算子工程使用说明

## 执行步骤
#### 打包
> 执行命令: `mvn assembly:assembly package` 打出jar包:"datascience-algo-1.0.0-SNAPSHOT-jar-with-dependencies.jar"
#### jar包上传
> 将打出的jar包上传到集群客户端，如"10.5.24.102"

#### 任务提交
> 如kmean任务提交命令样例如下: <br>
> `spark-submit --master yarn --deploy-mode cluster --class org.zjvis.datascience.spark.algorithm.AlgorithmEntry datascience-algo-1.0.0-SNAPSHOT-jar-with-dependencies.jar k-Means -s default.test_table -k 3 -f area,perimeter,compactness,width_kernel,length_kernel -m 20 -t default.target_table -uk 12345678911 -idcol record_id -id 12334` <br>
* 参数说明 * <br>
- `k-Means`表示算法类型
- `-uk`表示的是uniqueKey这个在job调用的时候确定，可以通过taskid+timestamp指定
- `-id` 表示taskinstanceId
- `-idcol` 可选
- 其它参数为算子需要的参数，因不同算子而异




package org.apache.spark.example

import java.io.{BufferedWriter, OutputStreamWriter}

import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.SparkConf
import org.apache.spark.ml.tsne.impl.{BHTSNE, SimpleTSNE}
import org.apache.spark.ml.tsne.tree.SPTree
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.linalg.distributed.RowMatrix
import org.apache.spark.sql.SparkSession
import org.slf4j.LoggerFactory

object MINST {
  private def logger = LoggerFactory.getLogger(MINST.getClass)

  def main (args: Array[String]) {
    val conf = new SparkConf()
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
//      .set("spark.master", "local[1]")
      .registerKryoClasses(Array(classOf[SPTree]))

    val spark = SparkSession.builder()
      .appName("test")
      .master("local[2]")
      .config(conf)
      .getOrCreate()
    val path = args(0)
    val cost = args(1)
    val resultPath = args(2)

//    val sc = new SparkContext(conf)
    val sc = spark.sparkContext
    val hadoopConf = sc.hadoopConfiguration
    val fs = FileSystem.get(hadoopConf)
//    val path = "D:\\zj-work\\spark-tsne-master\\spark-tsne-master\\data\\mnist\\mnist.csv.gz"
//    val path = "D:\\zj-work\\spark-tsne-master\\spark-tsne-master\\data\\mnist\\mnist.csv\\mnist.csv"
    val dataset = sc.textFile(path)
      .zipWithIndex()
      .filter(_._2 < 100)
      .sortBy(_._2, true, 60)
      .map(_._1)
      .map(_.split(","))
      .map(x => (x.head.toInt, x.tail.map(_.toDouble)))
      .cache()
    //logInfo(dataset.collect.map(_._2.toList).toList.toString)

    //val features = dataset.map(x => Vectors.dense(x._2))
    //val scaler = new StandardScaler(true, true).fit(features)
    //val scaledData = scaler.transform(features)
    //  .map(v => Vectors.dense(v.toArray.map(x => if(x.isNaN || x.isInfinite) 0.0 else x)))
    //  .cache()
    System.out.println(dataset.count())
    val data = dataset.flatMap(_._2)
    System.out.println(data.count())

    val mean = data.mean()

    val std = data.stdev()
    val scaledData = dataset.map(x => Vectors.dense(x._2.map(v => (v - mean) / std))).cache()

    val labels = dataset.map(_._1).collect()
    val matrix = new RowMatrix(scaledData)
    val pcaMatrix = matrix.multiply(matrix.computePrincipalComponents(50))
    pcaMatrix.rows.cache()

    val costWriter = new BufferedWriter(new OutputStreamWriter(fs.create(new Path(cost), true)))

    SimpleTSNE.tsne(pcaMatrix, perplexity = 20, maxIterations = 1, callback = {

   // BHTSNE.tsne(pcaMatrix, maxIterations = 1, callback = {
      //LBFGSTSNE.tsne(pcaMatrix, perplexity = 10, maxNumIterations = 500, numCorrections = 10, convergenceTol = 1e-8)
      case (i, y, loss) =>
        if(loss.isDefined) logger.info(s"$i iteration finished with loss $loss")

        val os = fs.create(new Path(s"$resultPath\\result${"%05d".format(i)}.csv"), true)
        val writer = new BufferedWriter(new OutputStreamWriter(os))
        try {
          (0 until y.rows).foreach {
            row =>
              writer.write(labels(row).toString)
              writer.write(y(row, ::).inner.toArray.mkString(",", ",", "\n"))
          }
          if(loss.isDefined) costWriter.write(loss.get + "\n")
        } finally {
          writer.close()
        }
    })
    costWriter.close()

    sc.stop()
  }
}
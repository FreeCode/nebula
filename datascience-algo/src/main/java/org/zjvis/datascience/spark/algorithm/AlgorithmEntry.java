package org.zjvis.datascience.spark.algorithm;

import cn.hutool.core.text.UnicodeUtil;
import org.alitouka.spark.dbscan.spatial.Box;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.SparkSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * @description Spark算子节点入口
 * @date 2021-12-23
 */
public class AlgorithmEntry {

    private final static Logger logger = LoggerFactory.getLogger("AlgorithmEntry");

    public static String getAlgorithmName(String[] args) {
        if (args.length < 1) {
            return "";
        }
        return args[0];
    }

    public static void main(String[] args) {
        for (int i = 0; i < args.length; i++) {
            args[i] = UnicodeUtil.toString(args[i]);
        }
        Class[] classes = new Class[]{Box.class, IsolationForestAlgorithm.class,
                KmeansAlgorithm.class, StatisticsAnomaly.class, PcaAlgorithm.class};

        String warehouseLocation = new File("spark-warehouse").getAbsolutePath();
        SparkConf sparkConf = new SparkConf()
                .set("spark.sql.warehouse.dir", warehouseLocation)
                .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
                .registerKryoClasses(classes)
                .set("spark.sql.crossJoin.enabled", "true");
        SparkSession spark = SparkSession
                .builder()
                .appName("Java Spark Hive Example")
                .config(sparkConf)
                .enableHiveSupport()
                .getOrCreate();

        String algoName = getAlgorithmName(args);
        BaseAlgorithm baseAlgorithm = null;
        if (algoName.equals("k-Means")) {
            baseAlgorithm = new KmeansAlgorithm(spark);
        } else if (algoName.equals("dbscan")) {
            baseAlgorithm = new DBscanAlgorithmV2(spark);
        } else if (algoName.equals("pca")) {
            baseAlgorithm = new PcaAlgorithm(spark);
        } else if (algoName.equals("linear")) {
            baseAlgorithm = new LinearRegressionAlgorithm(spark);
        } else if (algoName.equals("logistic")) {
            baseAlgorithm = new LogisticRegressionAlgorithm(spark);
        } else if (algoName.equals("fp-growth")) {
            baseAlgorithm = new FPGrowthAlgorithm(spark);
        } else if (algoName.equals("prefix-span")) {
            baseAlgorithm = new PrefixSpanAlgorithm(spark);
        } else if (algoName.equals("statistics-anomaly")) {
            baseAlgorithm = new StatisticsAnomaly(spark);
        } else if (algoName.equals("isolation-forest")) {
            baseAlgorithm = new IsolationForestAlgorithm(spark);
        } else if (algoName.equals("tsne")) {
            baseAlgorithm = new TSNEAlgorithm(spark);
        }
        baseAlgorithm.parseParams(args);
        baseAlgorithm.beginAlgorithm();
        spark.close();
    }
}

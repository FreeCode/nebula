package org.zjvis.datascience.spark.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author wangyizhong
 * @date 2020-12-23
 * @desc 配置类
 */
public class Config {

    public static String DB_URL_MYSQL = "";

    public static String USER_MYSQL = "";

    public static String PASSWORD_MYSQL = "";

    public static String DB_URL_GP = "";

    public static String USER_GP = "";

    public static String PASSWORD_GP = "";

    private volatile static Config config;

    private Config() {

    }

    private static boolean parseProperties(String path) throws IOException {
        Properties properties = new Properties();
        InputStream in = Config.class.getClassLoader().getResourceAsStream(path);
        properties.load(in);
        String env = properties.getProperty("env.switch");
        PASSWORD_GP = properties.getProperty("password.gp." + env);
        DB_URL_MYSQL = properties.getProperty("db.url.mysql." + env);
        USER_MYSQL = properties.getProperty("user.mysql." + env);
        PASSWORD_MYSQL = properties.getProperty("password.mysql." + env);
        DB_URL_GP = properties.getProperty("db.url.gp." + env);
        USER_GP = properties.getProperty("user.gp." + env);

        return true;
    }

    public static Config getInstance(String path) {
        if (config == null) {
            synchronized (Config.class) {
                if (config == null) {
                    config = new Config();
                    try {
                        if (!parseProperties(path)) {
                            return null;
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return config;
    }
}


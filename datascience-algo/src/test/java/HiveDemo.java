import org.apache.spark.sql.SparkSession;

import java.io.File;

public class HiveDemo {
    public static void main(String[] args) {
        String filePath = args[0];

        String warehouseLocation = new File("spark-warehouse").getAbsolutePath();

        SparkSession spark = SparkSession
                .builder()
//                .master("local[1]")
                .appName("Java Spark Hive Example")
                .config("spark.sql.warehouse.dir", warehouseLocation)
                .enableHiveSupport()
                .getOrCreate();
        spark.sql("CREATE TABLE IF NOT EXISTS test_table (record_id BIGINT, area DOUBLE," +
                " perimeter DOUBLE, compactness DOUBLE, length_kernel DOUBLE, width_kernel DOUBLE," +
                " asymmetry DOUBLE, groove_length DOUBLE, real_class BIGINT) ROW FORMAT DELIMITED " +
                "FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n'");
        spark.sql(String.format("LOAD DATA INPATH '%s' INTO TABLE test_table", filePath));

        spark.sql("select * from test_table limit 10").show();

        spark.close();

    }
}

package org.zjvis.datascience.service.dataprovider;

import org.zjvis.datascience.common.exception.DataScienceException;
import org.zjvis.datascience.common.model.AggrConfig;
import org.zjvis.datascience.common.model.AggregateResult;
import org.zjvis.datascience.common.model.Column;
import org.zjvis.datascience.common.model.Table;

import java.util.List;

/**
 * @description 数据provider通用接口 查询对应数据源中的表 字段 数据等
 * @date 2020-10-09
 */
public interface Aggregator {

    String getSql(Table table, AggrConfig config) throws DataScienceException;

    AggregateResult getData(Table table, AggrConfig config) throws DataScienceException;

    List<Column> getColumns(Table table) throws Exception;

    List<String> getColumnValues(Table table, String column, AggrConfig config) throws DataScienceException;

    List<String> showDatabases(Long dsId) throws DataScienceException;

    List<String> showTables(Long dsId, String db) throws DataScienceException;

}

package org.zjvis.datascience.service.csv.dto;

import cn.hutool.db.Entity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description 文件解析结果 ParseInfo
 * @date 2021-12-29
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ParseInfo {
    private ErrorInfo errorInfo;
    private List<Entity> previewData;
}

package org.zjvis.datascience.service.dag;

import org.zjvis.datascience.common.dto.TaskInstanceDTO;
import org.zjvis.datascience.service.JlabService;

import java.util.concurrent.Callable;

import static org.zjvis.datascience.common.constant.Constant.errorTpl;

/**
 * @description Jupyter Notebook任务调度器
 * @date 2021-08-20
 */
public class JLabRunner implements Callable<TaskRunnerResult>  {

    private JlabService jlabService;

    private TaskInstanceDTO instance;

    public JLabRunner(JlabService jlabService, TaskInstanceDTO instance){
        this.jlabService = jlabService;
        this.instance = instance;
    }

    @Override
    public TaskRunnerResult call() {
        if (instance.hasPrecautionaryError()) {
            return new TaskRunnerResult(500, String.format(errorTpl, "error happens when init stage."));
        }
        TaskRunnerResult result = null;
        try {
            //todo
            result = jlabService.execRunner(this.instance);
        } catch (Exception e) {
            result = new TaskRunnerResult(500, e.getMessage());
        }
        return result;
    }
}

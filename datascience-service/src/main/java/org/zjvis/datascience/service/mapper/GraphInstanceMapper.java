package org.zjvis.datascience.service.mapper;

import org.zjvis.datascience.common.dto.graph.GraphInstanceDTO;

/**
 * @description GraphInstance 数据表Mapper
 * @date 2021-08-20
 */
public interface GraphInstanceMapper {
    boolean save(GraphInstanceDTO graph);

    boolean update(GraphInstanceDTO graph);

    GraphInstanceDTO queryLatestInstanceByGraphId(Long graphId);

    GraphInstanceDTO queryById(Long id);
}

package org.zjvis.datascience.service.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;
import org.zjvis.datascience.common.widget.dto.WidgetDTO;

import java.util.List;
import java.util.Map;

/**
 * @description Widget 数据表Mapper
 * @date 2021-12-08
 */
@Component
public interface WidgetMapper {

    boolean save(WidgetDTO widget);

    boolean update(WidgetDTO widget);

    WidgetDTO queryById(Long id);

    List<WidgetDTO> queryByIds(List<Long> ids);

    /**
     * 数据来源是taskId的  在 "我的组件"上的widget
     *
     * @param taskId
     * @return
     */
    List<WidgetDTO> queryTemplateByTaskId(Long taskId);

    /**
     * 数据来源是taskId的  在画布上的widget
     *
     * @param taskId
     * @return
     */
    List<WidgetDTO> queryAllTypeByTaskId(Long taskId);

    List<WidgetDTO> queryByTaskIdAndType(Long taskId);

    /**
     * 数据来源是datasetId的  在 "我的组件"上的widget
     *
     * @param datasetId
     * @return
     */
    List<WidgetDTO> queryTemplateByDatasetId(Long datasetId);

    void deleteById(Long id);

    void delete(Map<String, Object> params);

    int deleteByTasks(@Param(value = "tasks") List<Long> tasks);

    List<WidgetDTO> queryTidByModify(String limitDate);

    List<WidgetDTO> queryByTaskIds(@Param(value = "ids") List<Long> ids);

    List<WidgetDTO> queryAllTypeByTaskIds(@Param(value = "ids") List<Long> ids);

    List<Long> queryIdByTasks(@Param(value = "tasks") List<Long> tasks);

    List<WidgetDTO> queryByTypeAndUser(String type, Long userId);

    /**
     * 数据来源是datasetId的  在画布上的widget
     *
     * @param datasetId
     * @return
     */
    List<WidgetDTO> queryAllTypeByDatasetId(Long datasetId);
}

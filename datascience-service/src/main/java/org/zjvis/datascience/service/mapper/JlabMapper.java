package org.zjvis.datascience.service.mapper;


import org.springframework.stereotype.Component;
import org.zjvis.datascience.common.dto.JlabDTO;

/**
 * @description 自定义算子 [Jupyter Notebook版本]信息表，Jlab Mapper
 * @date 2021-11-26
 */
@Component
public interface JlabMapper {

    Long queryExistence(Long userId);

    JlabDTO queryByUserId(Long userId);

    Long insert(JlabDTO dto);

    Long update(JlabDTO dto);

    Long updateByUser(JlabDTO dto);
}



package org.zjvis.datascience.service.csv.semiotic;

/**
 * @description 文件符号 分隔符常量 Separate
 * @date 2021-12-29
 */
public enum Separate {
    COMMA(1, ","),
    TAB(2, "\t"),
    SPACE(3, " "),
    NULL(0, ""),
    ;

    private final int value;

    private final String symbol;

    Separate(int value, String symbol) {
        this.value = value;
        this.symbol = symbol;
    }

    public int getValue() {
        return value;
    }

    public String getSymbol() {
        return symbol;
    }
}

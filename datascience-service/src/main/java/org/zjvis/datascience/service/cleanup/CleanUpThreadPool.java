package org.zjvis.datascience.service.cleanup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zjvis.datascience.common.pool.DefaultThreadFactoryImpl;
import org.zjvis.datascience.common.pool.ThreadPoolProperties;

import javax.annotation.PostConstruct;
import java.util.concurrent.*;

/**
 * @description 清洗操作 执行线程池
 * @date 2021-12-01
 */
@Component
public class CleanUpThreadPool {

    private ThreadPoolExecutor executorService;

    @Autowired
    private ThreadPoolProperties threadPoolProperties;

    @PostConstruct
    public void init() {
        BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<>(
                threadPoolProperties.getBlockQueueSize());
        ThreadFactory threadFactory = new DefaultThreadFactoryImpl("cleanup", false, 0);
        RejectedExecutionHandler handler = new ThreadPoolExecutor.DiscardPolicy();
        executorService = new ThreadPoolExecutor(threadPoolProperties.getCorePoolSize(),
                threadPoolProperties.getMaximumPoolSize(), threadPoolProperties.getKeepAliveTime(),
                TimeUnit.SECONDS,
                workQueue, threadFactory, handler);
        executorService.allowCoreThreadTimeOut(true);
    }

    public ThreadPoolExecutor getExecutor() {
        return executorService;
    }

}

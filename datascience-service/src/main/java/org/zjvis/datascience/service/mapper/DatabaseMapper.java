package org.zjvis.datascience.service.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.zjvis.datascience.common.dto.DatabaseDTO;

/**
 * @description 数据源信息表，数据驱动Mapper
 * @date 2020-07-29
 */
@Mapper
public interface DatabaseMapper {

    DatabaseDTO queryById(Long id);
}

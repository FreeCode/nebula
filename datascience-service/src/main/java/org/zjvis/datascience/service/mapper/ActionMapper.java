package org.zjvis.datascience.service.mapper;

import org.springframework.stereotype.Component;
import org.zjvis.datascience.common.dto.ActionDTO;

import java.util.List;

/**
 * @description Action 数据表Mapper
 * @date 2021-09-18
 */
@Component
public interface ActionMapper {
    List<ActionDTO> queryByProjectId(Long projectId);

    ActionDTO queryById(Long id);

    boolean save(ActionDTO dto);

    boolean update(ActionDTO dto);

    void delete(Long id);

    void deleteByProjectId(Long projectId);

    void deleteByPipelineId(Long pipelineId);

    List<ActionDTO> queryByPipeline(Long pipelineId);

    List<ActionDTO> queryLatestActionByProjectId(Long projectId, Long limitNum);

    List<ActionDTO> queryLatestActionByPipelineId(Long pipelineId, Long limitNum);
}
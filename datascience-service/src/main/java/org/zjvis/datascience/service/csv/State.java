package org.zjvis.datascience.service.csv;

/**
 * @description csv文本的不同状态
 * @date 2021-12-29
 */
public enum State {
    newStart(1, "新字段开始"),
    quote(2, "引号状态"),
    escape(3, "转义状态"),
    judgequote(4,"转义符原义判定状态"),
    endfield(5,"字段结束"),
    endrow(6,"行结束"),
    enddocument(7,"文档结束"),
    fail(8, "解析失败");

    private int id;
    private String tip;

    State(int id, String tip) {
        this.id = id;
        this.tip = tip;
    }
}

package org.zjvis.datascience.service.csv.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.zjvis.datascience.service.csv.ErrorLineCode;

import java.util.Map;

/**
 * @description 其他错误代码 ErrorInfo
 * @date 2021-12-29
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ErrorInfo {
    private Map<ErrorLineCode, ErrorLineCodeInfo> errorLineCodeInfoMap;

    private int errorCount;

    private int successCount;
}

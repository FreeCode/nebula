package org.zjvis.datascience.service.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.zjvis.datascience.common.dto.OperatorFavouriteDTO;

import java.util.List;

/**
 * @description 用户算子收藏信息表，用户算子收藏 Mapper
 * @date 2021-01-14
 */
@Mapper
public interface OperatorFavouriteMapper {
    int deleteByPrimaryKey(Long id);

    int delete(@Param(value = "record") OperatorFavouriteDTO record);

    int insert(OperatorFavouriteDTO record);

    int insertSelective(OperatorFavouriteDTO record);

    OperatorFavouriteDTO selectByPrimaryKey(Long id);

    List<OperatorFavouriteDTO> selectStatus(@Param(value = "userId") long userId);

    int updateByPrimaryKeySelective(OperatorFavouriteDTO record);

    int updateByPrimaryKey(OperatorFavouriteDTO record);
}

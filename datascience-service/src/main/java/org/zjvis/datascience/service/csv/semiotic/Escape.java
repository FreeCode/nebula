package org.zjvis.datascience.service.csv.semiotic;

/**
 * @description 文件符号 空格常量
 * @date 2021-12-29
 */
public enum Escape {
    DOUBLEQUOTES(1, "\""),
    NULL(0, ""),
    ;

    private final int value;

    private final String symbol;

    Escape(int value, String symbol) {
        this.value = value;
        this.symbol = symbol;
    }

    public int getValue() {
        return value;
    }

    public String getSymbol() {
        return symbol;
    }
}

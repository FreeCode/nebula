package org.zjvis.datascience.service.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.zjvis.datascience.common.dto.OperatorTemplateDTO;

import java.util.List;
import java.util.Map;

/**
 * @description 自定义算子模板信息表，自定义算子模板Mapper
 * @date 2021-01-08
 */
@Mapper
public interface OperatorTemplateMapper {
    boolean save(OperatorTemplateDTO dto);

    boolean update(OperatorTemplateDTO dto);

    OperatorTemplateDTO queryById(Long id);

    void delete(Map<String, Object> params);

    List<OperatorTemplateDTO> queryByUserId(Long userId);

    List<OperatorTemplateDTO> queryByStatus(@Param(value = "status") Integer status);

    Integer queryByNameAndFuncName(OperatorTemplateDTO dto);

    Integer queryByNameAndId(OperatorTemplateDTO dto);

}

package org.zjvis.datascience.service.mapper;

import java.util.List;
import org.springframework.stereotype.Repository;
import org.zjvis.datascience.common.dto.AuditDTO;
import org.zjvis.datascience.common.dto.AuditQueryDTO;

/**
 * @description Audit 数据表Mapper
 * @date 2021-08-20
 */
@Repository
public interface AuditMapper {

  List<AuditDTO> select(AuditQueryDTO queryVO);

  void insertBatch(List<AuditDTO> auditDTOS);

  List<AuditDTO> selectPage(AuditQueryDTO queryVO);

}

package org.zjvis.datascience.service.dataprovider;

import com.google.common.collect.Lists;
import org.springframework.stereotype.Service;
import org.zjvis.datascience.common.exception.DataScienceException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * @description HIVE数据provider 提供数据库连接池
 * @date 2020-10-09
 */
@Service
public class HiveDataProvider extends JdbcDataProvider {

    @Override
    public List<String> showDatabases(Long dsId) throws DataScienceException {
        List<String> ret = Lists.newArrayList();
        Connection conn = null;
        try {
            conn = getConn(dsId);
            Statement stat = conn.createStatement();
            ResultSet rs = stat.executeQuery("show databases");
            while (rs.next()) {
                ret.add(rs.getString(1));
            }
        } catch (Exception e) {
            logger.error("show databases error, dsId=" + dsId, e);
            throw new DataScienceException("dsId=" + dsId + "\n" + e.getMessage());
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
            }
        }
        return ret;
    }
}

package org.zjvis.datascience.service.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.zjvis.datascience.common.dto.TaskInstanceSnapshotDTO;

import java.util.List;

/**
 * @description 任务节点实例 TaskInstance 快照信息表，任务节点实例 快照Mapper
 * @date 2021-05-25
 */
@Mapper
public interface TaskInstanceSnapshotMapper {

    void batchAdd(List<TaskInstanceSnapshotDTO> taskInstanceSnapshots);

    List<TaskInstanceSnapshotDTO> queryByPipelineAndSnapshotId(@Param("pipelineId") Long pipelineId,
                                                               @Param("pipelineSnapshotId") Long pipelineSnapshotId);

    int deleteByPipelineSnapshotId(List<Long> asList);
}

package org.zjvis.datascience.service.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;
import org.zjvis.datascience.common.dto.UserProjectDTO;

import java.util.List;

/**
 * @description 用户账号下项目信息表，用户项目 Mapper
 * @date 2021-09-09
 */
@Component
public interface UserProjectMapper {

    int deleteByPrimaryKey(Long id);

    int delete(UserProjectDTO record);

    void insert(UserProjectDTO record);

    int insertSelective(UserProjectDTO record);

    void batchInsert(@Param("records") List<UserProjectDTO> records);

    UserProjectDTO selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UserProjectDTO record);

    int updateByPrimaryKey(UserProjectDTO record);

    List<UserProjectDTO> select(UserProjectDTO record);

    List<UserProjectDTO> selectWithUserName(UserProjectDTO record);

    int haveAuthority(@Param("userId") long userId, @Param("projectId") long projectId, @Param("auth") byte auth);

    List<UserProjectDTO> listByUserId(@Param(value = "userId") Long userId);

    Integer getRoleId(@Param(value = "userId") Long userId, @Param(value = "projectId") Long projectId);

    int updateUserProjectRole(UserProjectDTO dto);

    int getOperationalUserCount(Long projectId);

    List<Long> listUserProjectIds(@Param(value = "userId") Long userId, @Param(value = "userType") Integer userType);
}

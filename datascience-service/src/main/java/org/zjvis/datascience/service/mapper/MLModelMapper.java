package org.zjvis.datascience.service.mapper;

import org.springframework.stereotype.Component;
import org.zjvis.datascience.common.dto.FolderDTO;
import org.zjvis.datascience.common.dto.MLModelDTO;
import org.zjvis.datascience.common.dto.TaskInstanceDTO;
import org.zjvis.datascience.common.vo.JobStatusVO;
import org.zjvis.datascience.common.vo.MLModelVO;

import java.util.List;

/**
 * @description 模型训练信息表，模型训练Mapper
 * @date 2021-11-23
 */
@Component
public interface MLModelMapper {

    List<MLModelDTO> queryByProjectId(MLModelVO vo);

    MLModelDTO queryMetricsById(Long id);

    boolean save(MLModelDTO dto);

    boolean update(MLModelDTO dto);

    void delete(Long id);

    boolean updateStatus(MLModelVO model);

    void updateTrainTime(Long id);

    void updateProgressId(TaskInstanceDTO applicationId);

    JobStatusVO queryStatus(Long id);

    List<MLModelDTO> queryByStatus(MLModelVO model);

    void setInvisible(Long id);

    List<MLModelDTO> queryByFolder(FolderDTO dto);

    void removeFolderRecord(FolderDTO dto);

    void removeModelRecord(MLModelDTO dto);

    Long queryNumRunning(Long userId);

    /**
     * 通过查 projectId 和 userId 进行查询， userId可以为空
     *
     * @param vo
     * @return
     */
    List<MLModelDTO> queryModelPanel(MLModelVO vo);

}
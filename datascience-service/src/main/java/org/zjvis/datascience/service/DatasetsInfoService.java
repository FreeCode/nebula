package org.zjvis.datascience.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.zjvis.datascience.common.dto.datasetsinfo.DatasetsInfoDTO;
import org.zjvis.datascience.service.mapper.DatasetsInfoMapper;

/**
 * @description DatasetsInfo 数据集信息 Service
 * @date 2021-09-17
 */
@Service
public class DatasetsInfoService {

    @Autowired
    private DatasetsInfoMapper datasetsInfoMapper;


    public List<DatasetsInfoDTO> selectBy(long userId, DatasetsInfoDTO dataInfo) {
        String name = dataInfo.getName();
        if (name != null) {
            dataInfo.setName("%" + name + "%");
        }
        return datasetsInfoMapper.selectBy(userId, dataInfo);
    }
}

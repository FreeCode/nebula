package org.zjvis.datascience.service.mapper;

import org.springframework.stereotype.Component;
import org.zjvis.datascience.common.dto.DashboardDTO;

import java.util.List;
import java.util.Map;

/**
 * @description Dashboard 数据表Mapper
 * @date 2021-08-20
 */
@Component
public interface DashboardMapper {

    boolean save(DashboardDTO dashboard);

    boolean update(DashboardDTO dashboard);

    DashboardDTO queryById(Long id);

    List<DashboardDTO> queryByProjectId(Long projectId);

    DashboardDTO queryByPublishNo(String publishNo);

    void delete(Map<String, Object> params);

    Integer countPublishedNum(Long projectId);


}

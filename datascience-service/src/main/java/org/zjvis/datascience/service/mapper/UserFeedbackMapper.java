package org.zjvis.datascience.service.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.zjvis.datascience.common.dto.user.QueryFeedbackDTO;
import org.zjvis.datascience.common.dto.user.UserFeedbackDTO;

import java.util.List;

/**
 * @description UserFeedback 数据表Mapper
 * @date 2021-09-15
 */
@Mapper
public interface UserFeedbackMapper {

    int insertUserFeedback(UserFeedbackDTO record);

    UserFeedbackDTO selectByPrimaryKey(Long id);

    List<UserFeedbackDTO> selectFeedbacks(QueryFeedbackDTO queryVO);

    int updateUserFeedback(UserFeedbackDTO feedback);
}

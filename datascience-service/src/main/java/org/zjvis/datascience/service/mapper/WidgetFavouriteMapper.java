package org.zjvis.datascience.service.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.zjvis.datascience.common.widget.dto.WidgetFavouriteDTO;

import java.util.List;

/**
 * @description WidgetFavourite 数据表Mapper
 * @date 2021-12-01
 */
@Mapper
public interface WidgetFavouriteMapper {
    int deleteByPrimaryKey(Long id);

    int delete(@Param(value = "record") WidgetFavouriteDTO record);

    int deleteByPipelines(@Param(value = "pipelines") List<Long> pipelines);

    int insert(WidgetFavouriteDTO record);

    int insertSelective(WidgetFavouriteDTO record);

    WidgetFavouriteDTO selectByPrimaryKey(Long id);

    List<WidgetFavouriteDTO> select(@Param(value = "pipelineId") long pipelineId, @Param(value = "userId") long userId);

    List<WidgetFavouriteDTO> selectWithWidget(@Param(value = "pipelineId") long pipelineId, @Param(value = "userId") long userId);

    int updateByPrimaryKeySelective(WidgetFavouriteDTO record);

    int updateByPrimaryKey(WidgetFavouriteDTO record);
}

package org.zjvis.datascience.service.graph;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.zjvis.datascience.common.dto.graph.GraphFilterPipelineDTO;
import org.zjvis.datascience.common.exception.BaseErrorCode;
import org.zjvis.datascience.common.exception.DataScienceException;
import org.zjvis.datascience.common.util.JwtUtil;
import org.zjvis.datascience.service.mapper.GraphFilterPipelineMapper;

import java.util.List;

/**
 * @description GraphFilterPipelineService
 * @date 2021-12-29
 */
@Service
public class GraphFilterPipelineService {
    private static final Logger logger = LoggerFactory.getLogger(GraphFilterPipelineService.class);

    @Autowired
    GraphFilterPipelineMapper graphFilterPipelineMapper;

    public Long save(GraphFilterPipelineDTO dto) {
        graphFilterPipelineMapper.save(dto);
        return dto.getId();
    }

    public void update(GraphFilterPipelineDTO dto) {
        graphFilterPipelineMapper.update(dto);
    }

    public void deleteById(Long id) {
        graphFilterPipelineMapper.deleteById(id);
    }

    public GraphFilterPipelineDTO queryById(Long id) {
        return graphFilterPipelineMapper.queryById(id);
    }

    public List<GraphFilterPipelineDTO> queryByGraphId(Long id) {
        return graphFilterPipelineMapper.queryByGraphId(id);
    }

    public void deleteByGraphId(Long graphId) {
        graphFilterPipelineMapper.deleteByGraphId(graphId);
    }

    public Long queryUserById(Long id) {
        return graphFilterPipelineMapper.queryUserById(id);
    }

    public void checkAuth(Long id) {
        long userId = JwtUtil.getCurrentUserId();
        if (userId==0) {
            throw new DataScienceException(BaseErrorCode.UNAUTHORIZED);
        }
        Long owner = queryUserById(id);
        if (owner == null || JwtUtil.getCurrentUserId() != owner) {
            throw new DataScienceException(BaseErrorCode.GRAPH_NO_PERMISSION);
        }
    }

    public void checkPipelineAndGraph(Long id, Long graphId) {
        GraphFilterPipelineDTO pipeline = queryById(id);
        Long graph = pipeline.getGraphId();
        if (graph == null) {
            throw new DataScienceException(BaseErrorCode.UNAUTHORIZED);
        }
        if (!graph.equals(graphId)) {
            throw new DataScienceException(BaseErrorCode.GRAPH_FILTER_PIPELINE_RELATION_ERROR);
        }
    }


}

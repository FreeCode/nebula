package org.zjvis.datascience.service.dataprovider;

import com.google.common.collect.Maps;

import javax.sql.DataSource;
import java.util.Map;

/**
 * @description 数据provider抽象类 提供数据库连接池
 * @date 2020-10-09
 */
public abstract class DataProvider {

    protected static Map<Long, DataSource> dsMap = Maps.newConcurrentMap();

    protected static Map<String, Map<String, Integer>> columnTypeCache = Maps.newConcurrentMap();

}

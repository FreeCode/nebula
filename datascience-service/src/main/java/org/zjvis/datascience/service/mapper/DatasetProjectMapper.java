package org.zjvis.datascience.service.mapper;

import com.alibaba.fastjson.JSONObject;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;
import org.zjvis.datascience.common.dto.DatasetProjectDTO;
import org.zjvis.datascience.common.dto.ProjectDatasetDTO;
import org.zjvis.datascience.common.dto.dataset.DatasetUsedInProjectDTO;
import org.zjvis.datascience.common.vo.dataset.QueryDatasetUsedInProjectVO;

import java.util.Collection;
import java.util.List;

/**
 * @description 项目中引用数据集信息表，项目数据Mapper
 * @date 2021-09-07
 */
@Component
public interface DatasetProjectMapper {
    int deleteByPrimaryKey(Long id);

    int delete(@Param("projectId") long projectId, @Param("datasetIds") Collection<Long> datasetIds);

    int insert(DatasetProjectDTO record);

    int insertSelective(DatasetProjectDTO record);

    void batchInsert(@Param("records") Collection<DatasetProjectDTO> records);

    DatasetProjectDTO selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(DatasetProjectDTO record);

    int updateByPrimaryKey(DatasetProjectDTO record);

    int selectCount(JSONObject param);

    List<DatasetProjectDTO> queryByDatasetId(Long datasetId);

    List<ProjectDatasetDTO> queryProjectDataset(@Param("projectId") Long projectId, @Param("datasetName") String datasetName);

    List<ProjectDatasetDTO> queryProjectAllTables(@Param("projectId") Long projectId, @Param("pipelineId") Long pipelineId, @Param("datasetName") String datasetName);

    List<DatasetProjectDTO> queryByProjectAndUser(@Param("projectId") Long projectId, @Param("userId") Long userId);

    List<DatasetUsedInProjectDTO> queryDatasetUsedInProject(QueryDatasetUsedInProjectVO vo);

    int deleteByProjectAndUser(@Param("projectId") Long projectId, @Param("userId") Long userId);
}

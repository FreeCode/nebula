package org.zjvis.datascience.service;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.zjvis.datascience.common.dto.ExternalDataSourceDTO;
import org.zjvis.datascience.service.mapper.ExternalDataSourceMapper;

/**
 * @description ExternalDataSource 外部数据集 Service
 * @date 2021-09-29
 */
@Service
public class ExternalDataSourceService {
    private final static Logger logger = LoggerFactory.getLogger("ExternalDataSourceService");

    @Autowired
    private ExternalDataSourceMapper extDbTypeMapper;

    public List<ExternalDataSourceDTO> listExternalDataSource(String type) {
        return extDbTypeMapper.listExternalDataSource(type);
    }
}

package org.zjvis.datascience.service.mapper;

import org.springframework.stereotype.Component;
import org.zjvis.datascience.common.formula.dto.FormulaHistoryDTO;

import java.util.List;

/**
 * @description FormulaHistory 数据表Mapper
 * @date 2021-08-20
 */
@Component
public interface FormulaHistoryMapper {
    int addFormula(FormulaHistoryDTO formulaHistoryDTO);

    int deleteFormula(long formulaId);

    int updateFormula(FormulaHistoryDTO formulaHistoryDTO);

    List<FormulaHistoryDTO> queryFormulasByProjectId(long projectId);
}

package org.zjvis.datascience.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.zjvis.datascience.common.dto.OperatorTemplateDTO;
import org.zjvis.datascience.service.mapper.OperatorTemplateMapper;

import java.util.List;

/**
 * @description OperatorTemplateService 算子模板 Service
 * @date 2021-11-23
 */
@Service
public class OperatorTemplateService {

    @Autowired
    private OperatorTemplateMapper operatorTemplateMapper;

    private final static Logger logger = LoggerFactory.getLogger("OperatorTemplateService");

    public OperatorTemplateDTO queryById(Long id) {
        return operatorTemplateMapper.queryById(id);
    }

    public List<OperatorTemplateDTO> queryByStatus(Integer status) {
        return operatorTemplateMapper.queryByStatus(status);
    }

    public boolean update(OperatorTemplateDTO dto) {
        return operatorTemplateMapper.update(dto);
    }

}

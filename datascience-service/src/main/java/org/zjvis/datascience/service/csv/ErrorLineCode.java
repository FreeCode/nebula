package org.zjvis.datascience.service.csv;

/**
 * @description 解析上传失败 代码
 * @date 2021-12-29
 */
public enum ErrorLineCode {

    QuoteWithNoEscape(101, "引号未匹配转义字符"),
    EscapeWithNoQuote(102,"含转义字符句子需用引号包含"),
    ExceedColNum(103, "字段数超过列数"),
    UnderColNum(104, "字段数未达到列数"),
    ContainIllegalChar(105, "含非法字符");

    int code;
    String Message;

    ErrorLineCode(int code, String message) {
        this.code = code;
        this.Message = message;
    }
}

package org.zjvis.datascience.service.mapper;

import org.zjvis.datascience.common.dto.graph.GraphFilterDTO;

import java.util.List;

/**
 * @description GraphFilter 数据表Mapper
 * @date 2021-08-20
 */
public interface GraphFilterMapper {
    boolean save(GraphFilterDTO dto);

    boolean update(GraphFilterDTO dto);

    boolean deleteById(Long id);

    boolean deleteByGraphFilterPipelineId(Long id);

    boolean deleteByGraphId(Long id);

    GraphFilterDTO queryById(Long id);

    List<GraphFilterDTO> queryByGraphFilterPipelineId(Long id);

    List<GraphFilterDTO> queryByParentId(Long id);

    Long queryUserById(Long id);



}

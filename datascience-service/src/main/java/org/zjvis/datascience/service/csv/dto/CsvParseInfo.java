package org.zjvis.datascience.service.csv.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @description 文件解析信息CsvParseInfo
 * @date 2021-12-29
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CsvParseInfo implements Serializable {
    private ErrorInfo errorInfo;

    private String path;
}

package org.zjvis.datascience.service.mapper;

import org.springframework.stereotype.Component;
import org.zjvis.datascience.common.dto.SqlCategoryDTO;
import org.zjvis.datascience.common.vo.SqlCategoryVO;

import java.util.List;

/**
 * @description 数据集SQL查询记录信息表，数据集SQL查询记录 Mapper
 * @date 2021-04-21
 */
@Component
public interface SqlCategoryMapper {

    Long create(SqlCategoryDTO sqlCategoryDTO);

    List<SqlCategoryDTO> queryByUserId(Long userId);

    boolean update(SqlCategoryDTO sqlCategoryDTO);

    boolean delete(Long id, Long userId);

    int deleteAll(Long userId);
}

package org.zjvis.datascience.service.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.zjvis.datascience.common.dto.ExternalDataSourceDTO;

/**
 * @description ExternalDataSourceDTO 数据表Mapper
 * @date 2021-08-20
 */
@Mapper
public interface ExternalDataSourceMapper {

    List<String> listUsableDbTypeName();

    List<ExternalDataSourceDTO> listExternalDataSource(@Param("type") String type);
}

package org.zjvis.datascience.service;

import java.util.List;
//import jnr.ffi.annotations.LongLong;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.zjvis.datascience.common.dto.TaskSnapshotDTO;
import org.zjvis.datascience.service.mapper.TaskSnapshotMapper;

/**
 * @description TaskSnapshot  任务节点快照服务 Service
 * @date 2021-12-29
 */
@Service
public class TaskSnapshotService {
    @Autowired
    TaskSnapshotMapper taskSnapshotMapper;

    public void batchAdd(List<TaskSnapshotDTO> taskSnapshots) {
        taskSnapshotMapper.batchAdd(taskSnapshots);
    }
    public boolean deleteByPipelineSnapshotId(List<Long> pipelineSnapshotId) {
        int res = taskSnapshotMapper.deleteByPipelineSnapshotId(pipelineSnapshotId);
        return res>0?true:false;
    }

    public List<TaskSnapshotDTO> queryByPipelineAndSnapshotId(Long pipelineId, Long pipelineSnapshotId) {
        return taskSnapshotMapper.queryByPipelineAndSnapshotId(pipelineId, pipelineSnapshotId);
    }
}

package org.zjvis.datascience.service.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.zjvis.datascience.common.dto.DatasetActionDTO;
import org.zjvis.datascience.common.dto.DatasetActionUnreadDTO;

import java.util.List;

/**
 * @description 数据集操作信息表，数据集操作Mapper
 * @date 2021-04-20
 */
@Mapper
public interface DatasetActionMapper {

    List<DatasetActionDTO> selectAllAction(Long userId);

    void insertAction(DatasetActionDTO datasetActionDTO);

    List<DatasetActionDTO> selectActionByDtaId(Long userId, Long datasetId, Long daydiff);

    List<DatasetActionUnreadDTO> selectUnread(Long userId);

    void updateUnread(Long userId, Long datasetId);
}

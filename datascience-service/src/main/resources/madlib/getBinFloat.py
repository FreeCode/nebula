CREATE OR REPLACE FUNCTION "pipeline"."getBinFloat"("colValue" float8, "rangeList" text)
  RETURNS "pg_catalog"."float8" AS $BODY$
    import json
    def begin(colValue, rangeList):
        try:
            _list = json.loads(rangeList)
            for item in _list:
                if colValue >= item[0] and colValue < item[1]:
                    return item[0]
        except Exception as e:
            return -1.0
        return -1.0
    if __name__ == "__main__":
        return begin(colValue, rangeList)
$BODY$
  LANGUAGE plpythonu VOLATILE
  COST 100
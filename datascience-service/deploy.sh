set -x
set -e

PSQL=/usr/local/greenplum-db/bin/psql
DBNAME=aiworks
USER=gpadmin
PORT=5432
PASSWORD=123456 #密码需要修改
HOST=10.105.20.43
basePath=./src/main/resources/madlib/

for item in `ls $basePath`
do
  cmd="$PSQL -h $HOST -d $DBNAME -p $PORT -U $USER -w $PASSWORD -f '$basePath/$item'"
  echo $cmd
  eval $cmd
done

mkdir -p ./tmpfolder

cp $basePath/getBinInt2.py ./tmpfolder
cp $basePath/getBinInt4.py ./tmpfolder
cp $basePath/getBinInt8.py ./tmpfolder

cp $basePath/getBinFloat.py ./tmpfolder

cat tmpfolder/getBinInt2.py | sed 's/int4/int2/' > tmpfolder/int2.py

cmd="$PSQL -h $HOST -d $DBNAME -p $PORT -U $USER -w $PASSWORD -f tmpfolder/int2.py"
echo $cmd
eval $cmd

cat tmpfolder/getBinInt8.py | sed 's/int4/int8/' > tmpfolder/int8.py
cmd="$PSQL -h $HOST -d $DBNAME -p $PORT -U $USER -w $PASSWORD -f tmpfolder/int8.py"
echo $cmd
eval $cmd

cat tmpfolder/getBinFloat.py | sed 's/float8/float4/' > tmpfolder/float4.py
cmd="$PSQL -h $HOST -d $DBNAME -p $PORT -U $USER -w $PASSWORD -f tmpfolder/float4.py"
echo $cmd
eval $cmd

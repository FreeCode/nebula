import unittest
from app import app
import json

class TestApis(unittest.TestCase):

    def setUp(self):
        self.app = app
        app.config['TESTING'] = True
        self.client = app.test_client()

    def test_smoothing_success(self):
        input = {'col': 'x', 'sg_window_size': '3', 'method': 'ma', 'ma_window_size': '3', 'sg_rank': '1', 'source': 't_1594_1630568470309', 'ema_decay': '0.3', 'taskId': '34706', 'target': 'pipeline.smoothing_34706_1635729505106'}
        response = self.client.post("/smoothing", data=input)

        resp_json = response.data
        resp_dict = json.loads(resp_json)

        self.assertIn("status", resp_dict)

        status = resp_dict.get("status")
        self.assertEqual(status, "SUCCESS")


if __name__ == '__main__':
    unittest.main()

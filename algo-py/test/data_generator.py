import numpy as np
import pandas as pd

def get_sin(num=100):
	t = np.linspace(start=-4, stop=4, num=num)
	y = np.sin(t) + np.random.randn(len(t)) * 0.1

	d = {"t":t,"data":y}
	df = pd.DataFrame(data=d)
	return df

def get_distrib(distribution="normal",num=100):
	if distribution == "normal":
		data = np.random.normal(loc=5, scale=1, size=num)
	elif distribution == "gamma":
		data = np.random.gamma(shape=3, scale=1, size=num)
	elif distribution == "expo":
		data = np.random.exponential(scale=4, size=num)
	t = np.range(num=num)

	d = {"t":t,"data":data}
	df = pd.DataFrame(data=d)
	return df
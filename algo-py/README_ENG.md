# <center> algo-py
<center>Python web server and machine learning algorithms implemented for the main project 'aiworks'</center>

-----
## 1.How to deploy algo-py Web Server

### 1.1 Prerequisites:

- python3+


### 1.2 Clone or download folder <u>algo-py</u> in project aiworks


### 1.3 Install python package requirements

run the following line in your shell
```commandline 
pip install -r requirements.txt
```

### 1.4 Modify the configuration file

Write the configurations of your database MySQL, GreenPlum, and MinIO to
<a href="common/config/config">config</a>

Modify the absolute path of file 'config' in 
 <a href="common/config/config.py">config.py</a>


### 1.5 To start the flask web server:

change directory(cd) to directory 'algo-py' and run the following line

```bash
python3 app.py
```
(Message 'Server running at http://0.0.0.0:5001' indicates that the server is successfully initiated)



### 1.6 To stop the flask web server:

```commandline
sh stop.sh
```
-----
## 2. How to setup JupyterLab

### 2.0 Prerequisites
- Perform **1.1**, **1.2**, **1.3** （Ignore if already done）
- docker daemon process

### 2.1 Build docker image

- Change directory to where <a href="common/Dockerfile">Dockerfile</a> locates.
     ```commandline
     cd aiworks-py/common/
     ```
- Build Docker image named 'jlab_r", version 'v8'.
     ```commandline
     docker build -t jlab_r:v8 .
     ```

### 2.2 Setup configurations
- Modify configurations in <a href="common/config/config">config</a>：
  - NOTEBOOKDIR：the absolute path to store users' jupyter notebooks(working spaces)
  - HOMEDIR：absolute path of directory'algo-py/common' on your server
  - PYDIR：absolute path of 'python' on your server
  - DOCKER_IMG_VERSION：name and version of the docker image (default=jlab_r:v8)
- Optionals：
  - DOCKER_MEM: max memory capacity of a docker container (default=1500M)
  - DOCKER_CPU: max number of cpu a docker container could use (default=2)

### 2.3* Modify configuration files in the main project 'aiworks'

-----
## 3. How to register UDFs on Greenplum
Some functions on the Nebula Platform rely on UDFs(User Defined Functions) registerd on Greenplum database. Follow the steps below to register official UDFs:
- Modify the database configuration in <a href="../datascience-service/deploy.sh">deploy.sh</a>
- Run script 'deploy.sh'
```commandline
sh deploy.sh
```
*Please add the newly customized UDFs to directory <a href="../datascience-service/src/main/resources/madlib/">madlib</a>. 
You need to install the python library if they are missing in the server where the Greenplum installed. First you need to install pip, then install the library list in requirements_udf.txt
```
yum install epel-release
yum install python-pip
pip install -r requirements_udf.txt
```

## 4. System requirements for auto-sklearn
Auto-sklearn has the following system requirements:
- Linux operating system (for example Ubuntu)
- Python (>=3.7),
- C++ compiler (with C++11 supports).
```
yum install gcc-c++
```
In case you try to install Auto-sklearn on a system where no wheel files for the pyrfr package are provided (see here for available wheels) you also need: SWIG
```
yum install swig3
```
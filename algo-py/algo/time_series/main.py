# encoding:utf-8
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pylab as mpl
import matplotlib
from matplotlib.font_manager import FontProperties
import seaborn as sns

if __name__ == "__main__":
    df1 = pd.read_csv("../data/econ/table1.csv", encoding='gbk')
    df2 = pd.read_csv("../data/econ/table2.csv", encoding='gbk')

    print(df2.head())
    print(df1.head())
    columns = df2.columns
    print(columns)

    # for i in range(len(columns)):
    #     plt.plot(df2[columns[i]])
    #     plt.title(columns[i])
    #     plt.show()

    corr = df1.corr()

    plt.figure(figsize=(12, 10))
    ax = sns.heatmap(corr, vmin=-1, vmax=1, center=0, cmap=sns.diverging_palette(20, 220, n=200))
    ax.set_xticklabels(ax.get_xticklabels(), rotation=45, horizontalalignment='right')
    plt.title('Correlation Matrix')
    plt.show()

    print(columns[-18])

====================
NBTK(Nebula Toolkit)
====================

Nebula Toolkit provides services for developers and contributors of the Nebula platform. The toolkit includes defining
parameters and metrics of a model, generating model configuration file.

Installing
----------
Install nbtk(Nebula Toolkit) from PyPI via ``pip install nbtk``

Contributing
------------
We happily welcome contributions to Nebula Toolkit. Please see our
`contribution guide <CONTRIBUTING.rst>`_ to learn more about contributing to Nebula Toolkit.
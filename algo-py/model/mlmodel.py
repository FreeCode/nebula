from model.db import db

class Model(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column()
    project_id = db.Column()
    algorithm = db.Column()
    model_type = db.Column()
    name = db.Column()
    model_desc = db.Column()
    param = db.Column()
    source_table = db.Column()
    source_id = db.Column()
    source_name = db.Column()
    model_saved_path = db.Column()
    other_info = db.Column()
    status = db.Column()
    in_panel = db.Column()
    folder_id = db.Column()
    algotime = db.Column()
    sparktime = db.Column()
    runtime = db.Column()
    num = db.Column()
    progress_id = db.Column()
    gmt_creator = db.Column()
    gmt_create = db.Column()
    gmt_modifier = db.Column()
    gmt_modify = db.Column()
    invisible = db.Column()

    def __init__(self, name, project_id):
        self.name = name
        self.project_id = project_id
$title  之江实验室 开放经济的CGE模型+新古典主义闭合

*定义集合所有账户 ac和生产活动a
set ac       /com,sec,lab,cap,hh,ent,lbt,cbt,ysw,lgov,cgov,row,prow,insav,total,tartot/;
set x(ac)  /com,sec,lab,cap,hh,ent,lbt,cbt,ysw,lgov,cgov,row,prow,insav/;
set a(ac)    /sec/;
set c(ac)    /com/;
set f(ac)    /lab,cap/;
set tax(ac)  /lbt,cbt,ysw,lgov,cgov/; set acnt(ac)  除了总数之外的所有账户;
acnt(ac)=YES;
acnt('total')=NO;

alias (ac,acp),(x,xp),(a,ap),(c,cp),(f,fp),(tax,taxp),(acnt,acntp);

*注意下面对大型SAM表的写法。如果表上列账户太多，可以将右边的列的数据移到下面，
*重复每行的抬头，另外在列的抬头左边，加上一个"+"号。

table SAM(*,*) the social accounting matrix
        com         sec         lab          cap         hh           ent         lbt
com                 1004431456                           159158143.8
sec     893976378.2
lab                 249448227
cap                 201139638.7
hh                              249448227    24579027.3               5796119.00
ent                                          180797602.6
lbt                 -483400
cbt                 -18203414.1
ysw                 25782035.17
lgov                20315857.57                          2813166.391  6570138.63
cgov    7751257.626 41855537.86                          5949999.164  11808838.36
row     72196101.26                          -4236991.19
prow    605332133
insav                                                    120871962.4  156622506.6
total   1579255870  1524285939  249448227    201139638.7 288793271.8  180797602.6 0


        + cbt       ysw         lgov         cgov        row          prow        insav       total
com                 6528228.172 73692565.09  19686965.34                          315758510.9 1579255870.1
sec                                                      215041903.7  415267656.9             1524285939
lab                                                                                           249448227
cap                                                                                           201139638.7
hh                              8969898.481                                                   288793271.8
ent                                                                                           180797602.6
lbt                             483400                                                        0
cbt                                          18203414.17                                      0
ysw                                                                                           25782035.17
lgov                                         32116906.29                                      61816068.89
cgov                            1668770.519                                                   69034403.54
row                                                                                           67959110.06
prow                                                                                          605332133
insav               19253807.00 -22998565.20 -972882.273 -147082793.7 190064476.0 139966968.5 455725479.5
total               25782035.17 61816068.891 69034403.54 67959110.06  605332133   455725479.5

table sax(a,c)
   com
sec  1
;


*生产函数参数
parameter  rhoAa(a)     /sec =   0.2/
           rhoVA(a)     /sec =   0.3/
           rhoCETEX(a)  /sec =   2.0/
           rhoEX(a)     /sec =   2.0/
           rhoCETIM(c)  /com =   0.4/
           rhoIM(c)     /com =   0.4/;
*定义参数
parameters

YENT0                  企业收入
WK0                    资本价格
QKS0                   资本数量
shifentk               资本收入分配给企业的份额
tiEnt                  企业所得税税率
transfrhent0           企业对居民的转移收入(私人保险公司支付保险金等)
ENTSAV0                企业储蓄
PQ0(c)                 省内市场商品c的价格
EINV0                  投资总额
QINV0(c)               对商品c的投资的最终需求
WL0                    劳动价格
QLD0(a)                劳动需求
QKD0(a)                资本需求
scaleAVA(a)            VA的CES函数参数
deltaVA(a)             VA的CES函数劳动份额参数
QINT0(c,a)             中间投入量
QINTA0(a)              中间投入总量
ica(c,a)               中间投入的投入产出系数
PA0(a)                 生产活动a的价格
PVA0(a)                增值部分(含增值税)汇总价格
QVA0(a)                增值部分汇总量
PINTA0(a)              中间投入总价格
scaleAa(a)             QA的CES函数参数
deltaAa(a)             QA的CES函数份额参数
LGTAX(a)               地方政府税收
CGTAX(a)               中央政府税收
transfrentgov0         政府对企业生产补贴
YSW(a)                 预算外收入
tvap0(a)               净值税率
tvap0C(a)
tvapp0(a)
QA0(a)                 生产活动a的数量
QAA0(a)
sax(a,c)               生产活动到生产商品的对应关系
QDA0(a)                省内生产活动的数量
QDC0(c)                省内销售活动的数量
PDC0(c)                 省内销售活动的价格
QE0(a)                 省际与国外使用商品c的数量
PDA0(a)                省内生产活动的价格
PE0(a)                 省际与国外使用商品c的价格
scaleCETEX(a)          省内与出口的Arminton函数参数
deltaCETEX(a)          省内与出口的Arminton份额参数
QEF0(a)                国外使用商品c的数量
IEFV0(a)               出口额

QEP0(a)                省际使用商品c的数量
IEPV0(a)               省际出口额

PEF0(a)                国外使用商品c的价格
PEP0(a)                省际使用商品c的价格
scaleEX(a)             省际与出口间的Arminton函数参数
deltaEX(a)             省际与出口间的Arminton份额参数
EXR0                   汇率
pwe(a)                 出口生产活动a商品的国际价格
PM0(c)                 进口商品c的价格
QQ0(c)                 省内市场商品c的数量
QM0(c)                 进口商品c的数量
scaleCETIM(c)          省内与进口的Arminton函数参数
deltaCETIM(c)          省内与进口的Arminton份额参数
scaleIM(c)             省际与进口间的Arminton函数参数
deltaIM(c)             省际与进口间的Arminton份额参数
QMF0(c)                进口国外商品c的数量
IMFV0(c)               进口额

QMP0(c)                进口省际商品c的数量
IMPV0(C)               省际进口额

PMF0(c)                进口国外商品c的价格
PMP0(c)                进口省际商品c的价格
tm0(c)                 进口税率
pwm(c)                 出口生产活动a商品的国际价格
QLS0                   劳动量总供应
QKS0                   资本总供应
shifhk                 资本收入分配给居民的份额
transfrhgov0           政府对居民的转移收入
transfrent0            企业对居民的转移
YH0                    居民收入
EH0                    居民消费
QH0(c)                 居民对商品c的需求
shrh(c)                居民收入对商品c的消费支出份额
mpc                    居民的边际消费倾向(这里也是平均消费倾向)
tih0                   居民的所得税税率
YG0                    政府收入
VYH0                   可支配收入
transfrgovn0           中央政府对地方政府的净转移支付
QG0(c)                 政府对商品c的需求
GSAV0                  政府储蓄
FSAV0                  国外储蓄
PSAV0                  省际储蓄
transfrslgov0          地方政府对劳动的补贴
Transfrscgov0          中央政府对劳动的补贴
EG0                    政府支出
EG00
ROWCAP0                国外投资收益
VBIS0                  虚拟变量
GDP0                   实际国民生产总值
PGDP0                  国民生产总值价格指数
VGDP0
;
parameter
popu;
popu=4957.63;

*参数(包括外生变量)赋值与校调

WK0=1;
PQ0(c)=1;
WL0=1;
PVA0(a)=1;
PA0(a)=1;
PDA0(a)=1;
PE0(a)=1;
pwe(a)=1;
PEP0(a)=1;
EXR0=1;
PM0(c)=1;
PMF0(c)=1;
PWM(C)=1;
PMP0(c)=1;

QKS0=sam('total','cap')/WK0;
shifentk=sam('ent','cap')/WK0/QKS0;
YENT0=shifentk*WK0*QKS0;
tiEnt=(sam('lgov','ent')+sam('cgov','ent'))/YENT0;
transfrhent0=sam('hh','ent');
ENTSAV0=(1-tiEnt)*YENT0-transfrhent0;
QINV0(c)=sam(c,'insav')/PQ0(c);
EINV0=sum(c,PQ0(c)*QINV0(c));

QLD0(a)=sam('lab',a)/WL0;
QKD0(a)=sam('cap',a)/WK0;
deltaVA(a)=WK0*QLD0(a)**(1-rhoVA(a))/(WL0*QLD0(a)**(1-rhoVA(a))+WK0*QKD0(a)**(1-rhoVA(a)));
QVA0(a)=(WL0*QLD0(a)+WK0*QKD0(a))/PVA0(a);
scaleAVA(a)=QVA0(a)/(deltaVA(a)*QLD0(a)**rhoVA(a)+(1-deltaVA(a))*QKD0(a)**rhoVA(a))**(1/rhoVA(a));

QINT0(c,a)=sam(c,a)/PQ0(c);
QINTA0(a)=sum(c,QINT0(c,a));
ica(c,a)=QINT0(c,a)/QINTA0(a);
PINTA0(a)=sum(c,ica(c,a)*PQ0(c));

QA0(a)=sam('total',a)/PA0(a);
deltaAa(a)=PVA0(a)*QVA0(a)**(1-rhoAa(a))/(PVA0(a)*QVA0(a)**(1-rhoAa(a))+PINTA0(a)*QINTA0(a)**(1-rhoAa(a)));
scaleAa(a)=QA0(a)/(deltaAa(a)*QVA0(a)**rhoAa(a)+(1-deltaAa(a))*QINTA0(a)**rhoAa(a))**(1/rhoAa(a));

LGTAX(a)=sam('lgov','sec');
CGTAX(a)=sam('cgov','sec');
transfrentgov0=-sam('lbt','sec')-sam('cbt','sec');
YSW(a)=sam('ysw','sec');
tvap0(a)=(LGTAX(a)+CGTAX(a)+YSW(a)-transfrentgov0)/(WL0*QLD0(a)+WK0*QKD0(a)+PINTA0(a)*QINTA0(a));
tvap0C(a)=sam('total',a)/(WL0*QLD0(a)+WK0*QKD0(a)+PINTA0(a)*QINTA0(a))-1;
tvapp0(a)=(LGTAX(a)+CGTAX(a)+YSW(a))/(WL0*QLD0(a)+WK0*QKD0(a)+PINTA0(a)*QINTA0(a));
QAA0(a)=(1+TVAP0(A))*(WL0*QLD0(a)+WK0*QKD0(a)+PINTA0(a)*QINTA0(a))/PA0(a);

PEF0(a)=pwe(a)*EXR0;
QEF0(a)=sam('sec','row')/PEF0(a);
QEP0(a)=sam('sec','prow')/PEP0(a);
QE0(a)=(sam('sec','row')+sam('sec','prow'))/PE0(a);
deltaEX(a)=PEF0(a)*QEF0(a)**(1-rhoEX(a))/(PEF0(a)*QEF0(a)**(1-rhoEX(a))+PEP0(a)*QEP0(a)**(1-rhoEX(a)));
scaleEX(a)=QE0(a)/(deltaEX(a)*QEF0(a)**rhoEX(a)+(1-deltaEX(a))*QEP0(a)**rhoEX(a))**(1/rhoEX(a));

QDA0(a)=sam('sec','com')/PDA0(a);
deltaCETEX(a)=PDA0(a)*QDA0(a)**(1-rhoCETEX(a))/(PDA0(a)*QDA0(a)**(1-rhoCETEX(a))+PE0(a)*QE0(a)**(1-rhoCETEX(a)));
scaleCETEX(a)=QA0(a)/(deltaCETEX(a)*QDA0(a)**rhoCETEX(a)+(1-deltaCETEX(a))*QE0(a)**rhoCETEX(a))**(1/rhoCETEX(a));

tm0(c)$sam('row',c)=sam('cgov','com')/sam('row','com');
PMF0(C)=(1+TM0(C))*PWM(C)*EXR0;
QMF0(c)=(sam('row',c)+sam('cgov',c))/PMF0(c);
QMP0(c)=sam('prow',c)/PMP0(c);
QM0(c)=(PMF0(c)*QMF0(c)+PMP0(c)*QMP0(c))/PM0(c);
deltaIM(c)=PMF0(c)*QMF0(c)**(1-rhoIM(c))/(PMF0(c)*QMF0(c)**(1-rhoIM(c))+PMP0(c)*QMP0(c)**(1-rhoIM(c)));
scaleIM(c)=QM0(c)/(deltaIM(c)*QMF0(c)**rhoIM(c)+(1-deltaIM(c))*QMP0(c)**rhoIM(c))**(1/rhoIM(c));

QDC0(c)=sum(a,sax(a,c)*QDA0(a));
PDC0(c)=sum(a,sax(a,c)*PDA0(a));
QQ0(c)=(PDC0(c)*QDC0(c)+PM0(c)*QM0(c))/PQ0(c);
deltaCETIM(c)=PDC0(c)*QDC0(c)**(1-rhoCETIM(c))/(PDC0(c)*QDC0(c)**(1-rhoCETIM(c))+PM0(c)*QM0(c)**(1-rhoCETIM(c)));
scaleCETIM(c)=QQ0(c)/(deltaCETIM(c)*QDC0(c)**rhoCETIM(c)+(1-deltaCETIM(c))*QM0(c)**rhoCETIM(c))**(1/rhoCETIM(c));

QLS0=sam('total','lab')/WL0;
QKS0=sam('total','cap')/WK0;
shifhk=(sam('hh','cap')/WK0)/QKS0;
transfrhgov0=sam('hh','lgov');
transfrhent0=sam('hh','ent');
YH0=WL0*QLS0+shifhk*WK0*QKS0+transfrhgov0+transfrhent0;
QH0(c)=sam(c,'hh')/PQ0(c);
EH0=sum(c,QH0(c)*PQ0(c));
tih0=(sam('lgov','hh')+sam('cgov','hh'))/YH0;
mpc=sum(c,sam(c,'hh'))/((1-tih0)*YH0);
shrh(c)=(QH0(c)*PQ0(c))/(mpc*(1-tih0)*YH0);
VYH0=(1-tih0)*YH0;

YG0=sam('lgov','SEC')+sam('cgov','SEC')+sam('YSW','SEC')+tih0*YH0+tient*YENT0+sum(c,tm0(c)*pwm(c)*QMF0(c)*EXR0);
QG0(c)=(sam('com','cgov')+sam('com','lgov')+sam('com','ysw'))/PQ0(c);
GSAV0=sam('insav','cgov')+sam('insav','lgov')+sam('insav','YSW');
EG0=SUM(C,PQ0(C)*QG0(C))+SAM('LBT','LGOV')+SAM('CBT','CGOV')+SAM('HH','LGOV');
transfrslgov0=sam('lbt','lgov');
transfrscgov0=sam('cbt','cgov');
ROWCAP0=sam('row','cap');

GSAV0=YG0-EG0;
FSAV0=sam('insav','row');
PSAV0=sam('insav','prow');
VBIS0=0;

GDP0=sum(c,QH0(c)+QINV0(c)+QG0(c))+sum(a,QEF0(a)+QEP0(a))-sum(c,QMF0(c)+QMP0(c));
PGDP0=(sum(c,PQ0(c)*(QH0(c)+QINV0(c)+QG0(c)))+sum(a,PEF0(a)*QEF0(a))+sum(a,PEP0(a)*QEP0(a))-sum(c,PMF0(c)*QMF0(c))-sum(c,PMP0(c)*QMP0(c))+sum(c,tm0(c)*pwm(c)*EXR0*QMF0(c)))/GDP0;
VGDP0=GDP0*PGDP0;

IMFV0(c)=QMF0(c)*PMF0(c);
IEFV0(A)=QEF0(A)*PEF0(A);
IMPV0(C)=QMP0(C)*PMP0(C);
IEPV0(A)=PEP0(A)*QEP0(A);
display IMFV0,IEFV0,IMPV0,IEPV0,tvap0,tvap0C,YH0,TIH0,QLD0,QKD0,tvapp0,pwm,QMF0,QEF0,FSAV0,QA0,QAA0,EG0,EG0,QG0,YG0,transfrentgov0,tvap0,scaleAVA,deltaAa,qinta0,pinta0,YENT0,QKS0,SHIFENTK,tiEnt,ENTSAV0,transfrhENT0,transfrhgov0,QE0,QH0,QINV0,QG0,QM0,GDP0,EINV0,VYH0,EH0;

*65个变量
variable
YENT,WK,ENTSAV,EINV,WL,QINV(c),QLD(a),QKD(a),PVA(a),QVA(a),PINTA(a),tvap(a),QA(a),QDA(a),QE(a),
PE(a),PEF(a),QEF(a),PEP(a),QEP(a),EXR,PM(c),QQ(c),QM(c),QMF(c),QMP(c),PMF(c),PMP(c),QLS,QKS,
YH,VYH,EH,QH(c),YG,QG(c),GSAV,FSAV,PSAV,transfrhgov,transfrslgov,transfrscgov,EG,ROWCAP,VBIS,GDP,PGDP,
PQ(c),QINTA(a),PA(a),QINT(c,a),QDC(c),PDC(c),PDA(a),tih,tm(c),transfrentgovv,IMFV(c),IEFV(a),IMPV(c),IEPV(a),
VGDP,VMP,VMF,VEP,VEF;

*下面对等式定义，53个方程。为了节省空间,用逗号分开equation,以压缩空间。
*劳动和要素供应量等于禀赋的等式,由后面赋值固定的指令QLS.fx=QLS0和QKS.fx=QKS0 完成。
equation
YENTeq,ENTSAVeq,EINVeq,QAfn(a),QAFOCeq(a),PAeq(a),transfrentgoveq,tvapeq(a),QVAfn(a),QVAFOC(a),PVAeq(a),QINTfn(c,a),PINTAeq(a),
QDCqxeq(c),PDCpaeq(c),CETEXfn(a),CETEX(a),PCETEXeq(a),EXfn(a),EX(a),PEXeq(a),PEeq(a),CETIMfn(c),CETIM(c),PCETIMeq(c),
IMfn(c),IM(c),PIMeq(c),PMeq(c),YHeq,VYHeq,QHeq(c),EHeq,YGeq,EGeq,GSAVeq,ComEqui(c),Leq,Keq,FEFXeq,FEPXeq,ISeq,GDPeq,PGDPeq,IMFVEQ(c),IEFVEQ(a),IMPVEQ(c),IEPVEQ(a),
VGDPEQ,VMPEQ,VMFEQ,VEPEQ,VEFEQ;

YENTeq..
YENT=e=shifentk*WK*QKS;

ENTSAVeq..
ENTSAV=e=(1-tient)*YENT-transfrhent0;

EINVeq..
EINV=e=sum(c,PQ(c)*QINV(c));

QAfn(a)..
QA(a)=e=scaleAa(a)*(deltaAa(a)*QVA(a)**rhoAa(a)+(1-deltaAa(a))*QINTA(a)**rhoAa(a))**(1/rhoAa(a));

QAFOCeq(a)..
PVA(a)/PINTA(a)=e=(deltaAa(a)/(1-deltaAa(a)))*(QINTA(a)/QVA(a))**(1-rhoAa(a));

PAeq(a)..
PA(a)*QA(a)=e=(1+tvap(a))*(PVA(a)*QVA(a)+PINTA(a)*QINTA(a));

transfrentgoveq..
transfrentgovv=e=transfrslgov+transfrscgov;

tvapeq(a)..
tvap(a)=e=(LGTAX(a)+CGTAX(a)+YSW(a)-transfrentgovv)/(WL*QLD(a)+WK*QKD(a)+PINTA(a)*QINTA(a));

QVAfn(a)..
QVA(a)=e=scaleAVA(a)*(deltaVA(a)*QLD(a)**rhoVA(a)+(1-deltaVA(a))*QKD(a)**rhoVA(a))**(1/rhoVA(a));

QVAFOC(a)..
WL/WK=e=(deltaVA(a)/(1-deltaVA(a)))*(QKD(a)/QLD(a))**(1-rhoVA(a));

PVAeq(a)..
PVA(a)*QVA(a)=e=WL*QLD(a)+WK*QKD(a);

QINTfn(c,a)..
QINT(c,a)=e=ica(c,a)*QINTA(a);

PINTAeq(a)..
PINTA(a)=e=SUM(c,ica(c,a)*PQ(c));

*下面两个函数是从生产活动a到商品c的映射关系
QDCqxeq(c)..
QDC(c)=e=sum(a,sax(a,c)*QDA(a));

PDCpaeq(c)..
PDC(c)=e=sum(a,sax(a,c)*PDA(a));

CETEXfn(a)..
QA(a)=e=scaleCETEX(a)*(deltaCETEX(a)*QDA(a)**rhoCETEX(a)+(1-deltaCETEX(a))*QE(a)**rhoCETEX(a))**(1/rhoCETEX(a));

CETEX(a)..
PDA(a)/PE(a)=e=(deltaCETEX(a)/(1-deltaCETEX(a)))*(QE(a)/QDA(a))**(1-rhoCETEX(a));

PCETEXeq(a)..
PA(a)*QA(a)=e=PDA(a)*QDA(a)+PE(a)*QE(a);

EXfn(a)..
QE(a)=e=scaleEX(a)*(deltaEX(a)*QEF(a)**rhoEX(a)+(1-deltaEX(a))*QEP(a)**rhoEX(a))**(1/rhoEX(a));

EX(a)..
PEF(a)/PEP(a)=e=(deltaEX(a)/(1-deltaEX(a)))*(QEP(a)/QEF(a))**(1-rhoEX(a));

PEXeq(a)..
PE(a)*QE(a)=e=PEF(a)*QEF(a)+PEP(a)*QEP(a);

PEeq(a)..
PEF(a)=e=pwe(a)*EXR;

CETIMfn(c)..
QQ(c)=e=scaleCETIM(c)*(deltaCETIM(c)*QDC(c)**rhoCETIM(c)+(1-deltaCETIM(c))*QM(c)**rhoCETIM(c))**(1/rhoCETIM(c));

CETIM(c)..
PDC(c)/PM(c)=e=(deltaCETIM(c)/(1-deltaCETIM(c)))*(QM(c)/QDC(c))**(1-rhoCETIM(c));

PCETIMeq(c)..
PQ(c)*QQ(c)=e=PDC(c)*QDC(c)+PM(c)*QM(c);

IMfn(c)..
QM(c)=e=scaleIM(c)*(deltaIM(c)*QMF(c)**rhoIM(c)+(1-deltaIM(c))*QMP(c)**rhoIM(c))**(1/rhoIM(c));

IM(c)..
PMF(c)/PMP(c)=e=(deltaIM(c)/(1-deltaIM(c)))*(QMP(c)/QMF(c))**(1-rhoIM(c));

PIMeq(c)..
PM(c)*QM(c)=e=PMF(c)*QMF(c)+PMP(c)*QMP(c);

PMeq(c)..
PMF(c)=e=(1+tm(c))*pwm(c)*EXR;

YHeq..
YH=e=WL*QLS+shifhk*WK*QKS+transfrhent0+transfrhgov;

QHeq(c)..
PQ(c)*QH(c)=e=shrh(c)*mpc*(1-tih)*YH;

EHeq..
EH=e=sum(c,PQ(c)*QH(c));

VYHeq..
VYH=e=(1-tih)*YH;

YGeq..
YG=e=sum(a,tvapp0(a)*(WL*QLD(a)+WK*QKD(a)+PINTA(a)*QINTA(a)))+tih*YH+tient*YENT+sum(c,tm(c)*pwm(c)*QMF(c)*EXR);

EGeq..
EG=e=sum(c,PQ(c)*QG(c))+transfrhgov+transfrslgov+transfrscgov;

GSAVeq..
GSAV=e=YG-EG;

ComEqui(c)..
QQ(c)=e=sum(a,QINT(c,a))+QH(c)+QINV(c)+QG(c);

Leq..
Sum(a,QLD(a))=e=QLS;

Keq..
Sum(a,QKD(a))=e=QKS;

FEFXeq..
ROWCAP*EXR+sum(c,pwm(c)*QMF(c)*EXR)=e=sum(a,pwe(a)*QEF(a)*EXR)+FSAV*EXR;

FEPXeq..
sum(c,PMP(c)*QMP(c))=e=sum(a,PEP(a)*QEP(a))+PSAV;

ISeq..
EINV=e=(1-mpc)*(1-tih)*YH+ENTSAV+GSAV+PSAV+EXR*FSAV+VBIS;

GDPeq..
GDP=e=sum(c,QH(c)+QINV(c)+QG(c))+sum(a,QEF(a)+QEP(a))-sum(c,QMF(c)+QMP(c));

PGDPeq..
PGDP*GDP=e=sum(c,PQ(c)*(QH(c)+QINV(c)+QG(c)))+sum(a,PEF(a)*QEF(a))+sum(a,PEP(a)*QEP(a))-sum(c,PMF(c)*QMF(c))-sum(c,PMP(c)*QMP(c))+sum(c,tm(c)*pwm(c)*EXR*QMF(c));

IMFVEQ(c)..
IMFV(c)=e=QMF(c)*PMF(c);

IEFVEQ(a)..
IEFV(a)=e=QEF(a)*PEF(a);

IMPVEQ(c)..
IMPV(c)=e=QMP(c)*PMP(c);

IEPVEQ(a)..
IEPV(a)=e=PEP(a)*QEP(a);
VGDPEQ..
VGDP=E=PGDP*GDP;

VMPEQ..
VMP=E=sum(c,IMPV(c));

VEPEQ..
VEP=E=sum(a,IEPV(a));

VMFEQ..
VMF=E=sum(c,IMFV(c));

VEFEQ..
VEF =E=sum(a,IEFV(a));

*赋予变量的初始值


EINV.L=EINV0;
QINV.L(c)=QINV0(c);
QLD.L(a)=QLD0(a);
QKD.L(a)=QKD0(a);

QDA.L(a)=QDA0(a);
QE.L(a)=QE0(a);

QEF.L(a)=QEF0(a);
QEP.L(a)=QEP0(a);
QQ.L(c)=QQ0(c);
QM.L(c)=QM0(c);

QMP.L(c)=QMP0(c);




QH.L(c)=QH0(c);

YG.l=YG0;
GDP.L=GDP0;
PGDP.L=PGDP0;
QINT.L(c,a)=QINT0(c,a);
QDC.L(c)=QDC0(c);
PDC.L(c)=PDC0(c);
ENTSAV.L=ENTSAV0;
ROWCAP.L=ROWCAP0;
transfrentgovv.L=transfrentgov0;
PSAV.L=PSAV0;
PM.l(c)=PM0(c);

IMFV.L(C)=IMFV0(C);
IEFV.L(A)=IEFV0(A);
IMPV.L(C)=IMPV0(C);
IEPV.L(A)=IEPV0(A);
PE.l(a)=PE0(a);

QLS.l=QLS0;
YH.l=YH0;

QKS.l=QKS0;
QINTA.l(a)=QINTA0(a);
QVA.l(a)=QVA0(a);
GSAV.l=GSAV0;

tvap.l(a)=tvap0(a);
PINTA.l(a)=PINTA0(a);
PVA.l(a)=PVA0(a);

PQ.l(c)=PQ0(c);
QA.l(a)=QA0(a);
PA.l(a)=PA0(a);
EH.l=EH0;
EXR.l=EXR0;
PMF.l(c)=PMF0(c);
VYH.l=VYH0;
QMF.l(c)=QMF0(c);

tm.L(c)=tm0(c);
transfrslgov.L=transfrslgov0;
*下面对几个变量赋值固定,使它们变成参数或外生变量。

WL.FX=WL0;
FSAV.fx=FSAV0;

PDA.fx(a)=PDA0(a);
PEP.fx(a)=PEP0(a);
PMP.fx(c)=PMP0(c);
transfrhgov.fx=transfrhgov0;

transfrscgov.fx=transfrscgov0;
VBIS.FX=VBIS0;

EG.fx=EG0;
QG.fx(c)=QG0(c);

tih.fx=tih0;
VGDP.FX=VGDP0;
WK.fx=WK0;


model cge  /all/;
solve cge using mcp;
Parameter
AVEVYH 变化前人均可支配收入(单位：元)
*以下均为单位换算定义参数
VGDPS 变化前GDP(单位：亿元)
EINVS 变化前总投资额(单位：亿元)
EHS 变化前居民消费支出(单位：亿元)
YENTS 变化前企业总收入（单位：亿元）
VMPS 变化前省际进口额（单位：亿元）
VEPS 变化前省际出口额（单位：亿元）
VMFS 变化前国际进口额（单位：亿元）
VEFS 变化前国际进口额（单位：亿元）
tihs 变化前个税税率
tms(c) 变化前个税税率
transfrslgovss 变化前补贴 （单位：亿元）
Dtihs 变化前个税（单位：亿元）
Dtms(c) 变化前关税（单位：亿元）
;
AVEVYH=VYH.L/popu;
VGDPS=VGDP.L/10000;
EINVS=EINV.L/10000;
EHS=EH.L/10000;
YENTS=YENT.L/10000;
VMFS=VMF.L/10000;
VEFS=VEF.L/10000;
VMPS=VMP.L/10000;
VEPS=VEP.L/10000;
tihs=tih0;
tms(c)=tm.l(c);
transfrslgovss=transfrslgov.L/10000;
Dtihs=tih0*YH.l/10000;
Dtms(c)=tm.l(c)*pwm(c)*QMF.L(c)*EXR.l/10000;
display tihs,Dtihs,tms,Dtms,transfrslgovss,YENT.L,VGDPS,EINVS,AVEVYH,EHS,YENTS,VMPS,VEPS,VMFS,VEFS;
*以下参数用于存储上一步骤得到的值
Parameter
VGDPT0
EINVT0
VYHT0
AVEVYHT0
EHT0
YENTT0
VMFT0
VEFT0
VEPT0
VMPT0
tihT0
tmT0(c)
transfrslgovsT0
DtmT0(c)
DtihT0;
;
VGDPT0=VGDP.L;
EINVT0=EINV.L;
AVEVYHT0 = AVEVYH;
EHT0 = EH.L;
YENTT0=YENT.L;
VMFT0 = VMF.L;
VEFT0 = VEF.L;
VMPT0 = VMP.L;
VEPT0 = VEP.L;
tihT0=tih0;
tmT0(c)=tm.l(c);
transfrslgovsT0=transfrslgov.L/10000;
DtihT0=tih0*YH.l/10000;
DtmT0(c)=tm.l(c)*pwm(c)*QMF.L(c)*EXR.l/10000;

Scalar gdp_input;
$gdxin %gdxincname%
$load gdp_input
$gdxin
VGDP.FX=VGDP0*(1+gdp_input*0.01);

model sim  /all/;
solve sim using mcp;

Parameter
AVEVYH1 变化后人均可支配收入(单位：元)
*以下均为单位换算定义参数
VGDPS1 变化后GDP(单位：亿元)
EINVS1 变化后总投资额(单位：亿元)
EHS1 变化后居民消费支出(单位：亿元)
YENTS1 变化后企业总收入（单位：亿元）
VMPS1 变化后省际进口额（单位：亿元）
VEPS1 变化后省际出口额（单位：亿元）
VMFS1 变化后国际进口额（单位：亿元）
VEFS1 变化后国际出口额（单位：亿元）
tihs1 变化后个税税率
tms1(c) 变化后关税税率
transfrslgovss1 变化后补贴 （单位：亿元）
Dtihs1 变化后个税（单位：亿元）
Dtms1(c) 变化后关税（单位：亿元）
;
AVEVYH1=VYH.L/popu;
VGDPS1=VGDP.L/10000;
EINVS1=EINV.L/10000;
EHS1=EH.L/10000;
YENTS1=YENT.L/10000;
VMFS1=VMF.L/10000;
VMPS1=VMP.L/10000;
tihs1=tih0;
tms1(c)=tm.l(c);
transfrslgovss1=transfrslgov.L/10000;
Dtihs1=YH.l*tih0/10000;
Dtms1(c)=tm.l(c)*pwm(c)*QMF.L(c)*EXR.l/10000;
Parameter

PDELTAGDP GDP变化比值
PDELTAEINV 总投资额变化比值
PDELTAAVEVYH 人均可支配收入变化比值
PDELTAEH 居民消费变化比值
PDELTAYENT 企业收入变化比值
PDELTAVMF 国际进口变化比值
PDELTAVEF 国际出口额变化比值
PDELTAVEP 省际出口额变化比值
PDELTAVMP 省际进口额变化比值
PDELTAtm(c)  关税总量变化比值
PDELTAtih 个税总量变化比值
PDELTAtransfrslgovs 补贴变化比值
PDtih 个税变化绝对量(单位：亿元)
PDtm(c) 关税变化绝对量（单位：亿元）
;

PDELTAGDP=(VGDP.L-VGDPT0)/VGDPT0;
PDELTAEINV=(EINV.L-EINVT0)/EINVT0;
PDELTAAVEVYH=(AVEVYH1-AVEVYHT0)/AVEVYHT0;
PDELTAEH=(EH.L-EHT0)/EHT0;
PDELTAYENT=(YENT.L-YENTT0)/YENTT0;
PDELTAVMF=(VMF.L-VMFT0)/VMFT0;
PDELTAVEF=(VEF.L-VEFT0)/VEFT0;
PDELTAVEP= (VEP.L-VEPT0)/VEPT0;
PDELTAVMP =(VMP.L-VMPT0)/VMPT0;
PDELTAtm(c)=(Dtms1(c)-DtmT0(c))/DtmT0(c);
PDELTAtih =(Dtihs1-DtihT0)/DtihT0;
PDELTAtransfrslgovs =(transfrslgovss1-transfrslgovsT0)/transfrslgovsT0;
PDtih=Dtihs1-DtihT0;
PDtm(c)=Dtms1(c)-DtmT0(c);

display PDELTAtransfrslgovs,PDELTAtih ,PDELTAtm,PDELTAGDP,PDELTAEINV,PDELTAAVEVYH,PDELTAEH,PDELTAYENT,PDELTAVMF,PDELTAVEF,PDELTAVEP,PDELTAVMP,PDtih,PDtm,VGDPS1,EINVS1,AVEVYH,EHS1,YENTS1,VMPS1,VMFS1,tihs1,Dtihs1,tms1,Dtms1,transfrslgovss1;
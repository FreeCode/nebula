import os
import joblib
import shutil
from common.config.config import MINIO_CLIENT
import logging
logging.basicConfig(level=logging.INFO, format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")


# TODO
def model_type(algo):
    algo = algo.upper()
    classification_list = ["DTC", "AUTOC"]
    regression_list = ["LR", "AUTOR", "SVR"]
    cluster_list = ["KM"]
    reduce_dim_list = ["PCA"]
    if algo in classification_list:
        return "classification"
    elif algo in regression_list:
        return "regression"
    elif algo in cluster_list:
        return "cluster"
    elif algo in reduce_dim_list:
        return "reduce_dim"
    else:
        return "unknown"


# def load_model(model_save_path):
#     bucket_name = "ml-model"
#     file_path = os.path.join(bucket_name, model_save_path)
#     folder_path = os.path.split(file_path)[0]
#     if not os.path.exists(folder_path):
#         os.makedirs(folder_path)
#
#     MINIO_CLIENT.fget_object(bucket_name, model_save_path, file_path)
#     model = joblib.load(file_path)
#
#     shutil.rmtree(bucket_name)
#     return model
def load_model(model_save_path):
    bucket_name = "ml-model"
    path = "/home/zjlab/"
    if not os.path.exists(path):
       path = "./"
    path += "model_loaded_from_minio"
    file_path = os.path.join(path, model_save_path)
    folder_path = os.path.split(file_path)[0]
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)
    MINIO_CLIENT.fget_object(bucket_name, model_save_path, file_path)
    model = joblib.load(file_path)
    shutil.rmtree(path)
    return model


# def save_model(model, para_list):
#     bucket_name = "ml-model"
#     object_name = "{}/{}/{}_{}".format(para_list["usr_id"], model_type(para_list["algo"]),
#                                        para_list["algo"], para_list["model_id"])
#     path = "/home/zjlab/aiworks/flask"
#     file_path = os.path.join(path, object_name)
#     print(file_path)
#     folder_path = os.path.split(file_path)[0]
#     if not os.path.exists(folder_path):
#         os.makedirs(folder_path)
#     joblib.dump(model, file_path)
#     MINIO_CLIENT.fput_object(bucket_name=bucket_name, object_name=object_name, file_path=file_path)
#     shutil.rmtree(path)

def save_model(mlmodel, model, object_name):
    bucket_name = "ml-model"
    # object_name = "{}/{}/{}_{}".format(para_list["usr_id"], model_type(para_list["algo"]),
    #                                    para_list["algo"], para_list["model_id"])
    path = "./_tmp_"
    file_path = os.path.join(path, object_name)
    folder_path = os.path.split(file_path)[0]
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)
    joblib.dump(mlmodel, file_path)
    MINIO_CLIENT.fput_object(bucket_name=bucket_name, object_name=object_name, file_path=file_path)
    shutil.rmtree(path)

    if model.in_panel != None and model.in_panel != 0:
        logging.info("previous model is not exported, so its model file will be deleted")
        prev_object_name = model.model_saved_path.split("ml-model/")[1]
        logging.info("removing previous model with name: " + prev_object_name)
        MINIO_CLIENT.remove_object(bucket_name=bucket_name, object_name=prev_object_name)

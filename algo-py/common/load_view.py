from common.config.config import *
import psycopg2
import pandas as pd
import argparse

def load_view(tableName, target_path):
	sql = "select * from " + tableName + " order by _record_id_"
	conn = psycopg2.connect(dbname=GP_DBNAME,
	                        user=GP_USER,
	                        password=GP_PASSWORD,
	                        host=GP_HOST,
	                        port=GP_PORT)
	df = pd.read_sql(sql, conn)
	conn.close()
	df.to_csv(target_path)

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("--table_name", type=str, required=True)
	parser.add_argument("--target_path", type=str, required=True)

	args = parser.parse_args()

	load_view(args.table_name, args.target_path)


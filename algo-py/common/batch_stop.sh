batch_status_change_sql = "update jlab set active = 0 where user_id != 0"

source config/config

docker stop $(docker ps -a -q)

mysql -h${MYSQL_HOST} -P${MYSQL_PORT} -u${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DBNAME} -e "${batch_status_change_sql}"


source config/config

mkdir -p ${NOTEBOOKDIR}user_$1

docker run -d -m ${DOCKER_MEM} --cpus=${DOCKER_CPU} --name user_$1 -p $2:$2 --env PORT=$2 -v ${NOTEBOOKDIR}user_$1:/opt/notebooks ${DOCKER_IMG_VERSION}

sleep 1

echo `docker exec user_$1 jupyter lab list`


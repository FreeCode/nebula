source config/config

mkdir -p ${NOTEBOOKDIR}user_$1/$3

PGPASSWORD=${GP_PASSWORD} psql -h ${GP_HOST} -U ${GP_USER} -d ${GP_DBNAME} -p ${GP_PORT} -c "\copy $2 TO '${NOTEBOOKDIR}user_$1/$3/$4.csv' CSV HEADER"
